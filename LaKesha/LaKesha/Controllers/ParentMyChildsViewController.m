//
//  ParentMyChildsViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 12/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentMyChildsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentMyChildsTableViewCell.h"

#import "ParentChildPrivacySettingsViewController.h"
#import "ParentAddChildsViewController.h"

@interface ParentMyChildsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentMyChildsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewAdd;
@synthesize btnAdd;

@synthesize mainContainerView;
@synthesize mainTableView;
@synthesize lblNoData;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllChildsByParentIdEvent) name:@"gotAllChildsByParentIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removedChildByParentIdEvent) name:@"removedChildByParentIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllChildsByParentIdEvent
{
    self.dataRows = [MySingleton sharedManager].dataManager.arrayChildsByParentId;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

-(void)removedChildByParentIdEvent
{
    self.dataRows = [MySingleton sharedManager].dataManager.arrayChildsByParentId;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Our Children"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    
    imageViewAdd.layer.masksToBounds = YES;
    [btnAdd addTarget:self action:@selector(btnAddClicked:) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnAddClicked:(id)sender
{
    [self.view endEditing:YES];
    
    ParentAddChildsViewController *viewController = [[ParentAddChildsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblNoDataFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        }
        else
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        }
    }
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    mainTableView.hidden = true;
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsTableViewCell_iPhone6" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsTableViewCell_iPhone6Plus" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsTableViewCell_iPad" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
    [[MySingleton sharedManager].dataManager getAllChildsByParentId:strParentId];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 200;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        ParentMyChildsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if(cell != nil)
        {
            NSArray *nib;
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsTableViewCell_iPad" owner:self options:nil];
            }
            else
            {
                if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsTableViewCell" owner:self options:nil];
                }
                else if([MySingleton sharedManager].screenHeight == 667)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsTableViewCell_iPhone6" owner:self options:nil];
                }
                else if([MySingleton sharedManager].screenHeight == 736)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsTableViewCell_iPhone6Plus" owner:self options:nil];
                }
            }
            
            cell = [nib objectAtIndex:0];
        }
        
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
        
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        cell.imageViewChildProfilePicture.imageURL = [NSURL URLWithString:objChild.strProfilePictureImageUrl];
        cell.imageViewChildProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        cell.imageViewChildProfilePicture.layer.masksToBounds = YES;
        cell.imageViewChildProfilePicture.layer.cornerRadius = (cell.imageViewChildProfilePicture.frame.size.width/2);
        cell.imageViewChildProfilePicture.layer.borderWidth = 1.0f;
        cell.imageViewChildProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        
        cell.lblChildName.text = objChild.strName;
        cell.lblChildName.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
        cell.lblChildName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        cell.lblChildName.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildAge.text = [NSString stringWithFormat:@"%@ years old", objChild.strAge];
        cell.lblChildAge.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildAge.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildAge.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildGender.text = [NSString stringWithFormat:@"%@", objChild.strGender];
        cell.lblChildGender.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildGender.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildNationality.text = [NSString stringWithFormat:@"%@", objChild.strNationality];
        cell.lblChildNationality.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildNationality.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildNationality.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildHairColor.text = [NSString stringWithFormat:@"HAIR COLOR : %@", objChild.strHairColor];
        cell.lblChildHairColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildHairColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildHairColor.textAlignment = NSTextAlignmentLeft;
        
        NSString *strChildHairColor = [NSString stringWithFormat:@"HAIR COLOR : %@", objChild.strHairColor];
        NSMutableAttributedString *strAttrChildHairColor = [[NSMutableAttributedString alloc] initWithString:strChildHairColor attributes: nil];
        NSRange rangeOfHairColor = [strChildHairColor rangeOfString:@"HAIR COLOR :"];
        [strAttrChildHairColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHairColor];
        [strAttrChildHairColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHairColor];
        cell.lblChildHairColor.attributedText = strAttrChildHairColor;
        
        
        cell.lblChildEyeColor.text = [NSString stringWithFormat:@"EYE COLOR : %@", objChild.strEyeColor];
        cell.lblChildEyeColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildEyeColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildEyeColor.textAlignment = NSTextAlignmentRight;
        
        NSString *strChildEyeColor = [NSString stringWithFormat:@"EYE COLOR : %@", objChild.strEyeColor];
        NSMutableAttributedString *strAttrChildEyeColor = [[NSMutableAttributedString alloc] initWithString:strChildEyeColor attributes: nil];
        NSRange rangeOfEyeColor = [strChildEyeColor rangeOfString:@"EYE COLOR :"];
        [strAttrChildEyeColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfEyeColor];
        [strAttrChildEyeColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfEyeColor];
        cell.lblChildEyeColor.attributedText = strAttrChildEyeColor;
        
        
        cell.lblChildHeight.text = [NSString stringWithFormat:@"HEIGHT : %@", objChild.strHeight];
        cell.lblChildHeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildHeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildHeight.textAlignment = NSTextAlignmentLeft;
        
        NSString *strChildHeight = [NSString stringWithFormat:@"HEIGHT : %@", objChild.strHeight];
        NSMutableAttributedString *strAttrChildHeight = [[NSMutableAttributedString alloc] initWithString:strChildHeight attributes: nil];
        NSRange rangeOfHeight = [strChildHeight rangeOfString:@"HEIGHT :"];
        [strAttrChildHeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHeight];
        [strAttrChildHeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHeight];
        cell.lblChildHeight.attributedText = strAttrChildHeight;
        
        
        cell.lblChildWeight.text = [NSString stringWithFormat:@"WEIGHT : %@", objChild.strWeight];
        cell.lblChildWeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildWeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildWeight.textAlignment = NSTextAlignmentRight;
        
        NSString *strChildWeight = [NSString stringWithFormat:@"WEIGHT : %@", objChild.strWeight];
        NSMutableAttributedString *strAttrChildWeight = [[NSMutableAttributedString alloc] initWithString:strChildWeight attributes: nil];
        NSRange rangeOfWeight = [strChildWeight rangeOfString:@"WEIGHT :"];
        [strAttrChildWeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfWeight];
        [strAttrChildWeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfWeight];
        cell.lblChildWeight.attributedText = strAttrChildWeight;
        
        
        cell.lblChildSchoolName.text = [NSString stringWithFormat:@"SCHOOL : %@", objChild.strSchoolName];
        cell.lblChildSchoolName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildSchoolName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildSchoolName.textAlignment = NSTextAlignmentLeft;
        
        NSString *strChildSchool = [NSString stringWithFormat:@"SCHOOL : %@", objChild.strSchoolName];
        NSMutableAttributedString *strAttrChildSchool = [[NSMutableAttributedString alloc] initWithString:strChildSchool attributes: nil];
        NSRange rangeOfSchool = [strChildSchool rangeOfString:@"SCHOOL :"];
        [strAttrChildSchool addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfSchool];
        [strAttrChildSchool addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfSchool];
        cell.lblChildSchoolName.attributedText = strAttrChildSchool;
        
        
        cell.removeContainer.layer.masksToBounds = true;
        cell.removeContainer.layer.cornerRadius = 3.0f;
        cell.removeContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        cell.removeContainer.layer.borderWidth = 1.0f;
        
        cell.lblRemove.font = [MySingleton sharedManager].themeFontTenSizeBold;
        cell.lblRemove.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblRemove.textAlignment = NSTextAlignmentCenter;
        cell.lblRemove.layer.masksToBounds = YES;
        
        cell.imageViewRemove.contentMode = UIViewContentModeScaleAspectFit;
        cell.imageViewRemove.layer.masksToBounds = YES;
        
        cell.btnRemove.tag = indexPath.row;
        [cell.btnRemove addTarget:self action:@selector(btnRemoveClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.privacySettingsContainer.layer.masksToBounds = true;
        cell.privacySettingsContainer.layer.cornerRadius = 3.0f;
        cell.privacySettingsContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        cell.privacySettingsContainer.layer.borderWidth = 1.0f;
        
        cell.lblPrivacySettings.font = [MySingleton sharedManager].themeFontTenSizeBold;
        cell.lblPrivacySettings.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblPrivacySettings.textAlignment = NSTextAlignmentCenter;
        cell.lblPrivacySettings.layer.masksToBounds = YES;
        
        cell.imageViewPrivacySettings.contentMode = UIViewContentModeScaleAspectFit;
        cell.imageViewPrivacySettings.layer.masksToBounds = YES;
        
        cell.btnPrivacySettings.tag = indexPath.row;
        [cell.btnPrivacySettings addTarget:self action:@selector(btnPrivacySettingsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == mainTableView)
    {
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - Other Methods

-(IBAction)btnRemoveClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *btnSender = (UIButton *)sender;
    Child *objChild = [self.dataRows objectAtIndex:btnSender.tag];
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Are you sure you want to remove?";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [[MySingleton sharedManager].dataManager removeChildByParentId:strParentId withChildId:objChild.strChildID];
        
    }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(IBAction)btnPrivacySettingsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *btnSender = (UIButton *)sender;
    Child *objChild = [self.dataRows objectAtIndex:btnSender.tag];
    
    ParentChildPrivacySettingsViewController *viewController = [[ParentChildPrivacySettingsViewController alloc] init];
    viewController.objSelectedChild = objChild;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(IBAction)btnSettingsClicked:(id)sender
{
    [self.view endEditing:YES];
}

@end
