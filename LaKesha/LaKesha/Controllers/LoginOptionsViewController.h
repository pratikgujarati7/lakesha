//
//  LoginOptionsViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 21/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface LoginOptionsViewController : UIViewController<UIScrollViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewMainBackground;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMainLogo;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UIButton *btnLoginAsParent;
@property (nonatomic,retain) IBOutlet UIButton *btnLoginAsChild;
@property (nonatomic,retain) IBOutlet UIButton *btnLoginAsGuest;

@property (nonatomic,retain) IBOutlet UIButton *btnDonate;
@property (nonatomic,retain) IBOutlet UIButton *btnMoreInformation;

//========== OTHER VARIABLES ==========//

@end
