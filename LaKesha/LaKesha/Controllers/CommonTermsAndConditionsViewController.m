//
//  CommonTermsAndConditionsViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 04/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CommonTermsAndConditionsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface CommonTermsAndConditionsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation CommonTermsAndConditionsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblCongratulations;
@synthesize lblInstructions;

@synthesize termsAndConditionsCheckboxContainerView;
@synthesize imageViewTermsAndConditionsCheckBox;
@synthesize btnTermsAndConditionsCheckBox;
@synthesize lblTermsAndConditions;
@synthesize btnTermsAndConditions;

@synthesize emailPermissionsContainerView;
@synthesize imageViewEmailPermissionsCheckBox;
@synthesize btnEmailPermissionsCheckBox;
@synthesize lblEmailPermissions;
@synthesize btnEmailPermissions;

@synthesize btnNext;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    lblInstructions.text = [NSString stringWithFormat:@"The LaKesha alert app is the safest tool to help prevent child kidnapping.\n\nAny misuse of this app I as the parent will take full responsibility.\n\nI as a parent will inform my child/children to use this app responsibly.\n\nI as a parent will inform my child/children that calling 911 is always the appropriate thing to do in an emergency.\n\nI as a parent will inform my child/children that this application has the capabilities to call, text and or email authorities.\n\nFor customers service please contact us 18889598985 or Email allus@lakesha.com"];
    [lblInstructions sizeToFit];
    
    CGFloat lblInstructionsHeight;
    
    CGRect lblInstructionsTextRect = [lblInstructions.text boundingRectWithSize:lblInstructions.frame.size
                                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                                       attributes:@{NSFontAttributeName:lblInstructions.font}
                                                                          context:nil];
    
    CGSize lblInstructionsSize = lblInstructionsTextRect.size;
    
    lblInstructionsHeight = lblInstructionsSize.height;
    
    CGRect lblInstructionsFrame = lblInstructions.frame;
    lblInstructionsFrame.size.height = lblInstructionsHeight;
    lblInstructions.frame = lblInstructionsFrame;
    
    CGRect termsAndConditionsCheckboxContainerViewFrame = termsAndConditionsCheckboxContainerView.frame;
    termsAndConditionsCheckboxContainerViewFrame.origin.y = lblInstructions.frame.origin.y + lblInstructionsHeight + 10;
    termsAndConditionsCheckboxContainerView.frame = termsAndConditionsCheckboxContainerViewFrame;
    
    CGRect emailPermissionsContainerViewFrame = emailPermissionsContainerView.frame;
    emailPermissionsContainerViewFrame.origin.y = termsAndConditionsCheckboxContainerView.frame.origin.y + termsAndConditionsCheckboxContainerView.frame.size.height + 10;
    emailPermissionsContainerView.frame = emailPermissionsContainerViewFrame;
    
    CGRect btnNextFrame = btnNext.frame;
    btnNextFrame.origin.y = emailPermissionsContainerView.frame.origin.y + emailPermissionsContainerView.frame.size.height + 10;
    btnNext.frame = btnNextFrame;
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = btnNext.frame.origin.y + btnNext.frame.size.height + 20;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainContainerView.autoresizesSubviews = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parentSubscribedEvent) name:@"parentSubscribedEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)parentSubscribedEvent
{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.hidden = true;
    btnBack.hidden = true;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Terms & Conditions"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblCongratulationsFont, *lblInstructionsFont, *lblTermsAndConditionsFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblCongratulationsFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblInstructionsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblTermsAndConditionsFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblCongratulationsFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblTermsAndConditionsFont = [MySingleton sharedManager].themeFontTenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblCongratulationsFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblTermsAndConditionsFont = [MySingleton sharedManager].themeFontTenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblCongratulationsFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblTermsAndConditionsFont = [MySingleton sharedManager].themeFontTenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblCongratulationsFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblTermsAndConditionsFont = [MySingleton sharedManager].themeFontTenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    lblCongratulations.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblCongratulations.numberOfLines = 2;
    lblCongratulations.font = lblCongratulationsFont;
    lblCongratulations.layer.masksToBounds = true;
    lblCongratulations.textAlignment = NSTextAlignmentCenter;
    lblCongratulations.text = [NSString stringWithFormat:@"Congratulations you are a registered LaKesha app parent!"];
    
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.layer.masksToBounds = true;
    lblInstructions.textAlignment = NSTextAlignmentLeft;
    
    self.boolIsTermsAndConditionsChecked = false;
    imageViewTermsAndConditionsCheckBox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
    [btnTermsAndConditionsCheckBox addTarget:self action:@selector(btnTermsAndConditionsCheckBoxClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnTermsAndConditions addTarget:self action:@selector(btnTermsAndConditionsCheckBoxClicked) forControlEvents:UIControlEventTouchUpInside];
    
    lblTermsAndConditions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblTermsAndConditions.font = lblTermsAndConditionsFont;
    lblTermsAndConditions.numberOfLines = 0;
    lblTermsAndConditions.lineBreakMode = NSLineBreakByWordWrapping;
    lblTermsAndConditions.layer.masksToBounds = true;
    lblTermsAndConditions.text = [NSString stringWithFormat:@"I agree to all terms and conditions."];
    
    self.boolIsEmailChecked = false;
    imageViewEmailPermissionsCheckBox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
    [btnEmailPermissionsCheckBox addTarget:self action:@selector(btnEmailPermissionsCheckBoxClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnEmailPermissions addTarget:self action:@selector(btnEmailPermissionsCheckBoxClicked) forControlEvents:UIControlEventTouchUpInside];
    
    lblEmailPermissions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblEmailPermissions.font = lblTermsAndConditionsFont;
    lblEmailPermissions.numberOfLines = 0;
    lblEmailPermissions.lineBreakMode = NSLineBreakByWordWrapping;
    lblEmailPermissions.layer.masksToBounds = true;
//    lblEmailPermissions.text = [NSString stringWithFormat:@"Keep me updated on new & up and coming products & projects by email"];
    lblEmailPermissions.text = [NSString stringWithFormat:@"Check here if it's ok to contact you to help us collect data & make this app better"];
    
    btnNext.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnNext.layer.masksToBounds = true;
    btnNext.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnNext.titleLabel.font = btnFont;
    [btnNext setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(btnNextClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

-(void)btnTermsAndConditionsCheckBoxClicked
{
    if(self.boolIsTermsAndConditionsChecked)
    {
        self.boolIsTermsAndConditionsChecked = false;
        imageViewTermsAndConditionsCheckBox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
    }
    else
    {
        self.boolIsTermsAndConditionsChecked = true;
        imageViewTermsAndConditionsCheckBox.image = [UIImage imageNamed:@"checkbox_checked.png"];
    }
}

-(void)btnEmailPermissionsCheckBoxClicked
{
    if(self.boolIsEmailChecked)
    {
        self.boolIsEmailChecked = false;
        imageViewEmailPermissionsCheckBox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
    }
    else
    {
        self.boolIsEmailChecked = true;
        imageViewEmailPermissionsCheckBox.image = [UIImage imageNamed:@"checkbox_checked.png"];
    }
}

-(void)btnNextClicked
{
    if(self.boolIsTermsAndConditionsChecked)
    {
//        [self.navigationController dismissViewControllerAnimated:self completion:nil];
//        [self.navigationController popViewControllerAnimated:YES];
        
//        [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        
        if(self.boolIsEmailChecked)
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
            [[MySingleton sharedManager].dataManager getAllChildsByParentId:strParentId];
            [[MySingleton sharedManager].dataManager parentSubscribe:strParentId withFlag:@"1"];
        }
        else
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
            [[MySingleton sharedManager].dataManager getAllChildsByParentId:strParentId];
            [[MySingleton sharedManager].dataManager parentSubscribe:strParentId withFlag:@"0"];
        }
    }
    else
    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please agree to our terms and conditions."];
//        });
        
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"";
        alertViewController.message = @"Please agree to our terms and conditions.";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [self presentViewController:alertViewController animated:YES completion:nil];
    }
}

@end
