//
//  ParentHomeViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 10/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentHomeViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ChildListTableViewCell.h"

#import "ParentMyChildsAlertsViewController.h"
#import "CommonAllNotificationsViewController.h"
#import "CommonAllBoycottAlertsViewController.h"

@interface ParentHomeViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentHomeViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainMapView;
@synthesize mainTableView;
@synthesize lblNoData;

@synthesize bottomContainerView;

@synthesize homeContainerView;
@synthesize lblHome;
@synthesize imageViewHome;
@synthesize btnHome;

@synthesize alertsContainerView;
@synthesize lblAlerts;
@synthesize imageViewAlerts;
@synthesize btnAlerts;

@synthesize notificationContainerView;
@synthesize lblNotification;
@synthesize imageViewNotification;
@synthesize btnNotification;

@synthesize boycottAlertsContainerView;
@synthesize lblBoycottAlerts;
@synthesize imageViewBoycottAlerts;
@synthesize btnBoycottAlerts;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllChildsByParentIdEvent) name:@"gotAllChildsByParentIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllChildsByParentIdEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(![[prefs objectForKey:@"isAppOpenedByParentForFirstTime"] isEqualToString:@"1"])
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Update";
        alertViewController.message = @"Please update your information state, city and zipcode to use new features of this applicaton.";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = NO;
        alertViewController.swipeDismissalGestureEnabled = NO;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
            [prefs setObject:@"1" forKey:@"isAppOpenedByParentForFirstTime"];
            [prefs synchronize];
            
            self.dataRows = [MySingleton sharedManager].dataManager.arrayChildsByParentId;
            
            if(self.dataRows.count <= 0)
            {
                mainTableView.hidden = true;
                lblNoData.hidden = false;
            }
            else
            {
                mainTableView.hidden = false;
                lblNoData.hidden = true;
                [mainTableView reloadData];
            }
            
            [self placeParentLocationAndChildsLocationsPinsOnMap];
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertViewController animated:YES completion:nil];
        });
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayChildsByParentId;
        
        if(self.dataRows.count <= 0)
        {
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
        
        [self placeParentLocationAndChildsLocationsPinsOnMap];
    }
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Home"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblNoDataFont, *lblBottomBarFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        lblBottomBarFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblBottomBarFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblBottomBarFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            lblBottomBarFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblBottomBarFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    mainMapView.mapType = kGMSTypeNormal;
    mainMapView.delegate = self;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    mainTableView.hidden = true;
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    bottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
//    homeContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalBlueColor;
    
    lblHome.font = lblBottomBarFont;
    lblHome.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewHome.layer.masksToBounds = true;
    
    [btnHome addTarget:self action:@selector(btnHomeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    alertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    lblAlerts.font = lblBottomBarFont;
    lblAlerts.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewAlerts.layer.masksToBounds = true;
    
    [btnAlerts addTarget:self action:@selector(btnAlertsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //    alertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    lblNotification.font = lblBottomBarFont;
    lblNotification.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewNotification.layer.masksToBounds = true;
    
    [btnNotification addTarget:self action:@selector(btnNotificationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //    alertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    lblBoycottAlerts.font = lblBottomBarFont;
    lblBoycottAlerts.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewBoycottAlerts.layer.masksToBounds = true;
    
    [btnBoycottAlerts addTarget:self action:@selector(btnBoycottAlertsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
    [[MySingleton sharedManager].dataManager getAllChildsByParentId:strParentId];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 60;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        ChildListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[ChildListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
        
        if(objChild.boolIsChildAlertActive)
        {
            cell.mainInnerContainer.backgroundColor = [MySingleton sharedManager].themeGlobalActiveAlertRedColor;
        }
        
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        cell.imageViewChildProfilePicture.imageURL = [NSURL URLWithString:objChild.strProfilePictureImageUrl];
        
        cell.lblChildName.text = objChild.strName;
        
        cell.btnSettings.tag = indexPath.row;
        [cell.btnSettings addTarget:self action:@selector(btnSettingsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == mainTableView)
    {
        NSLog(@"didSelectRowAtIndexPath called.");
        
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
        NSLog(@"objChild.strPhoneNumber : %@", objChild.strPhoneNumber);
        
        NSString *phoneNumber = [@"tel://" stringByAppendingString:objChild.strPhoneNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
}

#pragma mark - Google Maps Delegate Methods

-(void)placeParentLocationAndChildsLocationsPinsOnMap
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[MySingleton sharedManager].dataManager.strLocationLatitude doubleValue] longitude:[[MySingleton sharedManager].dataManager.strLocationLongitude doubleValue] zoom:5];
    [mainMapView setCamera:camera];
//    [mainMapView animateToViewingAngle:45];
    
    GMSMarker *parentLocationPin = [[GMSMarker alloc] init];
    parentLocationPin.position = camera.target;
    parentLocationPin.title = @"Your Location";
    parentLocationPin.snippet = @"";
    parentLocationPin.appearAnimation = kGMSMarkerAnimationNone;
    parentLocationPin.icon = [UIImage imageNamed:@"current_location_pin.png"];
    parentLocationPin.icon = [self image:parentLocationPin.icon scaledToSize:CGSizeMake(30.0f, 30.0f)];
    [parentLocationPin setDraggable: NO];
    parentLocationPin.map = mainMapView;
    
    if(self.dataRows.count > 0)
    {
        for (int i = 0; i < self.dataRows.count; i++)
        {
            Child *objChild = [self.dataRows objectAtIndex:i];
            
            GMSMarker *childLocationPin = [[GMSMarker alloc] init];
            childLocationPin.position = CLLocationCoordinate2DMake([objChild.strChildLatitude doubleValue], [objChild.strChildLongitude doubleValue]);
            childLocationPin.title = [NSString stringWithFormat:@"%@'s Location", objChild.strName];
            childLocationPin.snippet = @"";
            childLocationPin.appearAnimation = kGMSMarkerAnimationNone;
            childLocationPin.icon = [UIImage imageNamed:@"child_location_pin.png"];
            childLocationPin.icon = [self image:childLocationPin.icon scaledToSize:CGSizeMake(30.0f, 30.0f)];
            [childLocationPin setDraggable: NO];
            childLocationPin.map = mainMapView;
        }
    }
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return image;
}

#pragma mark - Other Methods

-(IBAction)btnSettingsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *btnSender = (UIButton *)sender;
    
    Child *objChild = [self.dataRows objectAtIndex:btnSender.tag];
    NSLog(@"objChild.strPhoneNumber : %@", objChild.strPhoneNumber);
}

-(IBAction)btnHomeClicked:(id)sender
{
    [self.view endEditing:YES];
    
    [self navigateToParentHomeViewController];
}

-(IBAction)btnAlertsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    [self navigateToParentMyChildsAlertsViewController];
}

-(IBAction)btnNotificationClicked:(id)sender
{
    [self.view endEditing:YES];
    
    CommonAllNotificationsViewController *commonAllNotificationsViewController = [[CommonAllNotificationsViewController alloc]init];
    commonAllNotificationsViewController.strLoadedFor = @"1";
    [self.navigationController pushViewController:commonAllNotificationsViewController animated:false];
}

-(IBAction)btnBoycottAlertsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    CommonAllBoycottAlertsViewController *commonAllBoycottAlertsViewController = [[CommonAllBoycottAlertsViewController alloc]init];
    commonAllBoycottAlertsViewController.strLoadedFor = @"1";
    [self.navigationController pushViewController:commonAllBoycottAlertsViewController animated:false];
}

- (void)navigateToParentHomeViewController
{
    [self.view endEditing:true];
    
    ParentHomeViewController *viewController = [[ParentHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:NO];
}

- (void)navigateToParentMyChildsAlertsViewController
{
    [self.view endEditing:true];
    
    ParentMyChildsAlertsViewController *viewController = [[ParentMyChildsAlertsViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:NO];
}

@end
