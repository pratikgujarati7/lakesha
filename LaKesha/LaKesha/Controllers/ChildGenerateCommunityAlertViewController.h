//
//  ChildGenerateCommunityAlertViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 13/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface ChildGenerateCommunityAlertViewController : UIViewController<UIScrollViewDelegate, UITextViewDelegate, CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D coordinates;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;
@property (nonatomic,retain) IBOutlet UIView *txtViewMessageContainerView;
@property (nonatomic,retain) IBOutlet UITextView *txtViewMessage;
@property (nonatomic,retain) IBOutlet UIButton *btnGenerate;

//========== OTHER VARIABLES ==========//

@property (nonatomic,strong) NSString *strLatitude;
@property (nonatomic,strong) NSString *strLongitude;

@end
