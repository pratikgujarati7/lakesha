//
//  CommonVerifyPhoneNumberViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 20/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CommonVerifyPhoneNumberViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "CommonChangePhoneNumberViewController.h"
#import "ParentPaymentViewController.h"
#import "ChildHomeViewController.h"

@interface CommonVerifyPhoneNumberViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation CommonVerifyPhoneNumberViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblInstructions;
@synthesize txtOTP;
@synthesize txtOTPBottomSeparatorView;

@synthesize btnResentOTP;
@synthesize lblOr;
@synthesize btnChangePhoneNumber;

@synthesize btnVerify;

//========== OTHER VARIABLES ==========//

@synthesize keyboardDoneButtonView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resentOTPEvent) name:@"resentOTPEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(verifiedParentPhoneNumberEvent) name:@"verifiedParentPhoneNumberEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(verifiedChildPhoneNumberEvent) name:@"verifiedChildPhoneNumberEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)resentOTPEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Verification code has been sent successfully on your phone number.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(void)verifiedParentPhoneNumberEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"You have successfully verified your phone number.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
//        [self navigateToParentHomeViewController];
        [self navigateToParentPaymentViewController];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(void)verifiedChildPhoneNumberEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"You have successfully verified your phone number.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        [self navigateToChildHomeViewController];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Verify Phone Number"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    keyboardDoneButtonView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *flex1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)];
    [doneButton1 setTitleTextAttributes:@{ NSFontAttributeName: [MySingleton sharedManager].themeFontTwentySizeRegular,NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex1,doneButton1, nil]];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionsFont, *txtFieldFont, *btnResentOTPFont, *lblOrFont, *btnChangePhoneNumberFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnResentOTPFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblOrFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnChangePhoneNumberFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnResentOTPFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblOrFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnChangePhoneNumberFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnResentOTPFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblOrFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnChangePhoneNumberFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnResentOTPFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblOrFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnChangePhoneNumberFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnResentOTPFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblOrFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnChangePhoneNumberFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.numberOfLines = 0;
    lblInstructions.lineBreakMode = NSLineBreakByWordWrapping;
    lblInstructions.text = [NSString stringWithFormat:@"Please enter verification code you just received on your phone number "];
    
    txtOTP.font = txtFieldFont;
    txtOTP.delegate = self;
    [txtOTP setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"_placeholderLabel.textColor"];
    txtOTP.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtOTP.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtOTP setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtOTP setKeyboardType:UIKeyboardTypeNumberPad];
//    txtOTP.inputAccessoryView = keyboardDoneButtonView;
    
    txtOTPBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    btnResentOTP.titleLabel.font = btnResentOTPFont;
    [btnResentOTP setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnResentOTP addTarget:self action:@selector(btnResentOTPClicked) forControlEvents:UIControlEventTouchUpInside];
    
    lblOr.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblOr.font = lblOrFont;
    
    btnChangePhoneNumber.titleLabel.font = btnChangePhoneNumberFont;
    [btnChangePhoneNumber setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnChangePhoneNumber addTarget:self action:@selector(btnChangePhoneNumberClicked) forControlEvents:UIControlEventTouchUpInside];
    
    btnVerify.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnVerify.layer.masksToBounds = true;
    btnVerify.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnVerify.titleLabel.font = btnFont;
    [btnVerify setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnVerify addTarget:self action:@selector(btnVerifyClicked) forControlEvents:UIControlEventTouchUpInside];
    
    //========== PRATIK GUJARATI TEMP DATA ==========//
//    txtOTP.text = [MySingleton sharedManager].dataManager.strOTP;
    //========== PRATIK GUJARATI TEMP DATA ==========//
}


#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}


- (void)btnResentOTPClicked
{
    [self.view endEditing:YES];
    
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *strUserType;
    
    if([self.strLoadedFor isEqualToString:@"1"])
    {
        strUserType = @"1";
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [dictParameters setObject:strParentId forKey:@"user_id"];
    }
    else if([self.strLoadedFor isEqualToString:@"0"])
    {
        strUserType = @"0";
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [dictParameters setObject:strChildId forKey:@"user_id"];
    }
    
    [dictParameters setObject:strUserType forKey:@"user_type"];
    
    [[MySingleton sharedManager].dataManager resendOTP:dictParameters];
}

-(void)btnChangePhoneNumberClicked
{
    [self.view endEditing:YES];
    
    CommonChangePhoneNumberViewController *viewController = [[CommonChangePhoneNumberViewController alloc] init];
    
    if([self.strLoadedFor isEqualToString:@"1"])
    {
        viewController.strLoadedFor = @"1";
        viewController.strPhoneNumber = [MySingleton sharedManager].dataManager.objLoggedInParent.strPhoneNumber;
    }
    else if([self.strLoadedFor isEqualToString:@"0"])
    {
        viewController.strLoadedFor = @"0";
        viewController.strPhoneNumber = [MySingleton sharedManager].dataManager.objLoggedInChild.strPhoneNumber;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)btnVerifyClicked
{
    [self.view endEditing:YES];
    
    if(txtOTP.text.length > 0)
    {
        NSString *strEnteredOTP = txtOTP.text;
        
        NSLog(@"strEnteredOTP : %@", strEnteredOTP);
        NSLog(@"[MySingleton sharedManager].dataManager.strOTP : %@", [MySingleton sharedManager].dataManager.strOTP);
        
        if([strEnteredOTP isEqualToString:[MySingleton sharedManager].dataManager.strOTP])
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSString *strUserType;
            
            if([self.strLoadedFor isEqualToString:@"1"])
            {
                strUserType = @"1";
                NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
                [dictParameters setObject:strParentId forKey:@"user_id"];
            }
            else if([self.strLoadedFor isEqualToString:@"0"])
            {
                strUserType = @"0";
                NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
                [dictParameters setObject:strChildId forKey:@"user_id"];
            }
            
            [dictParameters setObject:strUserType forKey:@"user_type"];
            
            [[MySingleton sharedManager].dataManager verifyPhoneNumber:dictParameters];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Verification code does not match. Please enter correct verification code"];
            });
        }
    }
    else
    {
        if(txtOTP.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter verification code"];
            });
        }
    }
}

//-(void)navigateToParentHomeViewController
//{
//    [self.view endEditing:YES];
//    
//    ParentHomeViewController *viewController = [[ParentHomeViewController alloc] init];
//    SideMenuViewController *sideMenuViewController;
//    
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
//    }
//    else
//    {
//        if([MySingleton sharedManager].screenHeight == 568)
//        {
//            sideMenuViewController = [[SideMenuViewController alloc] init];
//        }
//        else if([MySingleton sharedManager].screenHeight == 667)
//        {
//            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
//        }
//        else if([MySingleton sharedManager].screenHeight == 736)
//        {
//            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
//        }
//        else
//        {
//            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
//        }
//    }
//    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
//    
//    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
//                                                                                           leftViewController:sideMenuViewController
//                                                                                          rightViewController:nil];
//    
//    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
//    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
//    
//    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
//    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
//    
//    [self.navigationController pushViewController:sideMenuController animated:YES];
//}

-(void)navigateToParentPaymentViewController
{
    [self.view endEditing:YES];
    
    ParentPaymentViewController *viewController = [[ParentPaymentViewController alloc] init];
    viewController.boolIsLoadedWithoutBack = true;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)navigateToChildHomeViewController
{
    [self.view endEditing:YES];
    
    ChildHomeViewController *viewController = [[ChildHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

@end
