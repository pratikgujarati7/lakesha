//
//  ParentChildPrivacySettingsViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentChildPrivacySettingsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentChildPrivacySettingsTableViewCell.h"

@interface ParentChildPrivacySettingsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentChildPrivacySettingsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblInstruction1;

@synthesize btnParentRespondAlertForOtherUsers;
@synthesize btnAuthorityAlertForOtherUsers;
@synthesize btnKidnappingAlertForOtherUsers;

@synthesize separatorView;

@synthesize lblInstruction2;

@synthesize btnAuthorityAlertForAuthority;
@synthesize btnKidnappingAlertForAuthority;

@synthesize informationSelectorPopupContainerView;
@synthesize informationSelectorBlackTransparentView;
@synthesize informationSelectorInnerContainerView;
@synthesize lblInformationSelectorPopupContainerViewInstructions;
@synthesize imageViewCloseInformationSelectorPopupContainerView;
@synthesize btnCloseInformationSelectorPopupContainerView;
@synthesize informationSelectorTableView;
@synthesize btnSelectAllInInformationSelectorPopupContainerView;
@synthesize btnSaveInInformationSelectorPopupContainerView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
//    [MySingleton sharedManager].floatParentChildPrivacySettingInformationSelectorTableViewWidth = informationSelectorTableView.frame.size.width;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
    
    [MySingleton sharedManager].floatParentChildPrivacySettingInformationSelectorTableViewWidth = informationSelectorTableView.frame.size.width;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedPrivacySettingsForChildEvent) name:@"updatedPrivacySettingsForChildEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)updatedPrivacySettingsForChildEvent
{
    if([self.strInformationSelectorPopupOpenedFor isEqualToString:@"1"])
    {
        self.objSelectedChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = self.arraySelectedInformationTypes;
    }
    else if([self.strInformationSelectorPopupOpenedFor isEqualToString:@"2"])
    {
        self.objSelectedChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = self.arraySelectedInformationTypes;
    }
    else if([self.strInformationSelectorPopupOpenedFor isEqualToString:@"3"])
    {
        self.objSelectedChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = self.arraySelectedInformationTypes;
    }
    else if([self.strInformationSelectorPopupOpenedFor isEqualToString:@"4"])
    {
        self.objSelectedChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = self.arraySelectedInformationTypes;
    }
    else if([self.strInformationSelectorPopupOpenedFor isEqualToString:@"5"])
    {
        self.objSelectedChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = self.arraySelectedInformationTypes;
    }
    
    informationSelectorPopupContainerView.hidden = TRUE;
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Parents Control"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionFont, *btnFont, *lblInformationSelectorPopupContainerViewInstructionsFont, *btnSaveInInformationSelectorPopupContainerViewFont;
    CGFloat informationSelectorPopupContainerViewCornerRadius, btnSaveInInformationSelectorPopupContainerViewCornerRadiusValue;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInstructionFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        
        lblInformationSelectorPopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnSaveInInformationSelectorPopupContainerViewFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        
        informationSelectorPopupContainerViewCornerRadius = 15.0f;
        btnSaveInInformationSelectorPopupContainerViewCornerRadiusValue = 8.0f;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            lblInformationSelectorPopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnSaveInInformationSelectorPopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            informationSelectorPopupContainerViewCornerRadius = 10.0f;
            btnSaveInInformationSelectorPopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            lblInformationSelectorPopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnSaveInInformationSelectorPopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            informationSelectorPopupContainerViewCornerRadius = 10.0f;
            btnSaveInInformationSelectorPopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            lblInformationSelectorPopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnSaveInInformationSelectorPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            informationSelectorPopupContainerViewCornerRadius = 12.0f;
            btnSaveInInformationSelectorPopupContainerViewCornerRadiusValue = 8.0f;
        }
        else
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            lblInformationSelectorPopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnSaveInInformationSelectorPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            informationSelectorPopupContainerViewCornerRadius = 15.0f;
            btnSaveInInformationSelectorPopupContainerViewCornerRadiusValue = 8.0f;
        }
    }
    
    lblInstruction1.font = lblInstructionFont;
    lblInstruction1.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    btnParentRespondAlertForOtherUsers.layer.borderWidth = 1.0f;
    btnParentRespondAlertForOtherUsers.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnParentRespondAlertForOtherUsers.layer.masksToBounds = true;
    btnParentRespondAlertForOtherUsers.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnParentRespondAlertForOtherUsers.titleLabel.font = btnFont;
    [btnParentRespondAlertForOtherUsers setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnParentRespondAlertForOtherUsers addTarget:self action:@selector(btnParentRespondAlertForOtherUsersClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnAuthorityAlertForOtherUsers.layer.borderWidth = 1.0f;
    btnAuthorityAlertForOtherUsers.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnAuthorityAlertForOtherUsers.layer.masksToBounds = true;
    btnAuthorityAlertForOtherUsers.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnAuthorityAlertForOtherUsers.titleLabel.font = btnFont;
    [btnAuthorityAlertForOtherUsers setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnAuthorityAlertForOtherUsers addTarget:self action:@selector(btnAuthorityAlertForOtherUsersClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnKidnappingAlertForOtherUsers.layer.borderWidth = 1.0f;
    btnKidnappingAlertForOtherUsers.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnKidnappingAlertForOtherUsers.layer.masksToBounds = true;
    btnKidnappingAlertForOtherUsers.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnKidnappingAlertForOtherUsers.titleLabel.font = btnFont;
    [btnKidnappingAlertForOtherUsers setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnKidnappingAlertForOtherUsers addTarget:self action:@selector(btnKidnappingAlertForOtherUsersClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
    
    lblInstruction2.font = lblInstructionFont;
    lblInstruction2.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    btnAuthorityAlertForAuthority.layer.borderWidth = 1.0f;
    btnAuthorityAlertForAuthority.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnAuthorityAlertForAuthority.layer.masksToBounds = true;
    btnAuthorityAlertForAuthority.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnAuthorityAlertForAuthority.titleLabel.font = btnFont;
    [btnAuthorityAlertForAuthority setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnAuthorityAlertForAuthority addTarget:self action:@selector(btnAuthorityAlertForAuthorityClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnKidnappingAlertForAuthority.layer.borderWidth = 1.0f;
    btnKidnappingAlertForAuthority.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnKidnappingAlertForAuthority.layer.masksToBounds = true;
    btnKidnappingAlertForAuthority.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnKidnappingAlertForAuthority.titleLabel.font = btnFont;
    [btnKidnappingAlertForAuthority setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnKidnappingAlertForAuthority addTarget:self action:@selector(btnKidnappingAlertForAuthorityClicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    self.dataRows = [[NSMutableArray alloc] initWithObjects:[MySingleton sharedManager].strKeyForName, [MySingleton sharedManager].strKeyForEmail, [MySingleton sharedManager].strKeyForPhoneNumber, [MySingleton sharedManager].strKeyForAge, [MySingleton sharedManager].strKeyForGender, [MySingleton sharedManager].strKeyForHeight, [MySingleton sharedManager].strKeyForWeight, [MySingleton sharedManager].strKeyForPhotos, [MySingleton sharedManager].strKeyForHairColor, [MySingleton sharedManager].strKeyForEyeColor, [MySingleton sharedManager].strKeyForNationality, [MySingleton sharedManager].strKeyForEmergencyContactInformation, [MySingleton sharedManager].strKeyForSchoolInformation, [MySingleton sharedManager].strKeyForBestFriendsInformation, [MySingleton sharedManager].strKeyForLocationInformation, nil];
    
    self.dataRows = [[NSMutableArray alloc] initWithObjects:[MySingleton sharedManager].strKeyForName, [MySingleton sharedManager].strKeyForPhoneNumber, [MySingleton sharedManager].strKeyForAge, [MySingleton sharedManager].strKeyForGender, [MySingleton sharedManager].strKeyForHeight, [MySingleton sharedManager].strKeyForWeight, [MySingleton sharedManager].strKeyForPhotos, [MySingleton sharedManager].strKeyForHairColor, [MySingleton sharedManager].strKeyForHairStyle, [MySingleton sharedManager].strKeyForEyeColor, [MySingleton sharedManager].strKeyForNationality, [MySingleton sharedManager].strKeyForEmergencyContactInformation, [MySingleton sharedManager].strKeyForSchoolInformation, [MySingleton sharedManager].strKeyForBestFriendsInformation, [MySingleton sharedManager].strKeyForLocationInformation, nil];
    
    informationSelectorInnerContainerView.layer.masksToBounds = YES;
    informationSelectorInnerContainerView.layer.cornerRadius = informationSelectorPopupContainerViewCornerRadius;
    informationSelectorInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblInformationSelectorPopupContainerViewInstructions.font = lblInformationSelectorPopupContainerViewInstructionsFont;
    lblInformationSelectorPopupContainerViewInstructions.text = [NSString stringWithFormat:@"Please select information you want to display"];
    lblInformationSelectorPopupContainerViewInstructions.numberOfLines = 2;
    [lblInformationSelectorPopupContainerViewInstructions setLineBreakMode:NSLineBreakByWordWrapping];
    lblInformationSelectorPopupContainerViewInstructions.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    imageViewCloseInformationSelectorPopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseInformationSelectorPopupContainerView addTarget:self action:@selector(btnCloseInformationSelectorPopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    informationSelectorTableView.delegate = self;
    informationSelectorTableView.dataSource = self;
    informationSelectorTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    informationSelectorTableView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    btnSelectAllInInformationSelectorPopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSelectAllInInformationSelectorPopupContainerView.layer.masksToBounds = true;
    btnSelectAllInInformationSelectorPopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSelectAllInInformationSelectorPopupContainerView.titleLabel.font = btnSaveInInformationSelectorPopupContainerViewFont;
    [btnSelectAllInInformationSelectorPopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSelectAllInInformationSelectorPopupContainerView addTarget:self action:@selector(btnSelectAllInInformationSelectorPopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    btnSaveInInformationSelectorPopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSaveInInformationSelectorPopupContainerView.layer.masksToBounds = true;
    btnSaveInInformationSelectorPopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSaveInInformationSelectorPopupContainerView.titleLabel.font = btnSaveInInformationSelectorPopupContainerViewFont;
    [btnSaveInInformationSelectorPopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSaveInInformationSelectorPopupContainerView addTarget:self action:@selector(btnSaveInInformationSelectorPopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == informationSelectorTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == informationSelectorTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == informationSelectorTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == informationSelectorTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == informationSelectorTableView)
    {
        return 50;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == informationSelectorTableView)
    {
        NSString *strInformationType = [self.dataRows objectAtIndex:indexPath.row];
        
        ParentChildPrivacySettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[ParentChildPrivacySettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        cell.lblInformationType.text = strInformationType;
        
        if([self.arraySelectedInformationTypes containsObject:strInformationType])
        {
            cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_checked_privacy_settings.png"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == informationSelectorTableView)
    {
        NSString *strInformationType = [self.dataRows objectAtIndex:indexPath.row];
        
        if([self.arraySelectedInformationTypes containsObject:strInformationType])
        {
            NSInteger indexOfObject = [self.arraySelectedInformationTypes indexOfObject:strInformationType];
            
            if(indexOfObject != NSNotFound)
            {
                [self.arraySelectedInformationTypes removeObjectAtIndex:indexOfObject];
                [informationSelectorTableView reloadData];
            }
        }
        else
        {
            [self.arraySelectedInformationTypes addObject:strInformationType];
        }
        
        [informationSelectorTableView reloadData];
    }
}

#pragma mark - UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - Other Methods

-(void)showInformationSelectorPopupContainerView
{
    [self.view endEditing:YES];
    
    informationSelectorPopupContainerView.hidden = false;
    
    [informationSelectorTableView reloadData];
}

- (IBAction)btnCloseInformationSelectorPopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    informationSelectorPopupContainerView.hidden = TRUE;
}

-(void)btnSelectAllInInformationSelectorPopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    self.arraySelectedInformationTypes = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < self.dataRows.count; i++)
    {
        NSString *strInformationType = [self.dataRows objectAtIndex:i];
        
        [self.arraySelectedInformationTypes addObject:strInformationType];
    }
    
    [informationSelectorTableView reloadData];
}

-(void)btnSaveInInformationSelectorPopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(self.arraySelectedInformationTypes.count > 0)
    {
        NSLog(@"self.strInformationSelectorPopupOpenedFor : %@", self.strInformationSelectorPopupOpenedFor);
        NSLog(@"self.arraySelectedInformationTypes : %@", self.arraySelectedInformationTypes);
        
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [dictParameters setObject:strParentId forKey:@"parent_id"];
        [dictParameters setObject:self.objSelectedChild.strChildID forKey:@"child_id"];
        [dictParameters setObject:self.strInformationSelectorPopupOpenedFor forKey:@"alert_type"];
        
        NSString *strSelectedInformationTypesForDisplay = [self.arraySelectedInformationTypes componentsJoinedByString:@","];
        [dictParameters setObject:strSelectedInformationTypesForDisplay forKey:@"selected_information_types_for_display"];
        [[MySingleton sharedManager].dataManager updatePrivacySettingsForChild:dictParameters];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please select atleast one information to display"];
        });
    }
}

- (IBAction)btnParentRespondAlertForOtherUsersClicked:(id)sender
{
    [self.view endEditing:YES];
    
    //Parent Respond Alert For Other Users
    self.strInformationSelectorPopupOpenedFor = @"1";
    self.arraySelectedInformationTypes = [[NSMutableArray alloc] init];
    
    if(self.objSelectedChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers.count > 0)
    {
        self.arraySelectedInformationTypes = self.objSelectedChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers;
    }
    
    [self showInformationSelectorPopupContainerView];
}

- (IBAction)btnAuthorityAlertForOtherUsersClicked:(id)sender
{
    [self.view endEditing:YES];
    
    //Authority Alert For Other Users
    self.strInformationSelectorPopupOpenedFor = @"2";
    self.arraySelectedInformationTypes = [[NSMutableArray alloc] init];
    
    if(self.objSelectedChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers.count > 0)
    {
        self.arraySelectedInformationTypes = self.objSelectedChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers;
    }
    
    [self showInformationSelectorPopupContainerView];
}

- (IBAction)btnKidnappingAlertForOtherUsersClicked:(id)sender
{
    [self.view endEditing:YES];
    
    //Kidnapping Alert For Other Users
    self.strInformationSelectorPopupOpenedFor = @"3";
    self.arraySelectedInformationTypes = [[NSMutableArray alloc] init];
    
    if(self.objSelectedChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers.count > 0)
    {
        self.arraySelectedInformationTypes = self.objSelectedChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers;
    }
    
    [self showInformationSelectorPopupContainerView];
}

- (IBAction)btnAuthorityAlertForAuthorityClicked:(id)sender
{
    [self.view endEditing:YES];
    
    //Authority Alert For Authority
    self.strInformationSelectorPopupOpenedFor = @"4";
    self.arraySelectedInformationTypes = [[NSMutableArray alloc] init];
    
    if(self.objSelectedChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert.count > 0)
    {
        self.arraySelectedInformationTypes = self.objSelectedChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert;
    }
    
    [self showInformationSelectorPopupContainerView];
}

- (IBAction)btnKidnappingAlertForAuthorityClicked:(id)sender
{
    [self.view endEditing:YES];
    
    //Kidnapping Alert For Authority
    self.strInformationSelectorPopupOpenedFor = @"5";
    self.arraySelectedInformationTypes = [[NSMutableArray alloc] init];
    
    if(self.objSelectedChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert.count > 0)
    {
        self.arraySelectedInformationTypes = self.objSelectedChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert;
    }
    
    [self showInformationSelectorPopupContainerView];
}

@end
