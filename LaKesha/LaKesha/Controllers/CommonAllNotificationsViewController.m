//
//  CommonAllNotificationsViewController.m
//  LaKesha
//
//  Created by INNOVATIVE ITERATION 4 on 09/10/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CommonAllNotificationsViewController.h"
#import "IQKeyboardManager.h"

#import "Notification.h"
#import "NotificationListTableViewCell.h"
#import "CommonAllBoycottAlertsViewController.h"
#import "ChildHomeViewController.h"
#import "ParentHomeViewController.h"

@interface CommonAllNotificationsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}
@end

@implementation CommonAllNotificationsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainTableView;

@synthesize bottomContainerView;

@synthesize notificationContainerView;
@synthesize lblNotification;
@synthesize imageViewNotification;
@synthesize btnNotification;

@synthesize boycottAlertsContainerView;
@synthesize lblBoycottAlerts;
@synthesize imageViewBoycottAlerts;
@synthesize btnBoycottAlerts;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(commonGotAllNotificationsEvent) name:@"commonGotAllNotificationsEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)commonGotAllNotificationsEvent
{
    self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNotifications;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.userInteractionEnabled = false;
    }
    else
    {
        mainTableView.userInteractionEnabled = true;
    }
    mainTableView.hidden = false;
    [mainTableView reloadData];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    if([self.strLoadedFor isEqualToString:@"0"] || [self.strLoadedFor isEqualToString:@"1"])
    {
        imageViewBack.image = [UIImage imageNamed:@"back_button_white.png"];
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        imageViewBack.image = [UIImage imageNamed:@"menu.png"];
    }
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Notifications"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    if ([self.strLoadedFor isEqualToString:@"0"])
    {
        [self navigateToChildHomeViewController];
    }
    else if ([self.strLoadedFor isEqualToString:@"1"])
    {
        [self navigateToParentHomeViewController];
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        if(self.sideMenuController.isLeftViewVisible)
        {
            [self.sideMenuController hideLeftViewAnimated];
        }
        else
        {
            [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
        }
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor whiteColor];
    mainTableView.hidden = true;
    
    UIFont *lblBottomBarFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblBottomBarFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblBottomBarFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblBottomBarFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblBottomBarFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else
        {
            lblBottomBarFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    bottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblNotification.font = lblBottomBarFont;
    lblNotification.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewNotification.layer.masksToBounds = true;
    
    [btnNotification addTarget:self action:@selector(btnNotificationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblBoycottAlerts.font = lblBottomBarFont;
    lblBoycottAlerts.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewBoycottAlerts.layer.masksToBounds = true;
    
    [btnBoycottAlerts addTarget:self action:@selector(btnBoycottAlertsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [dictParameters setObject:self.strLoadedFor forKey:@"user_type"];
    
    if([self.strLoadedFor isEqualToString:@"0"])
    {
        [dictParameters setObject:[prefs objectForKey:@"childid"] forKey:@"user_id"];
    }
    else if([self.strLoadedFor isEqualToString:@"1"])
    {
        [dictParameters setObject:[prefs objectForKey:@"parentid"] forKey:@"user_id"];
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
       [dictParameters setObject:[prefs objectForKey:@"guestid"] forKey:@"user_id"];
    }
    
    [[MySingleton sharedManager].dataManager commonGetAllNotifications:dictParameters];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.dataRows.count > 0)
    {
        return self.dataRows.count;
    }
    else
    {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.dataRows.count > 0)
    {
        Notification *objNotification = [self.dataRows objectAtIndex:indexPath.row];
        
        UILabel *lblNotificationTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, ([MySingleton sharedManager].screenWidth - 20), 20)];
        lblNotificationTitle.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblNotificationTitle.textAlignment = NSTextAlignmentLeft;
        lblNotificationTitle.numberOfLines = 0;
        lblNotificationTitle.text = [NSString stringWithFormat:@"Title: %@", objNotification.strNotificationTitle];
        [lblNotificationTitle sizeToFit];
        
        CGRect lblNotificationTitleTextRect = [lblNotificationTitle.text boundingRectWithSize:lblNotificationTitle.frame.size
                                                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                                                   attributes:@{NSFontAttributeName:lblNotificationTitle.font}
                                                                                      context:nil];
        
        CGSize lblNotificationTitleSize = lblNotificationTitleTextRect.size;
        
        CGFloat lblNotificationTitleHeight;
        
        if(objNotification.strNotificationTitle.length > 0)
        {
            lblNotificationTitleHeight = lblNotificationTitleSize.height;
        }
        else
        {
            lblNotificationTitleHeight = 0;
        }
        
        NSLog(@"[MySingleton sharedManager].screenWidth : %f", [MySingleton sharedManager].screenWidth);
        
        UILabel *lblNotificationText = [[UILabel alloc] initWithFrame:CGRectMake(10, (lblNotificationTitle.frame.origin.y + lblNotificationTitleHeight + 5), ([MySingleton sharedManager].screenWidth - 20), 15)];
        lblNotificationText.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblNotificationText.textAlignment = NSTextAlignmentLeft;
        lblNotificationText.numberOfLines = 0;
        lblNotificationText.text = [NSString stringWithFormat:@"Message: %@", objNotification.strNotificationText];
        [lblNotificationText sizeToFit];
        
        CGRect lblNotificationTextTextRect = [lblNotificationText.text boundingRectWithSize:lblNotificationText.frame.size
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:lblNotificationText.font}
                                                                                    context:nil];
        
        CGSize lblNotificationTextSize = lblNotificationTextTextRect.size;
        
        CGFloat lblNotificationTextHeight;
        
        if(objNotification.strNotificationText.length > 0)
        {
            lblNotificationTextHeight = lblNotificationTextSize.height;
        }
        else
        {
            lblNotificationTextHeight = 0;
        }
        
        CGFloat floatCellHeight = lblNotificationText.frame.origin.y + lblNotificationTextHeight + 5;
        
        if(floatCellHeight > 95)
        {
            return floatCellHeight;
        }
        else
        {
            return 95;
        }
    }
    else
    {
        return 44;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(self.dataRows.count > 0)
    {
        NotificationListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        Notification *objNotification = [self.dataRows objectAtIndex:indexPath.row];
        
        cell = [[NotificationListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        NSString *finalDate =  objNotification.strNotificationTime;
        
        ///////
        //NSString *finalDate = @"02-09-2011 20:54:18";
        
        // Prepare an NSDateFormatter to convert to and from the string representation
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:finalDate];
        
        [dateFormatter setDateFormat:@"hh:mm a MM-dd-yyyy"];
        // Write the date back out using the same format
        NSString *strNewFormattedDate = [dateFormatter stringFromDate:date];
        
        cell.lblNotificationTime.text = strNewFormattedDate;
        
        if ([objNotification.strNotificationType isEqualToString:@"0"])
        {
            cell.lblNotificationType.text = @"Alert Type: Nation Wide";
        }
        
        else if ([objNotification.strNotificationType isEqualToString:@"1"])
        {
            cell.lblNotificationType.text = @"Alert Type: State Wide";
        }
        
        else if ([objNotification.strNotificationType isEqualToString:@"2"])
        {
            cell.lblNotificationType.text = @"Alert Type: City Wide";
        }
        
        cell.lblNotificationTitle.text = [NSString stringWithFormat:@"Title: %@", objNotification.strNotificationTitle];
        [cell.lblNotificationTitle sizeToFit];
        
        cell.lblNotificationText.text = [NSString stringWithFormat:@"Message: %@", objNotification.strNotificationText];
        [cell.lblNotificationText sizeToFit];
        
        
        
        
        CGFloat lblNotificationTitleHeight;
        
        if(objNotification.strNotificationTitle.length > 0)
        {
            CGRect lblNotificationTitleTextRect = [cell.lblNotificationTitle.text boundingRectWithSize:cell.lblNotificationTitle.frame.size
                                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                                            attributes:@{NSFontAttributeName:cell.lblNotificationTitle.font}
                                                                                               context:nil];
            
            CGSize lblNotificationTitleSize = lblNotificationTitleTextRect.size;
            
            lblNotificationTitleHeight = lblNotificationTitleSize.height;
        }
        else
        {
            lblNotificationTitleHeight = 0;
        }
        
        
        
        CGFloat lblNotificationTextHeight;
        
        if(objNotification.strNotificationText.length > 0)
        {
            CGRect lblNotificationTextTextRect = [cell.lblNotificationText.text boundingRectWithSize:cell.lblNotificationText.frame.size
                                                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                                                          attributes:@{NSFontAttributeName:cell.lblNotificationText.font}
                                                                                             context:nil];
            
            CGSize lblNotificationTextSize = lblNotificationTextTextRect.size;
            
            lblNotificationTextHeight = lblNotificationTextSize.height;
        }
        else
        {
            lblNotificationTextHeight = 0;
        }
        
        CGRect lblNotificationTitleFrame = cell.lblNotificationTitle.frame;
        lblNotificationTitleFrame.size.height = lblNotificationTitleHeight;
        cell.lblNotificationTitle.frame = lblNotificationTitleFrame;
        
        CGRect lblNotificationTextFrame = cell.lblNotificationText.frame;
        lblNotificationTextFrame.origin.y = cell.lblNotificationTitle.frame.origin.y + lblNotificationTitleHeight + 5;
        lblNotificationTextFrame.size.height = lblNotificationTextHeight;
        cell.lblNotificationText.frame = lblNotificationTextFrame;
        
        
        
        CGFloat floatCellHeight = cell.lblNotificationText.frame.origin.y + lblNotificationTextHeight + 5;
        
        CGRect mainContainerFrame = cell.mainContainer.frame;
        
        if(floatCellHeight > 95)
        {
            mainContainerFrame.size.height = floatCellHeight;
        }
        else
        {
            mainContainerFrame.size.height = 95;
        }
        
        cell.mainContainer.frame = mainContainerFrame;
        
        
        
        CGRect separatorViewFrame = cell.separatorView.frame;
        separatorViewFrame.origin.y = mainContainerFrame.size.height - 1;
        cell.separatorView.frame = separatorViewFrame;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else
    {
        UIFont *lblNoDataFont;
        
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        }
        else
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        }
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:MyIdentifier];
        
        UILabel *lblNoData = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mainTableView.frame.size.width, cell.frame.size.height)];
        lblNoData.textAlignment = NSTextAlignmentCenter;
        lblNoData.font = lblNoDataFont;
        lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
        lblNoData.text = @"No notification found.";
        
        [cell.contentView addSubview:lblNoData];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - Other Methods

-(IBAction)btnNotificationClicked:(id)sender
{
    [self.view endEditing:YES];
    
    CommonAllNotificationsViewController *viewController = [[CommonAllNotificationsViewController alloc]init];
    
    viewController.strLoadedFor = self.strLoadedFor;
    
    if([self.strLoadedFor isEqualToString:@"0"] || [self.strLoadedFor isEqualToString:@"1"])
    {
        [self.navigationController pushViewController:viewController animated:false];
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        SideMenuViewController *sideMenuViewController;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
        }
        else
        {
            if([MySingleton sharedManager].screenHeight == 568)
            {
                sideMenuViewController = [[SideMenuViewController alloc] init];
            }
            else if([MySingleton sharedManager].screenHeight == 667)
            {
                sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
            }
            else if([MySingleton sharedManager].screenHeight == 736)
            {
                sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
            }
            else
            {
                sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
            }
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navigationController pushViewController:sideMenuController animated:false];
    }
}

-(IBAction)btnBoycottAlertsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    CommonAllBoycottAlertsViewController *viewController = [[CommonAllBoycottAlertsViewController alloc]init];
    
    viewController.strLoadedFor = self.strLoadedFor;
    
    if([self.strLoadedFor isEqualToString:@"0"] || [self.strLoadedFor isEqualToString:@"1"])
    {
        [self.navigationController pushViewController:viewController animated:false];
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        SideMenuViewController *sideMenuViewController;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
        }
        else
        {
            if([MySingleton sharedManager].screenHeight == 568)
            {
                sideMenuViewController = [[SideMenuViewController alloc] init];
            }
            else if([MySingleton sharedManager].screenHeight == 667)
            {
                sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
            }
            else if([MySingleton sharedManager].screenHeight == 736)
            {
                sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
            }
            else
            {
                sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
            }
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navigationController pushViewController:sideMenuController animated:false];
    }
}

- (void)navigateToChildHomeViewController
{
    [self.view endEditing:true];
    
    ChildHomeViewController *viewController = [[ChildHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:false];
}

- (void)navigateToParentHomeViewController
{
    [self.view endEditing:true];
    
    ParentHomeViewController *viewController = [[ParentHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:false];
}

@end
