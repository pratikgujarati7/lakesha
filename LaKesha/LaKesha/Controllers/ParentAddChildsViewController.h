//
//  ParentAddChildsViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 11/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Child.h"

@interface ParentAddChildsViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIView *searchTextContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtSearch;

@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;

@property (nonatomic,retain) IBOutlet UIView *parentRolePopupContainerView;
@property (nonatomic,retain) IBOutlet UIView *parentRolePopupBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *parentRolePopupInnerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblParentRolePopupContainerViewTitle;
@property (nonatomic,retain) IBOutlet UILabel *lblParentRolePopupContainerViewInstructions;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseParentRolePopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseParentRolePopupContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtParentRole;
@property (nonatomic,retain) IBOutlet UILabel *lblParentRolePopupContainerViewInstructions2;
@property (nonatomic,retain) IBOutlet UITextField *txtAuthorityEmailAddress;
@property (nonatomic,retain) IBOutlet UIButton *btnRequestInParentRolePopupContainerView;

//========== OTHER VARIABLES ==========//

@property (strong,nonatomic) UIToolbar* keyboardDoneButtonView;

@property (nonatomic,retain) NSMutableArray *dataRows;

@property (nonatomic,retain) Child *objSelectedChild;

@end
