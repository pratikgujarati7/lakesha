//
//  ParentPaymentViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 01/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

//@import Stripe;
#import "Stripe.h"

@interface ParentPaymentViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, STPShippingAddressViewControllerDelegate, STPAddCardViewControllerDelegate, PKPaymentAuthorizationViewControllerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewMainLogo;

@property (nonatomic,retain) IBOutlet UIView *emailPermissionsContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewEmailPermissionsCheckBox;
@property (nonatomic,retain) IBOutlet UIButton *btnEmailPermissionsCheckBox;
@property (nonatomic,retain) IBOutlet UILabel *lblEmailPermissions;
@property (nonatomic,retain) IBOutlet UIButton *btnEmailPermissions;

@property (nonatomic,retain) IBOutlet UIButton *btnPay;

//========== OTHER VARIABLES ==========//

@property (nonatomic,assign) BOOL boolIsLoadedWithoutBack;

@property (nonatomic,assign) BOOL boolIsEmailChecked;

@end
