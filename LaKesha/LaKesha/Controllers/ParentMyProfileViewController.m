//
//  ParentMyProfileViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 17/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentMyProfileViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "CommonChangePasswordViewController.h"

@interface ParentMyProfileViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentMyProfileViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewOptions;
@synthesize btnOptions;

@synthesize mainContainerView;

@synthesize imageViewProfilePicture;

@synthesize lblEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize lblName;
@synthesize txtName;
@synthesize txtNameBottomSeparatorView;

@synthesize lblDoYouWantToAddAddressContainerView;
@synthesize lblDoYouWantToAddAddress;

@synthesize imageViewDoNotAddAddress;
@synthesize lblDoNotAddAddress;
@synthesize btnDoNotAddAddress;

@synthesize imageViewAddAddress;
@synthesize lblAddAddress;
@synthesize btnAddAddress;

@synthesize addressContainerView;

@synthesize lblStreetAddress;
@synthesize txtViewStreetAddress;
@synthesize txtViewStreetAddressBottomSeparatorView;

@synthesize compulsoryAddressContainerView;

@synthesize lblState;
@synthesize txtState;
@synthesize txtStateBottomSeparatorView;
@synthesize imageViewStateDropDownArrow;

@synthesize lblCity;
@synthesize txtCity;
@synthesize txtCityBottomSeparatorView;
@synthesize imageViewCityDropDownArrow;

@synthesize lblZipCode;
@synthesize txtZipCode;
@synthesize txtZipCodeBottomSeparatorView;

@synthesize bottomInformationContainerView;

@synthesize lblPhoneNumber;
@synthesize txtPhoneNumber;
@synthesize txtPhoneNumberBottomSeparatorView;

@synthesize lblSecondaryPhoneNumber;
@synthesize txtSecondaryPhoneNumber;
@synthesize txtSecondaryPhoneNumberBottomSeparatorView;

@synthesize lblGender;
@synthesize txtGender;
@synthesize txtGenderBottomSeparatorView;
@synthesize imageViewGenderDropDownArrow;

@synthesize lblWorkPlaceInformation;

@synthesize lblWorkPlaceName;
@synthesize txtWorkPlaceName;
@synthesize txtWorkPlaceNameBottomSeparatorView;

@synthesize lblWorkPlacePhoneNumber;
@synthesize txtWorkPlacePhoneNumber;
@synthesize txtWorkPlacePhoneNumberBottomSeparatorView;

@synthesize btnUpdate;

//========== OTHER VARIABLES ==========//

@synthesize keyboardDoneButtonView;

@synthesize genderPickerView;

@synthesize statePickerView;

@synthesize cityPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
        
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageViewProfilePicture.layer.cornerRadius = (imageViewProfilePicture.frame.size.width / 2) - 40;
    }
    else
    {
        imageViewProfilePicture.layer.cornerRadius = imageViewProfilePicture.frame.size.width / 2;
    }
    
//    mainContainerView.autoresizesSubviews = false;
//
//    addressContainerView.hidden = true;
//
//    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
//    compulsoryAddressContainerViewFrame.origin.y = addressContainerView.frame.origin.y;
//    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
//
//    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
//    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
//    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
//
//    CGRect mainContainerViewFrame = mainContainerView.frame;
//    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
//    mainContainerView.frame = mainContainerViewFrame;
//
//    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
//
//    mainContainerView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotParentProfileDetailsByParentIdEvent) name:@"gotParentProfileDetailsByParentIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedParentProfileEvent) name:@"updatedParentProfileEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadedParentProfilePictureWith_FileEvent) name:@"uploadedParentProfilePictureWith_FileEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotParentProfileDetailsByParentIdEvent
{
    if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
    {
        txtState.userInteractionEnabled = true;
    }
    else
    {
        txtState.userInteractionEnabled = false;
    }
    
    [[AsyncImageLoader sharedLoader].cache removeAllObjects];
    imageViewProfilePicture.imageURL = [NSURL URLWithString:[MySingleton sharedManager].dataManager.objLoggedInParent.strProfilePictureImageUrl];
    
    txtEmail.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strEmail;
    txtName.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strName;
    txtState.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strStateName;
    txtCity.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strCityName;
    txtZipCode.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strZipcode;
    txtViewStreetAddress.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strStreetAddress;
    txtViewStreetAddress.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPhoneNumber.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strPhoneNumber;
    txtSecondaryPhoneNumber.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strSecondaryPhoneNumber;
    txtGender.text = [[MySingleton sharedManager].dataManager.objLoggedInParent.strGender capitalizedString];
    if([[txtGender.text lowercaseString] isEqualToString:@"male"])
    {
        [genderPickerView selectRow:0 inComponent:0 animated:YES];
    }
    else
    {
        [genderPickerView selectRow:1 inComponent:0 animated:YES];
    }
    txtWorkPlaceName.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strWorkPlaceName;
    txtWorkPlacePhoneNumber.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strWorkPlacePhoneNumber;
    
    NSMutableArray *arrayStateIds = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < [MySingleton sharedManager].dataManager.arrayStates.count; i++)
    {
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:i];
        [arrayStateIds addObject:objState.strStateID];
    }
    
    NSInteger indexOfStateId = [arrayStateIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInParent.strStateId];
    
    if(NSNotFound != indexOfStateId)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:indexOfStateId];
        [statePickerView selectRow:indexOfStateId inComponent:0 animated:NO];
        [cityPickerView reloadAllComponents];
        
        NSMutableArray *arrayCityIds = [[NSMutableArray alloc] init];
        
        for(int j = 0; j < self.objSelectedState.arrayCity.count; j++)
        {
            City *objCity = [self.objSelectedState.arrayCity objectAtIndex:j];
            [arrayCityIds addObject:objCity.strCityID];
        }
        
        NSInteger indexOfCityId = [arrayCityIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInParent.strCityId];
        
        if(NSNotFound != indexOfCityId)
        {
            self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:indexOfCityId];
            [cityPickerView selectRow:indexOfCityId inComponent:0 animated:NO];
        }
    }
    
    if([MySingleton sharedManager].dataManager.objLoggedInParent.strStreetAddress.length > 0)
    {
        self.boolIsAddAddressSelected = true;
        [MySingleton sharedManager].dataManager.objLoggedInParent.boolIsAddAddressSelected = true;
        
        imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
        imageViewAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
        
        mainContainerView.autoresizesSubviews = false;
        
        addressContainerView.hidden = false;
        
        float floatMargin = lblName.frame.origin.y - txtEmailBottomSeparatorView.frame.origin.y;
        
//        CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
//        bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height + floatMargin;
//        bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
        
        CGRect mainContainerViewFrame = mainContainerView.frame;
        mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
        mainContainerView.frame = mainContainerViewFrame;
        
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        mainContainerView.autoresizesSubviews = true;
    }
    else
    {
        self.boolIsAddAddressSelected = false;
        [MySingleton sharedManager].dataManager.objLoggedInParent.boolIsAddAddressSelected = false;
        
        imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
        imageViewAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
        
        mainContainerView.autoresizesSubviews = false;
        
        CGRect compulsoryAddressContainerViewFrame =  compulsoryAddressContainerView.frame;
        compulsoryAddressContainerViewFrame.origin.y = addressContainerView.frame.origin.y;
        compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
        
//        CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
//        bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y;
//        bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
        
        CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
        bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerViewFrame.origin.y + compulsoryAddressContainerViewFrame.size.height;
        bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
        
        CGRect mainContainerViewFrame = mainContainerView.frame;
        mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
        mainContainerView.frame = mainContainerViewFrame;
        
        addressContainerView.hidden = true;
        
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        mainContainerView.autoresizesSubviews = true;
    }
}

-(void)updatedParentProfileEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Profile Updated" withDetails:@"Your profile has been updated successfully."];
    });
}

-(void)uploadedParentProfilePictureWith_FileEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Profile Picture Updated" withDetails:@"Your profile picture has been changed successfully."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"My Profile"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    
    imageViewOptions.layer.masksToBounds = YES;
    [btnOptions addTarget:self action:@selector(btnOptionsClicked:) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnOptionsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Options" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Dismiss button tappped.
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Change Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self redirectToChangePasswordViewController];
    }]];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = btnOptions;
        popPresenter.sourceRect = btnOptions.bounds;
        popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
}

-(void)redirectToChangePasswordViewController
{
    CommonChangePasswordViewController *viewController = [[CommonChangePasswordViewController alloc] init];
    viewController.strLoadedFor = @"1";
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    keyboardDoneButtonView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *flex1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)];
    [doneButton1 setTitleTextAttributes:@{ NSFontAttributeName: [MySingleton sharedManager].themeFontTwentySizeRegular,NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex1,doneButton1, nil]];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    self.arrayGender = [[NSMutableArray alloc]initWithObjects:@"Male",@"Female",nil];
    
    UIFont *lblFont, *txtFieldFont, *lblHeaderFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblHeaderFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
        
    imageViewProfilePicture.layer.masksToBounds = YES;
    imageViewProfilePicture.layer.borderWidth = 2.0f;
    imageViewProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    imageViewProfilePicture.userInteractionEnabled = true;
    imageViewProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
    UITapGestureRecognizer *imageViewProfilePictureTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewProfilePictureTapped:)];
    imageViewProfilePictureTapGesture.delegate = self;
    [imageViewProfilePicture addGestureRecognizer:imageViewProfilePictureTapGesture];
    
    self.boolIsAddAddressSelected = false;
    
    lblEmail.font = lblFont;
    lblEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmail WithBottomSeparatorView:txtEmailBottomSeparatorView];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    txtEmail.userInteractionEnabled = false;
    txtEmail.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    lblName.font = lblFont;
    lblName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtName WithBottomSeparatorView:txtNameBottomSeparatorView];
    
    lblDoYouWantToAddAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblDoYouWantToAddAddress.font = lblHeaderFont;
    
    imageViewDoNotAddAddress.layer.masksToBounds = true;
    imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
    
    lblDoNotAddAddress.font = lblFont;
    lblDoNotAddAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnDoNotAddAddress addTarget:self action:@selector(btnDoNotAddAddressClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewAddAddress.layer.masksToBounds = true;
    imageViewAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    lblAddAddress.font = lblFont;
    lblAddAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnAddAddress addTarget:self action:@selector(btnAddAddressClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblStreetAddress.font = lblFont;
    lblStreetAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    txtViewStreetAddress.text = @"Street Address";
    txtViewStreetAddress.font = txtFieldFont;
    txtViewStreetAddress.delegate = self;
    txtViewStreetAddress.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewStreetAddress.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewStreetAddress setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    txtViewStreetAddressBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    statePickerView = [[UIPickerView alloc] init];
    statePickerView.delegate = self;
    statePickerView.dataSource = self;
    statePickerView.showsSelectionIndicator = YES;
    statePickerView.tag = 2;
    statePickerView.backgroundColor = [UIColor whiteColor];
    
    lblState.font = lblFont;
    lblState.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtState WithBottomSeparatorView:txtStateBottomSeparatorView];
    [txtState setInputView:statePickerView];
    imageViewStateDropDownArrow.layer.masksToBounds = true;
    
    cityPickerView = [[UIPickerView alloc] init];
    cityPickerView.delegate = self;
    cityPickerView.dataSource = self;
    cityPickerView.showsSelectionIndicator = YES;
    cityPickerView.tag = 3;
    cityPickerView.backgroundColor = [UIColor whiteColor];
    
    lblCity.font = lblFont;
    lblCity.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtCity WithBottomSeparatorView:txtCityBottomSeparatorView];
    [txtCity setInputView:cityPickerView];
    imageViewCityDropDownArrow.layer.masksToBounds = true;
    
    lblZipCode.font = lblFont;
    lblZipCode.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtZipCode WithBottomSeparatorView:txtZipCodeBottomSeparatorView];
    
    lblPhoneNumber.font = lblFont;
    lblPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPhoneNumber WithBottomSeparatorView:txtPhoneNumberBottomSeparatorView];
    [txtPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
    txtPhoneNumber.userInteractionEnabled = false;
    txtPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
//    txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblSecondaryPhoneNumber.font = lblFont;
    lblSecondaryPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSecondaryPhoneNumber WithBottomSeparatorView:txtSecondaryPhoneNumberBottomSeparatorView];
    [txtSecondaryPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
//    txtSecondaryPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    genderPickerView = [[UIPickerView alloc] init];
    genderPickerView.delegate = self;
    genderPickerView.dataSource = self;
    genderPickerView.showsSelectionIndicator = YES;
    genderPickerView.tag = 1;
    genderPickerView.backgroundColor = [UIColor whiteColor];
    
    lblGender.font = lblFont;
    lblGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtGender WithBottomSeparatorView:txtGenderBottomSeparatorView];
    [txtGender setInputView:genderPickerView];
    imageViewGenderDropDownArrow.layer.masksToBounds = true;
    
    lblWorkPlaceInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblWorkPlaceInformation.font = lblHeaderFont;
    
    lblWorkPlaceName.font = lblFont;
    lblWorkPlaceName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtWorkPlaceName WithBottomSeparatorView:txtWorkPlaceNameBottomSeparatorView];
    
    lblWorkPlacePhoneNumber.font = lblFont;
    lblWorkPlacePhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtWorkPlacePhoneNumber WithBottomSeparatorView:txtWorkPlacePhoneNumberBottomSeparatorView];
    [txtWorkPlacePhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
    
    btnUpdate.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnUpdate.layer.masksToBounds = true;
    btnUpdate.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnUpdate.titleLabel.font = btnFont;
    [btnUpdate setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnUpdate addTarget:self action:@selector(btnUpdateClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
    [[MySingleton sharedManager].dataManager getParentProfileDetailsByParentId:strParentId];
}

- (void)setupTextfield:(UITextField *)textField WithBottomSeparatorView:(UIView *)textFieldBottomSeparatorView
{
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    textField.font = txtFieldFont;
    textField.delegate = self;
    [textField setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [MySingleton sharedManager].textfieldTextColor;
    textField.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    textFieldBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtGender)
    {
        if(txtGender.text.length == 0 )
        {
            txtGender.text = [self.arrayGender objectAtIndex:0];
            [genderPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
    else if(textField == txtState)
    {
        if(txtState.text.length == 0 )
        {
            if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
            {
                self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:0];
                txtState.text = self.objSelectedState.strStateName;
                txtCity.userInteractionEnabled = true;
                [statePickerView selectRow:0 inComponent:0 animated:YES];
                [cityPickerView reloadAllComponents];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No States Added" withDetails:@"Currently we don't have any states added."];
            }
        }
    }
    else if(textField == txtCity)
    {
        if(txtCity.text.length == 0 )
        {
            if(self.objSelectedState.arrayCity.count > 0)
            {
                self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:0];
                txtCity.text = self.objSelectedCity.strCityName;
                [cityPickerView selectRow:0 inComponent:0 animated:YES];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewStreetAddress)
    {
        if ([textView.text isEqualToString:@"Street Address"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewStreetAddress)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Street Address";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayGender count];
    }
    else if(pickerView.tag == 2)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.arrayStates count];
    }
    else if(pickerView.tag == 3)
    {
        rowsInComponent = [self.objSelectedState.arrayCity count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == genderPickerView)
    {
        lblMain.text = self.arrayGender[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == statePickerView)
    {
        State *objState = [MySingleton sharedManager].dataManager.arrayStates[row];
        lblMain.text = objState.strStateName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == cityPickerView)
    {
        City *objCity = self.objSelectedState.arrayCity[row];
        lblMain.text = objCity.strCityName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtGender.text = [self.arrayGender objectAtIndex:row];
    }
    else if(pickerView.tag == 2)
    {
        NSLog(@"[MySingleton sharedManager].dataManager.arrayStates.count : %d", [MySingleton sharedManager].dataManager.arrayStates.count);
        
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:row];
        txtState.text = self.objSelectedState.strStateName;
        [cityPickerView reloadAllComponents];
        
        if(self.objSelectedState.arrayCity.count > 0)
        {
            [cityPickerView selectRow:0 inComponent:0 animated:NO];
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
        }
        else
        {
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
            
            [self.view endEditing:true];
            [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
        }
    }
    else if(pickerView.tag == 3)
    {
        self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:row];
        txtCity.text = self.objSelectedCity.strCityName;
    }
}

#pragma mark - UIImagePickerController Delegate Method

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try
    {
        UIImage *image = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
//        CGSize newSize = CGSizeMake(320, 320);
//        UIGraphicsBeginImageContext(newSize);
//        [image drawInRect:CGRectMake(0,0,320,320)];
//
//        UIImage* smaller = UIGraphicsGetImageFromCurrentImageContext();
        
        UIImage* smaller = [self imageWithImage:image scaledToWidth:320];
        
        self.imageSelectedProfilePicture = smaller;
        imageViewProfilePicture.image = self.imageSelectedProfilePicture;
        imageViewProfilePicture.layer.masksToBounds = YES;
        
        //SEND IMAGE DATA TO SERVER
        self.imageSelectedProfilePictureData = UIImagePNGRepresentation(smaller);
        
        UIGraphicsEndImageContext();
        
        [picker dismissViewControllerAnimated:NO completion:NULL];
        
        [[MySingleton sharedManager].dataManager uploadParentProfilePictureWith_File:self.imageSelectedProfilePictureData];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception in imagePickerController's didFinishPickingMediaWithInfo Method, %@",exception);
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Selection Methods

-(void)takeAPhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing = YES;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [imagePickerController setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [imagePickerController
                                                             popoverPresentationController];
            popPresenter.sourceView = imageViewProfilePicture;
            popPresenter.sourceRect = imageViewProfilePicture.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            });
        }
        else
        {
            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                }];
            }
            else
            {
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Camera Unavailable" withDetails:@"Unable to find a camera on your device."];
        });
    }
}

-(void)chooseFromGallery
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [imagePickerController setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [imagePickerController
                                                             popoverPresentationController];
            popPresenter.sourceView = imageViewProfilePicture;
            popPresenter.sourceRect = imageViewProfilePicture.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            });
        }
        else
        {
            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                }];
            }
            else
            {
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Photo library Unavailable" withDetails:@"Unable to find photo library on your device."];
        });
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void)imageViewProfilePictureTapped: (UITapGestureRecognizer *)recognizer
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Take a Photo
        [self dismissViewControllerAnimated:YES completion:nil];
        [self takeAPhoto];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Choose from Gallery
        [self dismissViewControllerAnimated:YES completion:nil];
        [self chooseFromGallery];
    }]];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = imageViewProfilePicture;
        popPresenter.sourceRect = imageViewProfilePicture.bounds;
        popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
}

- (IBAction)btnAddAddressClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddAddressSelected = true;
    
    imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
    imageViewAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    addressContainerView.hidden = false;
    
    float floatMargin = lblName.frame.origin.y - txtEmailBottomSeparatorView.frame.origin.y;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = addressContainerView.frame.origin.y + addressContainerView.frame.size.height;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerViewFrame.origin.y + compulsoryAddressContainerViewFrame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

- (IBAction)btnDoNotAddAddressClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddAddressSelected = false;
    
    imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
    imageViewAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = addressContainerView.frame.origin.y;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    addressContainerView.hidden = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

-(void)bindDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInParent == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent = [[Parent alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strName = txtName.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.boolIsAddAddressSelected = self.boolIsAddAddressSelected;
    
    if(self.boolIsAddAddressSelected)
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strStreetAddress = txtViewStreetAddress.text;
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strStreetAddress = @"";
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.strStateId = self.objSelectedState.strStateID;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strStateName = txtState.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strCityId = self.objSelectedCity.strCityID;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strCityName = txtCity.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strZipcode = txtZipCode.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.strPhoneNumber = txtPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strSecondaryPhoneNumber = txtSecondaryPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strGender = txtGender.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strWorkPlaceName = txtWorkPlaceName.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strWorkPlacePhoneNumber = txtWorkPlacePhoneNumber.text;
}

- (IBAction)btnUpdateClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self bindDataToObject];
    
    if([[MySingleton sharedManager].dataManager.objLoggedInParent isValidateParentForUpdation])
    {
        [[MySingleton sharedManager].dataManager updateParentProfile:[MySingleton sharedManager].dataManager.objLoggedInParent];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInParent.strValidationMessage];
        });
    }
}

@end
