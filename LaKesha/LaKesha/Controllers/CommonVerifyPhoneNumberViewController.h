//
//  CommonVerifyPhoneNumberViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 20/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface CommonVerifyPhoneNumberViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;
@property (nonatomic,retain) IBOutlet UITextField *txtOTP;
@property (nonatomic,retain) IBOutlet UIView *txtOTPBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIButton *btnResentOTP;
@property (nonatomic,retain) IBOutlet UILabel *lblOr;
@property (nonatomic,retain) IBOutlet UIButton *btnChangePhoneNumber;

@property (nonatomic,retain) IBOutlet UIButton *btnVerify;

//========== OTHER VARIABLES ==========//

@property (strong,nonatomic) UIToolbar* keyboardDoneButtonView;

@property (nonatomic,retain) NSString *strPhoneNumber;

@property (nonatomic,retain) NSString *strLoadedFor;

@end
