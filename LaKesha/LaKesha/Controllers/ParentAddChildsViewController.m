//
//  ParentAddChildsViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 11/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentAddChildsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentAddChildsTableViewCell.h"

#import "IQUIView+Hierarchy.h"

@interface ParentAddChildsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentAddChildsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize searchTextContainerView;
@synthesize txtSearch;

@synthesize mainTableView;
@synthesize lblNoData;

@synthesize parentRolePopupContainerView;
@synthesize parentRolePopupBlackTransparentView;
@synthesize parentRolePopupInnerContainerView;
@synthesize lblParentRolePopupContainerViewTitle;
@synthesize lblParentRolePopupContainerViewInstructions;
@synthesize imageViewCloseParentRolePopupContainerView;
@synthesize btnCloseParentRolePopupContainerView;
@synthesize txtParentRole;
@synthesize lblParentRolePopupContainerViewInstructions2;
@synthesize txtAuthorityEmailAddress;
@synthesize btnRequestInParentRolePopupContainerView;

//========== OTHER VARIABLES ==========//

@synthesize keyboardDoneButtonView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllChildsByParentIdAndSearchKeywordEvent) name:@"gotAllChildsByParentIdAndSearchKeywordEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sentRequestToChildByParentIdEvent) name:@"sentRequestToChildByParentIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllChildsByParentIdAndSearchKeywordEvent
{
    self.dataRows = [MySingleton sharedManager].dataManager.arrayAllChildsByParentIdAndSearchKeyword;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

-(void)sentRequestToChildByParentIdEvent
{
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Add Child"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    keyboardDoneButtonView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *flex1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)];
    [doneButton1 setTitleTextAttributes:@{ NSFontAttributeName: [MySingleton sharedManager].themeFontTwentySizeRegular,NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex1,doneButton1, nil]];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *txtFieldFont, *lblNoDataFont, *lblParentRolePopupContainerViewTitleFont, *lblParentRolePopupContainerViewInstructionsFont, *btnRequestInParentRolePopupContainerViewFont;
    CGFloat parentRolePopupContainerViewCornerRadius, btnRequestInParentRolePopupContainerViewCornerRadiusValue;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        lblNoDataFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        
        lblParentRolePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
        lblParentRolePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnRequestInParentRolePopupContainerViewFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        
        parentRolePopupContainerViewCornerRadius = 15.0f;
        btnRequestInParentRolePopupContainerViewCornerRadiusValue = 8.0f;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            lblParentRolePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblParentRolePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnRequestInParentRolePopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            parentRolePopupContainerViewCornerRadius = 10.0f;
            btnRequestInParentRolePopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            lblParentRolePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblParentRolePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnRequestInParentRolePopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            parentRolePopupContainerViewCornerRadius = 10.0f;
            btnRequestInParentRolePopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            lblParentRolePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblParentRolePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnRequestInParentRolePopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            parentRolePopupContainerViewCornerRadius = 12.0f;
            btnRequestInParentRolePopupContainerViewCornerRadiusValue = 8.0f;
        }
        else
        {
            txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
            lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            
            lblParentRolePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblParentRolePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnRequestInParentRolePopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            parentRolePopupContainerViewCornerRadius = 15.0f;
            btnRequestInParentRolePopupContainerViewCornerRadiusValue = 8.0f;
        }
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    txtSearch.placeholder = @"Please enter your child's phone number";
    [txtSearch setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtSearch.tintColor = [MySingleton sharedManager].textfieldTextColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    [txtSearch setKeyboardType:UIKeyboardTypePhonePad];
    txtSearch.inputAccessoryView = keyboardDoneButtonView;
//    txtSearch.text = @"9726958278";

    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    mainTableView.hidden = true;
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentAddChildsTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentAddChildsTableViewCell_iPhone6" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentAddChildsTableViewCell_iPhone6Plus" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentAddChildsTableViewCell_iPad" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    parentRolePopupInnerContainerView.layer.masksToBounds = YES;
    parentRolePopupInnerContainerView.layer.cornerRadius = parentRolePopupContainerViewCornerRadius;
    parentRolePopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblParentRolePopupContainerViewTitle.font = lblParentRolePopupContainerViewTitleFont;
    lblParentRolePopupContainerViewTitle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    lblParentRolePopupContainerViewInstructions.font = lblParentRolePopupContainerViewInstructionsFont;
    lblParentRolePopupContainerViewInstructions.text = [NSString stringWithFormat:@"Please enter your relationship with this child in the textbox below"];
    lblParentRolePopupContainerViewInstructions.numberOfLines = 2;
    [lblParentRolePopupContainerViewInstructions setLineBreakMode:NSLineBreakByWordWrapping];
    lblParentRolePopupContainerViewInstructions.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    imageViewCloseParentRolePopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseParentRolePopupContainerView addTarget:self action:@selector(btnCloseParentRolePopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtParentRole.font = txtFieldFont;
    txtParentRole.delegate = self;
    [txtParentRole setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"_placeholderLabel.textColor"];
    txtParentRole.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtParentRole.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtParentRole setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    lblParentRolePopupContainerViewInstructions2.font = lblParentRolePopupContainerViewInstructionsFont;
    lblParentRolePopupContainerViewInstructions2.text = [NSString stringWithFormat:@"Please enter an authority email address for this child in the textbox below"];
    lblParentRolePopupContainerViewInstructions2.numberOfLines = 2;
    [lblParentRolePopupContainerViewInstructions2 setLineBreakMode:NSLineBreakByWordWrapping];
    lblParentRolePopupContainerViewInstructions2.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    txtAuthorityEmailAddress.font = txtFieldFont;
    txtAuthorityEmailAddress.delegate = self;
    [txtAuthorityEmailAddress setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                 forKeyPath:@"_placeholderLabel.textColor"];
    txtAuthorityEmailAddress.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtAuthorityEmailAddress.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtAuthorityEmailAddress setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtAuthorityEmailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    
    btnRequestInParentRolePopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnRequestInParentRolePopupContainerView.layer.masksToBounds = true;
    btnRequestInParentRolePopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnRequestInParentRolePopupContainerView.titleLabel.font = btnRequestInParentRolePopupContainerViewFont;
    [btnRequestInParentRolePopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnRequestInParentRolePopupContainerView addTarget:self action:@selector(btnRequestInParentRolePopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 200;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        ParentAddChildsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if(cell != nil)
        {
            NSArray *nib;
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                nib = [[NSBundle mainBundle]loadNibNamed:@"ParentAddChildsTableViewCell_iPad" owner:self options:nil];
            }
            else
            {
                if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentAddChildsTableViewCell" owner:self options:nil];
                }
                else if([MySingleton sharedManager].screenHeight == 667)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentAddChildsTableViewCell_iPhone6" owner:self options:nil];
                }
                else if([MySingleton sharedManager].screenHeight == 736)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentAddChildsTableViewCell_iPhone6Plus" owner:self options:nil];
                }
            }
            
            cell = [nib objectAtIndex:0];
        }
        
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
        
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        cell.imageViewChildProfilePicture.imageURL = [NSURL URLWithString:objChild.strProfilePictureImageUrl];
        cell.imageViewChildProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        cell.imageViewChildProfilePicture.layer.masksToBounds = YES;
        cell.imageViewChildProfilePicture.layer.cornerRadius = (cell.imageViewChildProfilePicture.frame.size.width/2);
        cell.imageViewChildProfilePicture.layer.borderWidth = 1.0f;
        cell.imageViewChildProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        
        cell.lblChildName.text = objChild.strName;
        cell.lblChildName.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
        cell.lblChildName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        cell.lblChildName.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildAge.text = [NSString stringWithFormat:@"%@ years old", objChild.strAge];
        cell.lblChildAge.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildAge.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildAge.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildGender.text = [NSString stringWithFormat:@"%@", objChild.strGender];
        cell.lblChildGender.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildGender.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildNationality.text = [NSString stringWithFormat:@"%@", objChild.strNationality];
        cell.lblChildNationality.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildNationality.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildNationality.textAlignment = NSTextAlignmentLeft;
        
        cell.lblChildHairColor.text = [NSString stringWithFormat:@"HAIR COLOR : %@", objChild.strHairColor];
        cell.lblChildHairColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildHairColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildHairColor.textAlignment = NSTextAlignmentLeft;
        
        NSString *strChildHairColor = [NSString stringWithFormat:@"HAIR COLOR : %@", objChild.strHairColor];
        NSMutableAttributedString *strAttrChildHairColor = [[NSMutableAttributedString alloc] initWithString:strChildHairColor attributes: nil];
        NSRange rangeOfHairColor = [strChildHairColor rangeOfString:@"HAIR COLOR :"];
        [strAttrChildHairColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHairColor];
        [strAttrChildHairColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHairColor];
        cell.lblChildHairColor.attributedText = strAttrChildHairColor;
        
        
        cell.lblChildEyeColor.text = [NSString stringWithFormat:@"EYE COLOR : %@", objChild.strEyeColor];
        cell.lblChildEyeColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildEyeColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildEyeColor.textAlignment = NSTextAlignmentRight;
        
        NSString *strChildEyeColor = [NSString stringWithFormat:@"EYE COLOR : %@", objChild.strEyeColor];
        NSMutableAttributedString *strAttrChildEyeColor = [[NSMutableAttributedString alloc] initWithString:strChildEyeColor attributes: nil];
        NSRange rangeOfEyeColor = [strChildEyeColor rangeOfString:@"EYE COLOR :"];
        [strAttrChildEyeColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfEyeColor];
        [strAttrChildEyeColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfEyeColor];
        cell.lblChildEyeColor.attributedText = strAttrChildEyeColor;
        
        
        cell.lblChildHeight.text = [NSString stringWithFormat:@"HEIGHT : %@", objChild.strHeight];
        cell.lblChildHeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildHeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildHeight.textAlignment = NSTextAlignmentLeft;
        
        NSString *strChildHeight = [NSString stringWithFormat:@"HEIGHT : %@", objChild.strHeight];
        NSMutableAttributedString *strAttrChildHeight = [[NSMutableAttributedString alloc] initWithString:strChildHeight attributes: nil];
        NSRange rangeOfHeight = [strChildHeight rangeOfString:@"HEIGHT :"];
        [strAttrChildHeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHeight];
        [strAttrChildHeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHeight];
        cell.lblChildHeight.attributedText = strAttrChildHeight;
        
        
        cell.lblChildWeight.text = [NSString stringWithFormat:@"WEIGHT : %@", objChild.strWeight];
        cell.lblChildWeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildWeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildWeight.textAlignment = NSTextAlignmentRight;
        
        NSString *strChildWeight = [NSString stringWithFormat:@"WEIGHT : %@", objChild.strWeight];
        NSMutableAttributedString *strAttrChildWeight = [[NSMutableAttributedString alloc] initWithString:strChildWeight attributes: nil];
        NSRange rangeOfWeight = [strChildWeight rangeOfString:@"WEIGHT :"];
        [strAttrChildWeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfWeight];
        [strAttrChildWeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfWeight];
        cell.lblChildWeight.attributedText = strAttrChildWeight;
        
        
        cell.lblChildSchoolName.text = [NSString stringWithFormat:@"SCHOOL : %@", objChild.strSchoolName];
        cell.lblChildSchoolName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        cell.lblChildSchoolName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        cell.lblChildSchoolName.textAlignment = NSTextAlignmentLeft;
        
        NSString *strChildSchool = [NSString stringWithFormat:@"SCHOOL : %@", objChild.strSchoolName];
        NSMutableAttributedString *strAttrChildSchool = [[NSMutableAttributedString alloc] initWithString:strChildSchool attributes: nil];
        NSRange rangeOfSchool = [strChildSchool rangeOfString:@"SCHOOL :"];
        [strAttrChildSchool addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfSchool];
        [strAttrChildSchool addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfSchool];
        cell.lblChildSchoolName.attributedText = strAttrChildSchool;
        
        if([objChild.strParentRequest isEqualToString:@"Pending"])
        {
            cell.addContainer.layer.masksToBounds = true;
            cell.addContainer.layer.cornerRadius = 3.0f;
            cell.addContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalLightGreyColor.CGColor;
            cell.addContainer.layer.borderWidth = 1.0f;
            
            cell.lblAdd.font = [MySingleton sharedManager].themeFontTenSizeBold;
            cell.lblAdd.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
            cell.lblAdd.textAlignment = NSTextAlignmentCenter;
            cell.lblAdd.layer.masksToBounds = YES;
            cell.lblAdd.text = @"Request Sent";
            
            cell.imageViewAdd.hidden = true;
            cell.btnAdd.userInteractionEnabled = false;
            
            CGRect lblAddFrame = cell.lblAdd.frame;
            lblAddFrame.size.width = (cell.addContainer.frame.size.width - (cell.addContainer.frame.origin.x *2));
            cell.lblAdd.frame = lblAddFrame;
        }
        else if([objChild.strParentRequest isEqualToString:@"Accepted"])
        {
            cell.addContainer.layer.masksToBounds = true;
            cell.addContainer.layer.cornerRadius = 3.0f;
            cell.addContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalLightGreyColor.CGColor;
            cell.addContainer.layer.borderWidth = 1.0f;
            
            cell.lblAdd.font = [MySingleton sharedManager].themeFontTenSizeBold;
            cell.lblAdd.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
            cell.lblAdd.textAlignment = NSTextAlignmentCenter;
            cell.lblAdd.layer.masksToBounds = YES;
            cell.lblAdd.text = @"Already Added";
            
            cell.imageViewAdd.hidden = true;
            cell.btnAdd.userInteractionEnabled = false;
            
            CGRect lblAddFrame = cell.lblAdd.frame;
            lblAddFrame.size.width = (cell.addContainer.frame.size.width - (cell.addContainer.frame.origin.x *2));
            cell.lblAdd.frame = lblAddFrame;
        }
        else
        {
            cell.addContainer.layer.masksToBounds = true;
            cell.addContainer.layer.cornerRadius = 3.0f;
            cell.addContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
            cell.addContainer.layer.borderWidth = 1.0f;
            
            cell.lblAdd.font = [MySingleton sharedManager].themeFontTenSizeBold;
            cell.lblAdd.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblAdd.textAlignment = NSTextAlignmentCenter;
            cell.lblAdd.layer.masksToBounds = YES;
            
            cell.imageViewAdd.contentMode = UIViewContentModeScaleAspectFit;
            cell.imageViewAdd.layer.masksToBounds = YES;
            
            cell.btnAdd.tag = indexPath.row;
            [cell.btnAdd addTarget:self action:@selector(btnAddClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == mainTableView)
    {
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
        
        if(txtSearch.text.length > 0)
        {
            NSString *strSearchKeyword = txtSearch.text;
            
            [textField resignFirstResponder];
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
            [[MySingleton sharedManager].dataManager getAllChildsByParentId:strParentId withSearchKeyword:strSearchKeyword];
        }
        else
        {
            [textField resignFirstResponder];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter mobile number of your child to search"];
            });
        }
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(txtSearch.text.length > 0)
    {
        NSString *strSearchKeyword = txtSearch.text;
        
        [txtSearch resignFirstResponder];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [[MySingleton sharedManager].dataManager getAllChildsByParentId:strParentId withSearchKeyword:strSearchKeyword];
    }
    else
    {
        [txtSearch resignFirstResponder];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter mobile number of your child to search"];
        });
    }
}


-(void)showParentRolePopupContainerView
{
    [self.view endEditing:YES];
    
    parentRolePopupContainerView.hidden = false;
}

- (IBAction)btnCloseParentRolePopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    txtParentRole.text = @"";
    parentRolePopupContainerView.hidden = TRUE;
}

-(void)btnRequestInParentRolePopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if(txtParentRole.text.length > 0 && (txtAuthorityEmailAddress.text.length > 0 && [objUtility isValidEmailAddress:self.txtAuthorityEmailAddress.text]))
    {
        parentRolePopupContainerView.hidden = true;
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [[MySingleton sharedManager].dataManager sendRequestToChildByParentId:strParentId withChildId:self.objSelectedChild.strChildID withParentRole:txtParentRole.text withAuthorityEmail:txtAuthorityEmailAddress.text];
        
        txtParentRole.text = @"";
        txtAuthorityEmailAddress.text = @"";
    }
    else
    {
        if(txtParentRole.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter your relationship with this child."];
            });
        }
        else if(txtAuthorityEmailAddress.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter an authority email address for this child."];
            });
        }
        else if(![objUtility isValidEmailAddress:txtAuthorityEmailAddress.text])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter a valid authority email address for this child."];
            });
        }
    }
}

-(IBAction)btnAddClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *btnSender = (UIButton *)sender;
    self.objSelectedChild = [self.dataRows objectAtIndex:btnSender.tag];
    
    [self showParentRolePopupContainerView];
}

-(IBAction)btnSettingsClicked:(id)sender
{
    [self.view endEditing:YES];
}

@end
