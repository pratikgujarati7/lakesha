//
//  ChildHomeViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 05/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ChildHomeViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentListTableViewCell.h"

#import "ChildGenerateCommunityAlertViewController.h"
#import "CommonAllNotificationsViewController.h"
#import "CommonAllBoycottAlertsViewController.h"

@interface ChildHomeViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ChildHomeViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize imageViewTopBackground;

@synthesize parentRespondAlertContainerView;
@synthesize imageViewParentRespondAlert;
@synthesize lblParentRespondAlert;
@synthesize btnParentRespondAlert;

@synthesize communityRespondAlertContainerView;
@synthesize imageViewCommunityRespondAlert;
@synthesize lblCommunityRespondAlert;
@synthesize btnCommunityRespondAlert;

@synthesize nameContainerView;
@synthesize lblName;
@synthesize mainTableView;
@synthesize lblNoData;

@synthesize bottomContainerView;

@synthesize homeContainerView;
@synthesize lblHome;
@synthesize imageViewHome;
@synthesize btnHome;

@synthesize notificationContainerView;
@synthesize lblNotification;
@synthesize imageViewNotification;
@synthesize btnNotification;

@synthesize boycottAlertsContainerView;
@synthesize lblBoycottAlerts;
@synthesize imageViewBoycottAlerts;
@synthesize btnBoycottAlerts;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        parentRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.height/2;
        communityRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.height/2;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            parentRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.width/2;
            communityRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.width/2;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            parentRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.width/2;
            communityRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.width/2;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            parentRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.width/2;
            communityRespondAlertContainerView.layer.cornerRadius = parentRespondAlertContainerView.frame.size.width/2;
        }
    }
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllParentsByChildIdEvent) name:@"gotAllParentsByChildIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(generatedParentRespondByChildIdEvent) name:@"generatedParentRespondByChildIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllParentsByChildIdEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(![[prefs objectForKey:@"isAppOpenedByChildForFirstTime"] isEqualToString:@"1"])
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Update";
        alertViewController.message = @"Please update your information school state, school city and school zipcode to use new features of this applicaton.";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = NO;
        alertViewController.swipeDismissalGestureEnabled = NO;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
            [prefs setObject:@"1" forKey:@"isAppOpenedByChildForFirstTime"];
            [prefs synchronize];
            
            self.dataRows = [MySingleton sharedManager].dataManager.arrayParentsByChildId;
            
            if(self.dataRows.count <= 0)
            {
                mainTableView.hidden = true;
                lblNoData.hidden = false;
                
                [appDelegate showErrorAlertViewWithTitle:@"No Parents Found" withDetails:@"You do not have any parent(s) on the app. Please ask your parent(s) to join LaKesha and add you as their child."];
            }
            else
            {
                mainTableView.hidden = false;
                lblNoData.hidden = true;
                [mainTableView reloadData];
            }
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertViewController animated:YES completion:nil];
        });
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayParentsByChildId;
        
        if(self.dataRows.count <= 0)
        {
            mainTableView.hidden = true;
            lblNoData.hidden = false;
            
            [appDelegate showErrorAlertViewWithTitle:@"No Parents Found" withDetails:@"You do not have any parent(s) on the app. Please ask your parent(s) to join LaKesha and add you as their child."];
        }
        else
        {
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
    }
}

-(void)generatedParentRespondByChildIdEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Alert Generated";
    alertViewController.message = @"Alert has been generated and your parent(s) have been notified about your alert.";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
//        NSString *phoneNumber = [@"tel://" stringByAppendingString:@"911"];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Home"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    mainTableView.hidden = true;
    
    UIFont *lblNoDataFont, *lblNameFont, *lblParentRespondAlertFont, *lblBottomBarFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        lblNameFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblParentRespondAlertFont = [MySingleton sharedManager].themeFontTwentySizeBold;
        lblBottomBarFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblNameFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblParentRespondAlertFont = [MySingleton sharedManager].themeFontTenSizeBold;
            lblBottomBarFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblNameFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblParentRespondAlertFont = [MySingleton sharedManager].themeFontTenSizeBold;
            lblBottomBarFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            lblNameFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblParentRespondAlertFont = [MySingleton sharedManager].themeFontTenSizeBold;
            lblBottomBarFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblNameFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblParentRespondAlertFont = [MySingleton sharedManager].themeFontTenSizeBold;
            lblBottomBarFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    imageViewTopBackground.layer.masksToBounds = YES;
    
    parentRespondAlertContainerView.layer.masksToBounds = YES;
    parentRespondAlertContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    imageViewParentRespondAlert.layer.masksToBounds = YES;
    
    lblParentRespondAlert.font = lblParentRespondAlertFont;
    lblParentRespondAlert.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnParentRespondAlert addTarget:self action:@selector(btnParentRespondAlertClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    communityRespondAlertContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    communityRespondAlertContainerView.layer.masksToBounds = YES;
    
    imageViewCommunityRespondAlert.layer.masksToBounds = YES;
    
    lblCommunityRespondAlert.font = lblParentRespondAlertFont;
    lblCommunityRespondAlert.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnCommunityRespondAlert addTarget:self action:@selector(btnCommunityRespondAlertClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    nameContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    lblName.font = lblNameFont;
    lblName.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblName.text = [NSString stringWithFormat:@"%@", [prefs objectForKey:@"childname"]];
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    bottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblHome.font = lblBottomBarFont;
    lblHome.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewHome.layer.masksToBounds = true;
    
    [btnHome addTarget:self action:@selector(btnHomeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNotification.font = lblBottomBarFont;
    lblNotification.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewNotification.layer.masksToBounds = true;
    
    [btnNotification addTarget:self action:@selector(btnNotificationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblBoycottAlerts.font = lblBottomBarFont;
    lblBoycottAlerts.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewBoycottAlerts.layer.masksToBounds = true;
    
    [btnBoycottAlerts addTarget:self action:@selector(btnBoycottAlertsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
    [[MySingleton sharedManager].dataManager getAllParentsByChildId:strChildId];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 80;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        ParentListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[ParentListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        Parent *objParent = [self.dataRows objectAtIndex:indexPath.row];
        
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        cell.imageViewParentProfilePicture.imageURL = [NSURL URLWithString:objParent.strProfilePictureImageUrl];
        
        cell.lblParentName.text = objParent.strName;
        cell.lblParentRole.text = objParent.strParentRole;
        cell.lblParentWorkPlaceName.text = [NSString stringWithFormat:@"Works at %@", objParent.strWorkPlaceName];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == mainTableView)
    {
        NSLog(@"didSelectRowAtIndexPath called.");
    }
}

#pragma mark - Other Methods

-(IBAction)btnParentRespondAlertClicked:(id)sender
{
//    [self.view endEditing:true];
//    
//    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
//    alertViewController.title = @"";
//    alertViewController.message = @"Are you sure you want to generate a parent respond alert?";
//    
//    alertViewController.view.tintColor = [UIColor whiteColor];
//    alertViewController.backgroundTapDismissalGestureEnabled = YES;
//    alertViewController.swipeDismissalGestureEnabled = YES;
//    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
//    
//    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
//    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
//    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
//    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
//    
//    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
//        
//        [alertViewController dismissViewControllerAnimated:YES completion:nil];
    
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [[MySingleton sharedManager].dataManager generateParentRespondByChildId:strChildId];
//    }]];
//    
//    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
//        
//        [alertViewController dismissViewControllerAnimated:YES completion:nil];
//        
//    }]];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self presentViewController:alertViewController animated:YES completion:nil];
//    });
}

-(IBAction)btnCommunityRespondAlertClicked:(id)sender
{
    [self.view endEditing:true];
    
    ChildGenerateCommunityAlertViewController *viewController = [[ChildGenerateCommunityAlertViewController alloc] init];
    
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:NO];
}

-(IBAction)btnHomeClicked:(id)sender
{
    [self.view endEditing:YES];
    
    ChildHomeViewController *viewController = [[ChildHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:NO];
}

-(IBAction)btnNotificationClicked:(id)sender
{
    [self.view endEditing:YES];
    
    CommonAllNotificationsViewController *commonAllNotificationsViewController = [[CommonAllNotificationsViewController alloc]init];
    commonAllNotificationsViewController.strLoadedFor = @"0";
    [self.navigationController pushViewController:commonAllNotificationsViewController animated:true];
}

-(IBAction)btnBoycottAlertsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    CommonAllBoycottAlertsViewController *commonAllBoycottAlertsViewController = [[CommonAllBoycottAlertsViewController alloc]init];
    commonAllBoycottAlertsViewController.strLoadedFor = @"0";
    [self.navigationController pushViewController:commonAllBoycottAlertsViewController animated:true];
}

@end
