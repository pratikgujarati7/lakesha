//
//  RegisterParentViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 05/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "RegisterParentViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

//#import "HomeViewController.h"
#import "CommonVerifyPhoneNumberViewController.h"

@interface RegisterParentViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation RegisterParentViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize lblPassword;
@synthesize txtPassword;
@synthesize txtPasswordBottomSeparatorView;

@synthesize lblName;
@synthesize txtName;
@synthesize txtNameBottomSeparatorView;

@synthesize lblDoYouWantToAddAddressContainerView;
@synthesize lblDoYouWantToAddAddress;

@synthesize imageViewDoNotAddAddress;
@synthesize lblDoNotAddAddress;
@synthesize btnDoNotAddAddress;

@synthesize imageViewAddAddress;
@synthesize lblAddAddress;
@synthesize btnAddAddress;

@synthesize addressContainerView;

@synthesize lblStreetAddress;
@synthesize txtViewStreetAddress;
@synthesize txtViewStreetAddressBottomSeparatorView;

@synthesize compulsoryAddressContainerView;

@synthesize lblState;
@synthesize txtState;
@synthesize txtStateBottomSeparatorView;
@synthesize imageViewStateDropDownArrow;

@synthesize lblCity;
@synthesize txtCity;
@synthesize txtCityBottomSeparatorView;
@synthesize imageViewCityDropDownArrow;

@synthesize lblZipCode;
@synthesize txtZipCode;
@synthesize txtZipCodeBottomSeparatorView;

@synthesize bottomInformationContainerView;

@synthesize lblPhoneNumber;
@synthesize txtPhoneNumber;
@synthesize txtPhoneNumberBottomSeparatorView;

@synthesize lblSecondaryPhoneNumber;
@synthesize txtSecondaryPhoneNumber;
@synthesize txtSecondaryPhoneNumberBottomSeparatorView;

@synthesize lblGender;
@synthesize txtGender;
@synthesize txtGenderBottomSeparatorView;
@synthesize imageViewGenderDropDownArrow;

@synthesize lblWorkPlaceInformation;

@synthesize lblWorkPlaceName;
@synthesize txtWorkPlaceName;
@synthesize txtWorkPlaceNameBottomSeparatorView;

@synthesize lblWorkPlacePhoneNumber;
@synthesize txtWorkPlacePhoneNumber;
@synthesize txtWorkPlacePhoneNumberBottomSeparatorView;

@synthesize btnRegister;

//========== OTHER VARIABLES ==========//

@synthesize keyboardDoneButtonView;

@synthesize genderPickerView;

@synthesize statePickerView;

@synthesize cityPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = addressContainerView.frame.origin.y;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    addressContainerView.hidden = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllStatesEvent) name:@"gotAllStatesEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parentSignedUpEvent) name:@"parentSignedUpEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllStatesEvent
{
    if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
    {
        txtState.userInteractionEnabled = true;
    }
    else
    {
        txtState.userInteractionEnabled = false;
    }
}

-(void)parentSignedUpEvent
{
//    [self navigateToParentHomeViewController];
    [self navigateToCommonVerifyPhoneNumberViewController];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = true;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Parent Registration"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

- (IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    keyboardDoneButtonView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *flex1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)];
    [doneButton1 setTitleTextAttributes:@{ NSFontAttributeName: [MySingleton sharedManager].themeFontTwentySizeRegular,NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex1,doneButton1, nil]];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    self.arrayGender = [[NSMutableArray alloc]initWithObjects:@"Male",@"Female",nil];
    
    UIFont *lblFont, *txtFieldFont, *lblHeaderFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblHeaderFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    self.boolIsAddAddressSelected = false;
    
    lblEmail.font = lblFont;
    lblEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmail WithBottomSeparatorView:txtEmailBottomSeparatorView];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    lblPassword.font = lblFont;
    lblPassword.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPassword WithBottomSeparatorView:txtPasswordBottomSeparatorView];
    txtPassword.secureTextEntry = true;
    
    lblName.font = lblFont;
    lblName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtName WithBottomSeparatorView:txtNameBottomSeparatorView];
    
    lblDoYouWantToAddAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblDoYouWantToAddAddress.font = lblHeaderFont;
    
    imageViewDoNotAddAddress.layer.masksToBounds = true;
    imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
    
    lblDoNotAddAddress.font = lblFont;
    lblDoNotAddAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnDoNotAddAddress addTarget:self action:@selector(btnDoNotAddAddressClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewAddAddress.layer.masksToBounds = true;
    imageViewAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    lblAddAddress.font = lblFont;
    lblAddAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnAddAddress addTarget:self action:@selector(btnAddAddressClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtViewStreetAddress.text = @"Street Address";
    txtViewStreetAddress.font = txtFieldFont;
    txtViewStreetAddress.delegate = self;
    txtViewStreetAddress.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewStreetAddress.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewStreetAddress setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    txtViewStreetAddressBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    statePickerView = [[UIPickerView alloc] init];
    statePickerView.delegate = self;
    statePickerView.dataSource = self;
    statePickerView.showsSelectionIndicator = YES;
    statePickerView.tag = 2;
    statePickerView.backgroundColor = [UIColor whiteColor];
    
    lblState.font = lblFont;
    lblState.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtState WithBottomSeparatorView:txtStateBottomSeparatorView];
    [txtState setInputView:statePickerView];
    imageViewStateDropDownArrow.layer.masksToBounds = true;
    
    cityPickerView = [[UIPickerView alloc] init];
    cityPickerView.delegate = self;
    cityPickerView.dataSource = self;
    cityPickerView.showsSelectionIndicator = YES;
    cityPickerView.tag = 3;
    cityPickerView.backgroundColor = [UIColor whiteColor];
    
    lblCity.font = lblFont;
    lblCity.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtCity WithBottomSeparatorView:txtCityBottomSeparatorView];
    [txtCity setInputView:cityPickerView];
    txtCity.userInteractionEnabled = false;
    imageViewCityDropDownArrow.layer.masksToBounds = true;
    
    lblZipCode.font = lblFont;
    lblZipCode.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtZipCode WithBottomSeparatorView:txtZipCodeBottomSeparatorView];
    
    lblStreetAddress.font = lblFont;
    lblStreetAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblPhoneNumber.font = lblFont;
    lblPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPhoneNumber WithBottomSeparatorView:txtPhoneNumberBottomSeparatorView];
    [txtPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
//    txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblSecondaryPhoneNumber.font = lblFont;
    lblSecondaryPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSecondaryPhoneNumber WithBottomSeparatorView:txtSecondaryPhoneNumberBottomSeparatorView];
    [txtSecondaryPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
//    txtSecondaryPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    genderPickerView = [[UIPickerView alloc] init];
    genderPickerView.delegate = self;
    genderPickerView.dataSource = self;
    genderPickerView.showsSelectionIndicator = YES;
    genderPickerView.tag = 1;
    genderPickerView.backgroundColor = [UIColor whiteColor];
    
    lblGender.font = lblFont;
    lblGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtGender WithBottomSeparatorView:txtGenderBottomSeparatorView];
    [txtGender setInputView:genderPickerView];
    imageViewGenderDropDownArrow.layer.masksToBounds = true;
    
    lblWorkPlaceInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblWorkPlaceInformation.font = lblHeaderFont;
    
    lblWorkPlaceName.font = lblFont;
    lblWorkPlaceName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtWorkPlaceName WithBottomSeparatorView:txtWorkPlaceNameBottomSeparatorView];
    
    lblWorkPlacePhoneNumber.font = lblFont;
    lblWorkPlacePhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtWorkPlacePhoneNumber WithBottomSeparatorView:txtWorkPlacePhoneNumberBottomSeparatorView];
    [txtWorkPlacePhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
//    txtWorkPlacePhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    btnRegister.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnRegister.layer.masksToBounds = true;
    btnRegister.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnRegister.titleLabel.font = btnFont;
    [btnRegister setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnRegister addTarget:self action:@selector(btnRegisterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [[MySingleton sharedManager].dataManager getAllStates];
    
    //========== PRATIK GUJARATI TEMP DATA ==========//
//    txtEmail.text = @"abhi.jariwala@gmail.com";
//    txtPassword.text = @"123456";
//    txtName.text = @"abc";
//    txtZipCode.text = @"100112";
//    txtViewStreetAddress.text = @"USA";
//    txtViewStreetAddress.textColor = [MySingleton sharedManager].textfieldTextColor;
//    txtPhoneNumber.text = @"+13344448468";
//    txtSecondaryPhoneNumber.text = @"1231231232";
//    txtGender.text = @"Male";
//    txtWorkPlaceName.text = @"Apple";
//    txtWorkPlacePhoneNumber.text = @"1231231233";
    //========== PRATIK GUJARATI TEMP DATA ==========//
}

- (void)setupTextfield:(UITextField *)textField WithBottomSeparatorView:(UIView *)textFieldBottomSeparatorView
{
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    textField.font = txtFieldFont;
    textField.delegate = self;
    [textField setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [MySingleton sharedManager].textfieldTextColor;
    textField.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    textFieldBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtGender)
    {
        if(txtGender.text.length == 0 )
        {
            txtGender.text = [self.arrayGender objectAtIndex:0];
            [genderPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
    else if(textField == txtState)
    {
        if(txtState.text.length == 0 )
        {
            if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
            {
                self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:0];
                txtState.text = self.objSelectedState.strStateName;
                txtCity.userInteractionEnabled = true;
                [statePickerView selectRow:0 inComponent:0 animated:YES];
                [cityPickerView reloadAllComponents];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No States Added" withDetails:@"Currently we don't have any states added."];
            }
        }
    }
    else if(textField == txtCity)
    {
        if(txtCity.text.length == 0 )
        {
            if(self.objSelectedState.arrayCity.count > 0)
            {
                self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:0];
                txtCity.text = self.objSelectedCity.strCityName;
                [cityPickerView selectRow:0 inComponent:0 animated:YES];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewStreetAddress)
    {
        if ([textView.text isEqualToString:@"Street Address"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewStreetAddress)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Street Address";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayGender count];
    }
    else if(pickerView.tag == 2)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.arrayStates count];
    }
    else if(pickerView.tag == 3)
    {
        rowsInComponent = [self.objSelectedState.arrayCity count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == genderPickerView)
    {
        lblMain.text = self.arrayGender[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == statePickerView)
    {
        State *objState = [MySingleton sharedManager].dataManager.arrayStates[row];
        lblMain.text = objState.strStateName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == cityPickerView)
    {
        City *objCity = self.objSelectedState.arrayCity[row];
        lblMain.text = objCity.strCityName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtGender.text = [self.arrayGender objectAtIndex:row];
    }
    else if(pickerView.tag == 2)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:row];
        txtState.text = self.objSelectedState.strStateName;
        [cityPickerView reloadAllComponents];
        
        if(self.objSelectedState.arrayCity.count > 0)
        {
            [cityPickerView selectRow:0 inComponent:0 animated:NO];
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
        }
        else
        {
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
            
            [self.view endEditing:true];
            [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
        }
    }
    else if(pickerView.tag == 3)
    {
        self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:row];
        txtCity.text = self.objSelectedCity.strCityName;
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnAddAddressClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddAddressSelected = true;
    
    imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
    imageViewAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    addressContainerView.hidden = false;
    
    float floatMargin = lblPassword.frame.origin.y - txtEmailBottomSeparatorView.frame.origin.y;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = addressContainerView.frame.origin.y + addressContainerView.frame.size.height;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

- (IBAction)btnDoNotAddAddressClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddAddressSelected = false;
    
    imageViewDoNotAddAddress.image = [UIImage imageNamed:@"radio_selected.png"];
    imageViewAddAddress.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = addressContainerView.frame.origin.y;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    addressContainerView.hidden = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

-(void)bindDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInParent == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent = [[Parent alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strPassword = txtPassword.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strName = txtName.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.boolIsAddAddressSelected = self.boolIsAddAddressSelected;
    
    if(self.boolIsAddAddressSelected)
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strStreetAddress = txtViewStreetAddress.text;
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strStreetAddress = @"";
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.strStateId = self.objSelectedState.strStateID;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strStateName = txtState.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strCityId = self.objSelectedCity.strCityID;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strCityName = txtCity.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strZipcode = txtZipCode.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.strPhoneNumber = txtPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strSecondaryPhoneNumber = txtSecondaryPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strGender = txtGender.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strWorkPlaceName = txtWorkPlaceName.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strWorkPlacePhoneNumber = txtWorkPlacePhoneNumber.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strDeviceToken = [prefs objectForKey:@"deviceToken"];
    if(strDeviceToken.length > 0)
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strDeviceToken = @"";
    }
}

- (IBAction)btnRegisterClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self bindDataToObject];
    
    if([[MySingleton sharedManager].dataManager.objLoggedInParent isValidateParentForRegistration])
    {
        [[MySingleton sharedManager].dataManager parentSignUp:[MySingleton sharedManager].dataManager.objLoggedInParent];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInParent.strValidationMessage];
        });
    }
}

- (void)navigateToParentHomeViewController
{
    [self.view endEditing:true];
}

-(void)navigateToCommonVerifyPhoneNumberViewController
{
    [self.view endEditing:YES];
    
    CommonVerifyPhoneNumberViewController *viewController = [[CommonVerifyPhoneNumberViewController alloc] init];
    
    //PARENT
    
    viewController.strLoadedFor = @"1";
    viewController.strPhoneNumber = [MySingleton sharedManager].dataManager.objLoggedInParent.strPhoneNumber;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
