//
//  CommonAboutUsViewController.h
//  LaKesha
//
//  Created by INNOVATIVE ITERATION 4 on 12/10/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MySingleton.h"

#import <MessageUI/MessageUI.h>

@interface CommonAboutUsViewController : UIViewController <UIScrollViewDelegate, MFMailComposeViewControllerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewDataDogs;

@property (nonatomic,retain) IBOutlet UILabel *lblStaticTagLine;

@property (nonatomic,retain) IBOutlet UIButton *btnEmail;
@property (nonatomic,retain) IBOutlet UIButton *btnGmail;

@property (nonatomic,retain) IBOutlet UIButton *btnPhoneNumber;

@property (nonatomic,retain) IBOutlet UIImageView *imageView5000Elite;
@property (nonatomic,retain) IBOutlet UIButton *btn5000Elite;

@property (nonatomic,retain) IBOutlet UIView *bottomContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewTwitter;
@property (nonatomic,retain) IBOutlet UIButton *btnTwitter;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewYoutube;
@property (nonatomic,retain) IBOutlet UIButton *btnYoutube;

//========== OTHER VARIABLES ==========//

@end
