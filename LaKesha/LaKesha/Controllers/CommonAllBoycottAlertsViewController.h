//
//  CommonAllBoycottAlertsViewController.h
//  LaKesha
//
//  Created by INNOVATIVE ITERATION 4 on 09/10/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MySingleton.h"

@interface CommonAllBoycottAlertsViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic, retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;

@property (nonatomic,retain) IBOutlet UIView *bottomContainerView;

@property (nonatomic,retain) IBOutlet UIView *notificationContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblNotification;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewNotification;
@property (nonatomic,retain) IBOutlet UIButton *btnNotification;

@property (nonatomic,retain) IBOutlet UIView *boycottAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblBoycottAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBoycottAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnBoycottAlerts;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;

@property (nonatomic,retain) NSString *strLoadedFor;

@end
