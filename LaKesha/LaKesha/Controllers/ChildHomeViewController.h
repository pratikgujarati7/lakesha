//
//  ChildHomeViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 05/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface ChildHomeViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewTopBackground;

@property (nonatomic,retain) IBOutlet UIView *parentRespondAlertContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewParentRespondAlert;
@property (nonatomic,retain) IBOutlet UILabel *lblParentRespondAlert;
@property (nonatomic,retain) IBOutlet UIButton *btnParentRespondAlert;

@property (nonatomic,retain) IBOutlet UIView *communityRespondAlertContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCommunityRespondAlert;
@property (nonatomic,retain) IBOutlet UILabel *lblCommunityRespondAlert;
@property (nonatomic,retain) IBOutlet UIButton *btnCommunityRespondAlert;

@property (nonatomic,retain) IBOutlet UIView *nameContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblName;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;

@property (nonatomic,retain) IBOutlet UIView *bottomContainerView;

@property (nonatomic,retain) IBOutlet UIView *homeContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblHome;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewHome;
@property (nonatomic,retain) IBOutlet UIButton *btnHome;

@property (nonatomic,retain) IBOutlet UIView *notificationContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblNotification;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewNotification;
@property (nonatomic,retain) IBOutlet UIButton *btnNotification;

@property (nonatomic,retain) IBOutlet UIView *boycottAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblBoycottAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBoycottAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnBoycottAlerts;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;

@end
