//
//  CommonNearByAlertsViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 18/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CommonNearByAlertsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentMyChildsAlertsTableViewCell.h"
#import "ParentMyChildsCommunityAlertsTableViewCell.h"

#import "ParentMyChildsAlertDetailsViewController.h"
#import "CommonCommunityChildsAlertDetailsViewController.h"

@interface CommonNearByAlertsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation CommonNearByAlertsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainTableView;
@synthesize lblNoData;

@synthesize bottomContainerView;

@synthesize moreParentRespondAlertsContainerView;
@synthesize lblMoreParentRespondAlerts;
@synthesize imageViewMoreParentRespondAlerts;
@synthesize btnMoreParentRespondAlerts;

@synthesize moreCommunityRespondAlertsContainerView;
@synthesize lblMoreCommunityRespondAlerts;
@synthesize imageViewMoreCommunityRespondAlerts;
@synthesize btnMoreCommunityRespondAlerts;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllNearByAlertsByUserIdEvent) name:@"gotAllNearByAlertsByUserIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllNearByAlertsByUserIdEvent
{
    if([self.strLoadedFor isEqualToString:@"1"])
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByParentId;
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByParentId;
        }
    }
    else if([self.strLoadedFor isEqualToString:@"0"])
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByChildId;
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByChildId;
        }
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByGuestLocation;
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByGuestLocation;
        }
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Near By Alerts"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        }
        else
        {
            txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        }
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"_placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    if([self.strLoadedFor isEqualToString:@"1"])
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByParentId;
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByParentId;
        }
    }
    else if([self.strLoadedFor isEqualToString:@"0"])
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByChildId;
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByChildId;
        }
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByGuestLocation;
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByGuestLocation;
        }
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblNoDataFont, *lblMoreParentRespondAlertsFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        lblMoreParentRespondAlertsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblMoreParentRespondAlertsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblMoreParentRespondAlertsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            lblMoreParentRespondAlertsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else
        {
            lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblMoreParentRespondAlertsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
    }
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    mainTableView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    mainTableView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    mainTableView.hidden = true;
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsAlertsTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsAlertsTableViewCell_iPhone6" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsAlertsTableViewCell_iPhone6Plus" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsAlertsTableViewCell_iPad" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsCommunityAlertsTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsCommunityAlertsTableViewCell_iPhone6" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsCommunityAlertsTableViewCell_iPhone6Plus" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ParentMyChildsCommunityAlertsTableViewCell_iPad" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    moreParentRespondAlertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalBlueColor;
    
    lblMoreParentRespondAlerts.font = lblMoreParentRespondAlertsFont;
    lblMoreParentRespondAlerts.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewMoreParentRespondAlerts.layer.masksToBounds = true;
    
    [btnMoreParentRespondAlerts addTarget:self action:@selector(btnMoreParentRespondAlertsClicked) forControlEvents:UIControlEventTouchUpInside];
    
    moreCommunityRespondAlertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    
    lblMoreCommunityRespondAlerts.font = lblMoreParentRespondAlertsFont;
    lblMoreCommunityRespondAlerts.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    imageViewMoreCommunityRespondAlerts.layer.masksToBounds = true;
    
    [btnMoreCommunityRespondAlerts addTarget:self action:@selector(btnMoreCommunityRespondAlertsClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.strSelectedTab = @"1";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if([self.strLoadedFor isEqualToString:@"1"])
    {
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [[MySingleton sharedManager].dataManager getAllNearByAlertsByUserId:strParentId withUserType:@"1"];
    }
    else if([self.strLoadedFor isEqualToString:@"0"])
    {
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [[MySingleton sharedManager].dataManager getAllNearByAlertsByUserId:strChildId withUserType:@"0"];
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        NSString *strGuestId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"guestid"]];
        [[MySingleton sharedManager].dataManager getAllNearByAlertsByUserId:strGuestId withUserType:@"2"];
    }
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 230;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            ParentMyChildsAlertsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
            
            if(cell != nil)
            {
                NSArray *nib;
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsAlertsTableViewCell_iPad" owner:self options:nil];
                }
                else
                {
                    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
                    {
                        nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsAlertsTableViewCell" owner:self options:nil];
                    }
                    else if([MySingleton sharedManager].screenHeight == 667)
                    {
                        nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsAlertsTableViewCell_iPhone6" owner:self options:nil];
                    }
                    else if([MySingleton sharedManager].screenHeight == 736)
                    {
                        nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsAlertsTableViewCell_iPhone6Plus" owner:self options:nil];
                    }
                }
                
                cell = [nib objectAtIndex:0];
            }
            
            ChildAlert *objChildAlert = [self.dataRows objectAtIndex:indexPath.row];
            
            [[AsyncImageLoader sharedLoader].cache removeAllObjects];
            cell.imageViewChildProfilePicture.imageURL = [NSURL URLWithString:objChildAlert.strProfilePictureImageUrl];
            cell.imageViewChildProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
            cell.imageViewChildProfilePicture.layer.masksToBounds = YES;
            cell.imageViewChildProfilePicture.layer.cornerRadius = (cell.imageViewChildProfilePicture.frame.size.width/2);
            cell.imageViewChildProfilePicture.layer.borderWidth = 1.0f;
            cell.imageViewChildProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
            
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForName])
            {
                cell.lblChildName.text = objChildAlert.strName;
            }
            else
            {
                cell.lblChildName.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildName.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
            cell.lblChildName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
            cell.lblChildName.textAlignment = NSTextAlignmentLeft;
            
            
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForAge])
            {
                cell.lblChildAge.text = [NSString stringWithFormat:@"%@ years old", objChildAlert.strAge];
            }
            else
            {
                cell.lblChildAge.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildAge.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildAge.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildAge.textAlignment = NSTextAlignmentLeft;
            
            
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForGender])
            {
                cell.lblChildGender.text = [NSString stringWithFormat:@"%@", objChildAlert.strGender];
            }
            else
            {
                cell.lblChildGender.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildGender.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildGender.textAlignment = NSTextAlignmentLeft;
            
            
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForNationality])
            {
                cell.lblChildNationality.text = [NSString stringWithFormat:@"%@", objChildAlert.strNationality];
            }
            else
            {
                cell.lblChildNationality.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildNationality.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildNationality.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildNationality.textAlignment = NSTextAlignmentLeft;
            
            
            cell.lblChildHairColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildHairColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildHairColor.textAlignment = NSTextAlignmentLeft;
            NSString *strChildHairColor;
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHairColor])
            {
                cell.lblChildHairColor.text = [NSString stringWithFormat:@"HAIR COLOR : %@", objChildAlert.strHairColor];
                strChildHairColor = [NSString stringWithFormat:@"HAIR COLOR : %@", objChildAlert.strHairColor];
            }
            else
            {
                cell.lblChildHairColor.text = [NSString stringWithFormat:@"HAIR COLOR : -"];
                strChildHairColor = [NSString stringWithFormat:@"HAIR COLOR : -"];
            }
            NSMutableAttributedString *strAttrChildHairColor = [[NSMutableAttributedString alloc] initWithString:strChildHairColor attributes: nil];
            NSRange rangeOfHairColor = [strChildHairColor rangeOfString:@"HAIR COLOR :"];
            [strAttrChildHairColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHairColor];
            [strAttrChildHairColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHairColor];
            cell.lblChildHairColor.attributedText = strAttrChildHairColor;
            
            
            
            cell.lblChildEyeColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildEyeColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildEyeColor.textAlignment = NSTextAlignmentRight;
            NSString *strChildEyeColor;
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForEyeColor])
            {
                cell.lblChildEyeColor.text = [NSString stringWithFormat:@"EYE COLOR : %@", objChildAlert.strEyeColor];
                strChildEyeColor = [NSString stringWithFormat:@"EYE COLOR : %@", objChildAlert.strEyeColor];
            }
            else
            {
                cell.lblChildEyeColor.text = [NSString stringWithFormat:@"EYE COLOR : -"];
                strChildEyeColor = [NSString stringWithFormat:@"EYE COLOR : -"];
            }
            NSMutableAttributedString *strAttrChildEyeColor = [[NSMutableAttributedString alloc] initWithString:strChildEyeColor attributes: nil];
            NSRange rangeOfEyeColor = [strChildEyeColor rangeOfString:@"EYE COLOR :"];
            [strAttrChildEyeColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfEyeColor];
            [strAttrChildEyeColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfEyeColor];
            cell.lblChildEyeColor.attributedText = strAttrChildEyeColor;
            
            
            
            cell.lblChildHeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildHeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildHeight.textAlignment = NSTextAlignmentLeft;
            NSString *strChildHeight;
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHeight])
            {
                cell.lblChildHeight.text = [NSString stringWithFormat:@"HEIGHT : %@", objChildAlert.strHeight];
                strChildHeight = [NSString stringWithFormat:@"HEIGHT : %@", objChildAlert.strHeight];
            }
            else
            {
                cell.lblChildHeight.text = [NSString stringWithFormat:@"HEIGHT : -"];
                strChildHeight = [NSString stringWithFormat:@"HEIGHT : -"];
            }
            NSMutableAttributedString *strAttrChildHeight = [[NSMutableAttributedString alloc] initWithString:strChildHeight attributes: nil];
            NSRange rangeOfHeight = [strChildHeight rangeOfString:@"HEIGHT :"];
            [strAttrChildHeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHeight];
            [strAttrChildHeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHeight];
            cell.lblChildHeight.attributedText = strAttrChildHeight;
            
            
            
            cell.lblChildWeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildWeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildWeight.textAlignment = NSTextAlignmentRight;
            NSString *strChildWeight;
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForWeight])
            {
                cell.lblChildWeight.text = [NSString stringWithFormat:@"WEIGHT : %@", objChildAlert.strWeight];
                strChildWeight = [NSString stringWithFormat:@"WEIGHT : %@", objChildAlert.strWeight];
            }
            else
            {
                cell.lblChildWeight.text = [NSString stringWithFormat:@"WEIGHT : -"];
                strChildWeight = [NSString stringWithFormat:@"WEIGHT : -"];
            }
            NSMutableAttributedString *strAttrChildWeight = [[NSMutableAttributedString alloc] initWithString:strChildWeight attributes: nil];
            NSRange rangeOfWeight = [strChildWeight rangeOfString:@"WEIGHT :"];
            [strAttrChildWeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfWeight];
            [strAttrChildWeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfWeight];
            cell.lblChildWeight.attributedText = strAttrChildWeight;
            
            
            
            cell.lblChildSchoolName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildSchoolName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildSchoolName.textAlignment = NSTextAlignmentLeft;
            NSString *strChildSchool;
            if([objChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForSchoolInformation])
            {
                cell.lblChildSchoolName.text = [NSString stringWithFormat:@"SCHOOL : %@", objChildAlert.strSchoolName];
                strChildSchool = [NSString stringWithFormat:@"SCHOOL : %@", objChildAlert.strSchoolName];
            }
            else
            {
                cell.lblChildSchoolName.text = [NSString stringWithFormat:@"SCHOOL : -"];
                strChildSchool = [NSString stringWithFormat:@"SCHOOL : -"];
            }
            NSMutableAttributedString *strAttrChildSchool = [[NSMutableAttributedString alloc] initWithString:strChildSchool attributes: nil];
            NSRange rangeOfSchool = [strChildSchool rangeOfString:@"SCHOOL :"];
            [strAttrChildSchool addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfSchool];
            [strAttrChildSchool addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfSchool];
            cell.lblChildSchoolName.attributedText = strAttrChildSchool;
            
            
            
            cell.lblAlertType.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
            cell.lblAlertType.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblAlertType.textAlignment = NSTextAlignmentLeft;
            NSString *strChildAlertType;
            if([objChildAlert.strChildAlertType isEqualToString:@"1"])
            {
                cell.lblAlertTypeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalParentRespondAlertBackgroundColor;
                
                cell.lblAlertType.text = [NSString stringWithFormat:@"ALERT LEVEL : PARENT RESPOND"];
                strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : PARENT RESPOND"];
            }
            else if([objChildAlert.strChildAlertType isEqualToString:@"2"])
            {
                cell.lblAlertTypeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalAuthorityAlertBackgroundColor;
                
                cell.lblAlertType.text = [NSString stringWithFormat:@"ALERT LEVEL : AUTHORITY"];
                strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : AUTHORITY"];
            }
            else if([objChildAlert.strChildAlertType isEqualToString:@"3"])
            {
                cell.lblAlertTypeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalKidnappingAlertBackgroundColor;
                
                cell.lblAlertType.text = [NSString stringWithFormat:@"ALERT LEVEL : KIDNAPPING"];
                strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : KIDNAPPING"];
            }
            
            NSMutableAttributedString *strAttrChildAlertType = [[NSMutableAttributedString alloc] initWithString:strChildAlertType attributes: nil];
            NSRange rangeOfAlertLevel = [strChildAlertType rangeOfString:@"ALERT LEVEL :"];
            [strAttrChildAlertType addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfAlertLevel];
            [strAttrChildAlertType addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfAlertLevel];
            cell.lblAlertType.attributedText = strAttrChildAlertType;
            
            cell.lblAlertDateAndTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblAlertDateAndTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblAlertDateAndTime.text = objChildAlert.strChildAlertDateAndTime;
            cell.lblAlertDateAndTime.textAlignment = NSTextAlignmentLeft;
            
            cell.lblAlertDeactivated.hidden = true;
            cell.lblAlertDeactivated.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblAlertDeactivated.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblAlertDeactivated.textAlignment = NSTextAlignmentRight;
            if(objChildAlert.boolIsChildAlertActive)
            {
                cell.lblAlertDeactivated.text = @"ACTIVE";
            }
            else
            {
                cell.lblAlertDeactivated.text = @"DEACTIVATED";
            }
            
            cell.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            ParentMyChildsCommunityAlertsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
            
            if(cell != nil)
            {
                NSArray *nib;
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsCommunityAlertsTableViewCell_iPad" owner:self options:nil];
                }
                else
                {
                    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
                    {
                        nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsCommunityAlertsTableViewCell" owner:self options:nil];
                    }
                    else if([MySingleton sharedManager].screenHeight == 667)
                    {
                        nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsCommunityAlertsTableViewCell_iPhone6" owner:self options:nil];
                    }
                    else if([MySingleton sharedManager].screenHeight == 736)
                    {
                        nib = [[NSBundle mainBundle]loadNibNamed:@"ParentMyChildsCommunityAlertsTableViewCell_iPhone6Plus" owner:self options:nil];
                    }
                }
                
                cell = [nib objectAtIndex:0];
            }
            
            CommunityAlert *objCommunityAlert = [self.dataRows objectAtIndex:indexPath.row];
            
            [[AsyncImageLoader sharedLoader].cache removeAllObjects];
            cell.imageViewChildProfilePicture.imageURL = [NSURL URLWithString:objCommunityAlert.strProfilePictureImageUrl];
            cell.imageViewChildProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
            cell.imageViewChildProfilePicture.layer.masksToBounds = YES;
            cell.imageViewChildProfilePicture.layer.cornerRadius = (cell.imageViewChildProfilePicture.frame.size.width/2);
            cell.imageViewChildProfilePicture.layer.borderWidth = 1.0f;
            cell.imageViewChildProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
            
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForName])
            {
                cell.lblChildName.text = objCommunityAlert.strName;
            }
            else
            {
                cell.lblChildName.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildName.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
            cell.lblChildName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
            cell.lblChildName.textAlignment = NSTextAlignmentLeft;
            
            
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForAge])
            {
                cell.lblChildAge.text = [NSString stringWithFormat:@"%@ years old", objCommunityAlert.strAge];
            }
            else
            {
                cell.lblChildAge.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildAge.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildAge.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildAge.textAlignment = NSTextAlignmentLeft;
            
            
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForGender])
            {
                cell.lblChildGender.text = [NSString stringWithFormat:@"%@", objCommunityAlert.strGender];
            }
            else
            {
                cell.lblChildGender.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildGender.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildGender.textAlignment = NSTextAlignmentLeft;
            
            
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForNationality])
            {
                cell.lblChildNationality.text = [NSString stringWithFormat:@"%@", objCommunityAlert.strNationality];
            }
            else
            {
                cell.lblChildNationality.text = [NSString stringWithFormat:@"-"];
            }
            cell.lblChildNationality.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildNationality.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildNationality.textAlignment = NSTextAlignmentLeft;
            
            
            cell.lblChildHairColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildHairColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildHairColor.textAlignment = NSTextAlignmentLeft;
            NSString *strChildHairColor;
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHairColor])
            {
                cell.lblChildHairColor.text = [NSString stringWithFormat:@"HAIR COLOR : %@", objCommunityAlert.strHairColor];
                strChildHairColor = [NSString stringWithFormat:@"HAIR COLOR : %@", objCommunityAlert.strHairColor];
            }
            else
            {
                cell.lblChildHairColor.text = [NSString stringWithFormat:@"HAIR COLOR : -"];
                strChildHairColor = [NSString stringWithFormat:@"HAIR COLOR : -"];
            }
            NSMutableAttributedString *strAttrChildHairColor = [[NSMutableAttributedString alloc] initWithString:strChildHairColor attributes: nil];
            NSRange rangeOfHairColor = [strChildHairColor rangeOfString:@"HAIR COLOR :"];
            [strAttrChildHairColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHairColor];
            [strAttrChildHairColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHairColor];
            cell.lblChildHairColor.attributedText = strAttrChildHairColor;
            
            
            
            cell.lblChildEyeColor.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildEyeColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildEyeColor.textAlignment = NSTextAlignmentRight;
            NSString *strChildEyeColor;
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForEyeColor])
            {
                cell.lblChildEyeColor.text = [NSString stringWithFormat:@"EYE COLOR : %@", objCommunityAlert.strEyeColor];
                strChildEyeColor = [NSString stringWithFormat:@"EYE COLOR : %@", objCommunityAlert.strEyeColor];
            }
            else
            {
                cell.lblChildEyeColor.text = [NSString stringWithFormat:@"EYE COLOR : -"];
                strChildEyeColor = [NSString stringWithFormat:@"EYE COLOR : -"];
            }
            NSMutableAttributedString *strAttrChildEyeColor = [[NSMutableAttributedString alloc] initWithString:strChildEyeColor attributes: nil];
            NSRange rangeOfEyeColor = [strChildEyeColor rangeOfString:@"EYE COLOR :"];
            [strAttrChildEyeColor addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfEyeColor];
            [strAttrChildEyeColor addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfEyeColor];
            cell.lblChildEyeColor.attributedText = strAttrChildEyeColor;
            
            
            
            cell.lblChildHeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildHeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildHeight.textAlignment = NSTextAlignmentLeft;
            NSString *strChildHeight;
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHeight])
            {
                cell.lblChildHeight.text = [NSString stringWithFormat:@"HEIGHT : %@", objCommunityAlert.strHeight];
                strChildHeight = [NSString stringWithFormat:@"HEIGHT : %@", objCommunityAlert.strHeight];
            }
            else
            {
                cell.lblChildHeight.text = [NSString stringWithFormat:@"HEIGHT : -"];
                strChildHeight = [NSString stringWithFormat:@"HEIGHT : -"];
            }
            NSMutableAttributedString *strAttrChildHeight = [[NSMutableAttributedString alloc] initWithString:strChildHeight attributes: nil];
            NSRange rangeOfHeight = [strChildHeight rangeOfString:@"HEIGHT :"];
            [strAttrChildHeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfHeight];
            [strAttrChildHeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfHeight];
            cell.lblChildHeight.attributedText = strAttrChildHeight;
            
            
            
            cell.lblChildWeight.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildWeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildWeight.textAlignment = NSTextAlignmentRight;
            NSString *strChildWeight;
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForWeight])
            {
                cell.lblChildWeight.text = [NSString stringWithFormat:@"WEIGHT : %@", objCommunityAlert.strWeight];
                strChildWeight = [NSString stringWithFormat:@"WEIGHT : %@", objCommunityAlert.strWeight];
            }
            else
            {
                cell.lblChildWeight.text = [NSString stringWithFormat:@"WEIGHT : -"];
                strChildWeight = [NSString stringWithFormat:@"WEIGHT : -"];
            }
            NSMutableAttributedString *strAttrChildWeight = [[NSMutableAttributedString alloc] initWithString:strChildWeight attributes: nil];
            NSRange rangeOfWeight = [strChildWeight rangeOfString:@"WEIGHT :"];
            [strAttrChildWeight addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfWeight];
            [strAttrChildWeight addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfWeight];
            cell.lblChildWeight.attributedText = strAttrChildWeight;
            
            
            
            cell.lblChildSchoolName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblChildSchoolName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblChildSchoolName.textAlignment = NSTextAlignmentLeft;
            NSString *strChildSchool;
            if(self.boolIsLoadedFromMyAlert || [objCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForSchoolInformation])
            {
                cell.lblChildSchoolName.text = [NSString stringWithFormat:@"SCHOOL : %@", objCommunityAlert.strSchoolName];
                strChildSchool = [NSString stringWithFormat:@"SCHOOL : %@", objCommunityAlert.strSchoolName];
            }
            else
            {
                cell.lblChildSchoolName.text = [NSString stringWithFormat:@"SCHOOL : -"];
                strChildSchool = [NSString stringWithFormat:@"SCHOOL : -"];
            }
            NSMutableAttributedString *strAttrChildSchool = [[NSMutableAttributedString alloc] initWithString:strChildSchool attributes: nil];
            NSRange rangeOfSchool = [strChildSchool rangeOfString:@"SCHOOL :"];
            [strAttrChildSchool addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfSchool];
            [strAttrChildSchool addAttribute:NSFontAttributeName value:[MySingleton sharedManager].themeFontTenSizeBold range:rangeOfSchool];
            cell.lblChildSchoolName.attributedText = strAttrChildSchool;
            
            
            cell.lblAlertDateAndTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblAlertDateAndTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblAlertDateAndTime.text = objCommunityAlert.strCommunityAlertDateAndTime;
            cell.lblAlertDateAndTime.textAlignment = NSTextAlignmentLeft;
            
            cell.lblAlertDeactivated.hidden = true;
            
            cell.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == mainTableView)
    {
        if([self.strSelectedTab isEqualToString:@"1"])
        {
            ChildAlert *objChildAlert = [self.dataRows objectAtIndex:indexPath.row];
            
            ParentMyChildsAlertDetailsViewController *viewController = [[ParentMyChildsAlertDetailsViewController alloc] init];
            viewController.objSelectedChildAlert = objChildAlert;
            viewController.boolIsLoadedFromMyAlert = false;
            viewController.strIsLoadedBy = @"";
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if([self.strSelectedTab isEqualToString:@"2"])
        {
            CommunityAlert *objCommunityAlert = [self.dataRows objectAtIndex:indexPath.row];
            
            CommonCommunityChildsAlertDetailsViewController *viewController = [[CommonCommunityChildsAlertDetailsViewController alloc] init];
            viewController.objSelectedCommunityAlert = objCommunityAlert;
            viewController.boolIsLoadedFromMyAlert = false;
            viewController.strIsLoadedBy = @"";
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
        //        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        if([self.strLoadedFor isEqualToString:@"1"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByParentId;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByParentId;
            }
            
        }
        else if([self.strLoadedFor isEqualToString:@"0"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByChildId;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByChildId;
            }
        }
        else if([self.strLoadedFor isEqualToString:@"2"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByGuestLocation;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByGuestLocation;
            }
        }
        
        if(self.dataRows.count <= 0)
        {
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchForTableViewRecordsWithSubstring:substring];
    return YES;
}

#pragma mark - Other Methods

- (void)searchForTableViewRecordsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        NSArray *arrayToSearchFor;
        
        if([self.strLoadedFor isEqualToString:@"1"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                arrayToSearchFor = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByParentId;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                arrayToSearchFor = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByParentId;
            }
        }
        else if([self.strLoadedFor isEqualToString:@"0"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                arrayToSearchFor = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByChildId;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                arrayToSearchFor = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByChildId;
            }
            
        }
        else if([self.strLoadedFor isEqualToString:@"2"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                arrayToSearchFor = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByGuestLocation;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                arrayToSearchFor = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByGuestLocation;
            }
        }
        
        for(ChildAlert *objChildAlert in arrayToSearchFor)
        {
            if (([[objChildAlert.strName lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strAge lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strHairColor lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strEyeColor lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strGender lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strHeight lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strWeight lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strNationality lowercaseString] containsString:[substring lowercaseString]]) || ([[objChildAlert.strSchoolName lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objChildAlert];
            }
        }
    }
    else
    {
        if([self.strLoadedFor isEqualToString:@"1"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByParentId;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByParentId;
            }
        }
        else if([self.strLoadedFor isEqualToString:@"0"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByChildId;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByChildId;
            }
        }
        else if([self.strLoadedFor isEqualToString:@"2"])
        {
            if([self.strSelectedTab isEqualToString:@"1"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByGuestLocation;
            }
            else if([self.strSelectedTab isEqualToString:@"2"])
            {
                self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByGuestLocation;
            }
        }
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

-(void)btnMoreParentRespondAlertsClicked
{
    [self.view endEditing:YES];
    
    moreParentRespondAlertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalBlueColor;
    moreCommunityRespondAlertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    self.strSelectedTab = @"1";
    
    if([self.strLoadedFor isEqualToString:@"1"])
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByParentId;
    }
    else if([self.strLoadedFor isEqualToString:@"0"])
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByChildId;
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByAlertsByGuestLocation;
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

-(void)btnMoreCommunityRespondAlertsClicked
{
    [self.view endEditing:YES];
    
    moreParentRespondAlertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    moreCommunityRespondAlertsContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalBlueColor;
    
    self.strSelectedTab = @"2";
    
    if([self.strLoadedFor isEqualToString:@"1"])
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByParentId;
    }
    else if([self.strLoadedFor isEqualToString:@"0"])
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByChildId;
    }
    else if([self.strLoadedFor isEqualToString:@"2"])
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllNearByCommunityAlertsByGuestLocation;
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

@end
