//
//  SideMenuViewController.m
//  SetMyPace
//
//  Created by Pratik Gujarati on 28/01/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "SideMenuViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "SideBarTableViewCell.h"

#import "LoginOptionsViewController.h"
#import "CommonNearByAlertsViewController.h"

#import "ChildHomeViewController.h"
#import "ChildGenerateCommunityAlertViewController.h"
#import "ChildParentRequestsViewController.h"
#import "ChildMyProfileViewController.h"
#import "ChildMyParentsViewController.h"
#import "ChildAlertsRaisedByMeViewController.h"

#import "ParentHomeViewController.h"
#import "ParentMyChildsViewController.h"
#import "ParentMyChildsAlertsViewController.h"
#import "ParentMyProfileViewController.h"
#import "ParentSetResponseTimeViewController.h"
#import "ParentAuthorityEmailListViewController.h"

#import "CommonAllNotificationsViewController.h"
#import "CommonAllBoycottAlertsViewController.h"
#import "CommonAboutUsViewController.h"

@interface SideMenuViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation SideMenuViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;
@synthesize mainContainerView;

@synthesize mainImageViewBackground;
@synthesize mainTableViewContainerScrollView;
@synthesize mainTableView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
//    CGSize mainTableViewViewContentSize = mainTableView.contentSize;
//    
//    if(mainTableViewViewContentSize.height > ([MySingleton sharedManager].screenHeight - 20))
//    {
//        NSLog(@"side menu table view is bigger");
//        
//        mainTableViewContainerScrollView.contentSize = CGSizeMake(mainTableViewContainerScrollView.frame.size.width, mainTableViewViewContentSize.height);
//        
//        CGRect mainTableViewFrame = mainTableView.frame;
//        mainTableViewFrame.size.height = mainTableViewViewContentSize.height;
//        mainTableView.frame = mainTableViewFrame;
//    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGSize mainTableViewViewContentSize = mainTableView.contentSize;
    
    if(mainTableViewViewContentSize.height > ([MySingleton sharedManager].screenHeight - 20))
    {
        NSLog(@"side menu table view is bigger");
        
        mainTableViewContainerScrollView.contentSize = CGSizeMake(mainTableViewContainerScrollView.frame.size.width, mainTableViewViewContentSize.height);
        
        CGRect mainTableViewFrame = mainTableView.frame;
        mainTableViewFrame.size.height = mainTableViewViewContentSize.height;
        mainTableView.frame = mainTableViewFrame;
    }
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parentLoggedoutEvent) name:@"parentLoggedoutEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(childLoggedoutEvent) name:@"childLoggedoutEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(guestLoggedoutEvent) name:@"guestLoggedoutEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)parentLoggedoutEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:@"autologin"];
    [prefs removeObjectForKey:@"ispaid"];
    [prefs removeObjectForKey:@"parentid"];
    [prefs removeObjectForKey:@"parentemail"];
    [prefs removeObjectForKey:@"parentname"];
    [prefs removeObjectForKey:@"usertype"];
    [prefs synchronize];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)childLoggedoutEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:@"autologin"];
    [prefs removeObjectForKey:@"childid"];
    [prefs removeObjectForKey:@"childemail"];
    [prefs removeObjectForKey:@"childname"];
    [prefs removeObjectForKey:@"usertype"];
    [prefs synchronize];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)guestLoggedoutEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:@"autologin"];
    [prefs removeObjectForKey:@"usertype"];
    [prefs synchronize];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    mainScrollView.delegate = self;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainImageViewBackground.layer.masksToBounds = true;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    
    if([MySingleton sharedManager].screenHeight != 480)
    {
        mainTableView.scrollEnabled = false;
    }
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if(tableView == mainTableView)
//    {
//        return 110;
//    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"usertype"]];
        
        if([strUserType isEqualToString:@"1"])
        {
             return 9;
        }
        else if([strUserType isEqualToString:@"0"])
        {
             return 9;
        }
        else if([strUserType isEqualToString:@"2"])
        {
            return 5;
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"usertype"]];
        
        if([strUserType isEqualToString:@"1"])
        {
            return 50;
        }
        else if([strUserType isEqualToString:@"0"])
        {
            if([MySingleton sharedManager].screenHeight <= 568)
            {
//                if(indexPath.row == 1)
//                {
//                    return 70;
//                }
//                else
//                {
                    return 50;
//                }
            }
            else
            {
                return 50;
            }
        }
        else if([strUserType isEqualToString:@"2"])
        {
            return 50;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        SideBarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"usertype"]];
        
        if([strUserType isEqualToString:@"1"])
        {
            cell = [[SideBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
            
            if(indexPath.row == 0)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_home.png"];
                cell.lblMain.text = @"Home";
            }
            else if(indexPath.row == 1)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_my_childs.png"];
                cell.lblMain.text = @"Our Children";
            }
            else if(indexPath.row == 2)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_my_childs_alerts.png"];
                cell.lblMain.text = @"Our Children Alerts";
            }
            else if(indexPath.row == 3)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_near_by_alerts.png"];
                cell.lblMain.text = @"Near by Alerts";
            }
            else if(indexPath.row == 4)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_my_profile.png"];
                cell.lblMain.text = @"My Profile";
            }
            else if(indexPath.row == 5)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_set_response_time.png"];
                cell.lblMain.text = @"Set Response Time";
            }
            else if(indexPath.row == 6)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_set_authority_email.png"];
                cell.lblMain.text = @"Set Authority Email";
            }
            else if(indexPath.row == 7)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"about_us.png"];
                cell.lblMain.text = @"About Us";
            }
            else if(indexPath.row == 8)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"parent_logout.png"];
                cell.lblMain.text = @"Logout";
            }
        }
        else if([strUserType isEqualToString:@"0"])
        {
            if([MySingleton sharedManager].screenHeight <= 568)
            {
//                if(indexPath.row == 1)
//                {
//                    cell = [[SideBarTableViewCell alloc] initWithTwoLinesStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
//                }
//                else
//                {
                    cell = [[SideBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
//                }
            }
            else
            {
                cell = [[SideBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
            }
            
            if(indexPath.row == 0)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_home.png"];
                cell.lblMain.text = @"Home";
            }
            else if(indexPath.row == 1)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_raise_an_uncomfortable_situation.png"];
                cell.lblMain.text = @"Community Respond";
            }
            else if(indexPath.row == 2)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_parent_requests.png"];
                cell.lblMain.text = @"Parent Requests";
            }
            else if(indexPath.row == 3)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_near_by_alerts.png"];
                cell.lblMain.text = @"Near by Alerts";
            }
            else if(indexPath.row == 4)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_my_profile.png"];
                cell.lblMain.text = @"My Profile";
            }
            else if(indexPath.row == 5)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_my_parents.png"];
                cell.lblMain.text = @"My Parents";
            }
            else if(indexPath.row == 6)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_alerts_raised_by_me.png"];
                cell.lblMain.text = @"My Previous Alerts";
            }
            else if(indexPath.row == 7)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"about_us.png"];
                cell.lblMain.text = @"About Us";
            }
            else if(indexPath.row == 8)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"child_logout.png"];
                cell.lblMain.text = @"Logout";
            }
        }
        else if([strUserType isEqualToString:@"2"])
        {
            cell = [[SideBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
            
            if(indexPath.row == 0)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"guest_near_by_alerts.png"];
                cell.lblMain.text = @"Near by Alerts";
            }
            else if(indexPath.row == 1)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"notifications.png"];
                cell.lblMain.text = @"Notifications";
            }
            else if(indexPath.row == 2)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"boycott_alerts.png"];
                cell.lblMain.text = @"Boycott Notifications";
            }
            else if(indexPath.row == 3)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"about_us.png"];
                cell.lblMain.text = @"About Us";
            }
            else if(indexPath.row == 4)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"guest_logout.png"];
                cell.lblMain.text = @"Logout";
            }
        }
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:true];
    
    if(tableView == mainTableView)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"usertype"]];
        
        if([strUserType isEqualToString:@"1"])
        {
            if(indexPath.row == 0)
            {
                ParentHomeViewController *contentViewController = [[ParentHomeViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 1)
            {
                ParentMyChildsViewController *contentViewController = [[ParentMyChildsViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 2)
            {
                ParentMyChildsAlertsViewController *contentViewController = [[ParentMyChildsAlertsViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 3)
            {
                CommonNearByAlertsViewController *contentViewController = [[CommonNearByAlertsViewController alloc] init];
                contentViewController.strLoadedFor = @"1";
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 4)
            {
                ParentMyProfileViewController *contentViewController = [[ParentMyProfileViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 5)
            {
                ParentSetResponseTimeViewController *contentViewController = [[ParentSetResponseTimeViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 6)
            {
                ParentAuthorityEmailListViewController *contentViewController = [[ParentAuthorityEmailListViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 7)
            {
                CommonAboutUsViewController *contentViewController = [[CommonAboutUsViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 8)
            {
                [self logout];
            }
        }
        else if([strUserType isEqualToString:@"0"])
        {
            if(indexPath.row == 0)
            {
                ChildHomeViewController *contentViewController = [[ChildHomeViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            if(indexPath.row == 1)
            {
                ChildGenerateCommunityAlertViewController *contentViewController = [[ChildGenerateCommunityAlertViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            if(indexPath.row == 2)
            {
                ChildParentRequestsViewController *contentViewController = [[ChildParentRequestsViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            if(indexPath.row == 3)
            {
                CommonNearByAlertsViewController *contentViewController = [[CommonNearByAlertsViewController alloc] init];
                contentViewController.strLoadedFor = @"0";
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            if(indexPath.row == 4)
            {
                ChildMyProfileViewController *contentViewController = [[ChildMyProfileViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            if(indexPath.row == 5)
            {
                ChildMyParentsViewController *contentViewController = [[ChildMyParentsViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            if(indexPath.row == 6)
            {
                ChildAlertsRaisedByMeViewController *contentViewController = [[ChildAlertsRaisedByMeViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 7)
            {
                CommonAboutUsViewController *contentViewController = [[CommonAboutUsViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 8)
            {
                [self logout];
            }
        }
        else if([strUserType isEqualToString:@"2"])
        {
            if(indexPath.row == 0)
            {
                CommonNearByAlertsViewController *contentViewController = [[CommonNearByAlertsViewController alloc] init];
                contentViewController.strLoadedFor = @"2";
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 1)
            {
                CommonAllNotificationsViewController *contentViewController = [[CommonAllNotificationsViewController alloc] init];
                contentViewController.strLoadedFor = @"2";
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 2)
            {
                CommonAllBoycottAlertsViewController *contentViewController = [[CommonAllBoycottAlertsViewController alloc] init];
                contentViewController.strLoadedFor = @"2";
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 3)
            {
                CommonAboutUsViewController *contentViewController = [[CommonAboutUsViewController alloc] init];
                self.sideMenuController.rootViewController = contentViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 4)
            {
                [self logout];
            }
        }
    }
}

#pragma mark - Other Method

- (void)logout
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Are you sure you want to logout?";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"usertype"]];
        
        if([strUserType isEqualToString:@"1"])
        {
            NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
            [[MySingleton sharedManager].dataManager parentLogout:strParentId];
        }
        else if([strUserType isEqualToString:@"0"])
        {
            NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
            [[MySingleton sharedManager].dataManager childLogout:strChildId];
        }
        else if([strUserType isEqualToString:@"2"])
        {
            NSString *strGuestId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"guestid"]];
            [[MySingleton sharedManager].dataManager guestLogout:strGuestId];
        }
    }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

@end
