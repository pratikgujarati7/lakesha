//
//  SplashViewController.m
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import "SplashViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentHomeViewController.h"
#import "ChildHomeViewController.h"
#import "IntroductionViewController.h"
#import "LoginOptionsViewController.h"
#import "CommonNearByAlertsViewController.h"
#import "ParentPaymentViewController.h"

@interface SplashViewController ()
{
    AppDelegate *appDelegate;
    BOOL boolIsRedirected;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    BOOL boolIsExecuteRedirectionCalledOnce;
}

@end

@implementation SplashViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize mainSplashBackgroundImageView;
@synthesize mainLogoImageView;

@synthesize locationDisabledContainerView;
@synthesize mainLogoInLocationDisabledContainerView;
@synthesize lblLocationDisabledInstruction;

//========== OTHER VARIABLES ==========//

@synthesize strLatitude;
@synthesize strLongitude;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sentLoggedInChildOrParentOrGuestCurrentLocationToServerEvent) name:@"sentLoggedInChildOrParentOrGuestCurrentLocationToServerEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)sentLoggedInChildOrParentOrGuestCurrentLocationToServerEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserType = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"usertype"]];
    
    if([strUserType isEqualToString:@"1"])
    {
        boolIsRedirected = TRUE;
        
        NSString *strPaid = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"ispaid"]];
        
        if([strPaid isEqualToString:@"1"])
        {
            [self performSelector:@selector(navigateToParentHomeViewController) withObject:self afterDelay:1.0];
        }
        else
        {
            [self performSelector:@selector(navigateToParentPaymentViewController) withObject:self afterDelay:1.0];
        }
    }
    else if([strUserType isEqualToString:@"0"])
    {
        boolIsRedirected = TRUE;
        
        [self performSelector:@selector(navigateToChildHomeViewController) withObject:self afterDelay:1.0];
    }
    else if([strUserType isEqualToString:@"2"])
    {
        boolIsRedirected = TRUE;
        
        [self performSelector:@selector(navigateToGuestHomeViewController) withObject:self afterDelay:1.0];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
//    self.automaticallyAdjustsScrollViewInsets = NO;
    
    mainScrollView.delegate = self;
    
    mainSplashBackgroundImageView.layer.masksToBounds = true;
    mainLogoImageView.layer.masksToBounds = true;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIFont *lblLocationDisabledInstructionFont, *lblLocationDisabledInstructionBoldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblLocationDisabledInstructionFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        lblLocationDisabledInstructionBoldFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblLocationDisabledInstructionFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblLocationDisabledInstructionBoldFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblLocationDisabledInstructionFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblLocationDisabledInstructionBoldFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblLocationDisabledInstructionFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            lblLocationDisabledInstructionBoldFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else
        {
            lblLocationDisabledInstructionFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblLocationDisabledInstructionBoldFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    mainLogoInLocationDisabledContainerView.layer.masksToBounds = true;
    
    lblLocationDisabledInstruction.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblLocationDisabledInstruction.font = lblLocationDisabledInstructionFont;
    NSString *strLocationDisabledInstruction = [NSString stringWithFormat:@"We were unable to fetch your location as your location services is disabled. \n\nFollow these steps to use our app to its full extent. \n\n1. Go to Settings > Privacy and enable Location Services."];
    NSMutableAttributedString *strAttrLocationDisabledInstruction = [[NSMutableAttributedString alloc] initWithString:strLocationDisabledInstruction attributes: nil];
    NSRange rangeOfFollowTheseSteps = [strLocationDisabledInstruction rangeOfString:@"Follow these steps to use our app to its full extent."];
    [strAttrLocationDisabledInstruction addAttribute:NSFontAttributeName value:lblLocationDisabledInstructionBoldFont range:rangeOfFollowTheseSteps];
    lblLocationDisabledInstruction.attributedText = strAttrLocationDisabledInstruction;
    
    if([MySingleton sharedManager].dataManager.strLocationLatitude != nil && [MySingleton sharedManager].dataManager.strLocationLongitude != nil )
    {
        if(boolIsExecuteRedirectionCalledOnce == false)
        {
            boolIsExecuteRedirectionCalledOnce = true;
            
            [self executeRedirection];
        }
    }
    else
    {
        locationManager = [[CLLocationManager alloc] init];
        [locationManager startMonitoringSignificantLocationChanges];
        locationManager.delegate = self;
        
        #ifdef __IPHONE_8_0
        if(IS_OS_8_OR_LATER)
        {
            // Use one or the other, not both. Depending on what you put in info.plist
            [locationManager requestWhenInUseAuthorization];
        }
        #endif
        
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
        
        if ([CLLocationManager locationServicesEnabled])
        {
            locationDisabledContainerView.hidden = true;
            
            if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
            {
                locationDisabledContainerView.hidden = false;
            }
        }
        else
        {
            locationDisabledContainerView.hidden = false;
        }
    }
}

-(void)executeRedirection
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(![[prefs objectForKey:@"isAppLoadedForFirstTime"] isEqualToString:@"1"])
    {
        //========== APPLICATION IS OPENED FOR THE FIRST TIME ==========//
        dispatch_async(dispatch_get_main_queue(), ^{
            [prefs setObject:@"1" forKey:@"isAppLoadedForFirstTime"];
            [prefs synchronize];
        });
        
        boolIsRedirected = TRUE;
        
        [self performSelector:@selector(navigateToIntroductionScreen) withObject:self afterDelay:1.0];
    }
    else
    {
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        NSString *strGuestId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"guestid"]];
        NSString *strAutoLogin = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"autologin"]];
        
//        NSString *strParentId = [prefs objectForKey:@"parentid"];
//        NSString *strChildId = [prefs objectForKey:@"childid"];
//        NSString *strGuestId = [prefs objectForKey:@"guestid"];
//        NSString *strAutoLogin = [prefs objectForKey:@"autologin"];
        
        if(((strParentId != nil && ![strParentId isEqualToString:@"(null)"]) && strParentId.length > 0) && ([strAutoLogin isEqualToString:@"1"]))
        {
            [[MySingleton sharedManager].dataManager sendLoggedInChildOrParentOrGuestCurrentLocationToServer];
        }
        else if(((strChildId != nil && ![strChildId isEqualToString:@"(null)"]) && strChildId.length > 0) && ([strAutoLogin isEqualToString:@"1"]))
        {
            [[MySingleton sharedManager].dataManager sendLoggedInChildOrParentOrGuestCurrentLocationToServer];
        }
        else if(((strGuestId != nil && ![strGuestId isEqualToString:@"(null)"]) && strGuestId.length > 0) && ([strAutoLogin isEqualToString:@"1"]))
        {
            [[MySingleton sharedManager].dataManager sendLoggedInChildOrParentOrGuestCurrentLocationToServer];
        }
        else
        {
            boolIsRedirected = TRUE;
            
            [self performSelector:@selector(navigateToLoginOptionsScreen) withObject:self afterDelay:1.0];
        }
    }
}

#pragma mark - CLLocationManager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError Called.");
    
//    [locationManager startUpdatingLocation];
    
    locationDisabledContainerView.hidden = false;
    
    [appDelegate dismissGlobalHUD];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"didUpdateLocations Called.");
    
    locationDisabledContainerView.hidden = true;
    
    currentLocation = [locations objectAtIndex:0];
    
    if (currentLocation != nil)
    {
        strLatitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        strLongitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
        [MySingleton sharedManager].dataManager.strLocationLatitude = strLatitude;
        [MySingleton sharedManager].dataManager.strLocationLongitude = strLongitude;
        
        [locationManager stopUpdatingLocation];
        
        if(boolIsExecuteRedirectionCalledOnce == false)
        {
            boolIsExecuteRedirectionCalledOnce = true;
            
            [self executeRedirection];
        }
    }
    else
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = [MySingleton sharedManager].strLocationDisabledErrorTitle;
        alertViewController.message = [MySingleton sharedManager].strLocationDisabledErrorMessage;
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [self presentViewController:alertViewController animated:YES completion:nil];
    }
}

#pragma mark - Other Method

- (void)navigateToIntroductionScreen
{
    [self.view endEditing:true];
    
    IntroductionViewController *viewController = [[IntroductionViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToParentHomeViewController
{
    [self.view endEditing:true];
    
    ParentHomeViewController *viewController = [[ParentHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

-(void)navigateToParentPaymentViewController
{
    [self.view endEditing:true];
    
    ParentPaymentViewController *viewController = [[ParentPaymentViewController alloc] init];
    viewController.boolIsLoadedWithoutBack = true;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToChildHomeViewController
{
    [self.view endEditing:true];
    
    ChildHomeViewController *viewController = [[ChildHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

- (void)navigateToGuestHomeViewController
{
    [self.view endEditing:true];
    
    CommonNearByAlertsViewController *viewController = [[CommonNearByAlertsViewController alloc] init];
    viewController.strLoadedFor = @"2";
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

- (void)navigateToLoginOptionsScreen
{
    [self.view endEditing:true];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
