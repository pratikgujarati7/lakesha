//
//  CommonWebViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 24/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonWebViewController : UIViewController<UIScrollViewDelegate, UIWebViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIWebView *mainWebView;
@property (nonatomic,retain) IBOutlet UIActivityIndicatorView *activityIndicatorView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSString *strTitle;
@property (nonatomic,retain) NSString *strUrl;

@end
