//
//  ParentSetAuthorityEmailViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 11/07/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentSetAuthorityEmailViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentMoreInfoOnAuthorityEmailViewController.h"

#import "ChildListTableViewCell.h"

@interface ParentSetAuthorityEmailViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentSetAuthorityEmailViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize mainTableView;

@synthesize lblInstructions;

@synthesize txtAuthorityEmail;
@synthesize txtAuthorityEmailBottomSeparatorView;

@synthesize btnSave;

@synthesize btnMoreInfo;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedAuthorityEmailByParentIdEvent) name:@"updatedAuthorityEmailByParentIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAuthorityEmailByParentIdEvent
{
    txtAuthorityEmail.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strAuthorityEmail;
}

-(void)updatedAuthorityEmailByParentIdEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        txtAuthorityEmail.text = @"";
        self.arraySelectedChildIds = [[NSMutableArray alloc] init];
        self.dataRows = [MySingleton sharedManager].dataManager.arrayChildsWithAuthorityEmailByParentId;
        [mainTableView reloadData];
        
        [appDelegate showErrorAlertViewWithTitle:@"Authority Updated" withDetails:@"Authority email address has been updated successfully."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Set Authority Email"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *txtFieldFont, *lblInstructionFont, *btnFont, *btnMoreInfoFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblInstructionFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
        btnMoreInfoFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnMoreInfoFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnMoreInfoFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            btnMoreInfoFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            btnMoreInfoFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        }
    }
    
    self.arraySelectedChildIds = [[NSMutableArray alloc] init];
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    lblInstructions.font = lblInstructionFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.numberOfLines = 3;
    lblInstructions.lineBreakMode = NSLineBreakByWordWrapping;
    lblInstructions.text = [NSString stringWithFormat:@"Please enter authority email address.\nThese will be applied to all selected children."];
    
    [self setupTextfield:txtAuthorityEmail WithBottomSeparatorView:txtAuthorityEmailBottomSeparatorView];
    
    btnSave.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSave.layer.masksToBounds = true;
    btnSave.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSave.titleLabel.font = btnFont;
    [btnSave setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSave addTarget:self action:@selector(btnSaveClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnMoreInfo.layer.masksToBounds = true;
    btnMoreInfo.titleLabel.font = btnMoreInfoFont;
    [btnMoreInfo setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnMoreInfo addTarget:self action:@selector(btnMoreInfoClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupTextfield:(UITextField *)textField WithBottomSeparatorView:(UIView *)textFieldBottomSeparatorView
{
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    textField.font = txtFieldFont;
    textField.delegate = self;
    [textField setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [MySingleton sharedManager].textfieldTextColor;
    textField.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    textFieldBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 60;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        ChildListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[ChildListTableViewCell alloc] initWithUpdateAuthorityEmailListStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
        
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        cell.imageViewChildProfilePicture.imageURL = [NSURL URLWithString:objChild.strProfilePictureImageUrl];
        
        cell.lblChildName.text = objChild.strName;
        cell.lblChildAuthorityEmail.text = [NSString stringWithFormat:@"%@", objChild.strAuthorityEmail];
        
        if([self.arraySelectedChildIds containsObject:objChild.strChildID])
        {
            cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_checked.png"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == mainTableView)
    {
        Child *objChild = [self.dataRows objectAtIndex:indexPath.row];
        
        if([self.arraySelectedChildIds containsObject:objChild.strChildID])
        {
            NSInteger indexOfObject = [self.arraySelectedChildIds indexOfObject:objChild.strChildID];
            
            if(indexOfObject != NSNotFound)
            {
                [self.arraySelectedChildIds removeObjectAtIndex:indexOfObject];
                [mainTableView reloadData];
            }
        }
        else
        {
            [self.arraySelectedChildIds addObject:objChild.strChildID];
            [mainTableView reloadData];
        }
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnSaveClicked:(id)sender
{
    [self.view endEditing:true];
    
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if(self.arraySelectedChildIds.count > 0 && (txtAuthorityEmail.text.length > 0 && [objUtility isValidEmailAddress:txtAuthorityEmail.text]))
    {
        NSLog(@"self.arraySelectedChildIds : %@", self.arraySelectedChildIds);
        NSString *strSelectedChildIds = [self.arraySelectedChildIds componentsJoinedByString:@","];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [[MySingleton sharedManager].dataManager updateAuthorityEmailByParentId:strParentId withAuthorityEmail:txtAuthorityEmail.text withChildIds:strSelectedChildIds];
    }
    else
    {
        if(self.arraySelectedChildIds.count <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please select atleast one child"];
            });
        }
        else if(txtAuthorityEmail.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter authority email"];
            });
        }
        else if(![objUtility isValidEmailAddress:txtAuthorityEmail.text])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter a valid authority email"];
            });
        }
    }
}

- (IBAction)btnMoreInfoClicked:(id)sender
{
    [self.view endEditing:true];
    
    ParentMoreInfoOnAuthorityEmailViewController *viewController = [[ParentMoreInfoOnAuthorityEmailViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:true];
}

@end
