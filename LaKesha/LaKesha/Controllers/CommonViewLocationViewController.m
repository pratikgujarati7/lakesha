//
//  CommonViewLocationViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 15/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CommonViewLocationViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface CommonViewLocationViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation CommonViewLocationViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainMapView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
//    [MySingleton sharedManager].floatParentChildPrivacySettingInformationSelectorTableViewWidth = informationSelectorTableView.frame.size.width;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Location"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInstructionFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else
        {
            lblInstructionFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
    }
    
    mainMapView.mapType = kGMSTypeNormal;
    mainMapView.delegate = self;
    
    [self placeLocationsPinsOnMap];
    
//    lblInstruction1.font = lblInstructionFont;
//    lblInstruction1.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
}

#pragma mark - Google Maps Delegate Methods

-(void)placeLocationsPinsOnMap
{
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[MySingleton sharedManager].dataManager.strLocationLatitude doubleValue] longitude:[[MySingleton sharedManager].dataManager.strLocationLongitude doubleValue] zoom:5];
//    [mainMapView setCamera:camera];
//    [mainMapView animateToViewingAngle:45];
    
    if(self.objSelectedChildAlert != nil)
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.objSelectedChildAlert.strChildLatitude doubleValue] longitude:[self.objSelectedChildAlert.strChildLongitude doubleValue] zoom:12];
        [mainMapView setCamera:camera];
    }
    else if(self.objSelectedCommunityAlert != nil)
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.objSelectedCommunityAlert.strChildLatitude doubleValue] longitude:[self.objSelectedCommunityAlert.strChildLongitude doubleValue] zoom:12];
        [mainMapView setCamera:camera];
    }
    
    GMSMarker *parentLocationPin = [[GMSMarker alloc] init];
//    parentLocationPin.position = camera.target;
    parentLocationPin.position = CLLocationCoordinate2DMake([[MySingleton sharedManager].dataManager.strLocationLatitude doubleValue], [[MySingleton sharedManager].dataManager.strLocationLongitude doubleValue]);
    parentLocationPin.title = @"Your Location";
    parentLocationPin.snippet = @"";
    parentLocationPin.appearAnimation = kGMSMarkerAnimationNone;
    parentLocationPin.icon = [UIImage imageNamed:@"current_location_pin.png"];
    parentLocationPin.icon = [self image:parentLocationPin.icon scaledToSize:CGSizeMake(30.0f, 30.0f)];
    [parentLocationPin setDraggable: NO];
    parentLocationPin.map = mainMapView;
    
    
    GMSMarker *childLocationPin = [[GMSMarker alloc] init];
    if(self.objSelectedChildAlert != nil)
    {
        childLocationPin.position = CLLocationCoordinate2DMake([self.objSelectedChildAlert.strChildLatitude doubleValue], [self.objSelectedChildAlert.strChildLongitude doubleValue]);
    }
    else if(self.objSelectedCommunityAlert != nil)
    {
        childLocationPin.position = CLLocationCoordinate2DMake([self.objSelectedCommunityAlert.strChildLatitude doubleValue], [self.objSelectedCommunityAlert.strChildLongitude doubleValue]);
    }
//    childLocationPin.title = [NSString stringWithFormat:@"%@'s Location", self.objSelectedChildAlert.strName];
    childLocationPin.title = [NSString stringWithFormat:@"Child's Location"];
    childLocationPin.snippet = @"";
    childLocationPin.appearAnimation = kGMSMarkerAnimationNone;
    childLocationPin.icon = [UIImage imageNamed:@"child_location_pin.png"];
    childLocationPin.icon = [self image:childLocationPin.icon scaledToSize:CGSizeMake(30.0f, 30.0f)];
    [childLocationPin setDraggable: NO];
    childLocationPin.map = mainMapView;
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return image;
}

#pragma mark - Other Methods

@end
