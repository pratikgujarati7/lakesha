//
//  ParentSetResponseTimeViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 20/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentSetResponseTimeViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface ParentSetResponseTimeViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentSetResponseTimeViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblInstructions;

@synthesize txtResponseTime;
@synthesize imageViewResponseTimeDropDownArrow;
@synthesize txtResponseTimeBottomSeparatorView;

@synthesize btnSave;

//========== OTHER VARIABLES ==========//

@synthesize responseTimePickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotParentResponseTimeByParentIdEvent) name:@"gotParentResponseTimeByParentIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedParentResponseTimeByParentIdEvent) name:@"updatedParentResponseTimeByParentIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotParentResponseTimeByParentIdEvent
{
    txtResponseTime.text = [MySingleton sharedManager].dataManager.objLoggedInParent.strParentResponseTime;
    if([self.arrayResponseTime containsObject:txtResponseTime.text])
    {
        NSInteger indexOfObject = [self.arrayResponseTime indexOfObject:txtResponseTime.text];
        
        if(indexOfObject != NSNotFound)
        {
            [responseTimePickerView selectRow:indexOfObject inComponent:0 animated:YES];
        }
    }
}

-(void)updatedParentResponseTimeByParentIdEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Response Time Updated" withDetails:@"Your response time has been updated successfully."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Set Response Time"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    self.arrayResponseTime = [[NSMutableArray alloc]initWithObjects:@"10 Minutes", @"15 Minutes", @"20 Minutes", @"25 Minutes", @"30 Minutes",nil];
    
    UIFont *txtFieldFont, *lblInstructionFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblInstructionFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblInstructionFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    lblInstructions.font = lblInstructionFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.numberOfLines = 3;
    lblInstructions.lineBreakMode = NSLineBreakByWordWrapping;
    lblInstructions.text = [NSString stringWithFormat:@"Please select your response time period.\nThese will be applied to all your children alerts."];
    
    responseTimePickerView = [[UIPickerView alloc] init];
    responseTimePickerView.delegate = self;
    responseTimePickerView.dataSource = self;
    responseTimePickerView.showsSelectionIndicator = YES;
    responseTimePickerView.tag = 1;
    responseTimePickerView.backgroundColor = [UIColor whiteColor];
    
    [self setupTextfield:txtResponseTime WithBottomSeparatorView:txtResponseTimeBottomSeparatorView];
    [txtResponseTime setInputView:responseTimePickerView];
    imageViewResponseTimeDropDownArrow.layer.masksToBounds = YES;
    
    btnSave.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSave.layer.masksToBounds = true;
    btnSave.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSave.titleLabel.font = btnFont;
    [btnSave setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSave addTarget:self action:@selector(btnSaveClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
    [[MySingleton sharedManager].dataManager getParentResponseTimeByParentId:strParentId];
}

- (void)setupTextfield:(UITextField *)textField WithBottomSeparatorView:(UIView *)textFieldBottomSeparatorView
{
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    textField.font = txtFieldFont;
    textField.delegate = self;
    [textField setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [MySingleton sharedManager].textfieldTextColor;
    textField.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    textFieldBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtResponseTime)
    {
        if(txtResponseTime.text.length == 0 )
        {
            txtResponseTime.text = [self.arrayResponseTime objectAtIndex:0];
            [responseTimePickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayResponseTime count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == responseTimePickerView)
    {
        lblMain.text = self.arrayResponseTime[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtResponseTime.text = [self.arrayResponseTime objectAtIndex:row];
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnSaveClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(txtResponseTime.text.length > 0)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        NSString *strSelectedResponseTimeWithMinutes = txtResponseTime.text;
        NSArray *arraySelectedResponseTimeComponents = [strSelectedResponseTimeWithMinutes componentsSeparatedByString:@" "];
        if(arraySelectedResponseTimeComponents.count > 0)
        {
            NSString *strSelectedResponseTimeWithoutMinutes = [arraySelectedResponseTimeComponents objectAtIndex:0];
            [[MySingleton sharedManager].dataManager updateParentResponseTimeByParentId:strParentId withResponseTime:strSelectedResponseTimeWithoutMinutes];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please select a response time"];
        });
    }
}

@end
