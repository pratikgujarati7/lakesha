//
//  ParentMyChildsAlertDetailsViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentMyChildsAlertDetailsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "CommonViewLocationViewController.h"
#import "CommonMoreAlertsFromThisChildViewController.h"

@interface ParentMyChildsAlertDetailsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    NSTimer *childPictureSliderTimer;
    int pageNumber;
    int numberOfPages;
    
    UIFont *lblAlertLevelSmallFont, *lblAlertLevelBigFont, *lblResponseTimeSmallFont, *lblResponseTimeBigFont, *lblHeaderFont, *lblValueFont, *txtFieldFont, *lblDelayTimePopupContainerViewTitleFont, *lblDelayTimePopupContainerViewInstructionsFont, *btnSubmitInDelayTimePopupContainerViewFont, *lblMoreAlertsFromThisChildFont;
    CGFloat delayTimePopupContainerViewCornerRadius, btnSubmitInDelayTimePopupContainerViewCornerRadiusValue;
}

@end

@implementation ParentMyChildsAlertDetailsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;
@synthesize imageViewOptions;
@synthesize btnOptions;

@synthesize mainContainerView;

@synthesize childPicturesScrollView;
@synthesize mainPageControl;

@synthesize responseButtonsContainerView;

@synthesize btnDelay;
@synthesize btnRaise;
@synthesize btnDeactivate;

@synthesize mainInformationContainerView;


@synthesize alertLevelAndResponsTimeContainer;
@synthesize lblAlertLevel;
@synthesize lblResponseTime;
@synthesize lblResponseTimeBottomSeparatorView;



@synthesize viewLocationContainerView;

@synthesize imageViewLocation;
@synthesize lblViewLocation;
@synthesize lblViewLocationBottomSeparatorView;
@synthesize btnViewLocation;



@synthesize childMessageContainerView;

@synthesize lblChildMessageTitle;

@synthesize lblChildMessage;
@synthesize lblChildMessageBottomSeparatorView;



@synthesize parentMessageContainerView;

@synthesize lblParentMessageTitle;

@synthesize lblParentMessage;
@synthesize lblParentMessageBottomSeparatorView;



@synthesize basicInformationContainerView;

@synthesize lblName;
@synthesize lblNameBottomSeparatorView;

@synthesize lblPhoneNumber;
@synthesize lblPhoneNumberBottomSeparatorView;

@synthesize lblAge;
@synthesize lblAgeBottomSeparatorView;
@synthesize lblGender;
@synthesize lblGenderBottomSeparatorView;

@synthesize lblHeight;
@synthesize lblHeightBottomSeparatorView;
@synthesize lblWeight;
@synthesize lblWeightBottomSeparatorView;

@synthesize lblHairColor;
@synthesize lblHairColorBottomSeparatorView;
@synthesize lblEyeColor;
@synthesize lblEyeColorBottomSeparatorView;

@synthesize lblHairStyle;
@synthesize lblHairStyleBottomSeparatorView;

@synthesize lblNationality;
@synthesize lblNationalityBottomSeparatorView;


@synthesize emergencyContactInformationContainerView;

@synthesize lblEmergencyContactInformationTitle;

@synthesize lblEmergencyContactName;
@synthesize lblEmergencyContactNameBottomSeparatorView;
@synthesize lblEmergencyContactPhoneNumber;
@synthesize lblEmergencyContactPhoneNumberBottomSeparatorView;
@synthesize lblEmergencyContactEmail;
@synthesize lblEmergencyContactEmailBottomSeparatorView;


@synthesize schoolInformationContainerView;

@synthesize lblSchoolInformationTitle;

@synthesize lblSchoolName;
@synthesize lblSchoolNameBottomSeparatorView;
@synthesize lblSchoolAddress;
@synthesize lblSchoolAddressBottomSeparatorView;
@synthesize lblSchoolPhoneNumber;
@synthesize lblSchoolPhoneNumberBottomSeparatorView;
@synthesize lblSchoolCurrentGrade;
@synthesize lblSchoolCurrentGradeBottomSeparatorView;


@synthesize moreAlertsFromThisChildContainerView;
@synthesize lblMoreAlertsFromThisChild;
@synthesize btnMoreAlertsFromThisChild;


@synthesize bestFriendsInformationContainerView;

@synthesize lblBestFriendsInformationTitle;

@synthesize lblBestFriendsNames;
@synthesize lblBestFriendsNamesBottomSeparatorView;



@synthesize delayTimePopupContainerView;
@synthesize delayTimePopupBlackTransparentView;
@synthesize delayTimePopupInnerContainerView;
@synthesize lblDelayTimePopupContainerViewTitle;
@synthesize lblDelayTimePopupContainerViewInstructions;
@synthesize imageViewCloseDelayTimePopupContainerView;
@synthesize btnCloseDelayTimePopupContainerView;
@synthesize txtDelayTime;
@synthesize btnSubmitInDelayTimePopupContainerView;


@synthesize submitParentMessagePopupContainerView;
@synthesize submitParentMessagePopupBlackTransparentView;
@synthesize submitParentMessagePopupInnerContainerView;
@synthesize imageViewCloseSubmitParentMessagePopupContainerView;
@synthesize btnCloseSubmitParentMessagePopupContainerView;
@synthesize txtViewSubmitParentMessage;
@synthesize btnSendInSubmitParentMessagePopupContainerView;

//========== OTHER VARIABLES ==========//

@synthesize delayTimePickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    if(!self.boolIsLoadedFromMyAlert)
    {
        mainContainerView.hidden = true;
    }
    else if(self.boolIsLoadedFromMyAlert && [self.strIsLoadedBy isEqualToString:@"child"])
    {
        mainContainerView.hidden = true;
        moreAlertsFromThisChildContainerView.hidden = true;
    }
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    if(self.boolIsLoadedFromMyAlert && [self.strIsLoadedBy isEqualToString:@"parent"])
    {
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        numberOfPages = self.objSelectedChildAlert.arrayChildPhotoUrls.count;
        childPicturesScrollView.contentSize = CGSizeMake(childPicturesScrollView.frame.size.width * numberOfPages, childPicturesScrollView.frame.size.height);
        
        [self setUpChildPicturesSlider];
    }
    else if(self.boolIsLoadedFromMyAlert && [self.strIsLoadedBy isEqualToString:@"child"])
    {
        mainContainerView.hidden = false;
        
        CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
        mainInformationContainerViewFrame.origin.y = responseButtonsContainerView.frame.origin.y;
        mainInformationContainerView.frame = mainInformationContainerViewFrame;
        
        responseButtonsContainerView.hidden = true;
        
        mainScrollView.autoresizesSubviews = false;
        mainContainerView.autoresizesSubviews = false;
        mainInformationContainerView.autoresizesSubviews = false;
        
        CGRect mainInformationContainerViewFrame1 = mainInformationContainerView.frame;
        mainInformationContainerViewFrame1.size.height = bestFriendsInformationContainerView.frame.origin.y + bestFriendsInformationContainerView.frame.size.height + 10;
        mainInformationContainerView.frame = mainInformationContainerViewFrame1;
        
        CGRect mainContainerViewFrame = mainContainerView.frame;
        mainContainerViewFrame.size.height = mainInformationContainerView.frame.origin.y + mainInformationContainerView.frame.size.height;
        mainContainerView.frame = mainContainerViewFrame;
        
        CGRect mainScrollViewFrame = mainScrollView.frame;
        mainScrollViewFrame.size.height = mainScrollView.frame.size.height + moreAlertsFromThisChildContainerView.frame.size.height;
        mainScrollView.frame = mainScrollViewFrame;
        
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhotos])
        {
            numberOfPages = self.objSelectedChildAlert.arrayChildPhotoUrls.count;
        }
        else
        {
            numberOfPages = 1;
        }
        childPicturesScrollView.contentSize = CGSizeMake(childPicturesScrollView.frame.size.width * numberOfPages, childPicturesScrollView.frame.size.height);
        
        [self setUpChildPicturesSlider];
        
        mainScrollView.autoresizesSubviews = true;
        mainContainerView.autoresizesSubviews = true;
        mainInformationContainerView.autoresizesSubviews = true;
    }
    else
    {
        mainContainerView.hidden = false;
        
        CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
        mainInformationContainerViewFrame.origin.y = responseButtonsContainerView.frame.origin.y;
        mainInformationContainerView.frame = mainInformationContainerViewFrame;
        
        responseButtonsContainerView.hidden = true;
        
        mainScrollView.autoresizesSubviews = false;
        mainContainerView.autoresizesSubviews = false;
        mainInformationContainerView.autoresizesSubviews = false;
        
//        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForLocationInformation])
//        {
//            //ALLOW OTHER USERS TO SEE CHILD'S LOCATION HERE (CODE AND FLOW TO BE IMPLEMENTED)
//        }
        
//        CGRect parentMessageContainerViewFrame = parentMessageContainerView.frame;
//        parentMessageContainerViewFrame.origin.y = childMessageContainerView.frame.origin.y;
//        parentMessageContainerView.frame = parentMessageContainerViewFrame;
//        
//        CGRect basicInformationContainerViewFrame = basicInformationContainerView.frame;
//        basicInformationContainerViewFrame.origin.y = parentMessageContainerView.frame.origin.y + parentMessageContainerView.frame.size.height + 10;
//        basicInformationContainerView.frame = basicInformationContainerViewFrame;
//        
//        childMessageContainerView.hidden = true;
        
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForEmergencyContactInformation])
        {
            CGRect emergencyContactInformationContainerViewFrame = emergencyContactInformationContainerView.frame;
            emergencyContactInformationContainerViewFrame.origin.y = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
            emergencyContactInformationContainerView.frame = emergencyContactInformationContainerViewFrame;
        }
        else
        {
            emergencyContactInformationContainerView.hidden = true;
        }
        
        
        
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForSchoolInformation])
        {
            if(emergencyContactInformationContainerView.hidden == true)
            {
                CGRect schoolInformationContainerViewFrame = schoolInformationContainerView.frame;
                schoolInformationContainerViewFrame.origin.y = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
                schoolInformationContainerView.frame = schoolInformationContainerViewFrame;
            }
            else
            {
                CGRect schoolInformationContainerViewFrame = schoolInformationContainerView.frame;
                schoolInformationContainerViewFrame.origin.y = emergencyContactInformationContainerView.frame.origin.y + emergencyContactInformationContainerView.frame.size.height + 10;
                schoolInformationContainerView.frame = schoolInformationContainerViewFrame;
            }
        }
        else
        {
            schoolInformationContainerView.hidden = true;
        }
        
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForBestFriendsInformation])
        {
            if(schoolInformationContainerView.hidden == true)
            {
                if(emergencyContactInformationContainerView.hidden == true)
                {
                    CGRect bestFriendsInformationContainerViewFrame = bestFriendsInformationContainerView.frame;
                    bestFriendsInformationContainerViewFrame.origin.y = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
                    bestFriendsInformationContainerView.frame = bestFriendsInformationContainerViewFrame;
                }
                else
                {
                    CGRect bestFriendsInformationContainerViewFrame = bestFriendsInformationContainerView.frame;
                    bestFriendsInformationContainerViewFrame.origin.y = emergencyContactInformationContainerView.frame.origin.y + emergencyContactInformationContainerView.frame.size.height + 10;
                    bestFriendsInformationContainerView.frame = bestFriendsInformationContainerViewFrame;
                }
            }
            else
            {
                CGRect bestFriendsInformationContainerViewFrame = bestFriendsInformationContainerView.frame;
                bestFriendsInformationContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y + schoolInformationContainerView.frame.size.height + 10;
                bestFriendsInformationContainerView.frame = bestFriendsInformationContainerViewFrame;
            }
        }
        else
        {
            bestFriendsInformationContainerView.hidden = true;
        }
        
        if(bestFriendsInformationContainerView.hidden == false)
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = bestFriendsInformationContainerView.frame.origin.y + bestFriendsInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        else if(schoolInformationContainerView.hidden == false)
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = schoolInformationContainerView.frame.origin.y + schoolInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        else if(emergencyContactInformationContainerView.hidden == false)
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = emergencyContactInformationContainerView.frame.origin.y + emergencyContactInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        else
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        
        CGRect mainContainerViewFrame = mainContainerView.frame;
        mainContainerViewFrame.size.height = mainInformationContainerView.frame.origin.y + mainInformationContainerView.frame.size.height;
        mainContainerView.frame = mainContainerViewFrame;
        
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
        
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhotos])
        {
            numberOfPages = self.objSelectedChildAlert.arrayChildPhotoUrls.count;
        }
        else
        {
            numberOfPages = 1;
        }
        childPicturesScrollView.contentSize = CGSizeMake(childPicturesScrollView.frame.size.width * numberOfPages, childPicturesScrollView.frame.size.height);
        
        [self setUpChildPicturesSlider];
        
        mainScrollView.autoresizesSubviews = true;
        mainContainerView.autoresizesSubviews = true;
        mainInformationContainerView.autoresizesSubviews = true;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
    
    [childPictureSliderTimer invalidate];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(delayedMyChildAlertEvent) name:@"delayedMyChildAlertEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(raisedMyChildAlertEvent) name:@"raisedMyChildAlertEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deactivatedMyChildAlertEvent) name:@"deactivatedMyChildAlertEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedParentMessageForParentRespondAlertEvent) name:@"updatedParentMessageForParentRespondAlertEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedChildMessageForParentRespondAlertEvent) name:@"updatedChildMessageForParentRespondAlertEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)delayedMyChildAlertEvent
{
    [MySingleton sharedManager].boolIsMyChildAlertStatusUpdated = true;
    
    [self setupInitialView];
}

-(void)raisedMyChildAlertEvent
{
    lblParentMessage.text = txtViewSubmitParentMessage.text;
    
    [self btnCloseSubmitParentMessagePopupContainerViewClicked:self];
    
    [MySingleton sharedManager].boolIsMyChildAlertStatusUpdated = true;
    
    NSString *strChildAlertType;
    
    if([self.objSelectedChildAlert.strChildAlertType isEqualToString:@"1"])
    {
        self.objSelectedChildAlert.strChildAlertType = @"2";
        
        [btnDelay setImage:[UIImage imageNamed:@"delay_disabled.png"] forState:UIControlStateNormal];
        btnDelay.userInteractionEnabled = false;
        
        alertLevelAndResponsTimeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalAuthorityAlertBackgroundColor;
        
        lblAlertLevel.text = [NSString stringWithFormat:@"ALERT LEVEL : AUTHORITY"];
        strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : AUTHORITY"];
    }
    else if([self.objSelectedChildAlert.strChildAlertType isEqualToString:@"2"])
    {
        self.objSelectedChildAlert.strChildAlertType = @"3";
        
        [btnDelay setImage:[UIImage imageNamed:@"delay_disabled.png"] forState:UIControlStateNormal];
        btnDelay.userInteractionEnabled = false;
        
        [btnRaise setImage:[UIImage imageNamed:@"raise_disabled.png"] forState:UIControlStateNormal];
        btnRaise.userInteractionEnabled = false;
        
        alertLevelAndResponsTimeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalKidnappingAlertBackgroundColor;
        
        lblAlertLevel.text = [NSString stringWithFormat:@"ALERT LEVEL : KIDNAPPING"];
        strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : KIDNAPPING"];
    }
    
    NSMutableAttributedString *strAttrChildAlertType = [[NSMutableAttributedString alloc] initWithString:strChildAlertType attributes: nil];
    NSRange rangeOfAlertLevel = [strChildAlertType rangeOfString:@"ALERT LEVEL :"];
    [strAttrChildAlertType addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfAlertLevel];
    [strAttrChildAlertType addAttribute:NSFontAttributeName value:lblAlertLevelSmallFont range:rangeOfAlertLevel];
    lblAlertLevel.attributedText = strAttrChildAlertType;
}

-(void)deactivatedMyChildAlertEvent
{
    lblParentMessage.text = txtViewSubmitParentMessage.text;
    
    [self btnCloseSubmitParentMessagePopupContainerViewClicked:self];
    
    [MySingleton sharedManager].boolIsMyChildAlertStatusUpdated = true;
    
    [btnDelay setImage:[UIImage imageNamed:@"delay_disabled.png"] forState:UIControlStateNormal];
    btnDelay.userInteractionEnabled = false;
    
    [btnRaise setImage:[UIImage imageNamed:@"raise_disabled.png"] forState:UIControlStateNormal];
    btnRaise.userInteractionEnabled = false;
    
    [btnDeactivate setImage:[UIImage imageNamed:@"deactivate_disabled.png"] forState:UIControlStateNormal];
    btnDeactivate.userInteractionEnabled = false;
}

-(void)updatedParentMessageForParentRespondAlertEvent
{
    [MySingleton sharedManager].boolIsParentMessageUpdated = true;
    
    lblParentMessage.text = txtViewSubmitParentMessage.text;
    
    [self btnCloseSubmitParentMessagePopupContainerViewClicked:self];
}

-(void)updatedChildMessageForParentRespondAlertEvent
{
    [MySingleton sharedManager].boolIsChildMessageUpdated = true;
    
    lblChildMessage.text = txtViewSubmitParentMessage.text;
    
    [self btnCloseSubmitParentMessagePopupContainerViewClicked:self];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Alert Details"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    
    imageViewOptions.layer.masksToBounds = YES;
    [btnOptions addTarget:self action:@selector(btnOptionsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.boolIsChildAlertActive)
        {
            imageViewOptions.hidden = false;
            btnOptions.hidden = false;
        }
        else
        {
            imageViewOptions.hidden = true;
            btnOptions.hidden = true;
        }
    }
    else
    {
        imageViewOptions.hidden = true;
        btnOptions.hidden = true;
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnOptionsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Options" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Dismiss button tappped.
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Update Your Message" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if(self.boolIsLoadedFromMyAlert && [self.strIsLoadedBy isEqualToString:@"parent"])
        {
            self.strSubmitParentMessagePopupOpenedFor = @"update_message";
        }
        
        [self showSubmitParentMessagePopupContainerView];
    }]];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = btnOptions;
        popPresenter.sourceRect = btnOptions.bounds;
        popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    self.arrayDelayTime = [[NSMutableArray alloc]initWithObjects:@"10 Minutes", @"15 Minutes", @"20 Minutes", @"25 Minutes", @"30 Minutes",nil];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        
        lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblResponseTimeBigFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        
        lblHeaderFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblValueFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
        lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        
        lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        
        delayTimePopupContainerViewCornerRadius = 15.0f;
        btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 8.0f;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontEightSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontElevenSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 10.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontEightSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontElevenSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 10.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSeventeenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontNineSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 12.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 8.0f;
        }
        else
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSeventeenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontTenSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 15.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 8.0f;
        }
    }
    
    [btnDelay addTarget:self action:@selector(btnDelayClicked) forControlEvents:UIControlEventTouchUpInside];
    if(!self.objSelectedChildAlert.boolIsChildAlertActive || [self.objSelectedChildAlert.strChildAlertType isEqualToString:@"2"] || [self.objSelectedChildAlert.strChildAlertType isEqualToString:@"3"])
    {
        [btnDelay setImage:[UIImage imageNamed:@"delay_disabled.png"] forState:UIControlStateNormal];
        btnDelay.userInteractionEnabled = false;
    }
    
    [btnRaise addTarget:self action:@selector(btnRaiseClicked) forControlEvents:UIControlEventTouchUpInside];
    if(!self.objSelectedChildAlert.boolIsChildAlertActive || [self.objSelectedChildAlert.strChildAlertType isEqualToString:@"3"])
    {
        [btnRaise setImage:[UIImage imageNamed:@"raise_disabled.png"] forState:UIControlStateNormal];
        btnRaise.userInteractionEnabled = false;
    }
    
    [btnDeactivate addTarget:self action:@selector(btnDeactivateClicked) forControlEvents:UIControlEventTouchUpInside];
    if(!self.objSelectedChildAlert.boolIsChildAlertActive)
    {
        [btnDeactivate setImage:[UIImage imageNamed:@"deactivate_disabled.png"] forState:UIControlStateNormal];
        btnDeactivate.userInteractionEnabled = false;
    }
    
    
    
    
    lblAlertLevel.font = lblAlertLevelBigFont;
    lblAlertLevel.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAlertLevel.textAlignment = NSTextAlignmentLeft;
    NSString *strChildAlertType;
    if([self.objSelectedChildAlert.strChildAlertType isEqualToString:@"1"])
    {
        alertLevelAndResponsTimeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalParentRespondAlertBackgroundColor;
        
        lblAlertLevel.text = [NSString stringWithFormat:@"ALERT LEVEL : PARENT RESPOND"];
        strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : PARENT RESPOND"];
    }
    else if([self.objSelectedChildAlert.strChildAlertType isEqualToString:@"2"])
    {
        alertLevelAndResponsTimeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalAuthorityAlertBackgroundColor;
        
        lblAlertLevel.text = [NSString stringWithFormat:@"ALERT LEVEL : AUTHORITY"];
        strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : AUTHORITY"];
    }
    else if([self.objSelectedChildAlert.strChildAlertType isEqualToString:@"3"])
    {
        alertLevelAndResponsTimeContainer.backgroundColor = [MySingleton sharedManager].themeGlobalKidnappingAlertBackgroundColor;
        
        lblAlertLevel.text = [NSString stringWithFormat:@"ALERT LEVEL : KIDNAPPING"];
        strChildAlertType = [NSString stringWithFormat:@"ALERT LEVEL : KIDNAPPING"];
    }
    
    NSMutableAttributedString *strAttrChildAlertType = [[NSMutableAttributedString alloc] initWithString:strChildAlertType attributes: nil];
    NSRange rangeOfAlertLevel = [strChildAlertType rangeOfString:@"ALERT LEVEL :"];
    [strAttrChildAlertType addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfAlertLevel];
    [strAttrChildAlertType addAttribute:NSFontAttributeName value:lblAlertLevelSmallFont range:rangeOfAlertLevel];
    lblAlertLevel.attributedText = strAttrChildAlertType;
    
    
    lblResponseTime.font = lblResponseTimeBigFont;
    lblResponseTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblResponseTime.textAlignment = NSTextAlignmentLeft;
    
    if(self.objSelectedChildAlert.boolIsChildAlertActive)
    {
        lblResponseTime.text = [NSString stringWithFormat:@"PARENT RESPONSE TIME : %@", self.objSelectedChildAlert.strChildAlertParentResponseDateAndTime];
        NSString *strParentResponseTime = [NSString stringWithFormat:@"PARENT RESPONSE TIME : %@", self.objSelectedChildAlert.strChildAlertParentResponseDateAndTime];
        NSMutableAttributedString *strAttrParentResponseTime = [[NSMutableAttributedString alloc] initWithString:strParentResponseTime attributes: nil];
        NSRange rangeOfParentResponseTime = [strParentResponseTime rangeOfString:@"PARENT RESPONSE TIME :"];
        [strAttrParentResponseTime addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfParentResponseTime];
        [strAttrParentResponseTime addAttribute:NSFontAttributeName value:lblResponseTimeSmallFont range:rangeOfParentResponseTime];
        lblResponseTime.attributedText = strAttrParentResponseTime;
    }
    else
    {
        lblResponseTime.text = [NSString stringWithFormat:@"PARENT RESPONSE TIME : %@", self.objSelectedChildAlert.strChildAlertParentResponseDateAndTime];
        NSString *strParentResponseTime = [NSString stringWithFormat:@"PARENT RESPONSE TIME : ALERT DEACTIVATED"];
        NSMutableAttributedString *strAttrParentResponseTime = [[NSMutableAttributedString alloc] initWithString:strParentResponseTime attributes: nil];
        NSRange rangeOfParentResponseTime = [strParentResponseTime rangeOfString:@"PARENT RESPONSE TIME :"];
        [strAttrParentResponseTime addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfParentResponseTime];
        [strAttrParentResponseTime addAttribute:NSFontAttributeName value:lblResponseTimeSmallFont range:rangeOfParentResponseTime];
        lblResponseTime.attributedText = strAttrParentResponseTime;
    }

    
    imageViewLocation.layer.masksToBounds = true;
    
    lblViewLocation.font = lblHeaderFont;
    lblViewLocation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnViewLocation addTarget:self action:@selector(btnViewLocationClicked) forControlEvents:UIControlEventTouchUpInside];
    
    lblViewLocationBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    lblChildMessageTitle.font = lblHeaderFont;
    lblChildMessageTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblChildMessage.font = lblValueFont;
    lblChildMessage.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblChildMessage.numberOfLines = 3;
//    [lblChildMessage sizeToFit];
    if(self.objSelectedChildAlert.strChildAlertMessageFromChild != nil && self.objSelectedChildAlert.strChildAlertMessageFromChild.length > 0)
    {
        lblChildMessage.text = [NSString stringWithFormat:@"%@", self.objSelectedChildAlert.strChildAlertMessageFromChild];
    }
    else
    {
        lblChildMessage.text = [NSString stringWithFormat:@"-"];
    }
    
    lblChildMessageBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    lblParentMessageTitle.font = lblHeaderFont;
    lblParentMessageTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblParentMessage.font = lblValueFont;
    lblParentMessage.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblParentMessage.numberOfLines = 3;
//    [lblParentMessage sizeToFit];
    if(self.objSelectedChildAlert.strChildAlertMessageFromParent != nil && self.objSelectedChildAlert.strChildAlertMessageFromParent.length > 0)
    {
        lblParentMessage.text = [NSString stringWithFormat:@"%@", self.objSelectedChildAlert.strChildAlertMessageFromParent];
    }
    else
    {
        lblParentMessage.text = [NSString stringWithFormat:@"-"];
    }
    
    lblParentMessageBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    lblName.font = lblValueFont;
    lblName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strName != nil && self.objSelectedChildAlert.strName.length > 0)
        {
            lblName.text = [NSString stringWithFormat:@"Name : %@", self.objSelectedChildAlert.strName];
        }
        else
        {
            lblName.text = [NSString stringWithFormat:@"Name : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForName])
        {
            if(self.objSelectedChildAlert.strName != nil && self.objSelectedChildAlert.strName.length > 0)
            {
                lblName.text = [NSString stringWithFormat:@"Name : %@", self.objSelectedChildAlert.strName];
            }
            else
            {
                lblName.text = [NSString stringWithFormat:@"Name : -"];
            }
        }
        else
        {
            lblName.text = [NSString stringWithFormat:@"Name : -"];
        }
    }
    
    lblNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblPhoneNumber.font = lblValueFont;
    lblPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strPhoneNumber != nil && self.objSelectedChildAlert.strPhoneNumber.length > 0)
        {
            lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : %@", self.objSelectedChildAlert.strPhoneNumber];
        }
        else
        {
            lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhoneNumber])
        {
            if(self.objSelectedChildAlert.strPhoneNumber != nil && self.objSelectedChildAlert.strPhoneNumber.length > 0)
            {
                lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : %@", self.objSelectedChildAlert.strPhoneNumber];
            }
            else
            {
                lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
            }
        }
        else
        {
            lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
        }
    }

    lblPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblAge.font = lblValueFont;
    lblAge.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strAge != nil && self.objSelectedChildAlert.strAge.length > 0)
        {
            lblAge.text = [NSString stringWithFormat:@"Age : %@", self.objSelectedChildAlert.strAge];
        }
        else
        {
            lblAge.text = [NSString stringWithFormat:@"Age : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForAge])
        {
            if(self.objSelectedChildAlert.strAge != nil && self.objSelectedChildAlert.strAge.length > 0)
            {
                lblAge.text = [NSString stringWithFormat:@"Age : %@", self.objSelectedChildAlert.strAge];
            }
            else
            {
                lblAge.text = [NSString stringWithFormat:@"Age : -"];
            }
        }
        else
        {
            lblAge.text = [NSString stringWithFormat:@"Age : -"];
        }
    }
    
    lblAgeBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblGender.font = lblValueFont;
    lblGender.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strGender != nil && self.objSelectedChildAlert.strGender.length > 0)
        {
            lblGender.text = [NSString stringWithFormat:@"Gender : %@", self.objSelectedChildAlert.strGender];
        }
        else
        {
            lblGender.text = [NSString stringWithFormat:@"Gender : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForGender])
        {
            if(self.objSelectedChildAlert.strGender != nil && self.objSelectedChildAlert.strGender.length > 0)
            {
                lblGender.text = [NSString stringWithFormat:@"Gender : %@", self.objSelectedChildAlert.strGender];
            }
            else
            {
                lblGender.text = [NSString stringWithFormat:@"Gender : -"];
            }
        }
        else
        {
            lblGender.text = [NSString stringWithFormat:@"Gender : -"];
        }
    }
    
    lblGenderBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblHeight.font = lblValueFont;
    lblHeight.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strHeight != nil && self.objSelectedChildAlert.strHeight.length > 0)
        {
            lblHeight.text = [NSString stringWithFormat:@"Height : %@", self.objSelectedChildAlert.strHeight];
        }
        else
        {
            lblHeight.text = [NSString stringWithFormat:@"Height : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHeight])
        {
            if(self.objSelectedChildAlert.strHeight != nil && self.objSelectedChildAlert.strHeight.length > 0)
            {
                lblHeight.text = [NSString stringWithFormat:@"Height : %@", self.objSelectedChildAlert.strHeight];
            }
            else
            {
                lblHeight.text = [NSString stringWithFormat:@"Height : -"];
            }
        }
        else
        {
            lblHeight.text = [NSString stringWithFormat:@"Height : -"];
        }
    }
    
    lblHeightBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblWeight.font = lblValueFont;
    lblWeight.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strWeight != nil && self.objSelectedChildAlert.strWeight.length > 0)
        {
            lblWeight.text = [NSString stringWithFormat:@"Weight : %@", self.objSelectedChildAlert.strWeight];
        }
        else
        {
            lblWeight.text = [NSString stringWithFormat:@"Weight : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForWeight])
        {
            if(self.objSelectedChildAlert.strWeight != nil && self.objSelectedChildAlert.strWeight.length > 0)
            {
                lblWeight.text = [NSString stringWithFormat:@"Weight : %@", self.objSelectedChildAlert.strWeight];
            }
            else
            {
                lblWeight.text = [NSString stringWithFormat:@"Weight : -"];
            }
        }
        else
        {
            lblWeight.text = [NSString stringWithFormat:@"Weight : -"];
        }
    }
    
    lblWeightBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblHairColor.font = lblValueFont;
    lblHairColor.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strHairColor != nil && self.objSelectedChildAlert.strHairColor.length > 0)
        {
            lblHairColor.text = [NSString stringWithFormat:@"Hair Color : %@", self.objSelectedChildAlert.strHairColor];
        }
        else
        {
            lblHairColor.text = [NSString stringWithFormat:@"Hair Color : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHairColor])
        {
            if(self.objSelectedChildAlert.strHairColor != nil && self.objSelectedChildAlert.strHairColor.length > 0)
            {
                lblHairColor.text = [NSString stringWithFormat:@"Hair Color : %@", self.objSelectedChildAlert.strHairColor];
            }
            else
            {
                lblHairColor.text = [NSString stringWithFormat:@"Hair Color : -"];
            }
        }
        else
        {
            lblHairColor.text = [NSString stringWithFormat:@"Hair Color : -"];
        }
    }
    
    lblHairColorBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblEyeColor.font = lblValueFont;
    lblEyeColor.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strEyeColor != nil && self.objSelectedChildAlert.strEyeColor.length > 0)
        {
            lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : %@", self.objSelectedChildAlert.strEyeColor];
        }
        else
        {
            lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForEyeColor])
        {
            if(self.objSelectedChildAlert.strEyeColor != nil && self.objSelectedChildAlert.strEyeColor.length > 0)
            {
                lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : %@", self.objSelectedChildAlert.strEyeColor];
            }
            else
            {
                lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : -"];
            }
        }
        else
        {
            lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : -"];
        }
    }
    
    lblEyeColorBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblHairStyle.font = lblValueFont;
    lblHairStyle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strHairStyle != nil && self.objSelectedChildAlert.strHairStyle.length > 0)
        {
            lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : %@", self.objSelectedChildAlert.strHairStyle];
        }
        else
        {
            lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHairStyle])
        {
            if(self.objSelectedChildAlert.strHairStyle != nil && self.objSelectedChildAlert.strHairStyle.length > 0)
            {
                lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : %@", self.objSelectedChildAlert.strHairStyle];
            }
            else
            {
                lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : -"];
            }
        }
        else
        {
            lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : -"];
        }
    }
    
    lblHairStyleBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblNationality.font = lblValueFont;
    lblNationality.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedChildAlert.strNationality != nil && self.objSelectedChildAlert.strNationality.length > 0)
        {
            lblNationality.text = [NSString stringWithFormat:@"Nationality : %@", self.objSelectedChildAlert.strNationality];
        }
        else
        {
            lblNationality.text = [NSString stringWithFormat:@"Nationality : -"];
        }
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForNationality])
        {
            if(self.objSelectedChildAlert.strNationality != nil && self.objSelectedChildAlert.strNationality.length > 0)
            {
                lblNationality.text = [NSString stringWithFormat:@"Nationality : %@", self.objSelectedChildAlert.strNationality];
            }
            else
            {
                lblNationality.text = [NSString stringWithFormat:@"Nationality : -"];
            }
        }
        else
        {
            lblNationality.text = [NSString stringWithFormat:@"Nationality : -"];
        }
    }
    
    lblNationalityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblEmergencyContactInformationTitle.font = lblHeaderFont;
    lblEmergencyContactInformationTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblEmergencyContactName.font = lblValueFont;
    lblEmergencyContactName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedChildAlert.strEmergencyContactName != nil && self.objSelectedChildAlert.strEmergencyContactName.length > 0)
    {
        lblEmergencyContactName.text = [NSString stringWithFormat:@"Name : %@", self.objSelectedChildAlert.strEmergencyContactName];
    }
    else
    {
        lblEmergencyContactName.text = [NSString stringWithFormat:@"Name : -"];
    }
    
    lblEmergencyContactNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblEmergencyContactPhoneNumber.font = lblValueFont;
    lblEmergencyContactPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedChildAlert.strEmergencyContactPhoneNumber != nil && self.objSelectedChildAlert.strEmergencyContactPhoneNumber.length > 0)
    {
        lblEmergencyContactPhoneNumber.text = [NSString stringWithFormat:@"Phone : %@", self.objSelectedChildAlert.strEmergencyContactPhoneNumber];
    }
    else
    {
        lblEmergencyContactPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
    }
    
    lblEmergencyContactPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblEmergencyContactEmail.font = lblValueFont;
    lblEmergencyContactEmail.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedChildAlert.strEmergencyContactEmail != nil && self.objSelectedChildAlert.strEmergencyContactEmail.length > 0)
    {
        lblEmergencyContactEmail.text = [NSString stringWithFormat:@"Email : %@", self.objSelectedChildAlert.strEmergencyContactEmail];
    }
    else
    {
        lblEmergencyContactEmail.text = [NSString stringWithFormat:@"Email : -"];
    }
    
    lblEmergencyContactEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblSchoolInformationTitle.font = lblHeaderFont;
    lblSchoolInformationTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblSchoolName.font = lblValueFont;
    lblSchoolName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedChildAlert.strSchoolName != nil && self.objSelectedChildAlert.strSchoolName.length > 0)
    {
        lblSchoolName.text = [NSString stringWithFormat:@"School Name : %@", self.objSelectedChildAlert.strSchoolName];
    }
    else
    {
        lblSchoolName.text = [NSString stringWithFormat:@"School Name : -"];
    }
    
    lblSchoolNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblSchoolAddress.font = lblValueFont;
    lblSchoolAddress.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblSchoolAddress.numberOfLines = 3;
//    [lblSchoolAddress sizeToFit];
    if(self.objSelectedChildAlert.strSchoolAddress != nil && self.objSelectedChildAlert.strSchoolAddress.length > 0)
    {
        lblSchoolAddress.text = [NSString stringWithFormat:@"Address : %@", self.objSelectedChildAlert.strSchoolAddress];
    }
    else
    {
        lblSchoolAddress.text = [NSString stringWithFormat:@"Address : -"];
    }
    
    lblSchoolAddressBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;

    lblSchoolPhoneNumber.font = lblValueFont;
    lblSchoolPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedChildAlert.strSchoolPhoneNumber != nil && self.objSelectedChildAlert.strSchoolPhoneNumber.length > 0)
    {
        lblSchoolPhoneNumber.text = [NSString stringWithFormat:@"Phone Number : %@", self.objSelectedChildAlert.strSchoolPhoneNumber];
    }
    else
    {
        lblSchoolPhoneNumber.text = [NSString stringWithFormat:@"Phone Number : -"];
    }
    
   lblSchoolPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblSchoolCurrentGrade.font = lblValueFont;
    lblSchoolCurrentGrade.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedChildAlert.strSchoolCurrentGrade != nil && self.objSelectedChildAlert.strSchoolCurrentGrade.length > 0)
    {
        lblSchoolCurrentGrade.text = [NSString stringWithFormat:@"Current Grade : %@", self.objSelectedChildAlert.strSchoolCurrentGrade];
    }
    else
    {
        lblSchoolCurrentGrade.text = [NSString stringWithFormat:@"Current Grade : -"];
    }
    
    lblSchoolCurrentGradeBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblBestFriendsInformationTitle.font = lblHeaderFont;
    lblBestFriendsInformationTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblBestFriendsNames.font = lblValueFont;
    lblBestFriendsNames.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblBestFriendsNames.numberOfLines = 3;
//    [lblBestFriendsNames sizeToFit];
    if(self.objSelectedChildAlert.strBestFriendsNames != nil && self.objSelectedChildAlert.strBestFriendsNames.length > 0)
    {
        lblBestFriendsNames.text = [NSString stringWithFormat:@"Best Friends Names : %@", self.objSelectedChildAlert.strBestFriendsNames];
    }
    else
    {
        lblBestFriendsNames.text = [NSString stringWithFormat:@"Best Friends Names : -"];
    }
    
    lblBestFriendsNamesBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    moreAlertsFromThisChildContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
//    imageViewBack.layer.masksToBounds = YES;
    
    lblMoreAlertsFromThisChild.text = [NSString stringWithFormat:@"MORE ALERTS FROM THIS CHILD"];
    lblMoreAlertsFromThisChild.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblMoreAlertsFromThisChild.font = lblMoreAlertsFromThisChildFont;
    
    [btnMoreAlertsFromThisChild addTarget:self action:@selector(btnMoreAlertsFromThisChildClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    delayTimePopupInnerContainerView.layer.masksToBounds = YES;
    delayTimePopupInnerContainerView.layer.cornerRadius = delayTimePopupContainerViewCornerRadius;
    delayTimePopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblDelayTimePopupContainerViewTitle.font = lblDelayTimePopupContainerViewTitleFont;
    lblDelayTimePopupContainerViewTitle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    lblDelayTimePopupContainerViewInstructions.font = lblDelayTimePopupContainerViewInstructionsFont;
    lblDelayTimePopupContainerViewInstructions.text = [NSString stringWithFormat:@"Please select appropriate delay time. This time will be added to current parent response time."];
    lblDelayTimePopupContainerViewInstructions.numberOfLines = 2;
    [lblDelayTimePopupContainerViewInstructions setLineBreakMode:NSLineBreakByWordWrapping];
    lblDelayTimePopupContainerViewInstructions.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    imageViewCloseDelayTimePopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseDelayTimePopupContainerView addTarget:self action:@selector(btnCloseDelayTimePopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    delayTimePickerView = [[UIPickerView alloc] init];
    delayTimePickerView.delegate = self;
    delayTimePickerView.dataSource = self;
    delayTimePickerView.showsSelectionIndicator = YES;
    delayTimePickerView.tag = 1;
    delayTimePickerView.backgroundColor = [UIColor whiteColor];
    
    txtDelayTime.font = txtFieldFont;
    txtDelayTime.delegate = self;
    [txtDelayTime setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                 forKeyPath:@"_placeholderLabel.textColor"];
    txtDelayTime.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtDelayTime.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtDelayTime setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtDelayTime setInputView:delayTimePickerView];
    
    btnSubmitInDelayTimePopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSubmitInDelayTimePopupContainerView.layer.masksToBounds = true;
    btnSubmitInDelayTimePopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSubmitInDelayTimePopupContainerView.titleLabel.font = btnSubmitInDelayTimePopupContainerViewFont;
    [btnSubmitInDelayTimePopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSubmitInDelayTimePopupContainerView addTarget:self action:@selector(btnSubmitInDelayTimePopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    submitParentMessagePopupInnerContainerView.layer.masksToBounds = YES;
    submitParentMessagePopupInnerContainerView.layer.cornerRadius = delayTimePopupContainerViewCornerRadius;
    submitParentMessagePopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    imageViewCloseSubmitParentMessagePopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseSubmitParentMessagePopupContainerView addTarget:self action:@selector(btnCloseSubmitParentMessagePopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtViewSubmitParentMessage.layer.masksToBounds = true;
    txtViewSubmitParentMessage.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    txtViewSubmitParentMessage.layer.borderWidth = 1.0f;
    txtViewSubmitParentMessage.layer.borderColor = [MySingleton sharedManager].themeGlobalLightGreyColor.CGColor;
    txtViewSubmitParentMessage.text = @"Enter message here..";
    txtViewSubmitParentMessage.font = txtFieldFont;
    txtViewSubmitParentMessage.delegate = self;
    [txtViewSubmitParentMessage setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                forKeyPath:@"_placeholderLabel.textColor"];
    txtViewSubmitParentMessage.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewSubmitParentMessage.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewSubmitParentMessage setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    btnSendInSubmitParentMessagePopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSendInSubmitParentMessagePopupContainerView.layer.masksToBounds = true;
    btnSendInSubmitParentMessagePopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSendInSubmitParentMessagePopupContainerView.titleLabel.font = btnSubmitInDelayTimePopupContainerViewFont;
    [btnSendInSubmitParentMessagePopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSendInSubmitParentMessagePopupContainerView addTarget:self action:@selector(btnSendInSubmitParentMessagePopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Slider Methods

-(void)childPictureSliderTimerMethod:(NSTimer *)timer
{
    CGFloat pageWidth = childPicturesScrollView.frame.size.width;
    
    pageNumber++;
    
    if(pageNumber > (numberOfPages - 1))
    {
        pageNumber = 0;
    }
    
    float fractionalPage = pageNumber;
    NSInteger page = lround(fractionalPage);
    mainPageControl.currentPage = page;
    [mainPageControl reloadInputViews];
    
    if(pageNumber == 0)
    {
        [childPicturesScrollView setContentOffset:CGPointMake((pageWidth * pageNumber), 0) animated:NO];
    }
    else
    {
        [childPicturesScrollView setContentOffset:CGPointMake((pageWidth * pageNumber), 0) animated:YES];
    }
}

-(void)setUpChildPicturesSlider
{
    if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhotos])
    {
        numberOfPages = self.objSelectedChildAlert.arrayChildPhotoUrls.count;
    }
    else
    {
        numberOfPages = 1;
    }
    
    pageNumber = 0;
    
    childPictureSliderTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(childPictureSliderTimerMethod:) userInfo:nil repeats:YES];
    
    mainPageControl.userInteractionEnabled = FALSE;
    mainPageControl.numberOfPages = numberOfPages;
    mainPageControl.currentPageIndicatorTintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [childPicturesScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    childPicturesScrollView.contentSize = CGSizeMake(childPicturesScrollView.frame.size.width * numberOfPages, childPicturesScrollView.frame.size.height);
    childPicturesScrollView.delegate = self;
    
    if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhotos])
    {
        for(int i = 0; i < self.objSelectedChildAlert.arrayChildPhotoUrls.count; i++)
        {
            AsyncImageView *childPictureImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake((childPicturesScrollView.frame.size.width*i), 0, (childPicturesScrollView.frame.size.width), childPicturesScrollView.frame.size.height)];
            
            [[AsyncImageLoader sharedLoader].cache removeAllObjects];
            childPictureImageView.imageURL = [NSURL URLWithString:[self.objSelectedChildAlert.arrayChildPhotoUrls objectAtIndex:i]];
            childPictureImageView.layer.masksToBounds = YES;
            childPictureImageView.contentMode = UIViewContentModeScaleAspectFill;
            
            [childPicturesScrollView addSubview:childPictureImageView];
        }
        
        UITapGestureRecognizer *childPicturesScrollViewTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(childPicturesScrollViewTapped:)];
        childPicturesScrollView.delegate = self;
        childPicturesScrollView.userInteractionEnabled = true;
        [childPicturesScrollView addGestureRecognizer:childPicturesScrollViewTapGestureRecognizer];
    }
    else
    {
        AsyncImageView *childPictureImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, (childPicturesScrollView.frame.size.width), childPicturesScrollView.frame.size.height)];
        
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        childPictureImageView.image = [UIImage imageNamed:@"child_avatar_for_alert_details.png"];
        childPictureImageView.layer.masksToBounds = YES;
        childPictureImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [childPicturesScrollView addSubview:childPictureImageView];
    }
}

- (void) childPicturesScrollViewTapped: (UITapGestureRecognizer *)recognizer
{
    
}

#pragma mark - UIScrollView Delegate Methods

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == childPicturesScrollView)
    {
        int scrollEndPoint;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
        else
        {
            if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
            else if([MySingleton sharedManager].screenHeight == 667)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
            else if([MySingleton sharedManager].screenHeight == 736)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == childPicturesScrollView)
    {
        CGFloat pageWidth = childPicturesScrollView.frame.size.width;
        
        float fractionalPage = childPicturesScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        mainPageControl.currentPage = page;
        pageNumber = page;
        [mainPageControl reloadInputViews];
    }
}

#pragma mark - UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtDelayTime)
    {
        if(txtDelayTime.text.length == 0 )
        {
            txtDelayTime.text = [self.arrayDelayTime objectAtIndex:0];
            [delayTimePickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewSubmitParentMessage)
    {
        if ([textView.text isEqualToString:@"Enter message here.."]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewSubmitParentMessage)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Enter message here..";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayDelayTime count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == delayTimePickerView)
    {
        lblMain.text = self.arrayDelayTime[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtDelayTime.text = [self.arrayDelayTime objectAtIndex:row];
    }
}

#pragma mark - Other Methods

-(void)btnDelayClicked
{
    [self.view endEditing:YES];
    
    [self showDelayTimePopupContainerView];
}

-(void)btnRaiseClicked
{
    [self.view endEditing:YES];
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Are you sure you want to raise this alert?";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        self.strSubmitParentMessagePopupOpenedFor = @"raise";
        
        [self showSubmitParentMessagePopupContainerView];
        
    }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(void)btnDeactivateClicked
{
    [self.view endEditing:YES];
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Are you sure you want to deactivate this alert?";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        self.strSubmitParentMessagePopupOpenedFor = @"deactivate";
        
        [self showSubmitParentMessagePopupContainerView];
    }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(void)showDelayTimePopupContainerView
{
    [self.view endEditing:YES];
    
    delayTimePopupContainerView.hidden = false;
    
    [txtDelayTime becomeFirstResponder];
}

- (IBAction)btnCloseDelayTimePopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    txtDelayTime.text = @"";
    delayTimePopupContainerView.hidden = TRUE;
}

-(void)btnSubmitInDelayTimePopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(txtDelayTime.text.length > 0)
    {
        delayTimePopupContainerView.hidden = true;
        
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [dictParameters setObject:strParentId forKey:@"parent_id"];
        [dictParameters setObject:self.objSelectedChildAlert.strChildAlertID forKey:@"child_alert_id"];
//        [dictParameters setObject:txtDelayTime.text forKey:@"delayed_time"];
        
        NSString *strSelectedDelayMinutes;
        NSArray *arrayTxtDelayTimeComponents = [txtDelayTime.text componentsSeparatedByString:@" "];
        if(arrayTxtDelayTimeComponents.count > 0)
        {
            strSelectedDelayMinutes = [arrayTxtDelayTimeComponents objectAtIndex:0];
        }
        
        int intSelectedDelayMinutes;
        
        if(strSelectedDelayMinutes != nil)
        {
            intSelectedDelayMinutes = [strSelectedDelayMinutes integerValue];
        }
        else
        {
            intSelectedDelayMinutes = 0;
        }
        
//        19 May, 17 2:15 PM
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        dateFormat.dateStyle = NSDateFormatterMediumStyle;
        [dateFormat setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
        NSDate *dateParentResponseDateAndTime = [dateFormat dateFromString:self.objSelectedChildAlert.strChildAlertParentResponseDateAndTime];
        NSDate *dateParentResponseDateAndTimeAfterAddingDelayTime = [dateParentResponseDateAndTime dateByAddingTimeInterval:(intSelectedDelayMinutes*60)];
        
        self.objSelectedChildAlert.strChildAlertParentResponseDateAndTime = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:dateParentResponseDateAndTimeAfterAddingDelayTime]];
        
        [dictParameters setObject:strSelectedDelayMinutes forKey:@"delayed_time"];
        
        [[MySingleton sharedManager].dataManager delayMyChildAlert:dictParameters];
        
        txtDelayTime.text = @"";
    }
    else
    {
        if(txtDelayTime.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please select delay time."];
            });
        }
    }
}

-(void)showSubmitParentMessagePopupContainerView
{
    [self.view endEditing:YES];
    
    submitParentMessagePopupContainerView.hidden = false;
    
    [txtViewSubmitParentMessage becomeFirstResponder];
}

- (IBAction)btnCloseSubmitParentMessagePopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    txtViewSubmitParentMessage.text = @"";
    submitParentMessagePopupContainerView.hidden = TRUE;
}

-(void)btnSendInSubmitParentMessagePopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(txtViewSubmitParentMessage.text.length > 0 && ![txtViewSubmitParentMessage.text isEqualToString:@"Enter message here.."])
    {
        if(self.boolIsLoadedFromMyAlert && [self.strIsLoadedBy isEqualToString:@"parent"])
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
            [dictParameters setObject:strParentId forKey:@"parent_id"];
            [dictParameters setObject:self.objSelectedChildAlert.strChildAlertID forKey:@"child_alert_id"];
            [dictParameters setObject:txtViewSubmitParentMessage.text forKey:@"message_from_parent"];
            
            if([self.strSubmitParentMessagePopupOpenedFor isEqualToString:@"raise"])
            {
                [[MySingleton sharedManager].dataManager raiseMyChildAlert:dictParameters];
            }
            else if([self.strSubmitParentMessagePopupOpenedFor isEqualToString:@"deactivate"])
            {
                [[MySingleton sharedManager].dataManager deactivateMyChildAlert:dictParameters];
            }
            else if([self.strSubmitParentMessagePopupOpenedFor isEqualToString:@"update_message"])
            {
                [[MySingleton sharedManager].dataManager updateParentMessageForParentRespondAlert:dictParameters];
            }
        }
        else if(self.boolIsLoadedFromMyAlert && [self.strIsLoadedBy isEqualToString:@"child"])
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
            [dictParameters setObject:strChildId forKey:@"child_id"];
            [dictParameters setObject:self.objSelectedChildAlert.strChildAlertID forKey:@"child_alert_id"];
            [dictParameters setObject:txtViewSubmitParentMessage.text forKey:@"message_from_child"];
            
            [[MySingleton sharedManager].dataManager updateChildMessageForParentRespondAlert:dictParameters];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter a message to deactivate this alert."];
        });
    }
}

-(void)btnViewLocationClicked
{
    [self.view endEditing:YES];
    
    if(self.boolIsLoadedFromMyAlert)
    {
        CommonViewLocationViewController *viewController = [[CommonViewLocationViewController alloc] init];
        viewController.objSelectedChildAlert = self.objSelectedChildAlert;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else
    {
        if([self.objSelectedChildAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForLocationInformation])
        {
            CommonViewLocationViewController *viewController = [[CommonViewLocationViewController alloc] init];
            viewController.objSelectedChildAlert = self.objSelectedChildAlert;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Parent(s) of this child doesn't want to show their child's location."];
            });
        }
    }
}

- (IBAction)btnMoreAlertsFromThisChildClicked:(id)sender
{
    [self.view endEditing:YES];
    
    CommonMoreAlertsFromThisChildViewController *viewController = [[CommonMoreAlertsFromThisChildViewController alloc] init];
    viewController.strSelectedChildId = self.objSelectedChildAlert.strChildID;
    viewController.boolIsLoadedFromMyAlert = self.boolIsLoadedFromMyAlert;
    viewController.strIsLoadedBy = self.strIsLoadedBy;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
