//
//  ChildGenerateCommunityAlertViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 13/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ChildGenerateCommunityAlertViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface ChildGenerateCommunityAlertViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ChildGenerateCommunityAlertViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize lblInstructions;
@synthesize txtViewMessageContainerView;
@synthesize txtViewMessage;
@synthesize btnGenerate;

//========== OTHER VARIABLES ==========//

@synthesize strLatitude;
@synthesize strLongitude;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(generatedCommunityAlertByChildIdEvent) name:@"generatedCommunityAlertByChildIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)generatedCommunityAlertByChildIdEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Community Respond Alert Generated" withDetails:@"You have successfully generated an alert for the community."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Community Respond"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionsFont, *txtFieldFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    txtViewMessageContainerView.layer.masksToBounds = true;
    txtViewMessageContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    txtViewMessageContainerView.layer.borderWidth = 1.0f;
    txtViewMessageContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    
    txtViewMessage.text = @"Enter your message here";
    txtViewMessage.font = txtFieldFont;
    txtViewMessage.delegate = self;
    txtViewMessage.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewMessage.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewMessage setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    btnGenerate.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnGenerate.layer.masksToBounds = true;
    btnGenerate.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnGenerate.titleLabel.font = btnFont;
    [btnGenerate setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnGenerate addTarget:self action:@selector(btnGenerateClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager startMonitoringSignificantLocationChanges];
    locationManager.delegate = self;
    
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER)
    {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
    }
#endif
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    if (![CLLocationManager locationServicesEnabled])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:[MySingleton sharedManager].strLocationDisabledErrorTitle withDetails:[MySingleton sharedManager].strLocationDisabledErrorMessage];
        });
    }
}

#pragma mark - CLLocationManager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError Called.");
    
//    [locationManager startUpdatingLocation];
    
    [appDelegate dismissGlobalHUD];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"didUpdateLocations Called.");
    
    currentLocation = [locations objectAtIndex:0];
    
    if (currentLocation != nil)
    {
        strLatitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        strLongitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
        [MySingleton sharedManager].dataManager.strLocationLatitude = strLatitude;
        [MySingleton sharedManager].dataManager.strLocationLongitude = strLongitude;
        
        [locationManager stopUpdatingLocation];
    }
    else
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = [MySingleton sharedManager].strLocationDisabledErrorTitle;
        alertViewController.message = [MySingleton sharedManager].strLocationDisabledErrorMessage;
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [self presentViewController:alertViewController animated:YES completion:nil];
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewMessage)
    {
        if ([textView.text isEqualToString:@"Enter your message here"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewMessage)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Enter your message here";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - Other Methods

-(IBAction)btnGenerateClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(txtViewMessage.text.length <= 0 || [txtViewMessage.text isEqualToString:@"Enter your message here"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter message"];
        });
    }
    else
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [dictParameters setObject:strChildId forKey:@"child_id"];
        [dictParameters setObject:txtViewMessage.text forKey:@"child_message"];
        [dictParameters setObject:strLatitude forKey:@"latitude"];
        [dictParameters setObject:strLongitude forKey:@"longitude"];
        
        [[MySingleton sharedManager].dataManager generateCommunityAlertByChildId:dictParameters];
    }
}

@end

