//
//  ParentChildPrivacySettingsViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Child.h"

@interface ParentChildPrivacySettingsViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstruction1;

@property (nonatomic,retain) IBOutlet UIButton *btnParentRespondAlertForOtherUsers;
@property (nonatomic,retain) IBOutlet UIButton *btnAuthorityAlertForOtherUsers;
@property (nonatomic,retain) IBOutlet UIButton *btnKidnappingAlertForOtherUsers;

@property (nonatomic,retain) IBOutlet UIView *separatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstruction2;

@property (nonatomic,retain) IBOutlet UIButton *btnAuthorityAlertForAuthority;
@property (nonatomic,retain) IBOutlet UIButton *btnKidnappingAlertForAuthority;

@property (nonatomic,retain) IBOutlet UIView *informationSelectorPopupContainerView;
@property (nonatomic,retain) IBOutlet UIView *informationSelectorBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *informationSelectorInnerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblInformationSelectorPopupContainerViewInstructions;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseInformationSelectorPopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseInformationSelectorPopupContainerView;
@property (nonatomic,retain) IBOutlet UITableView *informationSelectorTableView;
@property (nonatomic,retain) IBOutlet UIButton *btnSelectAllInInformationSelectorPopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnSaveInInformationSelectorPopupContainerView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) Child *objSelectedChild;

@property (nonatomic,retain) NSMutableArray *dataRows;

@property (nonatomic,retain) NSString *strInformationSelectorPopupOpenedFor;
@property (nonatomic,retain) NSMutableArray *arraySelectedInformationTypes;

@end
