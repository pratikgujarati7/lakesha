//
//  ParentMyChildsAlertsViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface ParentMyChildsAlertsViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewSearch;
@property (nonatomic,retain) IBOutlet UIButton *btnSearch;

@property (nonatomic,retain) IBOutlet UIView *searchTextContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtSearch;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseSearchTextContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseSearchTextContainerView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;

@property (nonatomic,retain) IBOutlet UIView *bottomContainerView;

@property (nonatomic,retain) IBOutlet UIView *parentRespondAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblParentRespondAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewParentRespondAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnParentRespondAlerts;

@property (nonatomic,retain) IBOutlet UIView *communityRespondAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblCommunityRespondAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCommunityRespondAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnCommunityRespondAlerts;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;

@property (nonatomic,retain) NSString *strSelectedTab;

@end
