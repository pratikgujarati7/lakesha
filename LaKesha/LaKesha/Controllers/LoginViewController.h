//
//  LoginViewController.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 22/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface LoginViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewMainLogo;

@property (nonatomic,retain) IBOutlet UILabel *lblTagLineTitle;
@property (nonatomic,retain) IBOutlet UILabel *lblTagLine;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail;
@property (nonatomic,retain) IBOutlet UIView *txtEmailBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewPassword;
@property (nonatomic,retain) IBOutlet UITextField *txtPassword;
@property (nonatomic,retain) IBOutlet UIView *txtPasswordBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIButton *btnForgetPassword;

@property (nonatomic,retain) IBOutlet UIButton *btnLogin;
@property (nonatomic,retain) IBOutlet UIButton *btnRegister;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSString *strUserType;

@end
