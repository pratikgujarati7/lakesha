//
//  GuestLoginViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 28/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "GuestLoginViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "CommonNearByAlertsViewController.h"

@interface GuestLoginViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation GuestLoginViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize imageViewBack;
@synthesize btnBack;

@synthesize imageViewMainLogo;

@synthesize lblTagLineTitle;
@synthesize lblTagLine;

@synthesize areYouAnInternationalGuestView;
@synthesize lblAreYouAnInternationalGuest;

@synthesize imageViewLocalGuest;
@synthesize lblLocalGuest;
@synthesize btnLocalGuest;

@synthesize imageViewAnInternationalGuest;
@synthesize lblAnInternationalGuest;
@synthesize btnAnInternationalGuest;

@synthesize internationalGuestContainerView;

@synthesize imageViewContinent;
@synthesize txtContinent;
@synthesize imageViewContinentDropDownArrow;
@synthesize txtContinentBottomSeparatorView;

@synthesize imageViewEmail_IG;
@synthesize txtEmail_IG;
@synthesize txtEmail_IGBottomSeparatorView;

@synthesize imageViewZipCode_IG;
@synthesize txtZipCode_IG;
@synthesize txtZipCode_IGBottomSeparatorView;

@synthesize localGuestContainerView;

@synthesize imageViewEmail_LG;
@synthesize txtEmail_LG;
@synthesize txtEmail_LGBottomSeparatorView;

@synthesize imageViewState;
@synthesize txtState;
@synthesize imageViewStateDropDownArrow;
@synthesize txtStateBottomSeparatorView;

@synthesize imageViewCity;
@synthesize txtCity;
@synthesize imageViewCityDropDownArrow;
@synthesize txtCityBottomSeparatorView;

@synthesize imageViewZipCode_LG;
@synthesize txtZipCode_LG;
@synthesize txtZipCode_LGBottomSeparatorView;


@synthesize btnRegister;

//========== OTHER VARIABLES ==========//

@synthesize continentPickerView;

@synthesize statePickerView;

@synthesize cityPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
//    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
    
    mainContainerView.autoresizesSubviews = false;
    
    internationalGuestContainerView.hidden = true;
    localGuestContainerView.hidden = false;
    
    CGRect registerButtonFrame = btnRegister.frame;
    registerButtonFrame.origin.y = localGuestContainerView.frame.origin.y + localGuestContainerView.frame.size.height + 20;
    btnRegister.frame = registerButtonFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = btnRegister.frame.origin.y + btnRegister.frame.size.height + 20;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllStatesEvent) name:@"gotAllStatesEvent" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(internationalGuestSignedUpEvent) name:@"internationalGuestSignedUpEvent" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(guestSignedUpEvent) name:@"guestSignedUpEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllStatesEvent
{
}

-(void)internationalGuestSignedUpEvent
{
    [self navigateToGuestHomeViewController];
}

-(void)guestSignedUpEvent
{
    [self navigateToGuestHomeViewController];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInspiringFeatureFont, *lblDetailsFont, *txtFieldFont, *btnForgetPasswordFont, *btnFont, *lblFont, *lblHeaderFont;
        
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInspiringFeatureFont = [MySingleton sharedManager].themeFontTwentyTwoSizeBold;
        lblDetailsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
        lblFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblHeaderFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontNineteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            lblFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontTwentySizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
    }
    
    imageViewBack.layer.masksToBounds = true;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblTagLineTitle.font = lblInspiringFeatureFont;
    lblTagLineTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblTagLine.font = lblDetailsFont;
    lblTagLine.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    //GUEST
    
    lblTagLineTitle.text = @"As a Guest";
    lblTagLine.text = @"Protecting our future!";
    
    lblAreYouAnInternationalGuest.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAreYouAnInternationalGuest.font = lblHeaderFont;
    lblAreYouAnInternationalGuest.textAlignment = NSTextAlignmentLeft;
    
    imageViewLocalGuest.layer.masksToBounds = true;
    imageViewLocalGuest.image = [UIImage imageNamed:@"radio_selected.png"];
    
    lblLocalGuest.font = lblFont;
    lblLocalGuest.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnLocalGuest addTarget:self action:@selector(btnLocalGuestClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewAnInternationalGuest.layer.masksToBounds = true;
    imageViewAnInternationalGuest.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    lblAnInternationalGuest.font = lblFont;
    lblAnInternationalGuest.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnAnInternationalGuest addTarget:self action:@selector(btnAnInternationalGuestClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //LOCAL GUEST
    self.boolIsAnInternationalGuest = false;
    
    localGuestContainerView.backgroundColor = [UIColor clearColor];
    
    self.arrayContinent = [[NSMutableArray alloc]initWithObjects:@"Asia", @"Africa", @"North America", @"South America", @"Antarctica", @"Europe", @"Australia", nil];
        
    continentPickerView = [[UIPickerView alloc] init];
    continentPickerView.delegate = self;
    continentPickerView.dataSource = self;
    continentPickerView.showsSelectionIndicator = YES;
    continentPickerView.tag = 1;
    continentPickerView.backgroundColor = [UIColor whiteColor];
    
    [self setupTextfield:txtContinent WithBottomSeparatorView:txtContinentBottomSeparatorView];
    
    [txtContinent setInputView:continentPickerView];
    
    imageViewContinentDropDownArrow.layer.masksToBounds = true;
    
//    txtEmail_LG.font = txtFieldFont;
//    txtEmail_LG.delegate = self;
//    [txtEmail_LG setValue:[MySingleton sharedManager].textfieldPlaceholderColor
//            forKeyPath:@"_placeholderLabel.textColor"];
//    txtEmail_LG.textColor = [MySingleton sharedManager].textfieldTextColor;
//    txtEmail_LG.tintColor = [MySingleton sharedManager].textfieldTextColor;
//    [txtEmail_LG setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtEmail_LG setKeyboardType:UIKeyboardTypeEmailAddress];
//    txtEmail_LGBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    [self setupTextfield:txtEmail_LG WithBottomSeparatorView:txtEmail_LGBottomSeparatorView];
    
//    txtZipCode_LG.font = txtFieldFont;
//    txtZipCode_LG.delegate = self;
//    [txtZipCode_LG setValue:[MySingleton sharedManager].textfieldPlaceholderColor
//               forKeyPath:@"_placeholderLabel.textColor"];
//    txtZipCode_LG.textColor = [MySingleton sharedManager].textfieldTextColor;
//    txtZipCode_LG.tintColor = [MySingleton sharedManager].textfieldTextColor;
//    [txtZipCode_LG setAutocorrectionType:UITextAutocorrectionTypeNo];
//    txtZipCode_LGBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    [txtZipCode_LG setKeyboardType:UIKeyboardTypeNumberPad];
    [self setupTextfield:txtZipCode_LG WithBottomSeparatorView:txtZipCode_LGBottomSeparatorView];
    
    //INTERNATIONAL GUEST
    
    internationalGuestContainerView.backgroundColor = [UIColor clearColor];
    
    [txtEmail_IG setKeyboardType:UIKeyboardTypeEmailAddress];
    [self setupTextfield:txtEmail_IG WithBottomSeparatorView:txtEmail_IGBottomSeparatorView];
    
        //STATE PICKER VIEW
    
    statePickerView = [[UIPickerView alloc] init];
    statePickerView.delegate = self;
    statePickerView.dataSource = self;
    statePickerView.showsSelectionIndicator = YES;
    statePickerView.tag = 2;
    statePickerView.backgroundColor = [UIColor whiteColor];
    
    [self setupTextfield:txtState WithBottomSeparatorView:txtStateBottomSeparatorView];
    
    [txtState setInputView:statePickerView];
    
    imageViewStateDropDownArrow.layer.masksToBounds = true;
    
        //CITY PICKER VIEW
    
    cityPickerView = [[UIPickerView alloc] init];
    cityPickerView.delegate = self;
    cityPickerView.dataSource = self;
    cityPickerView.showsSelectionIndicator = YES;
    cityPickerView.tag = 3;
    cityPickerView.backgroundColor = [UIColor whiteColor];
    
    [self setupTextfield:txtCity WithBottomSeparatorView:txtCityBottomSeparatorView];
    
    [txtCity setInputView:cityPickerView];
    
    imageViewCityDropDownArrow.layer.masksToBounds = true;
    
    [txtZipCode_IG setKeyboardType:UIKeyboardTypeNumberPad];
    [self setupTextfield:txtZipCode_IG WithBottomSeparatorView:txtZipCode_IGBottomSeparatorView];
    
    btnRegister.layer.borderWidth = 1.0f;
    btnRegister.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnRegister.layer.masksToBounds = true;
    btnRegister.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnRegister.titleLabel.font = btnFont;
    [btnRegister setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnRegister addTarget:self action:@selector(btnRegisterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [[MySingleton sharedManager].dataManager getAllStates];
}

- (void)setupTextfield:(UITextField *)textField WithBottomSeparatorView:(UIView *)textFieldBottomSeparatorView
{
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    textField.font = txtFieldFont;
    textField.delegate = self;
    [textField setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [MySingleton sharedManager].textfieldTextColor;
    textField.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    textFieldBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtContinent)
    {
        if(txtContinent.text.length == 0 )
        {
            txtContinent.text = [self.arrayContinent objectAtIndex:0];
            [continentPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
    else if(textField == txtState)
    {
        if(txtState.text.length == 0 )
        {
            if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
            {
                self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:0];
                txtState.text = self.objSelectedState.strStateName;
                txtCity.userInteractionEnabled = true;
                [statePickerView selectRow:0 inComponent:0 animated:YES];
                [cityPickerView reloadAllComponents];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No States Added" withDetails:@"Currently we don't have any states added."];
            }
        }
    }
    else if(textField == txtCity)
    {
        if(txtCity.text.length == 0 )
        {
            if(self.objSelectedState.arrayCity.count > 0)
            {
                self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:0];
                txtCity.text = self.objSelectedCity.strCityName;
                [cityPickerView selectRow:0 inComponent:0 animated:YES];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayContinent count];
    }
    else if(pickerView.tag == 2)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.arrayStates count];
    }
    else if(pickerView.tag == 3)
    {
        rowsInComponent = [self.objSelectedState.arrayCity count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == continentPickerView)
    {
        lblMain.text = self.arrayContinent[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == statePickerView)
    {
        State *objState = [MySingleton sharedManager].dataManager.arrayStates[row];
        lblMain.text = objState.strStateName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == cityPickerView)
    {
        City *objCity = self.objSelectedState.arrayCity[row];
        lblMain.text = objCity.strCityName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtContinent.text = [self.arrayContinent objectAtIndex:row];
    }
    else if(pickerView.tag == 2)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:row];
        txtState.text = self.objSelectedState.strStateName;
        [cityPickerView reloadAllComponents];
        
        if(self.objSelectedState.arrayCity.count > 0)
        {
            [cityPickerView selectRow:0 inComponent:0 animated:NO];
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
        }
        else
        {
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
            
            [self.view endEditing:true];
            [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
        }
    }
    else if(pickerView.tag == 3)
    {
        self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:row];
        txtCity.text = self.objSelectedCity.strCityName;
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAnInternationalGuestClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAnInternationalGuest = true;
    
    imageViewLocalGuest.image = [UIImage imageNamed:@"radio_deselected.png"];
    imageViewAnInternationalGuest.image = [UIImage imageNamed:@"radio_selected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    localGuestContainerView.hidden = true;
    internationalGuestContainerView.hidden = false;
    
    CGRect registerButtonFrame = btnRegister.frame;
    registerButtonFrame.origin.y = internationalGuestContainerView.frame.origin.y + internationalGuestContainerView.frame.size.height + 20;
    btnRegister.frame = registerButtonFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = btnRegister.frame.origin.y + btnRegister.frame.size.height + 20;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

- (IBAction)btnLocalGuestClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAnInternationalGuest = false;
    
    imageViewLocalGuest.image = [UIImage imageNamed:@"radio_selected.png"];
    imageViewAnInternationalGuest.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    internationalGuestContainerView.hidden = true;
    localGuestContainerView.hidden = false;
    
    CGRect registerButtonFrame = btnRegister.frame;
    registerButtonFrame.origin.y = localGuestContainerView.frame.origin.y + localGuestContainerView.frame.size.height + 20;
    btnRegister.frame = registerButtonFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = btnRegister.frame.origin.y + btnRegister.frame.size.height + 20;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

- (IBAction)btnRegisterClicked:(id)sender
{
    [self.view endEditing:true];
    
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if (self.boolIsAnInternationalGuest == true)
    {
        if(txtContinent.text.length > 0 && txtEmail_IG.text.length > 0 && txtZipCode_IG.text.length > 0)
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            [dictParameters setObject:txtEmail_IG.text forKey:@"email"];
            [dictParameters setObject:txtContinent.text forKey:@"continent"];
            [dictParameters setObject:txtZipCode_IG.text forKey:@"zipcode"];
            
            [[MySingleton sharedManager].dataManager internationalGuestSignUp:dictParameters];
        }
        else
        {
            if(txtContinent.text.length <= 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please select a continent"];
                });
            }
            else if(txtEmail_IG.text.length <= 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter email"];
                });
            }
            else if(![objUtility isValidEmailAddress:txtEmail_IG.text])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Invalid email address"];
                });
            }
            else if(txtZipCode_IG.text.length <= 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter zipcode"];
                });
            }
        }
    }
    else
    {
        if(txtEmail_LG.text.length > 0 && txtState.text.length > 0 && txtCity.text.length > 0 && txtZipCode_LG.text.length > 0)
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            [dictParameters setObject:txtEmail_LG.text forKey:@"email"];
            [dictParameters setObject:txtState.text forKey:@"state"];
            [dictParameters setObject:self.objSelectedState.strStateID forKey:@"state_id"];
            [dictParameters setObject:txtCity.text forKey:@"city"];
            [dictParameters setObject:self.objSelectedCity.strCityID forKey:@"city_id"];
            [dictParameters setObject:txtZipCode_LG.text forKey:@"zipcode"];
        
            [[MySingleton sharedManager].dataManager guestSignUp:dictParameters];
        }
        else
        {
            if(txtEmail_LG.text.length <= 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter email"];
                });
            }
            else if(txtState.text.length <= 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please select a state"];
                });
            }
            else if(txtCity.text.length <= 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please select a city"];
                });
            }
            else if(![objUtility isValidEmailAddress:txtEmail_LG.text])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Invalid email address"];
                });
            }
            else if(txtZipCode_LG.text.length <= 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter zipcode"];
                });
            }
        }
    }
}

- (void)navigateToGuestHomeViewController
{
    [self.view endEditing:true];
    
    CommonNearByAlertsViewController *viewController = [[CommonNearByAlertsViewController alloc] init];
    viewController.strLoadedFor = @"2";
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

@end
