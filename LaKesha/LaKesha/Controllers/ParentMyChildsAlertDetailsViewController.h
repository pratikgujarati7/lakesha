//
//  ParentMyChildsAlertDetailsViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildAlert.h"

@interface ParentMyChildsAlertDetailsViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewOptions;
@property (nonatomic,retain) IBOutlet UIButton *btnOptions;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIScrollView *childPicturesScrollView;
@property (nonatomic,retain) IBOutlet UIPageControl *mainPageControl;

@property (nonatomic,retain) IBOutlet UIView *responseButtonsContainerView;

@property (nonatomic,retain) IBOutlet UIButton *btnDelay;
@property (nonatomic,retain) IBOutlet UIButton *btnRaise;
@property (nonatomic,retain) IBOutlet UIButton *btnDeactivate;

@property (nonatomic,retain) IBOutlet UIView *mainInformationContainerView;


@property (nonatomic, retain) IBOutlet UIView *alertLevelAndResponsTimeContainer;
@property (nonatomic, retain) IBOutlet UILabel *lblAlertLevel;
@property (nonatomic, retain) IBOutlet UILabel *lblResponseTime;
@property (nonatomic,retain) IBOutlet UIView *lblResponseTimeBottomSeparatorView;



@property (nonatomic,retain) IBOutlet UIView *viewLocationContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewLocation;
@property (nonatomic,retain) IBOutlet UILabel *lblViewLocation;
@property (nonatomic,retain) IBOutlet UIView *lblViewLocationBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIButton *btnViewLocation;



@property (nonatomic,retain) IBOutlet UIView *childMessageContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblChildMessageTitle;

@property (nonatomic,retain) IBOutlet UILabel *lblChildMessage;
@property (nonatomic,retain) IBOutlet UIView *lblChildMessageBottomSeparatorView;



@property (nonatomic,retain) IBOutlet UIView *parentMessageContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblParentMessageTitle;

@property (nonatomic,retain) IBOutlet UILabel *lblParentMessage;
@property (nonatomic,retain) IBOutlet UIView *lblParentMessageBottomSeparatorView;



@property (nonatomic,retain) IBOutlet UIView *basicInformationContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblName;
@property (nonatomic,retain) IBOutlet UIView *lblNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *lblPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblAge;
@property (nonatomic,retain) IBOutlet UIView *lblAgeBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblGender;
@property (nonatomic,retain) IBOutlet UIView *lblGenderBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblHeight;
@property (nonatomic,retain) IBOutlet UIView *lblHeightBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblWeight;
@property (nonatomic,retain) IBOutlet UIView *lblWeightBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblHairColor;
@property (nonatomic,retain) IBOutlet UIView *lblHairColorBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblEyeColor;
@property (nonatomic,retain) IBOutlet UIView *lblEyeColorBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblHairStyle;
@property (nonatomic,retain) IBOutlet UIView *lblHairStyleBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblNationality;
@property (nonatomic,retain) IBOutlet UIView *lblNationalityBottomSeparatorView;


@property (nonatomic,retain) IBOutlet UIView *emergencyContactInformationContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyContactInformationTitle;

@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyContactName;
@property (nonatomic,retain) IBOutlet UIView *lblEmergencyContactNameBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyContactPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *lblEmergencyContactPhoneNumberBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyContactEmail;
@property (nonatomic,retain) IBOutlet UIView *lblEmergencyContactEmailBottomSeparatorView;


@property (nonatomic,retain) IBOutlet UIView *schoolInformationContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolInformationTitle;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolName;
@property (nonatomic,retain) IBOutlet UIView *lblSchoolNameBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblSchoolAddress;
@property (nonatomic,retain) IBOutlet UIView *lblSchoolAddressBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblSchoolPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *lblSchoolPhoneNumberBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblSchoolCurrentGrade;
@property (nonatomic,retain) IBOutlet UIView *lblSchoolCurrentGradeBottomSeparatorView;


@property (nonatomic,retain) IBOutlet UIView *bestFriendsInformationContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblBestFriendsInformationTitle;

@property (nonatomic,retain) IBOutlet UILabel *lblBestFriendsNames;
@property (nonatomic,retain) IBOutlet UIView *lblBestFriendsNamesBottomSeparatorView;


@property (nonatomic,retain) IBOutlet UIView *moreAlertsFromThisChildContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblMoreAlertsFromThisChild;
@property (nonatomic,retain) IBOutlet UIButton *btnMoreAlertsFromThisChild;


@property (nonatomic,retain) IBOutlet UIView *delayTimePopupContainerView;
@property (nonatomic,retain) IBOutlet UIView *delayTimePopupBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *delayTimePopupInnerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblDelayTimePopupContainerViewTitle;
@property (nonatomic,retain) IBOutlet UILabel *lblDelayTimePopupContainerViewInstructions;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseDelayTimePopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseDelayTimePopupContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtDelayTime;
@property (nonatomic,retain) IBOutlet UIButton *btnSubmitInDelayTimePopupContainerView;



@property (nonatomic,retain) IBOutlet UIView *submitParentMessagePopupContainerView;
@property (nonatomic,retain) IBOutlet UIView *submitParentMessagePopupBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *submitParentMessagePopupInnerContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseSubmitParentMessagePopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseSubmitParentMessagePopupContainerView;
@property (nonatomic,retain) IBOutlet UITextView *txtViewSubmitParentMessage;
@property (nonatomic,retain) IBOutlet UIButton *btnSendInSubmitParentMessagePopupContainerView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) ChildAlert *objSelectedChildAlert;

@property (nonatomic,assign) BOOL boolIsLoadedFromMyAlert;
@property (nonatomic,retain) NSString *strIsLoadedBy;

@property (nonatomic,retain) NSMutableArray *arrayDelayTime;
@property (nonatomic,retain) UIPickerView *delayTimePickerView;

@property (nonatomic,assign) NSString *strSubmitParentMessagePopupOpenedFor;

@end
