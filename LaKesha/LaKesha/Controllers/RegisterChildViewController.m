//
//  RegisterChildViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 02/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "RegisterChildViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "CommonVerifyPhoneNumberViewController.h"

@interface RegisterChildViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation RegisterChildViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize lblPassword;
@synthesize txtPassword;
@synthesize txtPasswordBottomSeparatorView;

@synthesize lblName;
@synthesize txtName;
@synthesize txtNameBottomSeparatorView;

@synthesize lblPhoneNumber;
@synthesize txtPhoneNumber;
@synthesize txtPhoneNumberBottomSeparatorView;

@synthesize lblAge;
@synthesize txtAge;
@synthesize txtAgeBottomSeparatorView;

@synthesize lblGender;
@synthesize txtGender;
@synthesize txtGenderBottomSeparatorView;
@synthesize imageViewGenderDropDownArrow;

@synthesize lblHeight;
@synthesize txtHeight;
@synthesize txtHeightBottomSeparatorView;

@synthesize lblWeight;
@synthesize txtWeight;
@synthesize txtWeightBottomSeparatorView;

@synthesize lblPhotos;
@synthesize txtPhotos;
@synthesize txtPhotosBottomSeparatorView;

@synthesize uploadPhotosContainerView;
@synthesize imageViewUploadPhotos;
@synthesize lblUploadPhotos;
@synthesize btnUploadPhotos;

@synthesize lblHairColor;
@synthesize txtHairColor;
@synthesize txtHairColorBottomSeparatorView;
@synthesize imageViewHairColorDropDownArrow;

@synthesize lblHairStyle;
@synthesize txtHairStyle;
@synthesize txtHairStyleBottomSeparatorView;
@synthesize imageViewHairStyleDropDownArrow;

@synthesize lblEyeColor;
@synthesize txtEyeColor;
@synthesize txtEyeColorBottomSeparatorView;
@synthesize imageViewEyeColorDropDownArrow;

@synthesize lblNationality;
@synthesize txtNationality;
@synthesize txtNationalityBottomSeparatorView;

@synthesize lblEmergencyContactInformation;

@synthesize lblEmergencyName;
@synthesize txtEmergencyName;
@synthesize txtEmergencyNameBottomSeparatorView;

@synthesize lblEmergencyPhoneNumber;
@synthesize txtEmergencyPhoneNumber;
@synthesize txtEmergencyPhoneNumberBottomSeparatorView;

@synthesize lblEmergencyEmail;
@synthesize txtEmergencyEmail;
@synthesize txtEmergencyEmailBottomSeparatorView;

@synthesize lblDoYouWantToAddSchoolInformationContainerView;
@synthesize lblDoYouWantToAddSchoolInformation;

@synthesize imageViewDoNotAddSchoolInformation;
@synthesize lblDoNotAddSchoolInformation;
@synthesize btnDoNotAddSchoolInformation;

@synthesize imageViewAddSchoolInformation;
@synthesize lblAddSchoolInformation;
@synthesize btnAddSchoolInformation;

@synthesize schoolInformationContainerView;

@synthesize lblSchoolInformation;

@synthesize lblSchoolName;
@synthesize txtSchoolName;
@synthesize txtSchoolNameBottomSeparatorView;

@synthesize lblSchoolAddress;
@synthesize txtViewSchoolAddress;
@synthesize txtViewSchoolAddressBottomSeparatorView;

@synthesize lblSchoolPhoneNumber;
@synthesize txtSchoolPhoneNumber;
@synthesize txtSchoolPhoneNumberBottomSeparatorView;

@synthesize lblSchoolCurrentGrade;
@synthesize txtSchoolCurrentGrade;
@synthesize txtSchoolCurrentGradeBottomSeparatorView;

@synthesize compulsoryAddressContainerView;

@synthesize lblSchoolState;
@synthesize txtSchoolState;
@synthesize txtSchoolStateBottomSeparatorView;
@synthesize imageViewSchoolStateDropDownArrow;

@synthesize lblSchoolCity;
@synthesize txtSchoolCity;
@synthesize txtSchoolCityBottomSeparatorView;
@synthesize imageViewSchoolCityDropDownArrow;

@synthesize lblSchoolZipCode;
@synthesize txtSchoolZipCode;
@synthesize txtSchoolZipCodeBottomSeparatorView;

@synthesize bottomInformationContainerView;

@synthesize lblBestFriendsInformation;

@synthesize lblBestFriendsNames;
@synthesize txtViewBestFriendsNames;
@synthesize txtViewBestFriendsNamesBottomSeparatorView;

@synthesize btnRegister;

@synthesize otherPopupContainerView;
@synthesize otherPopupBlackTransparentView;
@synthesize otherPopupInnerContainerView;
@synthesize lblOtherPopupContainerViewTitle;
@synthesize imageViewCloseOtherPopupContainerView;
@synthesize btnCloseOtherPopupContainerView;
@synthesize txtOther;
@synthesize btnSaveInOtherPopupContainerView;

//========== OTHER VARIABLES ==========//

@synthesize keyboardDoneButtonView;

@synthesize genderPickerView;

@synthesize hairColorPickerView;

@synthesize hairStylePickerView;

@synthesize eyeColorPickerView;

@synthesize statePickerView;

@synthesize cityPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    schoolInformationContainerView.hidden = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllStatesEvent) name:@"gotAllStatesEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(childSignedUpEvent) name:@"childSignedUpEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllStatesEvent
{
    if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
    {
        txtSchoolState.userInteractionEnabled = true;
    }
    else
    {
        txtSchoolState.userInteractionEnabled = false;
    }
}

-(void)childSignedUpEvent
{
//    [self navigateToChildHomeViewController];
    [self navigateToCommonVerifyPhoneNumberViewController];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = true;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Child Registration"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

- (IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    keyboardDoneButtonView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *flex1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)];
    [doneButton1 setTitleTextAttributes:@{ NSFontAttributeName: [MySingleton sharedManager].themeFontTwentySizeRegular,NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex1,doneButton1, nil]];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    self.arrayGender = [[NSMutableArray alloc]initWithObjects:@"Male",@"Female",nil];
    
    self.arrayHairColor = [[NSMutableArray alloc]initWithObjects:@"Black", @"Other", nil];
    self.arrayHairStyle = [[NSMutableArray alloc]initWithObjects:@"Long", @"Medium", @"Short", @"Dreads", @"Braids", @"Micro braids", @"Afro", @"High Top Fade", @"Mohawk", @"Ponytail", @"Other", nil];
    self.arrayEyeColor = [[NSMutableArray alloc]initWithObjects:@"Brown", @"Light Brown", @"Other", nil];
    
    UIFont *lblFont, *txtFieldFont, *lblUploadPhotosFont, *lblHeaderFont, *btnFont, *lblOtherPopupContainerViewTitleFont, *btnSaveInOtherPopupContainerViewFont;
    CGFloat otherPopupContainerViewCornerRadius, btnSaveInOtherPopupContainerViewCornerRadiusValue;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblUploadPhotosFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblHeaderFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
        
        lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
        btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        
        otherPopupContainerViewCornerRadius = 15.0f;
        btnSaveInOtherPopupContainerViewCornerRadiusValue = 8.0f;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 10.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 10.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 12.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 8.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 15.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 8.0f;
        }
    }

    self.boolIsAddSchoolInformationSelected = false;
    
    lblEmail.font = lblFont;
    lblEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmail WithBottomSeparatorView:txtEmailBottomSeparatorView];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    lblPassword.font = lblFont;
    lblPassword.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPassword WithBottomSeparatorView:txtPasswordBottomSeparatorView];
    txtPassword.secureTextEntry = true;
    
    lblName.font = lblFont;
    lblName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtName WithBottomSeparatorView:txtNameBottomSeparatorView];
    
    lblPhoneNumber.font = lblFont;
    lblPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPhoneNumber WithBottomSeparatorView:txtPhoneNumberBottomSeparatorView];
    [txtPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
//    txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblAge.font = lblFont;
    lblAge.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtAge WithBottomSeparatorView:txtAgeBottomSeparatorView];
    [txtAge setKeyboardType:UIKeyboardTypeNumberPad];
//    txtAge.inputAccessoryView = keyboardDoneButtonView;
    
    genderPickerView = [[UIPickerView alloc] init];
    genderPickerView.delegate = self;
    genderPickerView.dataSource = self;
    genderPickerView.showsSelectionIndicator = YES;
    genderPickerView.tag = 1;
    genderPickerView.backgroundColor = [UIColor whiteColor];
    
    lblGender.font = lblFont;
    lblGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtGender WithBottomSeparatorView:txtGenderBottomSeparatorView];
    [txtGender setInputView:genderPickerView];
    imageViewGenderDropDownArrow.layer.masksToBounds = true;
    
    lblHeight.font = lblFont;
    lblHeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtHeight WithBottomSeparatorView:txtHeightBottomSeparatorView];
    [txtHeight setKeyboardType:UIKeyboardTypeDecimalPad];
//    txtHeight.inputAccessoryView = keyboardDoneButtonView;
    
    lblWeight.font = lblFont;
    lblWeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtWeight WithBottomSeparatorView:txtWeightBottomSeparatorView];
    [txtWeight setKeyboardType:UIKeyboardTypeDecimalPad];
//    txtWeight.inputAccessoryView = keyboardDoneButtonView;
    
    lblPhotos.font = lblFont;
    lblPhotos.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPhotos WithBottomSeparatorView:txtPhotosBottomSeparatorView];
    txtPhotos.userInteractionEnabled = false;
    
    uploadPhotosContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalBlueColor;
    
    imageViewUploadPhotos.layer.masksToBounds = true;
    
    lblUploadPhotos.font = lblUploadPhotosFont;
    lblUploadPhotos.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [btnUploadPhotos addTarget:self action:@selector(btnUploadPhotosClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    hairColorPickerView = [[UIPickerView alloc] init];
    hairColorPickerView.delegate = self;
    hairColorPickerView.dataSource = self;
    hairColorPickerView.showsSelectionIndicator = YES;
    hairColorPickerView.tag = 2;
    hairColorPickerView.backgroundColor = [UIColor whiteColor];
    
    lblHairColor.font = lblFont;
    lblHairColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtHairColor WithBottomSeparatorView:txtHairColorBottomSeparatorView];
    [txtHairColor setInputView:hairColorPickerView];
    imageViewHairColorDropDownArrow.layer.masksToBounds = true;
    
    hairStylePickerView = [[UIPickerView alloc] init];
    hairStylePickerView.delegate = self;
    hairStylePickerView.dataSource = self;
    hairStylePickerView.showsSelectionIndicator = YES;
    hairStylePickerView.tag = 6;
    hairStylePickerView.backgroundColor = [UIColor whiteColor];
    
    lblHairStyle.font = lblFont;
    lblHairStyle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtHairStyle WithBottomSeparatorView:txtHairStyleBottomSeparatorView];
    [txtHairStyle setInputView:hairStylePickerView];
    imageViewHairStyleDropDownArrow.layer.masksToBounds = true;
    
    eyeColorPickerView = [[UIPickerView alloc] init];
    eyeColorPickerView.delegate = self;
    eyeColorPickerView.dataSource = self;
    eyeColorPickerView.showsSelectionIndicator = YES;
    eyeColorPickerView.tag = 3;
    eyeColorPickerView.backgroundColor = [UIColor whiteColor];
    
    lblEyeColor.font = lblFont;
    lblEyeColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEyeColor WithBottomSeparatorView:txtEyeColorBottomSeparatorView];
    [txtEyeColor setInputView:eyeColorPickerView];
    imageViewEyeColorDropDownArrow.layer.masksToBounds = true;
    
    lblNationality.font = lblFont;
    lblNationality.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtNationality WithBottomSeparatorView:txtNationalityBottomSeparatorView];
    
    lblEmergencyContactInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblEmergencyContactInformation.font = lblHeaderFont;
    
    lblEmergencyName.font = lblFont;
    lblEmergencyName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmergencyName WithBottomSeparatorView:txtEmergencyNameBottomSeparatorView];
    
    lblEmergencyPhoneNumber.font = lblFont;
    lblEmergencyPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmergencyPhoneNumber WithBottomSeparatorView:txtEmergencyPhoneNumberBottomSeparatorView];
    [txtEmergencyPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
//    txtEmergencyPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblEmergencyEmail.font = lblFont;
    lblEmergencyEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmergencyEmail WithBottomSeparatorView:txtEmergencyEmailBottomSeparatorView];
    [txtEmergencyEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    lblDoYouWantToAddSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblDoYouWantToAddSchoolInformation.font = lblHeaderFont;
    
    imageViewDoNotAddSchoolInformation.layer.masksToBounds = true;
    imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
    
    lblDoNotAddSchoolInformation.font = lblFont;
    lblDoNotAddSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnDoNotAddSchoolInformation addTarget:self action:@selector(btnDoNotAddSchoolInformationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewAddSchoolInformation.layer.masksToBounds = true;
    imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    lblAddSchoolInformation.font = lblFont;
    lblAddSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnAddSchoolInformation addTarget:self action:@selector(btnAddSchoolInformationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblSchoolInformation.font = lblHeaderFont;
    
    lblSchoolName.font = lblFont;
    lblSchoolName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolName WithBottomSeparatorView:txtSchoolNameBottomSeparatorView];
    
    lblSchoolAddress.font = lblFont;
    lblSchoolAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    txtViewSchoolAddress.text = @"Address";
    txtViewSchoolAddress.font = txtFieldFont;
    txtViewSchoolAddress.delegate = self;
    txtViewSchoolAddress.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewSchoolAddress.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewSchoolAddress setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    txtViewSchoolAddressBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblSchoolPhoneNumber.font = lblFont;
    lblSchoolPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolPhoneNumber WithBottomSeparatorView:txtSchoolPhoneNumberBottomSeparatorView];
    [txtSchoolPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
    //    txtSchoolPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblSchoolCurrentGrade.font = lblFont;
    lblSchoolCurrentGrade.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolCurrentGrade WithBottomSeparatorView:txtSchoolCurrentGradeBottomSeparatorView];
    
    compulsoryAddressContainerView.backgroundColor = [UIColor clearColor];
    
    statePickerView = [[UIPickerView alloc] init];
    statePickerView.delegate = self;
    statePickerView.dataSource = self;
    statePickerView.showsSelectionIndicator = YES;
    statePickerView.tag = 4;
    statePickerView.backgroundColor = [UIColor whiteColor];
    
    lblSchoolState.font = lblFont;
    lblSchoolState.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolState WithBottomSeparatorView:txtSchoolStateBottomSeparatorView];
    [txtSchoolState setInputView:statePickerView];
    imageViewSchoolStateDropDownArrow.layer.masksToBounds = true;
    
    cityPickerView = [[UIPickerView alloc] init];
    cityPickerView.delegate = self;
    cityPickerView.dataSource = self;
    cityPickerView.showsSelectionIndicator = YES;
    cityPickerView.tag = 5;
    cityPickerView.backgroundColor = [UIColor whiteColor];
    
    lblSchoolCity.font = lblFont;
    lblSchoolCity.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolCity WithBottomSeparatorView:txtSchoolCityBottomSeparatorView];
    [txtSchoolCity setInputView:cityPickerView];
    txtSchoolCity.userInteractionEnabled = false;
    imageViewSchoolCityDropDownArrow.layer.masksToBounds = true;
    
    lblSchoolZipCode.font = lblFont;
    lblSchoolZipCode.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolZipCode WithBottomSeparatorView:txtSchoolZipCodeBottomSeparatorView];
    
    lblBestFriendsInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblBestFriendsInformation.font = lblHeaderFont;
    
    lblBestFriendsNames.font = lblFont;
    lblBestFriendsNames.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    txtViewBestFriendsNames.text = @"Best Friends Names";
    txtViewBestFriendsNames.font = txtFieldFont;
    txtViewBestFriendsNames.delegate = self;
    txtViewBestFriendsNames.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewBestFriendsNames.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewBestFriendsNames setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    txtViewBestFriendsNamesBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    btnRegister.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnRegister.layer.masksToBounds = true;
    btnRegister.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnRegister.titleLabel.font = btnFont;
    [btnRegister setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnRegister addTarget:self action:@selector(btnRegisterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    otherPopupInnerContainerView.layer.masksToBounds = YES;
    otherPopupInnerContainerView.layer.cornerRadius = otherPopupContainerViewCornerRadius;
    otherPopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblOtherPopupContainerViewTitle.font = lblOtherPopupContainerViewTitleFont;
    lblOtherPopupContainerViewTitle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    imageViewCloseOtherPopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseOtherPopupContainerView addTarget:self action:@selector(btnCloseOtherPopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtOther.font = txtFieldFont;
    txtOther.delegate = self;
    [txtOther setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    txtOther.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtOther.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtOther setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    btnSaveInOtherPopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSaveInOtherPopupContainerView.layer.masksToBounds = true;
    btnSaveInOtherPopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSaveInOtherPopupContainerView.titleLabel.font = btnFont;
    [btnSaveInOtherPopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSaveInOtherPopupContainerView addTarget:self action:@selector(btnSaveInOtherPopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.arraySelectedPhotosData = [[NSMutableArray alloc] init];
    
    [[MySingleton sharedManager].dataManager getAllStates];
    
    //========== PRATIK GUJARATI TEMP DATA ==========//
//    txtEmail.text = @"pratikgujarati@gmail.com";
//    txtPassword.text = @"123456";
//    txtName.text = @"abc";
//    txtPhoneNumber.text = @"3213213211";
//    txtAge.text = @"5";
//    txtGender.text = @"Male";
//    txtHeight.text = @"4";
//    txtWeight.text = @"40";
//    txtHairColor.text = @"Light Brown";
//    txtEyeColor.text = @"Light Brown";
//    txtNationality.text = @"Indian";
//    txtEmergencyName.text = @"Pratik";
//    txtEmergencyPhoneNumber.text = @"3213213212";
//    txtEmergencyEmail.text = @"pratik@yahoo.com";
////    txtSchoolName.text = @"Experimental";
////    txtSchoolZipCode.text = @"111222";
////    txtViewSchoolAddress.text = @"Parle Point";
////    txtViewSchoolAddress.textColor = [MySingleton sharedManager].textfieldTextColor;
////    txtSchoolPhoneNumber.text = @"3213213213";
////    txtSchoolCurrentGrade.text = @"7";
//    txtViewBestFriendsNames.text = @"Rahul";
//    txtViewBestFriendsNames.textColor = [MySingleton sharedManager].textfieldTextColor;
    //========== PRATIK GUJARATI TEMP DATA ==========//
}

- (void)setupTextfield:(UITextField *)textField WithBottomSeparatorView:(UIView *)textFieldBottomSeparatorView
{
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    textField.font = txtFieldFont;
    textField.delegate = self;
    [textField setValue:[MySingleton sharedManager].textfieldPlaceholderColor
           forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [MySingleton sharedManager].textfieldTextColor;
    textField.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    textFieldBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtGender)
    {
        if(txtGender.text.length == 0 )
        {
            txtGender.text = [self.arrayGender objectAtIndex:0];
            [genderPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
    else if(textField == txtHairColor)
    {
        if(txtHairColor.text.length == 0 || ![self.arrayHairColor containsObject:txtHairColor.text])
        {
            [hairColorPickerView selectRow:0 inComponent:0 animated:YES];
            txtHairColor.text = [self.arrayHairColor objectAtIndex:0];
        }
    }
    else if(textField == txtHairStyle)
    {
        if(txtHairStyle.text.length == 0 || ![self.arrayHairStyle containsObject:txtHairStyle.text])
        {
            [hairStylePickerView selectRow:0 inComponent:0 animated:YES];
            txtHairStyle.text = [self.arrayHairStyle objectAtIndex:0];
        }
    }
    else if(textField == txtEyeColor)
    {
        if(txtEyeColor.text.length == 0 || ![self.arrayEyeColor containsObject:txtEyeColor.text])
        {
            [eyeColorPickerView selectRow:0 inComponent:0 animated:YES];
            txtEyeColor.text = [self.arrayEyeColor objectAtIndex:0];
        }
    }
    else if(textField == txtSchoolState)
    {
        if(txtSchoolState.text.length == 0 )
        {
            if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
            {
                self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:0];
                txtSchoolState.text = self.objSelectedState.strStateName;
                txtSchoolCity.userInteractionEnabled = true;
                [statePickerView selectRow:0 inComponent:0 animated:YES];
                [cityPickerView reloadAllComponents];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No States Added" withDetails:@"Currently we don't have any states added."];
            }
        }
    }
    else if(textField == txtSchoolCity)
    {
        if(txtSchoolCity.text.length == 0 )
        {
            if(self.objSelectedState.arrayCity.count > 0)
            {
                self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:0];
                txtSchoolCity.text = self.objSelectedCity.strCityName;
                [cityPickerView selectRow:0 inComponent:0 animated:YES];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewSchoolAddress)
    {
        if ([textView.text isEqualToString:@"Address"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
    else if(textView == txtViewBestFriendsNames)
    {
        if ([textView.text isEqualToString:@"Best Friends Names"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewSchoolAddress)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Address";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
    else if(textView == txtViewBestFriendsNames)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Best Friends Names";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - UIImagePickerController Delegate Method

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try
    {
        UIImage *image = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage* smaller = [self imageWithImage:image scaledToWidth:320];
        
        //SEND IMAGE DATA TO SERVER
        NSData *selectedImageData = UIImagePNGRepresentation(smaller);
        NSString *selectedImageBase64Data = [selectedImageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
                
        UIGraphicsEndImageContext();
        
        [picker dismissViewControllerAnimated:NO completion:NULL];
        
        [self.arraySelectedPhotosData addObject:selectedImageData];
        
        txtPhotos.text = [NSString stringWithFormat:@"%d Photo(s) selected", self.arraySelectedPhotosData.count];
    }
    @catch (NSException *exception) {
        
        NSLog(@"Exception in imagePickerController's didFinishPickingMediaWithInfo Method, Exception : %@",exception);
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Selection Methods

-(void)takeAPhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing = YES;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [imagePickerController setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [imagePickerController
                                                             popoverPresentationController];
            popPresenter.sourceView = btnUploadPhotos;
            popPresenter.sourceRect = btnUploadPhotos.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            });
        }
        else
        {
            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                }];
            }
            else
            {
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Camera Unavailable" withDetails:@"Unable to find a camera on your device."];
        });
    }
}

-(void)chooseFromGallery
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [imagePickerController setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [imagePickerController
                                                             popoverPresentationController];
            popPresenter.sourceView = btnUploadPhotos;
            popPresenter.sourceRect = btnUploadPhotos.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            });
        }
        else
        {
            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                }];
            }
            else
            {
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Photo library Unavailable" withDetails:@"Unable to find photo library on your device."];
        });
    }
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayGender count];
    }
    else if(pickerView.tag == 2)
    {
        rowsInComponent = [self.arrayHairColor count];
    }
    else if(pickerView.tag == 3)
    {
        rowsInComponent = [self.arrayEyeColor count];
    }
    else if(pickerView.tag == 4)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.arrayStates count];
    }
    else if(pickerView.tag == 5)
    {
        rowsInComponent = [self.objSelectedState.arrayCity count];
    }
    else if(pickerView.tag == 6)
    {
        rowsInComponent = [self.arrayHairStyle count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == genderPickerView)
    {
        lblMain.text = self.arrayGender[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == hairColorPickerView)
    {
        lblMain.text = self.arrayHairColor[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == hairStylePickerView)
    {
        lblMain.text = self.arrayHairStyle[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == eyeColorPickerView)
    {
        lblMain.text = self.arrayEyeColor[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == statePickerView)
    {
        State *objState = [MySingleton sharedManager].dataManager.arrayStates[row];
        lblMain.text = objState.strStateName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == cityPickerView)
    {
        City *objCity = self.objSelectedState.arrayCity[row];
        lblMain.text = objCity.strCityName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtGender.text = [self.arrayGender objectAtIndex:row];
    }
    else if(pickerView.tag == 2)
    {
        if([[self.arrayHairColor objectAtIndex:row] isEqualToString:@"Other"])
        {
            [self.view endEditing:YES];
            
            txtHairColor.text = @"";
            
            self.strOtherPopupOpenedFor = @"hair color";
            lblOtherPopupContainerViewTitle.text = @"Hair Color";
            otherPopupContainerView.hidden = false;
            
            [hairColorPickerView selectRow:0 inComponent:0 animated:YES];
        }
        else
        {
            txtHairColor.text = [self.arrayHairColor objectAtIndex:row];
        }
        
    }
    else if(pickerView.tag == 3)
    {
        if([[self.arrayEyeColor objectAtIndex:row] isEqualToString:@"Other"])
        {
            [self.view endEditing:YES];
            
            txtEyeColor.text = @"";
            
            self.strOtherPopupOpenedFor = @"eye color";
            lblOtherPopupContainerViewTitle.text = @"Eye Color";
            otherPopupContainerView.hidden = false;
            
            [eyeColorPickerView selectRow:0 inComponent:0 animated:YES];
        }
        else
        {
            txtEyeColor.text = [self.arrayEyeColor objectAtIndex:row];
        }
    }
    else if(pickerView.tag == 4)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:row];
        txtSchoolState.text = self.objSelectedState.strStateName;
        [cityPickerView reloadAllComponents];
        
        if(self.objSelectedState.arrayCity.count > 0)
        {
            [cityPickerView selectRow:0 inComponent:0 animated:NO];
            txtSchoolCity.text = @"";
            txtSchoolCity.userInteractionEnabled = true;
        }
        else
        {
            txtSchoolCity.text = @"";
            txtSchoolCity.userInteractionEnabled = true;
            
            [self.view endEditing:true];
            [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
        }
    }
    else if(pickerView.tag == 5)
    {
        self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:row];
        txtSchoolCity.text = self.objSelectedCity.strCityName;
    }
    else if(pickerView.tag == 6)
    {
        if([[self.arrayHairStyle objectAtIndex:row] isEqualToString:@"Other"])
        {
            [self.view endEditing:YES];
            
            txtHairStyle.text = @"";
            
            self.strOtherPopupOpenedFor = @"hair style";
            lblOtherPopupContainerViewTitle.text = @"Hair Style";
            otherPopupContainerView.hidden = false;
            
            [hairStylePickerView selectRow:0 inComponent:0 animated:YES];
        }
        else
        {
            txtHairStyle.text = [self.arrayHairStyle objectAtIndex:row];
        }
        
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnUploadPhotosClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.arraySelectedPhotosData.count < 4)
    {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Take a Photo
            [self dismissViewControllerAnimated:YES completion:nil];
            [self takeAPhoto];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Choose from Gallery
            [self dismissViewControllerAnimated:YES completion:nil];
            [self chooseFromGallery];
        }]];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [actionSheet
                                                             popoverPresentationController];
            popPresenter.sourceView = btnUploadPhotos;
            popPresenter.sourceRect = btnUploadPhotos.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:actionSheet animated:YES completion:nil];
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:actionSheet animated:YES completion:nil];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Max Limit Reached" withDetails:@"You can select maximum 4 photos"];
        });
    }
}

- (IBAction)btnCloseOtherPopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    otherPopupContainerView.hidden = TRUE;
}

-(void)btnSaveInOtherPopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(txtOther.text.length > 0)
    {
        otherPopupContainerView.hidden = true;
        
        if([self.strOtherPopupOpenedFor isEqualToString:@"hair color"])
        {
            txtHairColor.text = txtOther.text;
            txtOther.text = @"";
        }
        else if([self.strOtherPopupOpenedFor isEqualToString:@"hair style"])
        {
            txtHairStyle.text = txtOther.text;
            txtOther.text = @"";
        }
        else if([self.strOtherPopupOpenedFor isEqualToString:@"eye color"])
        {
            txtEyeColor.text = txtOther.text;
            txtOther.text = @"";
        }
    }
    else
    {
        if(txtOther.text.length <= 0)
        {
            if([self.strOtherPopupOpenedFor isEqualToString:@"hair color"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter your hair color."];
                });
            }
            else if([self.strOtherPopupOpenedFor isEqualToString:@"hair style"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter your hair style."];
                });
            }
            else if([self.strOtherPopupOpenedFor isEqualToString:@"eye color"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter your eye color."];
                });
            }
        }
    }
}

- (IBAction)btnAddSchoolInformationClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddSchoolInformationSelected = true;
    
    imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
    imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    schoolInformationContainerView.hidden = false;
    
    float floatMargin = lblPassword.frame.origin.y - txtEmailBottomSeparatorView.frame.origin.y;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y + schoolInformationContainerView.frame.size.height;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

- (IBAction)btnDoNotAddSchoolInformationClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddSchoolInformationSelected = false;
    
    imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
    imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    schoolInformationContainerView.hidden = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

-(void)bindDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInChild == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild = [[Child alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strPassword = txtPassword.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strName = txtName.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strPhoneNumber = txtPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strAge = txtAge.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strGender = txtGender.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strHeight = txtHeight.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strWeight = txtWeight.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData = self.arraySelectedPhotosData;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strHairColor = txtHairColor.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strHairStyle = txtHairStyle.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEyeColor = txtEyeColor.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strNationality = txtNationality.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactName = txtEmergencyName.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactPhoneNumber = txtEmergencyPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactEmail = txtEmergencyEmail.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.boolIsAddSchoolInformationSelected = self.boolIsAddSchoolInformationSelected;
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolStateId = self.objSelectedState.strStateID;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolStateName = txtSchoolState.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCityId = self.objSelectedCity.strCityID;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCityName = txtSchoolCity.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolZipcode = txtSchoolZipCode.text;
    
    if(self.boolIsAddSchoolInformationSelected)
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolName = txtSchoolName.text;
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolAddress = txtViewSchoolAddress.text;
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolPhoneNumber = txtSchoolPhoneNumber.text;
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCurrentGrade = txtSchoolCurrentGrade.text;
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolName = @"";
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolAddress = @"";
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolPhoneNumber = @"";
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCurrentGrade = @"";
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strBestFriendsNames = txtViewBestFriendsNames.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strDeviceToken = [prefs objectForKey:@"deviceToken"];
    if(strDeviceToken.length > 0)
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strDeviceToken = @"";
    }
    
}

- (IBAction)btnRegisterClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self bindDataToObject];
    
    if([[MySingleton sharedManager].dataManager.objLoggedInChild isValidateChildForRegistration])
    {
        [[MySingleton sharedManager].dataManager childSignUp:[MySingleton sharedManager].dataManager.objLoggedInChild];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInChild.strValidationMessage];
        });
    }
}

- (void)navigateToChildHomeViewController
{
    [self.view endEditing:true];
}

-(void)navigateToCommonVerifyPhoneNumberViewController
{
    [self.view endEditing:YES];
    
    CommonVerifyPhoneNumberViewController *viewController = [[CommonVerifyPhoneNumberViewController alloc] init];
    
    //CHILD
    
    viewController.strLoadedFor = @"0";
    viewController.strPhoneNumber = [MySingleton sharedManager].dataManager.objLoggedInChild.strPhoneNumber;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
