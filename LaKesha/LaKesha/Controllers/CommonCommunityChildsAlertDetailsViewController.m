//
//  CommonCommunityChildsAlertDetailsViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 19/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CommonCommunityChildsAlertDetailsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "CommonViewLocationViewController.h"

@interface CommonCommunityChildsAlertDetailsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    NSTimer *childPictureSliderTimer;
    int pageNumber;
    int numberOfPages;
}

@end

@implementation CommonCommunityChildsAlertDetailsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;
@synthesize imageViewOptions;
@synthesize btnOptions;

@synthesize mainContainerView;

@synthesize childPicturesScrollView;
@synthesize mainPageControl;


@synthesize mainInformationContainerView;



@synthesize viewLocationContainerView;

@synthesize imageViewLocation;
@synthesize lblViewLocation;
@synthesize lblViewLocationBottomSeparatorView;
@synthesize btnViewLocation;



@synthesize childMessageContainerView;

@synthesize lblChildMessageTitle;

@synthesize lblChildMessage;
@synthesize lblChildMessageBottomSeparatorView;



@synthesize parentMessageContainerView;

@synthesize lblParentMessageTitle;

@synthesize lblParentMessage;
@synthesize lblParentMessageBottomSeparatorView;



@synthesize basicInformationContainerView;

@synthesize lblName;
@synthesize lblNameBottomSeparatorView;

@synthesize lblPhoneNumber;
@synthesize lblPhoneNumberBottomSeparatorView;

@synthesize lblAge;
@synthesize lblAgeBottomSeparatorView;
@synthesize lblGender;
@synthesize lblGenderBottomSeparatorView;

@synthesize lblHeight;
@synthesize lblHeightBottomSeparatorView;
@synthesize lblWeight;
@synthesize lblWeightBottomSeparatorView;

@synthesize lblHairColor;
@synthesize lblHairColorBottomSeparatorView;
@synthesize lblEyeColor;
@synthesize lblEyeColorBottomSeparatorView;

@synthesize lblHairStyle;
@synthesize lblHairStyleBottomSeparatorView;

@synthesize lblNationality;
@synthesize lblNationalityBottomSeparatorView;


@synthesize emergencyContactInformationContainerView;

@synthesize lblEmergencyContactInformationTitle;

@synthesize lblEmergencyContactName;
@synthesize lblEmergencyContactNameBottomSeparatorView;
@synthesize lblEmergencyContactPhoneNumber;
@synthesize lblEmergencyContactPhoneNumberBottomSeparatorView;
@synthesize lblEmergencyContactEmail;
@synthesize lblEmergencyContactEmailBottomSeparatorView;


@synthesize schoolInformationContainerView;

@synthesize lblSchoolInformationTitle;

@synthesize lblSchoolName;
@synthesize lblSchoolNameBottomSeparatorView;
@synthesize lblSchoolAddress;
@synthesize lblSchoolAddressBottomSeparatorView;
@synthesize lblSchoolPhoneNumber;
@synthesize lblSchoolPhoneNumberBottomSeparatorView;
@synthesize lblSchoolCurrentGrade;
@synthesize lblSchoolCurrentGradeBottomSeparatorView;


@synthesize bestFriendsInformationContainerView;

@synthesize lblBestFriendsInformationTitle;

@synthesize lblBestFriendsNames;
@synthesize lblBestFriendsNamesBottomSeparatorView;


@synthesize submitParentMessagePopupContainerView;
@synthesize submitParentMessagePopupBlackTransparentView;
@synthesize submitParentMessagePopupInnerContainerView;
@synthesize imageViewCloseSubmitParentMessagePopupContainerView;
@synthesize btnCloseSubmitParentMessagePopupContainerView;
@synthesize txtViewSubmitParentMessage;
@synthesize btnSendInSubmitParentMessagePopupContainerView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    if(self.boolIsLoadedFromMyAlert)
    {
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        numberOfPages = self.objSelectedCommunityAlert.arrayChildPhotoUrls.count;
        childPicturesScrollView.contentSize = CGSizeMake(childPicturesScrollView.frame.size.width * numberOfPages, childPicturesScrollView.frame.size.height);
        
        [self setUpChildPicturesSlider];
    }
    else
    {
        mainContainerView.hidden = false;
        
        mainScrollView.autoresizesSubviews = false;
        mainContainerView.autoresizesSubviews = false;
        mainInformationContainerView.autoresizesSubviews = false;
        
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForEmergencyContactInformation])
        {
            CGRect emergencyContactInformationContainerViewFrame = emergencyContactInformationContainerView.frame;
            emergencyContactInformationContainerViewFrame.origin.y = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
            emergencyContactInformationContainerView.frame = emergencyContactInformationContainerViewFrame;
        }
        else
        {
            emergencyContactInformationContainerView.hidden = true;
        }
        
        
        
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForSchoolInformation])
        {
            if(emergencyContactInformationContainerView.hidden == true)
            {
                CGRect schoolInformationContainerViewFrame = schoolInformationContainerView.frame;
                schoolInformationContainerViewFrame.origin.y = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
                schoolInformationContainerView.frame = schoolInformationContainerViewFrame;
            }
            else
            {
                CGRect schoolInformationContainerViewFrame = schoolInformationContainerView.frame;
                schoolInformationContainerViewFrame.origin.y = emergencyContactInformationContainerView.frame.origin.y + emergencyContactInformationContainerView.frame.size.height + 10;
                schoolInformationContainerView.frame = schoolInformationContainerViewFrame;
            }
        }
        else
        {
            schoolInformationContainerView.hidden = true;
        }
        
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForBestFriendsInformation])
        {
            if(schoolInformationContainerView.hidden == true)
            {
                if(emergencyContactInformationContainerView.hidden == true)
                {
                    CGRect bestFriendsInformationContainerViewFrame = bestFriendsInformationContainerView.frame;
                    bestFriendsInformationContainerViewFrame.origin.y = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
                    bestFriendsInformationContainerView.frame = bestFriendsInformationContainerViewFrame;
                }
                else
                {
                    CGRect bestFriendsInformationContainerViewFrame = bestFriendsInformationContainerView.frame;
                    bestFriendsInformationContainerViewFrame.origin.y = emergencyContactInformationContainerView.frame.origin.y + emergencyContactInformationContainerView.frame.size.height + 10;
                    bestFriendsInformationContainerView.frame = bestFriendsInformationContainerViewFrame;
                }
            }
            else
            {
                CGRect bestFriendsInformationContainerViewFrame = bestFriendsInformationContainerView.frame;
                bestFriendsInformationContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y + schoolInformationContainerView.frame.size.height + 10;
                bestFriendsInformationContainerView.frame = bestFriendsInformationContainerViewFrame;
            }
        }
        else
        {
            bestFriendsInformationContainerView.hidden = true;
        }
        
        if(bestFriendsInformationContainerView.hidden == false)
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = bestFriendsInformationContainerView.frame.origin.y + bestFriendsInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        else if(schoolInformationContainerView.hidden == false)
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = schoolInformationContainerView.frame.origin.y + schoolInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        else if(emergencyContactInformationContainerView.hidden == false)
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = emergencyContactInformationContainerView.frame.origin.y + emergencyContactInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        else
        {
            CGRect mainInformationContainerViewFrame = mainInformationContainerView.frame;
            mainInformationContainerViewFrame.size.height = basicInformationContainerView.frame.origin.y + basicInformationContainerView.frame.size.height + 10;
            mainInformationContainerView.frame = mainInformationContainerViewFrame;
        }
        
        CGRect mainContainerViewFrame = mainContainerView.frame;
        mainContainerViewFrame.size.height = mainInformationContainerView.frame.origin.y + mainInformationContainerView.frame.size.height;
        mainContainerView.frame = mainContainerViewFrame;
        
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhotos])
        {
            numberOfPages = self.objSelectedCommunityAlert.arrayChildPhotoUrls.count;
        }
        else
        {
            numberOfPages = 1;
        }
        childPicturesScrollView.contentSize = CGSizeMake(childPicturesScrollView.frame.size.width * numberOfPages, childPicturesScrollView.frame.size.height);
        
        [self setUpChildPicturesSlider];
        
        mainScrollView.autoresizesSubviews = true;
        mainContainerView.autoresizesSubviews = true;
        mainInformationContainerView.autoresizesSubviews = true;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
    
    [childPictureSliderTimer invalidate];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedParentMessageForCommunityAlertEvent) name:@"updatedParentMessageForCommunityAlertEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedChildMessageForCommunityAlertEvent) name:@"updatedChildMessageForCommunityAlertEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)updatedParentMessageForCommunityAlertEvent
{
    [MySingleton sharedManager].boolIsParentMessageUpdated = true;
    
    lblParentMessage.text = txtViewSubmitParentMessage.text;
    
    [self btnCloseSubmitParentMessagePopupContainerViewClicked:self];
}

-(void)updatedChildMessageForCommunityAlertEvent
{
    [MySingleton sharedManager].boolIsChildMessageUpdated = true;
    
    lblChildMessage.text = txtViewSubmitParentMessage.text;
    
    [self btnCloseSubmitParentMessagePopupContainerViewClicked:self];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Alert Details"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    
    imageViewOptions.layer.masksToBounds = YES;
    [btnOptions addTarget:self action:@selector(btnOptionsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.boolIsLoadedFromMyAlert)
    {
        imageViewOptions.hidden = false;
        btnOptions.hidden = false;
    }
    else
    {
        imageViewOptions.hidden = true;
        btnOptions.hidden = true;
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnOptionsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Options" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Dismiss button tappped.
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Update Your Message" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self showSubmitParentMessagePopupContainerView];
    }]];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = btnOptions;
        popPresenter.sourceRect = btnOptions.bounds;
        popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblAlertLevelSmallFont, *lblAlertLevelBigFont, *lblResponseTimeSmallFont, *lblResponseTimeBigFont, *lblHeaderFont, *lblValueFont, *txtFieldFont, *lblDelayTimePopupContainerViewTitleFont, *lblDelayTimePopupContainerViewInstructionsFont, *btnSubmitInDelayTimePopupContainerViewFont, *lblMoreAlertsFromThisChildFont;
    CGFloat delayTimePopupContainerViewCornerRadius, btnSubmitInDelayTimePopupContainerViewCornerRadiusValue;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        
        lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblResponseTimeBigFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        
        lblHeaderFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblValueFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
        lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        
        lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        
        delayTimePopupContainerViewCornerRadius = 15.0f;
        btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 8.0f;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontEightSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontElevenSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 10.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontEightSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontElevenSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 10.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSeventeenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontNineSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 12.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 8.0f;
        }
        else
        {
            lblAlertLevelSmallFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            lblAlertLevelBigFont = [MySingleton sharedManager].themeFontSeventeenSizeBold;
            
            lblResponseTimeSmallFont = [MySingleton sharedManager].themeFontTenSizeBold;
            lblResponseTimeBigFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblValueFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            
            txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
            lblDelayTimePopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            lblDelayTimePopupContainerViewInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnSubmitInDelayTimePopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            lblMoreAlertsFromThisChildFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            
            delayTimePopupContainerViewCornerRadius = 15.0f;
            btnSubmitInDelayTimePopupContainerViewCornerRadiusValue = 8.0f;
        }
    }
    
    
    imageViewLocation.layer.masksToBounds = true;
    
    lblViewLocation.font = lblHeaderFont;
    lblViewLocation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnViewLocation addTarget:self action:@selector(btnViewLocationClicked) forControlEvents:UIControlEventTouchUpInside];
    
    lblViewLocationBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    lblChildMessageTitle.font = lblHeaderFont;
    lblChildMessageTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblChildMessage.font = lblValueFont;
    lblChildMessage.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblChildMessage.numberOfLines = 3;
//    [lblChildMessage sizeToFit];
    if(self.objSelectedCommunityAlert.strCommunityAlertMessageFromChild != nil && self.objSelectedCommunityAlert.strCommunityAlertMessageFromChild.length > 0)
    {
        lblChildMessage.text = [NSString stringWithFormat:@"%@", self.objSelectedCommunityAlert.strCommunityAlertMessageFromChild];
    }
    else
    {
        lblChildMessage.text = [NSString stringWithFormat:@"-"];
    }
    
    lblChildMessageBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    lblParentMessageTitle.font = lblHeaderFont;
    lblParentMessageTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblParentMessage.font = lblValueFont;
    lblParentMessage.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblParentMessage.numberOfLines = 3;
    //    [lblParentMessage sizeToFit];
    if(self.objSelectedCommunityAlert.strCommunityAlertMessageFromParent != nil && self.objSelectedCommunityAlert.strCommunityAlertMessageFromParent.length > 0)
    {
        lblParentMessage.text = [NSString stringWithFormat:@"%@", self.objSelectedCommunityAlert.strCommunityAlertMessageFromParent];
    }
    else
    {
        lblParentMessage.text = [NSString stringWithFormat:@"-"];
    }
    
    lblParentMessageBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    lblName.font = lblValueFont;
    lblName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strName != nil && self.objSelectedCommunityAlert.strName.length > 0)
        {
            lblName.text = [NSString stringWithFormat:@"Name : %@", self.objSelectedCommunityAlert.strName];
        }
        else
        {
            lblName.text = [NSString stringWithFormat:@"Name : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForName])
        {
            if(self.objSelectedCommunityAlert.strName != nil && self.objSelectedCommunityAlert.strName.length > 0)
            {
                lblName.text = [NSString stringWithFormat:@"Name : %@", self.objSelectedCommunityAlert.strName];
            }
            else
            {
                lblName.text = [NSString stringWithFormat:@"Name : -"];
            }
        }
        else
        {
            lblName.text = [NSString stringWithFormat:@"Name : -"];
        }
    }
    
    lblNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblPhoneNumber.font = lblValueFont;
    lblPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strPhoneNumber != nil && self.objSelectedCommunityAlert.strPhoneNumber.length > 0)
        {
            lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : %@", self.objSelectedCommunityAlert.strPhoneNumber];
        }
        else
        {
            lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhoneNumber])
        {
            if(self.objSelectedCommunityAlert.strPhoneNumber != nil && self.objSelectedCommunityAlert.strPhoneNumber.length > 0)
            {
                lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : %@", self.objSelectedCommunityAlert.strPhoneNumber];
            }
            else
            {
                lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
            }
        }
        else
        {
            lblPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
        }
    }
    
    lblPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblAge.font = lblValueFont;
    lblAge.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strAge != nil && self.objSelectedCommunityAlert.strAge.length > 0)
        {
            lblAge.text = [NSString stringWithFormat:@"Age : %@", self.objSelectedCommunityAlert.strAge];
        }
        else
        {
            lblAge.text = [NSString stringWithFormat:@"Age : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForAge])
        {
            if(self.objSelectedCommunityAlert.strAge != nil && self.objSelectedCommunityAlert.strAge.length > 0)
            {
                lblAge.text = [NSString stringWithFormat:@"Age : %@", self.objSelectedCommunityAlert.strAge];
            }
            else
            {
                lblAge.text = [NSString stringWithFormat:@"Age : -"];
            }
        }
        else
        {
            lblAge.text = [NSString stringWithFormat:@"Age : -"];
        }
    }
    
    lblAgeBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblGender.font = lblValueFont;
    lblGender.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strGender != nil && self.objSelectedCommunityAlert.strGender.length > 0)
        {
            lblGender.text = [NSString stringWithFormat:@"Gender : %@", self.objSelectedCommunityAlert.strGender];
        }
        else
        {
            lblGender.text = [NSString stringWithFormat:@"Gender : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForGender])
        {
            if(self.objSelectedCommunityAlert.strGender != nil && self.objSelectedCommunityAlert.strGender.length > 0)
            {
                lblGender.text = [NSString stringWithFormat:@"Gender : %@", self.objSelectedCommunityAlert.strGender];
            }
            else
            {
                lblGender.text = [NSString stringWithFormat:@"Gender : -"];
            }
        }
        else
        {
            lblGender.text = [NSString stringWithFormat:@"Gender : -"];
        }
    }
    
    lblGenderBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblHeight.font = lblValueFont;
    lblHeight.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strHeight != nil && self.objSelectedCommunityAlert.strHeight.length > 0)
        {
            lblHeight.text = [NSString stringWithFormat:@"Height : %@", self.objSelectedCommunityAlert.strHeight];
        }
        else
        {
            lblHeight.text = [NSString stringWithFormat:@"Height : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHeight])
        {
            if(self.objSelectedCommunityAlert.strHeight != nil && self.objSelectedCommunityAlert.strHeight.length > 0)
            {
                lblHeight.text = [NSString stringWithFormat:@"Height : %@", self.objSelectedCommunityAlert.strHeight];
            }
            else
            {
                lblHeight.text = [NSString stringWithFormat:@"Height : -"];
            }
        }
        else
        {
            lblHeight.text = [NSString stringWithFormat:@"Height : -"];
        }
    }
    
    lblHeightBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblWeight.font = lblValueFont;
    lblWeight.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strWeight != nil && self.objSelectedCommunityAlert.strWeight.length > 0)
        {
            lblWeight.text = [NSString stringWithFormat:@"Weight : %@", self.objSelectedCommunityAlert.strWeight];
        }
        else
        {
            lblWeight.text = [NSString stringWithFormat:@"Weight : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForWeight])
        {
            if(self.objSelectedCommunityAlert.strWeight != nil && self.objSelectedCommunityAlert.strWeight.length > 0)
            {
                lblWeight.text = [NSString stringWithFormat:@"Weight : %@", self.objSelectedCommunityAlert.strWeight];
            }
            else
            {
                lblWeight.text = [NSString stringWithFormat:@"Weight : -"];
            }
        }
        else
        {
            lblWeight.text = [NSString stringWithFormat:@"Weight : -"];
        }
    }
    
    lblWeightBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblHairColor.font = lblValueFont;
    lblHairColor.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strHairColor != nil && self.objSelectedCommunityAlert.strHairColor.length > 0)
        {
            lblHairColor.text = [NSString stringWithFormat:@"Hair Color : %@", self.objSelectedCommunityAlert.strHairColor];
        }
        else
        {
            lblHairColor.text = [NSString stringWithFormat:@"Hair Color : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHairColor])
        {
            if(self.objSelectedCommunityAlert.strHairColor != nil && self.objSelectedCommunityAlert.strHairColor.length > 0)
            {
                lblHairColor.text = [NSString stringWithFormat:@"Hair Color : %@", self.objSelectedCommunityAlert.strHairColor];
            }
            else
            {
                lblHairColor.text = [NSString stringWithFormat:@"Hair Color : -"];
            }
        }
        else
        {
            lblHairColor.text = [NSString stringWithFormat:@"Hair Color : -"];
        }
    }
    
    lblHairColorBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblEyeColor.font = lblValueFont;
    lblEyeColor.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strEyeColor != nil && self.objSelectedCommunityAlert.strEyeColor.length > 0)
        {
            lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : %@", self.objSelectedCommunityAlert.strEyeColor];
        }
        else
        {
            lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForEyeColor])
        {
            if(self.objSelectedCommunityAlert.strEyeColor != nil && self.objSelectedCommunityAlert.strEyeColor.length > 0)
            {
                lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : %@", self.objSelectedCommunityAlert.strEyeColor];
            }
            else
            {
                lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : -"];
            }
        }
        else
        {
            lblEyeColor.text = [NSString stringWithFormat:@"Eye Color : -"];
        }
    }
    
    lblEyeColorBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblHairStyle.font = lblValueFont;
    lblHairStyle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strHairStyle != nil && self.objSelectedCommunityAlert.strHairStyle.length > 0)
        {
            lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : %@", self.objSelectedCommunityAlert.strHairStyle];
        }
        else
        {
            lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForHairStyle])
        {
            if(self.objSelectedCommunityAlert.strHairStyle != nil && self.objSelectedCommunityAlert.strHairStyle.length > 0)
            {
                lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : %@", self.objSelectedCommunityAlert.strHairStyle];
            }
            else
            {
                lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : -"];
            }
        }
        else
        {
            lblHairStyle.text = [NSString stringWithFormat:@"Hair Style : -"];
        }
    }
    
    lblHairStyleBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblNationality.font = lblValueFont;
    lblNationality.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.boolIsLoadedFromMyAlert)
    {
        if(self.objSelectedCommunityAlert.strNationality != nil && self.objSelectedCommunityAlert.strNationality.length > 0)
        {
            lblNationality.text = [NSString stringWithFormat:@"Nationality : %@", self.objSelectedCommunityAlert.strNationality];
        }
        else
        {
            lblNationality.text = [NSString stringWithFormat:@"Nationality : -"];
        }
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForNationality])
        {
            if(self.objSelectedCommunityAlert.strNationality != nil && self.objSelectedCommunityAlert.strNationality.length > 0)
            {
                lblNationality.text = [NSString stringWithFormat:@"Nationality : %@", self.objSelectedCommunityAlert.strNationality];
            }
            else
            {
                lblNationality.text = [NSString stringWithFormat:@"Nationality : -"];
            }
        }
        else
        {
            lblNationality.text = [NSString stringWithFormat:@"Nationality : -"];
        }
    }
    
    lblNationalityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblEmergencyContactInformationTitle.font = lblHeaderFont;
    lblEmergencyContactInformationTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblEmergencyContactName.font = lblValueFont;
    lblEmergencyContactName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedCommunityAlert.strEmergencyContactName != nil && self.objSelectedCommunityAlert.strEmergencyContactName.length > 0)
    {
        lblEmergencyContactName.text = [NSString stringWithFormat:@"Name : %@", self.objSelectedCommunityAlert.strEmergencyContactName];
    }
    else
    {
        lblEmergencyContactName.text = [NSString stringWithFormat:@"Name : -"];
    }
    
    lblEmergencyContactNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblEmergencyContactPhoneNumber.font = lblValueFont;
    lblEmergencyContactPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedCommunityAlert.strEmergencyContactPhoneNumber != nil && self.objSelectedCommunityAlert.strEmergencyContactPhoneNumber.length > 0)
    {
        lblEmergencyContactPhoneNumber.text = [NSString stringWithFormat:@"Phone : %@", self.objSelectedCommunityAlert.strEmergencyContactPhoneNumber];
    }
    else
    {
        lblEmergencyContactPhoneNumber.text = [NSString stringWithFormat:@"Phone : -"];
    }
    
    lblEmergencyContactPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblEmergencyContactEmail.font = lblValueFont;
    lblEmergencyContactEmail.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedCommunityAlert.strEmergencyContactEmail != nil && self.objSelectedCommunityAlert.strEmergencyContactEmail.length > 0)
    {
        lblEmergencyContactEmail.text = [NSString stringWithFormat:@"Email : %@", self.objSelectedCommunityAlert.strEmergencyContactEmail];
    }
    else
    {
        lblEmergencyContactEmail.text = [NSString stringWithFormat:@"Email : -"];
    }
    
    lblEmergencyContactEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblSchoolInformationTitle.font = lblHeaderFont;
    lblSchoolInformationTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblSchoolName.font = lblValueFont;
    lblSchoolName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedCommunityAlert.strSchoolName != nil && self.objSelectedCommunityAlert.strSchoolName.length > 0)
    {
        lblSchoolName.text = [NSString stringWithFormat:@"School Name : %@", self.objSelectedCommunityAlert.strSchoolName];
    }
    else
    {
        lblSchoolName.text = [NSString stringWithFormat:@"School Name : -"];
    }
    
    lblSchoolNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblSchoolAddress.font = lblValueFont;
    lblSchoolAddress.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblSchoolAddress.numberOfLines = 3;
//    [lblSchoolAddress sizeToFit];
    if(self.objSelectedCommunityAlert.strSchoolAddress != nil && self.objSelectedCommunityAlert.strSchoolAddress.length > 0)
    {
        lblSchoolAddress.text = [NSString stringWithFormat:@"Address : %@", self.objSelectedCommunityAlert.strSchoolAddress];
    }
    else
    {
        lblSchoolAddress.text = [NSString stringWithFormat:@"Address : -"];
    }
    
    lblSchoolAddressBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblSchoolPhoneNumber.font = lblValueFont;
    lblSchoolPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedCommunityAlert.strSchoolPhoneNumber != nil && self.objSelectedCommunityAlert.strSchoolPhoneNumber.length > 0)
    {
        lblSchoolPhoneNumber.text = [NSString stringWithFormat:@"Phone Number : %@", self.objSelectedCommunityAlert.strSchoolPhoneNumber];
    }
    else
    {
        lblSchoolPhoneNumber.text = [NSString stringWithFormat:@"Phone Number : -"];
    }
    
    lblSchoolPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblSchoolCurrentGrade.font = lblValueFont;
    lblSchoolCurrentGrade.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    if(self.objSelectedCommunityAlert.strSchoolCurrentGrade != nil && self.objSelectedCommunityAlert.strSchoolCurrentGrade.length > 0)
    {
        lblSchoolCurrentGrade.text = [NSString stringWithFormat:@"Current Grade : %@", self.objSelectedCommunityAlert.strSchoolCurrentGrade];
    }
    else
    {
        lblSchoolCurrentGrade.text = [NSString stringWithFormat:@"Current Grade : -"];
    }
    
    lblSchoolCurrentGradeBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    lblBestFriendsInformationTitle.font = lblHeaderFont;
    lblBestFriendsInformationTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblBestFriendsNames.font = lblValueFont;
    lblBestFriendsNames.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblBestFriendsNames.numberOfLines = 3;
    //    [lblBestFriendsNames sizeToFit];
    if(self.objSelectedCommunityAlert.strBestFriendsNames != nil && self.objSelectedCommunityAlert.strBestFriendsNames.length > 0)
    {
        lblBestFriendsNames.text = [NSString stringWithFormat:@"Best Friends Names : %@", self.objSelectedCommunityAlert.strBestFriendsNames];
    }
    else
    {
        lblBestFriendsNames.text = [NSString stringWithFormat:@"Best Friends Names : -"];
    }
    
    lblBestFriendsNamesBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    
    
    
    submitParentMessagePopupInnerContainerView.layer.masksToBounds = YES;
    submitParentMessagePopupInnerContainerView.layer.cornerRadius = delayTimePopupContainerViewCornerRadius;
    submitParentMessagePopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    imageViewCloseSubmitParentMessagePopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseSubmitParentMessagePopupContainerView addTarget:self action:@selector(btnCloseSubmitParentMessagePopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtViewSubmitParentMessage.layer.masksToBounds = true;
    txtViewSubmitParentMessage.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    txtViewSubmitParentMessage.layer.borderWidth = 1.0f;
    txtViewSubmitParentMessage.layer.borderColor = [MySingleton sharedManager].themeGlobalLightGreyColor.CGColor;
    txtViewSubmitParentMessage.text = @"Enter message here..";
    txtViewSubmitParentMessage.font = txtFieldFont;
    txtViewSubmitParentMessage.delegate = self;
    [txtViewSubmitParentMessage setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                              forKeyPath:@"_placeholderLabel.textColor"];
    txtViewSubmitParentMessage.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewSubmitParentMessage.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewSubmitParentMessage setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    btnSendInSubmitParentMessagePopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSendInSubmitParentMessagePopupContainerView.layer.masksToBounds = true;
    btnSendInSubmitParentMessagePopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSendInSubmitParentMessagePopupContainerView.titleLabel.font = btnSubmitInDelayTimePopupContainerViewFont;
    [btnSendInSubmitParentMessagePopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSendInSubmitParentMessagePopupContainerView addTarget:self action:@selector(btnSendInSubmitParentMessagePopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Slider Methods

-(void)childPictureSliderTimerMethod:(NSTimer *)timer
{
    CGFloat pageWidth = childPicturesScrollView.frame.size.width;
    
    pageNumber++;
    
    if(pageNumber > (numberOfPages - 1))
    {
        pageNumber = 0;
    }
    
    float fractionalPage = pageNumber;
    NSInteger page = lround(fractionalPage);
    mainPageControl.currentPage = page;
    [mainPageControl reloadInputViews];
    
    if(pageNumber == 0)
    {
        [childPicturesScrollView setContentOffset:CGPointMake((pageWidth * pageNumber), 0) animated:NO];
    }
    else
    {
        [childPicturesScrollView setContentOffset:CGPointMake((pageWidth * pageNumber), 0) animated:YES];
    }
}

-(void)setUpChildPicturesSlider
{
    if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhotos])
    {
        numberOfPages = self.objSelectedCommunityAlert.arrayChildPhotoUrls.count;
    }
    else
    {
        numberOfPages = 1;
    }
    
    pageNumber = 0;
    
    childPictureSliderTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(childPictureSliderTimerMethod:) userInfo:nil repeats:YES];
    
    mainPageControl.userInteractionEnabled = FALSE;
    mainPageControl.numberOfPages = numberOfPages;
    mainPageControl.currentPageIndicatorTintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [childPicturesScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    childPicturesScrollView.contentSize = CGSizeMake(childPicturesScrollView.frame.size.width * numberOfPages, childPicturesScrollView.frame.size.height);
    childPicturesScrollView.delegate = self;
    
    if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForPhotos])
    {
        for(int i = 0; i < self.objSelectedCommunityAlert.arrayChildPhotoUrls.count; i++)
        {
            AsyncImageView *childPictureImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake((childPicturesScrollView.frame.size.width*i), 0, (childPicturesScrollView.frame.size.width), childPicturesScrollView.frame.size.height)];
            
            [[AsyncImageLoader sharedLoader].cache removeAllObjects];
            childPictureImageView.imageURL = [NSURL URLWithString:[self.objSelectedCommunityAlert.arrayChildPhotoUrls objectAtIndex:i]];
            childPictureImageView.layer.masksToBounds = YES;
            childPictureImageView.contentMode = UIViewContentModeScaleAspectFill;
            
            [childPicturesScrollView addSubview:childPictureImageView];
        }
        
        UITapGestureRecognizer *childPicturesScrollViewTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(childPicturesScrollViewTapped:)];
        childPicturesScrollView.delegate = self;
        childPicturesScrollView.userInteractionEnabled = true;
        [childPicturesScrollView addGestureRecognizer:childPicturesScrollViewTapGestureRecognizer];
    }
    else
    {
        AsyncImageView *childPictureImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, (childPicturesScrollView.frame.size.width), childPicturesScrollView.frame.size.height)];
        
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        childPictureImageView.image = [UIImage imageNamed:@"child_avatar_for_alert_details.png"];
        childPictureImageView.layer.masksToBounds = YES;
        childPictureImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [childPicturesScrollView addSubview:childPictureImageView];
    }
}

- (void) childPicturesScrollViewTapped: (UITapGestureRecognizer *)recognizer
{
    
}

#pragma mark - UIScrollView Delegate Methods

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == childPicturesScrollView)
    {
        int scrollEndPoint;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
        else
        {
            if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
            else if([MySingleton sharedManager].screenHeight == 667)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
            else if([MySingleton sharedManager].screenHeight == 736)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == childPicturesScrollView)
    {
        CGFloat pageWidth = childPicturesScrollView.frame.size.width;
        
        float fractionalPage = childPicturesScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        mainPageControl.currentPage = page;
        pageNumber = page;
        [mainPageControl reloadInputViews];
    }
}

#pragma mark - UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewSubmitParentMessage)
    {
        if ([textView.text isEqualToString:@"Enter message here.."]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewSubmitParentMessage)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Enter message here..";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - Other Methods

-(void)showSubmitParentMessagePopupContainerView
{
    [self.view endEditing:YES];
    
    submitParentMessagePopupContainerView.hidden = false;
    
    [txtViewSubmitParentMessage becomeFirstResponder];
}

- (IBAction)btnCloseSubmitParentMessagePopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    txtViewSubmitParentMessage.text = @"";
    submitParentMessagePopupContainerView.hidden = TRUE;
}

-(void)btnSendInSubmitParentMessagePopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(txtViewSubmitParentMessage.text.length > 0 && ![txtViewSubmitParentMessage.text isEqualToString:@"Enter message here.."])
    {
        if(self.strIsLoadedBy != nil && [self.strIsLoadedBy isEqualToString:@"parent"])
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
            [dictParameters setObject:strParentId forKey:@"parent_id"];
            [dictParameters setObject:self.objSelectedCommunityAlert.strCommunityAlertID forKey:@"community_alert_id"];
            [dictParameters setObject:txtViewSubmitParentMessage.text forKey:@"message_from_parent"];
            
            
            [[MySingleton sharedManager].dataManager updateParentMessageForCommunityAlert:dictParameters];
        }
        else if(self.strIsLoadedBy != nil && [self.strIsLoadedBy isEqualToString:@"child"])
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
            [dictParameters setObject:strChildId forKey:@"child_id"];
            [dictParameters setObject:self.objSelectedCommunityAlert.strCommunityAlertID forKey:@"community_alert_id"];
            [dictParameters setObject:txtViewSubmitParentMessage.text forKey:@"message_from_child"];
            
            
            [[MySingleton sharedManager].dataManager updateChildMessageForCommunityAlert:dictParameters];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter a message to deactivate this alert."];
        });
    }
}

-(void)btnViewLocationClicked
{
    [self.view endEditing:YES];
    
    if(self.boolIsLoadedFromMyAlert)
    {
        CommonViewLocationViewController *viewController = [[CommonViewLocationViewController alloc] init];
        viewController.objSelectedCommunityAlert = self.objSelectedCommunityAlert;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else
    {
        if([self.objSelectedCommunityAlert.arrayInformationTypesToDisplayToOthers containsObject:[MySingleton sharedManager].strKeyForLocationInformation])
        {
            CommonViewLocationViewController *viewController = [[CommonViewLocationViewController alloc] init];
            viewController.objSelectedCommunityAlert = self.objSelectedCommunityAlert;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Parent(s) of this child doesn't want to show their child's location."];
            });
        }
    }
}

@end
