//
//  CommonTermsAndConditionsViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 04/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonTermsAndConditionsViewController : UIViewController<UIScrollViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblCongratulations;
@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UIView *termsAndConditionsCheckboxContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewTermsAndConditionsCheckBox;
@property (nonatomic,retain) IBOutlet UIButton *btnTermsAndConditionsCheckBox;
@property (nonatomic,retain) IBOutlet UILabel *lblTermsAndConditions;
@property (nonatomic,retain) IBOutlet UIButton *btnTermsAndConditions;

@property (nonatomic,retain) IBOutlet UIView *emailPermissionsContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewEmailPermissionsCheckBox;
@property (nonatomic,retain) IBOutlet UIButton *btnEmailPermissionsCheckBox;
@property (nonatomic,retain) IBOutlet UILabel *lblEmailPermissions;
@property (nonatomic,retain) IBOutlet UIButton *btnEmailPermissions;

@property (nonatomic,retain) IBOutlet UIButton *btnNext;

//========== OTHER VARIABLES ==========//

@property (nonatomic,assign) BOOL boolIsTermsAndConditionsChecked;
@property (nonatomic,assign) BOOL boolIsEmailChecked;

@end
