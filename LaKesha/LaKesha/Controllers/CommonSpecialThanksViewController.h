//
//  CommonSpecialThanksViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 14/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonSpecialThanksViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblCreator;
@property (nonatomic,retain) IBOutlet UILabel *lblDesigner;
@property (nonatomic,retain) IBOutlet UILabel *lblDeveloper;
@property (nonatomic,retain) IBOutlet UIView *separatorView;
@property (nonatomic,retain) IBOutlet UILabel *lblTopSupporters;

@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;

@end
