//
//  GuestLoginViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 28/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MySingleton.h"

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface GuestLoginViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewMainLogo;

@property (nonatomic,retain) IBOutlet UILabel *lblTagLineTitle;
@property (nonatomic,retain) IBOutlet UILabel *lblTagLine;

@property (nonatomic,retain) IBOutlet UIView *areYouAnInternationalGuestView;
@property (nonatomic,retain) IBOutlet UILabel *lblAreYouAnInternationalGuest;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewLocalGuest;
@property (nonatomic,retain) IBOutlet UILabel *lblLocalGuest;
@property (nonatomic,retain) IBOutlet UIButton *btnLocalGuest;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewAnInternationalGuest;
@property (nonatomic,retain) IBOutlet UILabel *lblAnInternationalGuest;
@property (nonatomic,retain) IBOutlet UIButton *btnAnInternationalGuest;

@property (nonatomic,retain) IBOutlet UIView *internationalGuestContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewContinent;
@property (nonatomic,retain) IBOutlet UITextField *txtContinent;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewContinentDropDownArrow;
@property (nonatomic,retain) IBOutlet UIView *txtContinentBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewEmail_IG;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail_IG;
@property (nonatomic,retain) IBOutlet UIView *txtEmail_IGBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewZipCode_IG;
@property (nonatomic,retain) IBOutlet UITextField *txtZipCode_IG;
@property (nonatomic,retain) IBOutlet UIView *txtZipCode_IGBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *localGuestContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewEmail_LG;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail_LG;
@property (nonatomic,retain) IBOutlet UIView *txtEmail_LGBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewState;
@property (nonatomic,retain) IBOutlet UITextField *txtState;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewStateDropDownArrow;
@property (nonatomic,retain) IBOutlet UIView *txtStateBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewCity;
@property (nonatomic,retain) IBOutlet UITextField *txtCity;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCityDropDownArrow;
@property (nonatomic,retain) IBOutlet UIView *txtCityBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewZipCode_LG;
@property (nonatomic,retain) IBOutlet UITextField *txtZipCode_LG;
@property (nonatomic,retain) IBOutlet UIView *txtZipCode_LGBottomSeparatorView;


@property (nonatomic,retain) IBOutlet UIButton *btnRegister;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *arrayContinent;
@property (nonatomic,retain) UIPickerView *continentPickerView;

@property (nonatomic,retain) UIPickerView *statePickerView;

@property (nonatomic,retain) State *objSelectedState;

@property (nonatomic,retain) UIPickerView *cityPickerView;

@property (nonatomic,retain) City *objSelectedCity;

@property (nonatomic,assign) BOOL boolIsAnInternationalGuest;

@end
