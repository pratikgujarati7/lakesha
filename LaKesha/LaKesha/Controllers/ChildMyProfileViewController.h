//
//  ChildMyProfileViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 17/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "AsyncImageView.h"

#import "State.h"
#import "City.h"

#import "ChildPhoto.h"

@interface ChildMyProfileViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewOptions;
@property (nonatomic,retain) IBOutlet UIButton *btnOptions;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet AsyncImageView *imageViewProfilePicture;

@property (nonatomic,retain) IBOutlet UILabel *lblEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail;
@property (nonatomic,retain) IBOutlet UIView *txtEmailBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblName;
@property (nonatomic,retain) IBOutlet UITextField *txtName;
@property (nonatomic,retain) IBOutlet UIView *txtNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblPhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblAge;
@property (nonatomic,retain) IBOutlet UITextField *txtAge;
@property (nonatomic,retain) IBOutlet UIView *txtAgeBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblGender;
@property (nonatomic,retain) IBOutlet UITextField *txtGender;
@property (nonatomic,retain) IBOutlet UIView *txtGenderBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewGenderDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblHeight;
@property (nonatomic,retain) IBOutlet UITextField *txtHeight;
@property (nonatomic,retain) IBOutlet UIView *txtHeightBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblWeight;
@property (nonatomic,retain) IBOutlet UITextField *txtWeight;
@property (nonatomic,retain) IBOutlet UIView *txtWeightBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblPhotos;
@property (nonatomic,retain) IBOutlet UITextField *txtPhotos;
@property (nonatomic,retain) IBOutlet UIButton *btnPhotos;
@property (nonatomic,retain) IBOutlet UIView *txtPhotosBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *uploadPhotosContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewUploadPhotos;
@property (nonatomic,retain) IBOutlet UILabel *lblUploadPhotos;
@property (nonatomic,retain) IBOutlet UIButton *btnUploadPhotos;

@property (nonatomic,retain) IBOutlet UILabel *lblHairColor;
@property (nonatomic,retain) IBOutlet UITextField *txtHairColor;
@property (nonatomic,retain) IBOutlet UIView *txtHairColorBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewHairColorDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblHairStyle;
@property (nonatomic,retain) IBOutlet UITextField *txtHairStyle;
@property (nonatomic,retain) IBOutlet UIView *txtHairStyleBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewHairStyleDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblEyeColor;
@property (nonatomic,retain) IBOutlet UITextField *txtEyeColor;
@property (nonatomic,retain) IBOutlet UIView *txtEyeColorBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewEyeColorDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblNationality;
@property (nonatomic,retain) IBOutlet UITextField *txtNationality;
@property (nonatomic,retain) IBOutlet UIView *txtNationalityBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyContactInformation;

@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyName;
@property (nonatomic,retain) IBOutlet UITextField *txtEmergencyName;
@property (nonatomic,retain) IBOutlet UIView *txtEmergencyNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyPhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtEmergencyPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtEmergencyPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblEmergencyEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtEmergencyEmail;
@property (nonatomic,retain) IBOutlet UIView *txtEmergencyEmailBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *lblDoYouWantToAddSchoolInformationContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblDoYouWantToAddSchoolInformation;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewDoNotAddSchoolInformation;
@property (nonatomic,retain) IBOutlet UILabel *lblDoNotAddSchoolInformation;
@property (nonatomic,retain) IBOutlet UIButton *btnDoNotAddSchoolInformation;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewAddSchoolInformation;
@property (nonatomic,retain) IBOutlet UILabel *lblAddSchoolInformation;
@property (nonatomic,retain) IBOutlet UIButton *btnAddSchoolInformation;

@property (nonatomic,retain) IBOutlet UIView *schoolInformationContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolInformation;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolName;
@property (nonatomic,retain) IBOutlet UITextField *txtSchoolName;
@property (nonatomic,retain) IBOutlet UIView *txtSchoolNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolAddress;
@property (nonatomic,retain) IBOutlet UITextView *txtViewSchoolAddress;
@property (nonatomic,retain) IBOutlet UIView *txtViewSchoolAddressBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolPhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtSchoolPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtSchoolPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolCurrentGrade;
@property (nonatomic,retain) IBOutlet UITextField *txtSchoolCurrentGrade;
@property (nonatomic,retain) IBOutlet UIView *txtSchoolCurrentGradeBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *compulsoryAddressContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolState;
@property (nonatomic,retain) IBOutlet UITextField *txtSchoolState;
@property (nonatomic,retain) IBOutlet UIView *txtSchoolStateBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewSchoolStateDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolCity;
@property (nonatomic,retain) IBOutlet UITextField *txtSchoolCity;
@property (nonatomic,retain) IBOutlet UIView *txtSchoolCityBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewSchoolCityDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblSchoolZipCode;
@property (nonatomic,retain) IBOutlet UITextField *txtSchoolZipCode;
@property (nonatomic,retain) IBOutlet UIView *txtSchoolZipCodeBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *bottomInformationContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblBestFriendsInformation;

@property (nonatomic,retain) IBOutlet UILabel *lblBestFriendsNames;
@property (nonatomic,retain) IBOutlet UITextView *txtViewBestFriendsNames;
@property (nonatomic,retain) IBOutlet UIView *txtViewBestFriendsNamesBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIButton *btnUpdate;

@property (nonatomic,retain) IBOutlet UIView *otherPopupContainerView;
@property (nonatomic,retain) IBOutlet UIView *otherPopupBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *otherPopupInnerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblOtherPopupContainerViewTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseOtherPopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseOtherPopupContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtOther;
@property (nonatomic,retain) IBOutlet UIButton *btnSaveInOtherPopupContainerView;

@property (nonatomic,retain) IBOutlet UIView *photosPopupContainerView;
@property (nonatomic,retain) IBOutlet UIView *photosPopupBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *photosPopupInnerContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewClosePhotosPopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnClosePhotosPopupContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *photosScrollView;

//========== OTHER VARIABLES ==========//

@property (strong,nonatomic) UIToolbar* keyboardDoneButtonView;

@property (nonatomic,retain) NSMutableArray *arraySelectedPhotosData;

@property (nonatomic,retain) NSMutableArray *arrayGender;
@property (nonatomic,retain) UIPickerView *genderPickerView;

@property (nonatomic,retain) NSMutableArray *arrayHairColor;
@property (nonatomic,retain) UIPickerView *hairColorPickerView;

@property (nonatomic,retain) NSMutableArray *arrayHairStyle;
@property (nonatomic,retain) UIPickerView *hairStylePickerView;

@property (nonatomic,retain) NSMutableArray *arrayEyeColor;
@property (nonatomic,retain) UIPickerView *eyeColorPickerView;

@property (nonatomic,retain) UIImage *imageSelectedProfilePicture;
@property (nonatomic,retain) NSData *imageSelectedProfilePictureData;

@property (nonatomic,retain) NSString *strOtherPopupOpenedFor;

@property (nonatomic,retain) UIPickerView *statePickerView;

@property (nonatomic,retain) State *objSelectedState;

@property (nonatomic,retain) UIPickerView *cityPickerView;

@property (nonatomic,retain) City *objSelectedCity;

@property (nonatomic,assign) BOOL boolIsImagePickerOpenedForProfilePicture;

@property (nonatomic,assign) float childPhotoViewViewWidth, childPhotoViewViewHeight;

@property (nonatomic,assign) BOOL boolIsAddSchoolInformationSelected;

@end
