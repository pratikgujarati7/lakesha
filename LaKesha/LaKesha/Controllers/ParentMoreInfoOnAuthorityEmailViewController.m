//
//  ParentMoreInfoOnAuthorityEmailViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 12/07/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentMoreInfoOnAuthorityEmailViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface ParentMoreInfoOnAuthorityEmailViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentMoreInfoOnAuthorityEmailViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblHowToFindAuthorityEmailAddress;
@synthesize lblInstructions;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"More Info"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblHowToFindAuthorityEmailAddressFont, *lblInstructionsFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblHowToFindAuthorityEmailAddressFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblInstructionsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblHowToFindAuthorityEmailAddressFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblHowToFindAuthorityEmailAddressFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblHowToFindAuthorityEmailAddressFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblHowToFindAuthorityEmailAddressFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    lblHowToFindAuthorityEmailAddress.font = lblHowToFindAuthorityEmailAddressFont;
    lblHowToFindAuthorityEmailAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblHowToFindAuthorityEmailAddress.textAlignment = NSTextAlignmentLeft;
    lblHowToFindAuthorityEmailAddress.text = [NSString stringWithFormat:@"How to find authority email address"];
    
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.textAlignment = NSTextAlignmentLeft;
//    lblInstructions.text = [NSString stringWithFormat:@"• Go to Google.\n\n• Search for \"Police Email\".\n\n• Copy and paste the email address you get from Google for your region."];
    lblInstructions.text = [NSString stringWithFormat:@"• Open your browser and search \"keyword\" police email.\n\n• Select your city's police department and copy their email address.\n\n• Select your child/children and then paste the email address in the authority email address section."];
    [lblInstructions sizeToFit];
//
//    CGRect mainContainerViewFrame = mainContainerView.frame;
//    mainContainerViewFrame.size.height = lblInstructions.frame.origin.y + lblInstructions.frame.size.height + 20;
//    mainContainerView.frame = mainContainerViewFrame;
//    
//    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainContainerView.frame.size.height);
}

#pragma mark - Other Methods

- (void)btnClicked
{
    [self.view endEditing:YES];
}

@end
