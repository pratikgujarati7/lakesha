//
//  SplashViewController.h
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface SplashViewController : UIViewController<UIScrollViewDelegate, CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D coordinates;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *mainSplashBackgroundImageView;
@property (nonatomic,retain) IBOutlet UIImageView *mainLogoImageView;

@property (nonatomic,retain) IBOutlet UIView *locationDisabledContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *mainLogoInLocationDisabledContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblLocationDisabledInstruction;

//========== OTHER VARIABLES ==========//

@property (nonatomic,strong) NSString *strLatitude;
@property (nonatomic,strong) NSString *strLongitude;

@end
