//
//  ParentSetResponseTimeViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 20/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface ParentSetResponseTimeViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UITextField *txtResponseTime;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewResponseTimeDropDownArrow;
@property (nonatomic,retain) IBOutlet UIView *txtResponseTimeBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIButton *btnSave;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *arrayResponseTime;
@property (nonatomic,retain) UIPickerView *responseTimePickerView;

@end
