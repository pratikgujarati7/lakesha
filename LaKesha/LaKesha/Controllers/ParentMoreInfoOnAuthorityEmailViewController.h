//
//  ParentMoreInfoOnAuthorityEmailViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 12/07/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentMoreInfoOnAuthorityEmailViewController : UIViewController<UIScrollViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblHowToFindAuthorityEmailAddress;
@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

//========== OTHER VARIABLES ==========//

@end
