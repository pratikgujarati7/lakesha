//
//  LoginViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 22/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "LoginViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "RegisterChildViewController.h"
#import "RegisterParentViewController.h"

#import "ParentHomeViewController.h"
#import "ChildHomeViewController.h"
#import "CommonForgetPasswordViewController.h"

#import "CommonVerifyPhoneNumberViewController.h"
#import "ParentPaymentViewController.h"

@interface LoginViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation LoginViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize imageViewBack;
@synthesize btnBack;

@synthesize imageViewMainLogo;

@synthesize lblTagLineTitle;
@synthesize lblTagLine;

@synthesize imageViewEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize imageViewPassword;
@synthesize txtPassword;
@synthesize txtPasswordBottomSeparatorView;

@synthesize btnForgetPassword;

@synthesize btnLogin;
@synthesize btnRegister;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parentLoggedinEvent) name:@"parentLoggedinEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(childLoggedinEvent) name:@"childLoggedinEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)parentLoggedinEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInParent.boolIsPhoneNumberVerified && [MySingleton sharedManager].dataManager.objLoggedInParent.boolIsPaid)
    {
        [self navigateToParentHomeViewController];
    }
    else
    {
        if(![MySingleton sharedManager].dataManager.objLoggedInParent.boolIsPhoneNumberVerified)
        {
            [self navigateToCommonVerifyPhoneNumberViewController];
        }
        else if(![MySingleton sharedManager].dataManager.objLoggedInParent.boolIsPaid)
        {
            [self navigateToParentPaymentViewController];
        }
    }
}

-(void)childLoggedinEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInChild.boolIsPhoneNumberVerified)
    {
        [self navigateToChildHomeViewController];
    }
    else
    {
        [self navigateToCommonVerifyPhoneNumberViewController];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInspiringFeatureFont, *lblDetailsFont, *txtFieldFont, *btnForgetPasswordFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInspiringFeatureFont = [MySingleton sharedManager].themeFontTwentyTwoSizeBold;
        lblDetailsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontNineteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontTwentySizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnForgetPasswordFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    imageViewBack.layer.masksToBounds = true;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblTagLineTitle.font = lblInspiringFeatureFont;
    lblTagLineTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblTagLine.font = lblDetailsFont;
    lblTagLine.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    if([self.strUserType isEqualToString:@"1"])
    {
        //PARENT
        
        lblTagLineTitle.text = @"As a Parent";
        lblTagLine.text = @"Protecting our future!";
    }
    else if ([self.strUserType isEqualToString:@"0"])
    {
        //CHILD
        
        lblTagLineTitle.text = @"As a Child";
        lblTagLine.text = @"You are our future!";
    }
    
    txtEmail.font = txtFieldFont;
    txtEmail.delegate = self;
    [txtEmail setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"_placeholderLabel.textColor"];
    txtEmail.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtEmail.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    txtPassword.font = txtFieldFont;
    txtPassword.delegate = self;
    [txtPassword setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"_placeholderLabel.textColor"];
    txtPassword.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPassword.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtPassword.secureTextEntry = true;
    
    txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    btnForgetPassword.titleLabel.font = btnForgetPasswordFont;
    [btnForgetPassword setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnForgetPassword addTarget:self action:@selector(btnForgetPasswordClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnLogin.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnLogin.layer.masksToBounds = true;
    btnLogin.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnLogin.titleLabel.font = btnFont;
    [btnLogin setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnLogin addTarget:self action:@selector(btnLoginClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnRegister.layer.borderWidth = 1.0f;
    btnRegister.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnRegister.layer.masksToBounds = true;
    btnRegister.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnRegister.titleLabel.font = btnFont;
    [btnRegister setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnRegister addTarget:self action:@selector(btnRegisterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if([self.strUserType isEqualToString:@"0"])
    {
        //CHILD
        
//        //========== PRATIK GUJARATI TEMP DATA ==========//
//        txtEmail.text = @"datadogs33@gmail.com";
//        txtPassword.text = @"1234567";
//        //========== PRATIK GUJARATI TEMP DATA ==========//
//        
        //========== PRATIK GUJARATI TEMP DATA ==========//
//        txtEmail.text = @"pratik.gujarati@yahoo.com";
//        txtPassword.text = @"123456";
        //========== PRATIK GUJARATI TEMP DATA ==========//
    }
    else if ([self.strUserType isEqualToString:@"1"])
    {
        //PARENT
        
//        //========== PRATIK GUJARATI TEMP DATA ==========//
//        txtEmail.text = @"data.dogs@yahoo.com";
//        txtPassword.text = @"123456";
//        //========== PRATIK GUJARATI TEMP DATA ==========//
//        
        //========== PRATIK GUJARATI TEMP DATA ==========//
//        txtEmail.text = @"pratikgujarati7@gmail.com";
//        txtPassword.text = @"123456";
        //========== PRATIK GUJARATI TEMP DATA ==========//
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnForgetPasswordClicked:(id)sender
{
    [self.view endEditing:true];
    
    CommonForgetPasswordViewController *viewController = [[CommonForgetPasswordViewController alloc] init];
    
    if([self.strUserType isEqualToString:@"1"])
    {
        //PARENT
        
        viewController.strLoadedFor = @"1";
    }
    else if ([self.strUserType isEqualToString:@"0"])
    {
        //CHILD
        
        viewController.strLoadedFor = @"0";
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)bindParentDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInParent == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent = [[Parent alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInParent.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInParent.strPassword = txtPassword.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strDeviceToken = [prefs objectForKey:@"deviceToken"];
    if(strDeviceToken.length > 0)
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInParent.strDeviceToken = @"";
    }
}

-(void)bindChildDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInChild == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild = [[Child alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strPassword = txtPassword.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strDeviceToken = [prefs objectForKey:@"deviceToken"];
    if(strDeviceToken.length > 0)
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strDeviceToken = @"";
    }
}

- (IBAction)btnLoginClicked:(id)sender
{
    [self.view endEditing:true];
    
    if([self.strUserType isEqualToString:@"1"])
    {
        //PARENT
        
        [self bindParentDataToObject];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInParent isValidateParentForLogin])
        {
            [[MySingleton sharedManager].dataManager parentLogin:[MySingleton sharedManager].dataManager.objLoggedInParent];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInParent.strValidationMessage];
            });
        }
    }
    else if ([self.strUserType isEqualToString:@"0"])
    {
        //CHILD
        
        [self bindChildDataToObject];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInChild isValidateChildForLogin])
        {
            [[MySingleton sharedManager].dataManager childLogin:[MySingleton sharedManager].dataManager.objLoggedInChild];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInChild.strValidationMessage];
            });
        }
    }
}

- (IBAction)btnRegisterClicked:(id)sender
{
    [self.view endEditing:true];
//    
//    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Options" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//        
//        // Dismiss button tappped.
//        
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }]];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"REGISTER AS PARENT" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        
//        [self dismissViewControllerAnimated:YES completion:nil];
//        
//        RegisterParentViewController *viewController = [[RegisterParentViewController alloc] init];
//        [self.navigationController pushViewController:viewController animated:YES];
//    }]];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"REGISTER AS CHILD" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        
//        [self dismissViewControllerAnimated:YES completion:nil];
//        
//        RegisterChildViewController *viewController = [[RegisterChildViewController alloc] init];
//        [self.navigationController pushViewController:viewController animated:YES];
//    }]];
//    
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
//        UIPopoverPresentationController *popPresenter = [actionSheet
//                                                         popoverPresentationController];
//        popPresenter.sourceView = btnRegister;
//        popPresenter.sourceRect = btnRegister.bounds;
//        popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self presentViewController:actionSheet animated:YES completion:nil];
//        });
//    }
//    else
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self presentViewController:actionSheet animated:YES completion:nil];
//        });
//    }
    
    if([self.strUserType isEqualToString:@"0"])
    {
        //CHILD
        
        RegisterChildViewController *viewController = [[RegisterChildViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if ([self.strUserType isEqualToString:@"1"])
    {
        //PARENT
        
        RegisterParentViewController *viewController = [[RegisterParentViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)navigateToParentHomeViewController
{
    [self.view endEditing:true];
    
    ParentHomeViewController *viewController = [[ParentHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

- (void)navigateToChildHomeViewController
{
    [self.view endEditing:true];
    
    ChildHomeViewController *viewController = [[ChildHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    //========== ADDITIONAL CUSTOMIZABLE PROPERTIES ==========//
//    sideMenuController.leftViewBackgroundBlurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
//    sideMenuController.rootViewCoverBlurEffectForLeftView = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    sideMenuController.leftViewCoverBlurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
//    
//    sideMenuController.leftViewBackgroundColor = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:0.1];
//    sideMenuController.rootViewCoverColorForLeftView = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:0.1];
//    sideMenuController.leftViewCoverColor = [UIColor colorWithRed:0.0 green:0.5 blue:1.0 alpha:0.1];
//    
//    sideMenuController.leftViewBackgroundAlpha = 0.9;
//    
//    sideMenuController.rootViewStatusBarHidden = false;
//    sideMenuController.rootViewStatusBarStyle = UIStatusBarStyleDefault;
//    sideMenuController.rootViewStatusBarUpdateAnimation = UIStatusBarAnimationNone;
//    
//    sideMenuController.leftViewStatusBarHidden = false;
//    sideMenuController.leftViewStatusBarStyle = UIStatusBarStyleDefault;
//    sideMenuController.leftViewStatusBarUpdateAnimation = UIStatusBarAnimationNone;
//    
//    sideMenuController.rightViewStatusBarHidden = false;
//    sideMenuController.rightViewStatusBarStyle = UIStatusBarStyleDefault;
//    sideMenuController.rightViewStatusBarUpdateAnimation = UIStatusBarAnimationNone;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

-(void)navigateToCommonVerifyPhoneNumberViewController
{
    [self.view endEditing:YES];
    
    CommonVerifyPhoneNumberViewController *viewController = [[CommonVerifyPhoneNumberViewController alloc] init];
    
    if([self.strUserType isEqualToString:@"1"])
    {
        //PARENT
        
        viewController.strLoadedFor = @"1";
        viewController.strPhoneNumber = [MySingleton sharedManager].dataManager.objLoggedInParent.strPhoneNumber;
    }
    else if ([self.strUserType isEqualToString:@"0"])
    {
        //CHILD
        
        viewController.strLoadedFor = @"0";
        viewController.strPhoneNumber = [MySingleton sharedManager].dataManager.objLoggedInChild.strPhoneNumber;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)navigateToParentPaymentViewController
{
    [self.view endEditing:YES];
    
    ParentPaymentViewController *viewController = [[ParentPaymentViewController alloc] init];
    viewController.boolIsLoadedWithoutBack = false;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
