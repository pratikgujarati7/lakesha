//
//  CommonChangePasswordViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 18/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonChangePasswordViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UITextField *txtOldPassword;
@property (nonatomic,retain) IBOutlet UIView *txtOldPasswordBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UITextField *txtNewPassword;
@property (nonatomic,retain) IBOutlet UIView *txtNewPasswordBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UITextField *txtRepeatPassword;
@property (nonatomic,retain) IBOutlet UIView *txtRepeatPasswordBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIButton *btnChangePassword;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSString *strLoadedFor;

@end
