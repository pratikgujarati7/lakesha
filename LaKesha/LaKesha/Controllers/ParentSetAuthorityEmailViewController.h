//
//  ParentSetAuthorityEmailViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 11/07/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentSetAuthorityEmailViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UITableView *mainTableView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UITextField *txtAuthorityEmail;
@property (nonatomic,retain) IBOutlet UIView *txtAuthorityEmailBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIButton *btnSave;

@property (nonatomic,retain) IBOutlet UIButton *btnMoreInfo;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;

@property (nonatomic,retain) NSMutableArray *arraySelectedChildIds;

@end
