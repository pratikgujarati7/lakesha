//
//  LoginOptionsViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 21/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "LoginOptionsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "LoginViewController.h"
#import "CommonNearByAlertsViewController.h"
#import "CommonSpecialThanksViewController.h"
#import "CommonWebViewController.h"
#import "GuestLoginViewController.h"

@interface LoginOptionsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation LoginOptionsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize imageViewMainBackground;
@synthesize imageViewMainLogo;

@synthesize lblInstructions;

@synthesize btnLoginAsParent;
@synthesize btnLoginAsChild;
@synthesize btnLoginAsGuest;

@synthesize btnDonate;
@synthesize btnMoreInformation;

//========== OTHER VARIABLES ==========//

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(guestSignedUpEvent) name:@"guestSignedUpEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)guestSignedUpEvent
{
    [self navigateToGuestHomeViewController];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionsFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    btnLoginAsParent.layer.borderWidth = 1.0f;
    btnLoginAsParent.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnLoginAsParent.layer.masksToBounds = true;
    btnLoginAsParent.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnLoginAsParent.titleLabel.font = btnFont;
    [btnLoginAsParent setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnLoginAsParent addTarget:self action:@selector(btnLoginAsParentClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnLoginAsChild.layer.borderWidth = 1.0f;
    btnLoginAsChild.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnLoginAsChild.layer.masksToBounds = true;
    btnLoginAsChild.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnLoginAsChild.titleLabel.font = btnFont;
    [btnLoginAsChild setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnLoginAsChild addTarget:self action:@selector(btnLoginAsChildClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnLoginAsGuest.layer.borderWidth = 1.0f;
    btnLoginAsGuest.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnLoginAsGuest.layer.masksToBounds = true;
    btnLoginAsGuest.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnLoginAsGuest.titleLabel.font = btnFont;
    [btnLoginAsGuest setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnLoginAsGuest addTarget:self action:@selector(btnLoginAsGuestClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnDonate.layer.borderWidth = 1.0f;
    btnDonate.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnDonate.layer.masksToBounds = true;
    btnDonate.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnDonate.titleLabel.font = btnFont;
    [btnDonate setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnDonate addTarget:self action:@selector(btnDonateClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnMoreInformation.layer.borderWidth = 1.0f;
    btnMoreInformation.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    btnMoreInformation.layer.masksToBounds = true;
    btnMoreInformation.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnMoreInformation.titleLabel.font = btnFont;
    [btnMoreInformation setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnMoreInformation addTarget:self action:@selector(btnMoreInformationClicked:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnLoginAsParentClicked:(id)sender
{
    [self.view endEditing:true];
    
    LoginViewController *viewController = [[LoginViewController alloc] init];
    viewController.strUserType = @"1";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)btnLoginAsChildClicked:(id)sender
{
    [self.view endEditing:true];
    
    LoginViewController *viewController = [[LoginViewController alloc] init];
    viewController.strUserType = @"0";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)btnLoginAsGuestClicked:(id)sender
{
    [self.view endEditing:true];
    
//    [[MySingleton sharedManager].dataManager guestSignUp];
    
    GuestLoginViewController *viewController = [[GuestLoginViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToGuestHomeViewController
{
    [self.view endEditing:true];
    
    CommonNearByAlertsViewController *viewController = [[CommonNearByAlertsViewController alloc] init];
    viewController.strLoadedFor = @"2";
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

- (IBAction)btnDonateClicked:(id)sender
{
    [self.view endEditing:true];
    
//    CommonWebViewController *viewController = [[CommonWebViewController alloc] init];
//    viewController.strTitle = @"Donate";
////    viewController.strUrl = @"https://www.gofundme.com/lakesha-alert-app";
//    viewController.strUrl = @"https://www.cognitoforms.com/DDNation1/ThankYou";
//    [self.navigationController pushViewController:viewController animated:YES];
    
    NSURL *url = [NSURL URLWithString:@"https://www.cognitoforms.com/DDNation1/ThankYou"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)btnMoreInformationClicked:(id)sender
{
    [self.view endEditing:true];
    
    CommonSpecialThanksViewController *viewController = [[CommonSpecialThanksViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
