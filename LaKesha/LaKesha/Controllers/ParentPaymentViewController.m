//
//  ParentPaymentViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 01/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentPaymentViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ParentHomeViewController.h"
#import "CommonTermsAndConditionsViewController.h"

@interface ParentPaymentViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ParentPaymentViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblInstructions;

@synthesize imageViewMainLogo;

@synthesize emailPermissionsContainerView;
@synthesize imageViewEmailPermissionsCheckBox;
@synthesize btnEmailPermissionsCheckBox;
@synthesize lblEmailPermissions;
@synthesize btnEmailPermissions;

@synthesize btnPay;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    UIFont *lblInstructionsFont, *lblInstructionsBoldFont, *lblEmailPermissionsFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblInstructionsBoldFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionsBoldFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionsBoldFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblInstructionsBoldFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblInstructionsBoldFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    NSString *strLblInstructionsText = [NSString stringWithFormat:@"All transactions are processed by stripe!\n\nDatadogs Communications LLC is legally bound not to share or expose any of your information without your consent unless confronted by a district court.\n\nWe do not allow any third-party sources to review or purchase any of our users information.\n\nPlease help us to protect our people"];
//    lblInstructions.text = strLblInstructionsText;
    NSMutableAttributedString *strAttrLblInstructionsText = [[NSMutableAttributedString alloc] initWithString:strLblInstructionsText attributes: nil];
    NSRange rangeOfStripe = [strLblInstructionsText rangeOfString:@"All transactions are processed by stripe!"];
    [strAttrLblInstructionsText addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfStripe];
    [strAttrLblInstructionsText addAttribute:NSFontAttributeName value:lblInstructionsBoldFont range:rangeOfStripe];
    NSRange rangeOfPleaseHelp = [strLblInstructionsText rangeOfString:@"Please help us to protect our people"];
    [strAttrLblInstructionsText addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:rangeOfPleaseHelp];
    [strAttrLblInstructionsText addAttribute:NSFontAttributeName value:lblInstructionsBoldFont range:rangeOfPleaseHelp];
    lblInstructions.attributedText = strAttrLblInstructionsText;
    [lblInstructions sizeToFit];
    
    CGFloat lblInstructionsHeight;
    
    CGRect lblInstructionsTextRect = [lblInstructions.text boundingRectWithSize:lblInstructions.frame.size
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{NSFontAttributeName:lblInstructions.font}
                                                                        context:nil];
    
    CGSize lblInstructionsSize = lblInstructionsTextRect.size;
    
    lblInstructionsHeight = lblInstructionsSize.height;
    
    CGRect lblInstructionsFrame = lblInstructions.frame;
    lblInstructionsFrame.size.height = lblInstructionsHeight;
    lblInstructions.frame = lblInstructionsFrame;
    
    CGRect emailPermissionsContainerViewFrame = emailPermissionsContainerView.frame;
    emailPermissionsContainerViewFrame.origin.y = lblInstructions.frame.origin.y + lblInstructionsHeight + 10;
    emailPermissionsContainerView.frame = emailPermissionsContainerViewFrame;
    
    
    CGRect imageViewMainLogoFrame = imageViewMainLogo.frame;
    imageViewMainLogoFrame.origin.y = lblInstructions.frame.origin.y + lblInstructionsHeight + 10;
    imageViewMainLogo.frame = imageViewMainLogoFrame;
    
    CGRect btnPayFrame = btnPay.frame;
//    btnPayFrame.origin.y = emailPermissionsContainerView.frame.origin.y + emailPermissionsContainerView.frame.size.height + 10;
    btnPayFrame.origin.y = imageViewMainLogo.frame.origin.y + imageViewMainLogo.frame.size.height + 10;
    btnPay.frame = btnPayFrame;
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = btnPay.frame.origin.y + btnPay.frame.size.height + 20;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainContainerView.autoresizesSubviews = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sentParentPaymentEvent) name:@"sentParentPaymentEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)sentParentPaymentEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"We received your payment. You can use our app now.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        [self navigateToParentHomeViewController];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.boolIsLoadedWithoutBack)
    {
        imageViewBack.hidden = true;
        btnBack.hidden = true;
    }
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Payment"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionsFont, *lblInstructionsBoldFont, *lblEmailPermissionsFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblInstructionsBoldFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionsBoldFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblInstructionsBoldFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblInstructionsBoldFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            lblEmailPermissionsFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        }
    }
    
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.numberOfLines = 0;
    lblInstructions.lineBreakMode = NSLineBreakByWordWrapping;
    lblInstructions.layer.masksToBounds = true;
    lblInstructions.textAlignment = NSTextAlignmentCenter;
    
    self.boolIsEmailChecked = false;
    imageViewEmailPermissionsCheckBox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
    [btnEmailPermissionsCheckBox addTarget:self action:@selector(btnEmailPermissionsCheckBoxClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnEmailPermissions addTarget:self action:@selector(btnEmailPermissionsCheckBoxClicked) forControlEvents:UIControlEventTouchUpInside];
    
    lblEmailPermissions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblEmailPermissions.font = lblEmailPermissionsFont;
    lblEmailPermissions.numberOfLines = 0;
    lblEmailPermissions.lineBreakMode = NSLineBreakByWordWrapping;
    lblEmailPermissions.layer.masksToBounds = true;
    lblEmailPermissions.text = [NSString stringWithFormat:@"Keep me updated on new & up and coming products & projects by email"];
    
    btnPay.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnPay.layer.masksToBounds = true;
    btnPay.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnPay.titleLabel.font = btnFont;
    [btnPay setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnPay addTarget:self action:@selector(btnPayClicked) forControlEvents:UIControlEventTouchUpInside];
    NSString *strBtnPayTitle = [NSString stringWithFormat:@"Payment of $%@", [MySingleton sharedManager].strParentAmount];
    [btnPay setTitle:strBtnPayTitle forState:UIControlStateNormal];
    
    CommonTermsAndConditionsViewController *viewController = [[CommonTermsAndConditionsViewController alloc] init];
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}


#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - Stripe Payment STPAddCardViewController Delegate Methods (Custom Integration)

- (void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController
{
    [addCardViewController.view endEditing:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

- (void)addCardViewController:(STPAddCardViewController *)addCardViewController
               didCreateToken:(STPToken *)token
                   completion:(STPErrorBlock)completion
{
    [addCardViewController.view endEditing:YES];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Stripe Token : %@", token);
        
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [dictParameters setObject:strParentId forKey:@"parent_id"];
        [dictParameters setObject:[MySingleton sharedManager].strParentAmount forKey:@"amount"];
        [dictParameters setObject:token forKey:@"stripe_token"];
        [[MySingleton sharedManager].dataManager sendParentPayment:dictParameters];
    }];
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

-(void)btnPayClicked
{
//    if(self.boolIsEmailChecked)
//    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        
        [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
        [[IQKeyboardManager sharedManager] setEnable:false];
        
        [[STPPaymentConfiguration sharedConfiguration] setSmsAutofillDisabled:YES];
        
        STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
        addCardViewController.delegate = self;
        addCardViewController.title = @"Pay Now";
    
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
        [self presentViewController:navigationController animated:YES completion:nil];
//    }
//    else
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please provide email permissions."];
//        });
//    }
}

-(void)navigateToParentHomeViewController
{
    [self.view endEditing:YES];
    
    ParentHomeViewController *viewController = [[ParentHomeViewController alloc] init];
    SideMenuViewController *sideMenuViewController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPad" bundle:nil];
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] init];
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        else
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

-(void)btnEmailPermissionsCheckBoxClicked
{
    if(self.boolIsEmailChecked)
    {
        self.boolIsEmailChecked = false;
        imageViewEmailPermissionsCheckBox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
    }
    else
    {
        self.boolIsEmailChecked = true;
        imageViewEmailPermissionsCheckBox.image = [UIImage imageNamed:@"checkbox_checked.png"];
    }
}

@end
