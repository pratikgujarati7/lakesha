//
//  CommonAboutUsViewController.m
//  LaKesha
//
//  Created by INNOVATIVE ITERATION 4 on 12/10/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CommonAboutUsViewController.h"

@interface CommonAboutUsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}
@end

@implementation CommonAboutUsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize imageViewDataDogs;

@synthesize lblStaticTagLine;

@synthesize btnEmail;
@synthesize btnGmail;

@synthesize btnPhoneNumber;

@synthesize imageView5000Elite;
@synthesize btn5000Elite;

@synthesize bottomContainerView;

@synthesize imageViewTwitter;
@synthesize btnTwitter;

@synthesize imageViewYoutube;
@synthesize btnYoutube;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(commonGotAllNotificationsEvent) name:@"commonGotAllNotificationsEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.image = [UIImage imageNamed:@"menu.png"];
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"About Us"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    imageViewDataDogs.layer.masksToBounds = true;
    imageViewDataDogs.image = [UIImage imageNamed:@"datadogs.jpg"];
    
    UIFont *lblStaticTagLineFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblStaticTagLineFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblStaticTagLineFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblStaticTagLineFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblStaticTagLineFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        }
        else
        {
            lblStaticTagLineFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        }
    }
    
    lblStaticTagLine.font = lblStaticTagLineFont;
    lblStaticTagLine.text = [NSString stringWithFormat:@"Know Something?\nSeen Something?\nLet Us Know!"];
    lblStaticTagLine.backgroundColor = [UIColor clearColor];
    lblStaticTagLine.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblStaticTagLine.textAlignment = NSTextAlignmentCenter;
    lblStaticTagLine.numberOfLines = 3;
    
    [btnEmail setTitle:@"Email: Aallus@lakeshaalert.com" forState:UIControlStateNormal];
    btnEmail.titleLabel.font = btnFont;
    btnEmail.titleLabel.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    btnEmail.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnEmail.tintColor = [MySingleton sharedManager].themeGlobalBlackColor;
    btnEmail.backgroundColor = [UIColor clearColor];
    [btnEmail addTarget:self action:@selector(btnEmailClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnGmail setTitle:@"Gmail: Lakeshanetwk@gmail.com" forState:UIControlStateNormal];
    btnGmail.titleLabel.font = btnFont;
    btnGmail.titleLabel.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    btnGmail.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnGmail.tintColor = [MySingleton sharedManager].themeGlobalBlackColor;
    btnGmail.backgroundColor = [UIColor clearColor];
    [btnGmail addTarget:self action:@selector(btnGmailClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnPhoneNumber setTitle:@"Phone: 1 888-959-8985" forState:UIControlStateNormal];
    btnPhoneNumber.titleLabel.font = btnFont;
    btnPhoneNumber.titleLabel.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    btnPhoneNumber.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnPhoneNumber.tintColor = [MySingleton sharedManager].themeGlobalBlackColor;
    btnPhoneNumber.backgroundColor = [UIColor clearColor];
    [btnPhoneNumber addTarget:self action:@selector(btnPhoneNumberClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageView5000Elite.layer.masksToBounds = true;
    imageView5000Elite.image = [UIImage imageNamed:@"5000_elite.jpg"];
    
    [btn5000Elite setTitle:@"" forState:UIControlStateNormal];
    btn5000Elite.backgroundColor = [UIColor clearColor];
    [btn5000Elite addTarget:self action:@selector(btn5000EliteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    bottomContainerView.backgroundColor = [UIColor clearColor];
    
    imageViewTwitter.layer.masksToBounds = true;
    imageViewTwitter.image = [UIImage imageNamed:@"twitter.png"];
    
    [btnTwitter addTarget:self action:@selector(btnTwitterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewYoutube.layer.masksToBounds = true;
    imageViewYoutube.image = [UIImage imageNamed:@"youtube.png"];
    
    [btnYoutube addTarget:self action:@selector(btnYoutubeClicked:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - MFMailComposeViewController Delegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    bool boolIsCancelled = false;
    
    UIAlertController *alert;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            boolIsCancelled = true;
            break;
        case MFMailComposeResultSaved:
            alert = [UIAlertController alertControllerWithTitle:@"Draft Saved" message:@"Composed Mail is saved in draft." preferredStyle:UIAlertControllerStyleAlert];
            break;
        case MFMailComposeResultSent:
            alert = [UIAlertController alertControllerWithTitle:@"Success" message:[NSString stringWithFormat:@"Your email has been sent successfully."] preferredStyle:UIAlertControllerStyleAlert];
            break;
        case MFMailComposeResultFailed:
            alert = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Sorry! Failed to send." preferredStyle:UIAlertControllerStyleAlert];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (boolIsCancelled == false) {
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
}

#pragma mark - Other Methods

-(IBAction)btnEmailClicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:@"Aallus@lakeshaalert.com"]];
        [controller setSubject:@""];
        [controller setMessageBody:@"" isHTML:NO];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        // Show some error message here
        
        UIAlertController *alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"No mail account setup on device." preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
}

-(IBAction)btnGmailClicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:@"Lakeshanetwk@gmail.com"]];
        [controller setSubject:@""];
        [controller setMessageBody:@"" isHTML:NO];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        // Show some error message here
        
        UIAlertController *alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"No mail account setup on device." preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
}

-(IBAction)btnPhoneNumberClicked:(id)sender
{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:@"18889598985"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(IBAction)btn5000EliteClicked:(id)sender
{
    NSString *str5000Elite = @"https://www.cognitoforms.com/_5000Elite/_5000EliteApp";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str5000Elite]];
}

-(IBAction)btnTwitterClicked:(id)sender
{
    NSString *strTwitter = @"https://www.twitter.com/LaKeshanetwork";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strTwitter]];
}

-(IBAction)btnYoutubeClicked:(id)sender
{
    NSString *strYoutube = @"https://www.youtube.com/channel/UCj-zSFtXPtl2FbOyR81DqFw";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strYoutube]];
}

@end
