//
//  IntroductionViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 22/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "IntroductionViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "LoginOptionsViewController.h"

@interface IntroductionViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    int pageNumber;
    int numberOfPages;
}

@end

@implementation IntroductionViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;
@synthesize mainContainerView;

@synthesize imageViewMainLogo;

@synthesize AdvertisementScrollView;

@synthesize imageViewAdvertisement1;
@synthesize lblAdvertisementTitle1;
@synthesize lblAdvertisementDetails1;

@synthesize imageViewAdvertisement2;
@synthesize lblAdvertisementTitle2;
@synthesize lblAdvertisementDetails2;

@synthesize imageViewAdvertisement3;
@synthesize lblAdvertisementTitle3;
@synthesize lblAdvertisementDetails3;

@synthesize mainPageControl;

@synthesize btnExploreNow;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
//    NSLog(@"AdvertisementScrollView.frame.size.width : %f", AdvertisementScrollView.frame.size.width);
//    NSLog(@"AdvertisementScrollView.frame.size.height : %f", AdvertisementScrollView.frame.size.height);
//    
//    AdvertisementScrollView.contentSize = CGSizeMake(AdvertisementScrollView.frame.size.width * 3, AdvertisementScrollView.frame.size.height);
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    NSLog(@"AdvertisementScrollView.frame.size.width : %f", AdvertisementScrollView.frame.size.width);
    NSLog(@"AdvertisementScrollView.frame.size.height : %f", AdvertisementScrollView.frame.size.height);
    
    AdvertisementScrollView.contentSize = CGSizeMake(AdvertisementScrollView.frame.size.width * 3, AdvertisementScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    mainScrollView.delegate = self;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIFont *lblInspiringFeatureFont, *lblDetailsFont, *btnFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblInspiringFeatureFont = [MySingleton sharedManager].themeFontTwentyTwoSizeBold;
        lblDetailsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontTwentyTwoSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontNineteenSizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontNineteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblInspiringFeatureFont = [MySingleton sharedManager].themeFontTwentySizeBold;
            lblDetailsFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            btnFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
        }
    }
    
    lblAdvertisementTitle1.font = lblInspiringFeatureFont;
    lblAdvertisementTitle1.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAdvertisementTitle1.text = @"As a Parent";
    
    lblAdvertisementDetails1.font = lblDetailsFont;
    lblAdvertisementDetails1.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAdvertisementDetails1.text = @"Protecting our future!";
    
    lblAdvertisementTitle2.font = lblInspiringFeatureFont;
    lblAdvertisementTitle2.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAdvertisementTitle2.text = @"As a Child";
    
    lblAdvertisementDetails2.font = lblDetailsFont;
    lblAdvertisementDetails2.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAdvertisementDetails2.text = @"You are our future!";
    
    lblAdvertisementTitle3.font = lblInspiringFeatureFont;
    lblAdvertisementTitle3.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAdvertisementTitle3.text = @"As a Guest";
    
    lblAdvertisementDetails3.font = lblDetailsFont;
    lblAdvertisementDetails3.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblAdvertisementDetails3.text = @"Protecting our future!";
    
    btnExploreNow.titleLabel.font = btnFont;
    [btnExploreNow setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnExploreNow addTarget:self action:@selector(btnExploreNowClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self setUpSlider];
}

-(void)setUpSlider
{
    numberOfPages = 3;
    pageNumber = 0;
    
    mainPageControl.userInteractionEnabled = FALSE;
    mainPageControl.numberOfPages = numberOfPages;
    mainPageControl.currentPageIndicatorTintColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    mainPageControl.pageIndicatorTintColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    [AdvertisementScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
//    AdvertisementScrollView.contentSize = CGSizeMake(AdvertisementScrollView.frame.size.width * 3, AdvertisementScrollView.frame.size.height);
    AdvertisementScrollView.delegate = self;
}

#pragma mark - UIScrollView Delegate Methods

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == AdvertisementScrollView)
    {
        int scrollEndPoint;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
        else
        {
            if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
            else if([MySingleton sharedManager].screenHeight == 667)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
            else if([MySingleton sharedManager].screenHeight == 736)
            {
                scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == AdvertisementScrollView)
    {
        CGFloat pageWidth = AdvertisementScrollView.frame.size.width;
        
        float fractionalPage = AdvertisementScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        mainPageControl.currentPage = page;
        pageNumber = page;
        [mainPageControl reloadInputViews];
    }
}

#pragma mark - Other Method

- (void)btnExploreNowClicked
{
    [self.view endEditing:YES];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
