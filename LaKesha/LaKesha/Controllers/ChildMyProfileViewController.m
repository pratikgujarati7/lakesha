//
//  ChildMyProfileViewController.m
//  LaKesha
//
//  Created by Pratik Gujarati on 17/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ChildMyProfileViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ChildPhotoView.h"

#import "CommonChangePasswordViewController.h"

@interface ChildMyProfileViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}


@end

@implementation ChildMyProfileViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewOptions;
@synthesize btnOptions;

@synthesize mainContainerView;

@synthesize imageViewProfilePicture;

@synthesize lblEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize lblName;
@synthesize txtName;
@synthesize txtNameBottomSeparatorView;

@synthesize lblPhoneNumber;
@synthesize txtPhoneNumber;
@synthesize txtPhoneNumberBottomSeparatorView;

@synthesize lblAge;
@synthesize txtAge;
@synthesize txtAgeBottomSeparatorView;

@synthesize lblGender;
@synthesize txtGender;
@synthesize txtGenderBottomSeparatorView;
@synthesize imageViewGenderDropDownArrow;

@synthesize lblHeight;
@synthesize txtHeight;
@synthesize txtHeightBottomSeparatorView;

@synthesize lblWeight;
@synthesize txtWeight;
@synthesize txtWeightBottomSeparatorView;

@synthesize lblPhotos;
@synthesize txtPhotos;
@synthesize btnPhotos;
@synthesize txtPhotosBottomSeparatorView;

@synthesize uploadPhotosContainerView;
@synthesize imageViewUploadPhotos;
@synthesize lblUploadPhotos;
@synthesize btnUploadPhotos;

@synthesize lblHairColor;
@synthesize txtHairColor;
@synthesize txtHairColorBottomSeparatorView;
@synthesize imageViewHairColorDropDownArrow;

@synthesize lblHairStyle;
@synthesize txtHairStyle;
@synthesize txtHairStyleBottomSeparatorView;
@synthesize imageViewHairStyleDropDownArrow;

@synthesize lblEyeColor;
@synthesize txtEyeColor;
@synthesize txtEyeColorBottomSeparatorView;
@synthesize imageViewEyeColorDropDownArrow;

@synthesize lblNationality;
@synthesize txtNationality;
@synthesize txtNationalityBottomSeparatorView;

@synthesize lblEmergencyContactInformation;

@synthesize lblEmergencyName;
@synthesize txtEmergencyName;
@synthesize txtEmergencyNameBottomSeparatorView;

@synthesize lblEmergencyPhoneNumber;
@synthesize txtEmergencyPhoneNumber;
@synthesize txtEmergencyPhoneNumberBottomSeparatorView;

@synthesize lblEmergencyEmail;
@synthesize txtEmergencyEmail;
@synthesize txtEmergencyEmailBottomSeparatorView;

@synthesize lblDoYouWantToAddSchoolInformationContainerView;
@synthesize lblDoYouWantToAddSchoolInformation;

@synthesize imageViewDoNotAddSchoolInformation;
@synthesize lblDoNotAddSchoolInformation;
@synthesize btnDoNotAddSchoolInformation;

@synthesize imageViewAddSchoolInformation;
@synthesize lblAddSchoolInformation;
@synthesize btnAddSchoolInformation;

@synthesize schoolInformationContainerView;

@synthesize lblSchoolInformation;

@synthesize lblSchoolName;
@synthesize txtSchoolName;
@synthesize txtSchoolNameBottomSeparatorView;

@synthesize lblSchoolAddress;
@synthesize txtViewSchoolAddress;
@synthesize txtViewSchoolAddressBottomSeparatorView;

@synthesize lblSchoolPhoneNumber;
@synthesize txtSchoolPhoneNumber;
@synthesize txtSchoolPhoneNumberBottomSeparatorView;

@synthesize lblSchoolCurrentGrade;
@synthesize txtSchoolCurrentGrade;
@synthesize txtSchoolCurrentGradeBottomSeparatorView;

@synthesize compulsoryAddressContainerView;

@synthesize lblSchoolState;
@synthesize txtSchoolState;
@synthesize txtSchoolStateBottomSeparatorView;
@synthesize imageViewSchoolStateDropDownArrow;

@synthesize lblSchoolCity;
@synthesize txtSchoolCity;
@synthesize txtSchoolCityBottomSeparatorView;
@synthesize imageViewSchoolCityDropDownArrow;

@synthesize lblSchoolZipCode;
@synthesize txtSchoolZipCode;
@synthesize txtSchoolZipCodeBottomSeparatorView;

@synthesize bottomInformationContainerView;

@synthesize lblBestFriendsInformation;

@synthesize lblBestFriendsNames;
@synthesize txtViewBestFriendsNames;
@synthesize txtViewBestFriendsNamesBottomSeparatorView;

@synthesize btnUpdate;

@synthesize otherPopupContainerView;
@synthesize otherPopupBlackTransparentView;
@synthesize otherPopupInnerContainerView;
@synthesize lblOtherPopupContainerViewTitle;
@synthesize imageViewCloseOtherPopupContainerView;
@synthesize btnCloseOtherPopupContainerView;
@synthesize txtOther;
@synthesize btnSaveInOtherPopupContainerView;

@synthesize photosPopupContainerView;
@synthesize photosPopupBlackTransparentView;
@synthesize photosPopupInnerContainerView;
@synthesize imageViewClosePhotosPopupContainerView;
@synthesize btnClosePhotosPopupContainerView;
@synthesize photosScrollView;

//========== OTHER VARIABLES ==========//

@synthesize keyboardDoneButtonView;

@synthesize genderPickerView;

@synthesize hairColorPickerView;

@synthesize hairStylePickerView;

@synthesize eyeColorPickerView;

@synthesize statePickerView;

@synthesize cityPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageViewProfilePicture.layer.cornerRadius = (imageViewProfilePicture.frame.size.width / 2) - 40;
    }
    else
    {
        imageViewProfilePicture.layer.cornerRadius = imageViewProfilePicture.frame.size.width / 2;
    }
    
    self.childPhotoViewViewWidth = (photosScrollView.frame.size.width - 30)/2;
    self.childPhotoViewViewHeight = (photosScrollView.frame.size.width - 30)/2;
    
    
    
//    mainContainerView.autoresizesSubviews = false;
//    
//    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
//    bottomInformationContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y;
//    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
//    
//    CGRect mainContainerViewFrame = mainContainerView.frame;
//    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
//    mainContainerView.frame = mainContainerViewFrame;
//    
//    schoolInformationContainerView.hidden = true;
//    
//    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
//    
//    mainContainerView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotChildProfileDetailsByChildIdEvent) name:@"gotChildProfileDetailsByChildIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedChildProfileEvent) name:@"updatedChildProfileEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadedChildProfilePictureWith_FileEvent) name:@"uploadedChildProfilePictureWith_FileEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletedChildPhotoByPhotoIdEvent) name:@"deletedChildPhotoByPhotoIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotChildProfileDetailsByChildIdEvent
{
    if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
    {
        txtSchoolState.userInteractionEnabled = true;
    }
    else
    {
        txtSchoolState.userInteractionEnabled = false;
    }
    
    imageViewProfilePicture.imageURL = [NSURL URLWithString:[MySingleton sharedManager].dataManager.objLoggedInChild.strProfilePictureImageUrl];
    
    txtEmail.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strEmail;
    txtName.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strName;
    txtPhoneNumber.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strPhoneNumber;
    txtAge.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strAge;
    txtGender.text = [[MySingleton sharedManager].dataManager.objLoggedInChild.strGender capitalizedString];
    if([[txtGender.text lowercaseString] isEqualToString:@"male"])
    {
        [genderPickerView selectRow:0 inComponent:0 animated:YES];
    }
    else
    {
        [genderPickerView selectRow:1 inComponent:0 animated:YES];
    }
    
    txtHeight.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strHeight;
    txtWeight.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strWeight;
    
    self.arraySelectedPhotosData = [[NSMutableArray alloc] init];
    txtPhotos.text = [NSString stringWithFormat:@"%d Photo(s) selected", ([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count + self.arraySelectedPhotosData.count)];
    
    txtHairColor.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strHairColor;
    if([self.arrayHairColor containsObject:txtHairColor.text])
    {
        NSInteger indexOfObject = [self.arrayHairColor indexOfObject:txtHairColor.text];
        
        if(indexOfObject != NSNotFound)
        {
            [hairColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
        }
    }
    else
    {
        NSInteger indexOfObject = [self.arrayHairColor indexOfObject:@"Other"];
        
        if(indexOfObject != NSNotFound)
        {
            [hairColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
        }
    }
    
    txtHairStyle.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strHairStyle;
    if([self.arrayHairStyle containsObject:txtHairStyle.text])
    {
        NSInteger indexOfObject = [self.arrayHairStyle indexOfObject:txtHairStyle.text];
        
        if(indexOfObject != NSNotFound)
        {
            [hairStylePickerView selectRow:indexOfObject inComponent:0 animated:YES];
        }
    }
    else
    {
        NSInteger indexOfObject = [self.arrayHairStyle indexOfObject:@"Other"];
        
        if(indexOfObject != NSNotFound)
        {
            [hairStylePickerView selectRow:indexOfObject inComponent:0 animated:YES];
        }
    }
    
    txtEyeColor.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strEyeColor;
    if([self.arrayEyeColor containsObject:txtEyeColor.text])
    {
        NSInteger indexOfObject = [self.arrayEyeColor indexOfObject:txtEyeColor.text];
        
        if(indexOfObject != NSNotFound)
        {
            [eyeColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
        }
    }
    else
    {
        NSInteger indexOfObject = [self.arrayEyeColor indexOfObject:@"Other"];
        
        if(indexOfObject != NSNotFound)
        {
            [eyeColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
        }
    }
    
    txtNationality.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strNationality;
    
    txtEmergencyName.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactName;
    txtEmergencyPhoneNumber.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactPhoneNumber;
    txtEmergencyEmail.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactEmail;
    
    txtSchoolName.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolName;
    txtSchoolState.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolStateName;
    txtSchoolCity.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCityName;
    txtSchoolZipCode.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolZipcode;
    txtViewSchoolAddress.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolAddress;
    txtViewSchoolAddress.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtSchoolPhoneNumber.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolPhoneNumber;
    txtSchoolCurrentGrade.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCurrentGrade;
    
    txtViewBestFriendsNames.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strBestFriendsNames;
    txtViewBestFriendsNames.textColor = [MySingleton sharedManager].textfieldTextColor;
    
    NSMutableArray *arrayStateIds = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < [MySingleton sharedManager].dataManager.arrayStates.count; i++)
    {
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:i];
        [arrayStateIds addObject:objState.strStateID];
    }
    
    NSInteger indexOfStateId = [arrayStateIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolStateId];
    
    if(NSNotFound != indexOfStateId)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:indexOfStateId];
        [statePickerView selectRow:indexOfStateId inComponent:0 animated:NO];
        [cityPickerView reloadAllComponents];
        
        NSMutableArray *arrayCityIds = [[NSMutableArray alloc] init];
        
        for(int j = 0; j < self.objSelectedState.arrayCity.count; j++)
        {
            City *objCity = [self.objSelectedState.arrayCity objectAtIndex:j];
            [arrayCityIds addObject:objCity.strCityID];
        }
        
        NSInteger indexOfCityId = [arrayCityIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCityId];
        
        if(NSNotFound != indexOfCityId)
        {
            self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:indexOfCityId];
            [cityPickerView selectRow:indexOfCityId inComponent:0 animated:NO];
        }
    }
    
    if([MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolName.length > 0)
    {
        self.boolIsAddSchoolInformationSelected = true;
        [MySingleton sharedManager].dataManager.objLoggedInChild.boolIsAddSchoolInformationSelected = true;
        
        imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
        imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
        
        mainContainerView.autoresizesSubviews = false;
        
        schoolInformationContainerView.hidden = false;
        
        float floatMargin = lblName.frame.origin.y - txtEmailBottomSeparatorView.frame.origin.y;

//        CGRect compulsoryAddressContainerViewFrame =  compulsoryAddressContainerView.frame;
//        compulsoryAddressContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y;
//        compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;

        CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
        bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height + 10;
        bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
        
        CGRect mainContainerViewFrame = mainContainerView.frame;
        mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
        mainContainerView.frame = mainContainerViewFrame;
        
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        mainContainerView.autoresizesSubviews = true;
    }
    else
    {
        self.boolIsAddSchoolInformationSelected = false;
        [MySingleton sharedManager].dataManager.objLoggedInChild.boolIsAddSchoolInformationSelected = false;
        
        imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
        imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
        
        mainContainerView.autoresizesSubviews = false;
        
        CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
        compulsoryAddressContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y;
        compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
        
        CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
        bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height + 10;
        bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
        
        CGRect mainContainerViewFrame = mainContainerView.frame;
        mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
        mainContainerView.frame = mainContainerViewFrame;
        
        schoolInformationContainerView.hidden = true;
        
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
        
        mainContainerView.autoresizesSubviews = true;
    }
}

-(void)updatedChildProfileEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Profile Updated" withDetails:@"Your profile has been updated successfully."];
    });
}

-(void)uploadedChildProfilePictureWith_FileEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Profile Picture Updated" withDetails:@"Your profile picture has been changed successfully."];
    });
}

-(void)deletedChildPhotoByPhotoIdEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Photo Deleted" withDetails:@"Your photo has been deleted successfully."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"My Profile"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if(lblNavigationTitle.text.length > 32)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    else
    {
        if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 667 && lblNavigationTitle.text.length > 20)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
        else if([MySingleton sharedManager].screenHeight == 736 && lblNavigationTitle.text.length > 22)
        {
            lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
        }
    }
    
    imageViewOptions.layer.masksToBounds = YES;
    [btnOptions addTarget:self action:@selector(btnOptionsClicked:) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnOptionsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Options" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Dismiss button tappped.
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Change Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self redirectToChangePasswordViewController];
    }]];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = btnOptions;
        popPresenter.sourceRect = btnOptions.bounds;
        popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
}

-(void)redirectToChangePasswordViewController
{
    CommonChangePasswordViewController *viewController = [[CommonChangePasswordViewController alloc] init];
    viewController.strLoadedFor = @"0";
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    keyboardDoneButtonView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *flex1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)];
    [doneButton1 setTitleTextAttributes:@{ NSFontAttributeName: [MySingleton sharedManager].themeFontTwentySizeRegular,NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex1,doneButton1, nil]];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    self.arrayGender = [[NSMutableArray alloc]initWithObjects:@"Male",@"Female",nil];
    
    self.arrayHairColor = [[NSMutableArray alloc]initWithObjects:@"Black", @"Other", nil];
    self.arrayHairStyle = [[NSMutableArray alloc]initWithObjects:@"Long", @"Medium", @"Short", @"Dreads", @"Braids", @"Micro braids", @"Afro", @"High Top Fade", @"Mohawk", @"Ponytail", @"Other", nil];
    self.arrayEyeColor = [[NSMutableArray alloc]initWithObjects:@"Brown", @"Light Brown", @"Other", nil];
    
    UIFont *lblFont, *txtFieldFont, *lblUploadPhotosFont, *lblHeaderFont, *btnFont, *lblOtherPopupContainerViewTitleFont, *btnSaveInOtherPopupContainerViewFont;
    CGFloat otherPopupContainerViewCornerRadius, btnSaveInOtherPopupContainerViewCornerRadiusValue;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblUploadPhotosFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblHeaderFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
        
        lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
        btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
        
        otherPopupContainerViewCornerRadius = 15.0f;
        btnSaveInOtherPopupContainerViewCornerRadiusValue = 8.0f;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 10.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            lblFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 10.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 5.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            lblFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 12.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 8.0f;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            lblUploadPhotosFont = [MySingleton sharedManager].themeFontTenSizeRegular;
            lblHeaderFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
            btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
            
            lblOtherPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
            btnSaveInOtherPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            
            otherPopupContainerViewCornerRadius = 15.0f;
            btnSaveInOtherPopupContainerViewCornerRadiusValue = 8.0f;
        }
    }
    
    imageViewProfilePicture.layer.masksToBounds = YES;
    imageViewProfilePicture.layer.borderWidth = 2.0f;
    imageViewProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    imageViewProfilePicture.userInteractionEnabled = true;
    imageViewProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
    UITapGestureRecognizer *imageViewProfilePictureTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewProfilePictureTapped:)];
    imageViewProfilePictureTapGesture.delegate = self;
    [imageViewProfilePicture addGestureRecognizer:imageViewProfilePictureTapGesture];
    
    lblEmail.font = lblFont;
    lblEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmail WithBottomSeparatorView:txtEmailBottomSeparatorView];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    txtEmail.userInteractionEnabled = false;
    txtEmail.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    lblName.font = lblFont;
    lblName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtName WithBottomSeparatorView:txtNameBottomSeparatorView];
    
    lblPhoneNumber.font = lblFont;
    lblPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPhoneNumber WithBottomSeparatorView:txtPhoneNumberBottomSeparatorView];
    [txtPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
    txtPhoneNumber.userInteractionEnabled = false;
    txtPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
//    txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblAge.font = lblFont;
    lblAge.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtAge WithBottomSeparatorView:txtAgeBottomSeparatorView];
    [txtAge setKeyboardType:UIKeyboardTypeNumberPad];
//    txtAge.inputAccessoryView = keyboardDoneButtonView;
    
    genderPickerView = [[UIPickerView alloc] init];
    genderPickerView.delegate = self;
    genderPickerView.dataSource = self;
    genderPickerView.showsSelectionIndicator = YES;
    genderPickerView.tag = 1;
    genderPickerView.backgroundColor = [UIColor whiteColor];
    
    lblGender.font = lblFont;
    lblGender.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtGender WithBottomSeparatorView:txtGenderBottomSeparatorView];
    [txtGender setInputView:genderPickerView];
    imageViewGenderDropDownArrow.layer.masksToBounds = true;
    
    lblHeight.font = lblFont;
    lblHeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtHeight WithBottomSeparatorView:txtHeightBottomSeparatorView];
    [txtHeight setKeyboardType:UIKeyboardTypeDecimalPad];
//    txtHeight.inputAccessoryView = keyboardDoneButtonView;
    
    lblWeight.font = lblFont;
    lblWeight.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtWeight WithBottomSeparatorView:txtWeightBottomSeparatorView];
    [txtWeight setKeyboardType:UIKeyboardTypeDecimalPad];
//    txtWeight.inputAccessoryView = keyboardDoneButtonView;
    
    lblPhotos.font = lblFont;
    lblPhotos.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtPhotos WithBottomSeparatorView:txtPhotosBottomSeparatorView];
    txtPhotos.userInteractionEnabled = false;
    
    [btnPhotos addTarget:self action:@selector(btnPhotosClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    uploadPhotosContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalBlueColor;
    
    imageViewUploadPhotos.layer.masksToBounds = true;
    
    lblUploadPhotos.font = lblUploadPhotosFont;
    lblUploadPhotos.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [btnUploadPhotos addTarget:self action:@selector(btnUploadPhotosClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    hairColorPickerView = [[UIPickerView alloc] init];
    hairColorPickerView.delegate = self;
    hairColorPickerView.dataSource = self;
    hairColorPickerView.showsSelectionIndicator = YES;
    hairColorPickerView.tag = 2;
    hairColorPickerView.backgroundColor = [UIColor whiteColor];
    
    lblHairColor.font = lblFont;
    lblHairColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtHairColor WithBottomSeparatorView:txtHairColorBottomSeparatorView];
    [txtHairColor setInputView:hairColorPickerView];
    imageViewHairColorDropDownArrow.layer.masksToBounds = true;
    
    hairStylePickerView = [[UIPickerView alloc] init];
    hairStylePickerView.delegate = self;
    hairStylePickerView.dataSource = self;
    hairStylePickerView.showsSelectionIndicator = YES;
    hairStylePickerView.tag = 6;
    hairStylePickerView.backgroundColor = [UIColor whiteColor];
    
    lblHairStyle.font = lblFont;
    lblHairStyle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtHairStyle WithBottomSeparatorView:txtHairStyleBottomSeparatorView];
    [txtHairStyle setInputView:hairStylePickerView];
    imageViewHairStyleDropDownArrow.layer.masksToBounds = true;
    
    eyeColorPickerView = [[UIPickerView alloc] init];
    eyeColorPickerView.delegate = self;
    eyeColorPickerView.dataSource = self;
    eyeColorPickerView.showsSelectionIndicator = YES;
    eyeColorPickerView.tag = 3;
    eyeColorPickerView.backgroundColor = [UIColor whiteColor];
    
    lblEyeColor.font = lblFont;
    lblEyeColor.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEyeColor WithBottomSeparatorView:txtEyeColorBottomSeparatorView];
    [txtEyeColor setInputView:eyeColorPickerView];
    imageViewEyeColorDropDownArrow.layer.masksToBounds = true;
    
    lblNationality.font = lblFont;
    lblNationality.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtNationality WithBottomSeparatorView:txtNationalityBottomSeparatorView];
    
    lblEmergencyContactInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblEmergencyContactInformation.font = lblHeaderFont;
    
    lblEmergencyName.font = lblFont;
    lblEmergencyName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmergencyName WithBottomSeparatorView:txtEmergencyNameBottomSeparatorView];
    
    lblEmergencyPhoneNumber.font = lblFont;
    lblEmergencyPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmergencyPhoneNumber WithBottomSeparatorView:txtEmergencyPhoneNumberBottomSeparatorView];
    [txtEmergencyPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
//    txtEmergencyPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblEmergencyEmail.font = lblFont;
    lblEmergencyEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtEmergencyEmail WithBottomSeparatorView:txtEmergencyEmailBottomSeparatorView];
    [txtEmergencyEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    lblDoYouWantToAddSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblDoYouWantToAddSchoolInformation.font = lblHeaderFont;
    
    imageViewDoNotAddSchoolInformation.layer.masksToBounds = true;
    imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
    
    lblDoNotAddSchoolInformation.font = lblFont;
    lblDoNotAddSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnDoNotAddSchoolInformation addTarget:self action:@selector(btnDoNotAddSchoolInformationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewAddSchoolInformation.layer.masksToBounds = true;
    imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    lblAddSchoolInformation.font = lblFont;
    lblAddSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [btnAddSchoolInformation addTarget:self action:@selector(btnAddSchoolInformationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblSchoolInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblSchoolInformation.font = lblHeaderFont;
    
    lblSchoolName.font = lblFont;
    lblSchoolName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolName WithBottomSeparatorView:txtSchoolNameBottomSeparatorView];
    
    lblSchoolAddress.font = lblFont;
    lblSchoolAddress.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    txtViewSchoolAddress.text = @"Address";
    txtViewSchoolAddress.font = txtFieldFont;
    txtViewSchoolAddress.delegate = self;
    txtViewSchoolAddress.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewSchoolAddress.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewSchoolAddress setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    txtViewSchoolAddressBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    lblSchoolPhoneNumber.font = lblFont;
    lblSchoolPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolPhoneNumber WithBottomSeparatorView:txtSchoolPhoneNumberBottomSeparatorView];
    [txtSchoolPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
    //    txtSchoolPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    
    lblSchoolCurrentGrade.font = lblFont;
    lblSchoolCurrentGrade.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolCurrentGrade WithBottomSeparatorView:txtSchoolCurrentGradeBottomSeparatorView];
    
    compulsoryAddressContainerView.backgroundColor = [UIColor clearColor];
    
    statePickerView = [[UIPickerView alloc] init];
    statePickerView.delegate = self;
    statePickerView.dataSource = self;
    statePickerView.showsSelectionIndicator = YES;
    statePickerView.tag = 4;
    statePickerView.backgroundColor = [UIColor whiteColor];
    
    lblSchoolState.font = lblFont;
    lblSchoolState.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolState WithBottomSeparatorView:txtSchoolStateBottomSeparatorView];
    [txtSchoolState setInputView:statePickerView];
    imageViewSchoolStateDropDownArrow.layer.masksToBounds = true;
    
    cityPickerView = [[UIPickerView alloc] init];
    cityPickerView.delegate = self;
    cityPickerView.dataSource = self;
    cityPickerView.showsSelectionIndicator = YES;
    cityPickerView.tag = 5;
    cityPickerView.backgroundColor = [UIColor whiteColor];
    
    lblSchoolCity.font = lblFont;
    lblSchoolCity.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolCity WithBottomSeparatorView:txtSchoolCityBottomSeparatorView];
    [txtSchoolCity setInputView:cityPickerView];
    imageViewSchoolCityDropDownArrow.layer.masksToBounds = true;
    
    lblSchoolZipCode.font = lblFont;
    lblSchoolZipCode.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    [self setupTextfield:txtSchoolZipCode WithBottomSeparatorView:txtSchoolZipCodeBottomSeparatorView];
    
    lblBestFriendsInformation.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblBestFriendsInformation.font = lblHeaderFont;
    
    lblBestFriendsNames.font = lblFont;
    lblBestFriendsNames.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    txtViewBestFriendsNames.text = @"Best Friends Names";
    txtViewBestFriendsNames.font = txtFieldFont;
    txtViewBestFriendsNames.delegate = self;
    txtViewBestFriendsNames.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewBestFriendsNames.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtViewBestFriendsNames setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    txtViewBestFriendsNamesBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
    
    btnUpdate.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnUpdate.layer.masksToBounds = true;
    btnUpdate.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnUpdate.titleLabel.font = btnFont;
    [btnUpdate setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnUpdate addTarget:self action:@selector(btnUpdateClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    otherPopupInnerContainerView.layer.masksToBounds = YES;
    otherPopupInnerContainerView.layer.cornerRadius = otherPopupContainerViewCornerRadius;
    otherPopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblOtherPopupContainerViewTitle.font = lblOtherPopupContainerViewTitleFont;
    lblOtherPopupContainerViewTitle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    imageViewCloseOtherPopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseOtherPopupContainerView addTarget:self action:@selector(btnCloseOtherPopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtOther.font = txtFieldFont;
    txtOther.delegate = self;
    [txtOther setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"_placeholderLabel.textColor"];
    txtOther.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtOther.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtOther setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    btnSaveInOtherPopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    btnSaveInOtherPopupContainerView.layer.masksToBounds = true;
    btnSaveInOtherPopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSaveInOtherPopupContainerView.titleLabel.font = btnFont;
    [btnSaveInOtherPopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSaveInOtherPopupContainerView addTarget:self action:@selector(btnSaveInOtherPopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    photosPopupInnerContainerView.layer.masksToBounds = YES;
    photosPopupInnerContainerView.layer.cornerRadius = otherPopupContainerViewCornerRadius;
    photosPopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    imageViewClosePhotosPopupContainerView.layer.masksToBounds = YES;
    
    [btnClosePhotosPopupContainerView addTarget:self action:@selector(btnClosePhotosPopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    photosScrollView.delegate = self;
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
    [[MySingleton sharedManager].dataManager getChildProfileDetailsByChildId:strChildId];
}

- (void)setupTextfield:(UITextField *)textField WithBottomSeparatorView:(UIView *)textFieldBottomSeparatorView
{
    UIFont *txtFieldFont;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        if([MySingleton sharedManager].screenHeight == 480)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 568)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 667)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        }
        else if([MySingleton sharedManager].screenHeight == 736)
        {
            txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        }
    }
    
    textField.font = txtFieldFont;
    textField.delegate = self;
    [textField setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"_placeholderLabel.textColor"];
    textField.textColor = [MySingleton sharedManager].textfieldTextColor;
    textField.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    textFieldBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldBottomSeparatorColor;
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtGender)
    {
        if(txtGender.text.length == 0 )
        {
            txtGender.text = [self.arrayGender objectAtIndex:0];
            [genderPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
    else if(textField == txtHairColor)
    {
        if(![self.arrayHairColor containsObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strHairColor])
        {
            NSInteger indexOfObject = [self.arrayHairColor indexOfObject:@"Other"];
            
            if(indexOfObject != NSNotFound)
            {
                [hairColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
            }
        }
        else if(txtHairColor.text.length == 0 || ![self.arrayHairColor containsObject:txtHairColor.text])
        {
            [hairColorPickerView selectRow:0 inComponent:0 animated:YES];
            txtHairColor.text = [self.arrayHairColor objectAtIndex:0];
        }
    }
    else if(textField == txtHairStyle)
    {
        if(![self.arrayHairStyle containsObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strHairStyle])
        {
            NSInteger indexOfObject = [self.arrayHairStyle indexOfObject:@"Other"];
            
            if(indexOfObject != NSNotFound)
            {
                [hairStylePickerView selectRow:indexOfObject inComponent:0 animated:YES];
            }
        }
        else if(txtHairStyle.text.length == 0 || ![self.arrayHairStyle containsObject:txtHairStyle.text])
        {
            [hairStylePickerView selectRow:0 inComponent:0 animated:YES];
            txtHairStyle.text = [self.arrayHairStyle objectAtIndex:0];
        }
    }
    else if(textField == txtEyeColor)
    {
        if(![self.arrayEyeColor containsObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strEyeColor])
        {
            NSInteger indexOfObject = [self.arrayEyeColor indexOfObject:@"Other"];
            
            if(indexOfObject != NSNotFound)
            {
                [eyeColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
            }
        }
        else if(txtEyeColor.text.length == 0 || ![self.arrayEyeColor containsObject:txtEyeColor.text])
        {
            [eyeColorPickerView selectRow:0 inComponent:0 animated:YES];
            txtEyeColor.text = [self.arrayEyeColor objectAtIndex:0];
        }
    }
    else if(textField == txtSchoolState)
    {
        if(txtSchoolState.text.length == 0 )
        {
            if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
            {
                self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:0];
                txtSchoolState.text = self.objSelectedState.strStateName;
                txtSchoolCity.userInteractionEnabled = true;
                [statePickerView selectRow:0 inComponent:0 animated:YES];
                [cityPickerView reloadAllComponents];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No States Added" withDetails:@"Currently we don't have any states added."];
            }
        }
    }
    else if(textField == txtSchoolCity)
    {
        if(txtSchoolCity.text.length == 0 )
        {
            if(self.objSelectedState.arrayCity.count > 0)
            {
                self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:0];
                txtSchoolCity.text = self.objSelectedCity.strCityName;
                [cityPickerView selectRow:0 inComponent:0 animated:YES];
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewSchoolAddress)
    {
        if ([textView.text isEqualToString:@"Address"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
    else if(textView == txtViewBestFriendsNames)
    {
        if ([textView.text isEqualToString:@"Best Friends Names"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewSchoolAddress)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Address";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
    else if(textView == txtViewBestFriendsNames)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Best Friends Names";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayGender count];
    }
    else if(pickerView.tag == 2)
    {
        rowsInComponent = [self.arrayHairColor count];
    }
    else if(pickerView.tag == 3)
    {
        rowsInComponent = [self.arrayEyeColor count];
    }
    else if(pickerView.tag == 4)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.arrayStates count];
    }
    else if(pickerView.tag == 5)
    {
        rowsInComponent = [self.objSelectedState.arrayCity count];
    }
    else if(pickerView.tag == 6)
    {
        rowsInComponent = [self.arrayHairStyle count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == genderPickerView)
    {
        lblMain.text = self.arrayGender[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == hairColorPickerView)
    {
        lblMain.text = self.arrayHairColor[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == hairStylePickerView)
    {
        lblMain.text = self.arrayHairStyle[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == eyeColorPickerView)
    {
        lblMain.text = self.arrayEyeColor[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == statePickerView)
    {
        State *objState = [MySingleton sharedManager].dataManager.arrayStates[row];
        lblMain.text = objState.strStateName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == cityPickerView)
    {
        City *objCity = self.objSelectedState.arrayCity[row];
        lblMain.text = objCity.strCityName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtGender.text = [self.arrayGender objectAtIndex:row];
    }
    else if(pickerView.tag == 2)
    {
        if([[self.arrayHairColor objectAtIndex:row] isEqualToString:@"Other"])
        {
            [self.view endEditing:YES];
            
            if(![self.arrayHairColor containsObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strHairColor])
            {
                txtHairColor.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strHairColor;
                
                NSInteger indexOfObject = [self.arrayHairColor indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strHairColor];
                
                if(indexOfObject != NSNotFound)
                {
                    [hairColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
                }
            }
            else
            {
                txtHairColor.text = @"";
                [hairColorPickerView selectRow:0 inComponent:0 animated:YES];
            }
            
            self.strOtherPopupOpenedFor = @"hair color";
            lblOtherPopupContainerViewTitle.text = @"Hair Color";
            otherPopupContainerView.hidden = false;
        }
        else
        {
            txtHairColor.text = [self.arrayHairColor objectAtIndex:row];
        }
        
    }
    else if(pickerView.tag == 3)
    {
        if([[self.arrayEyeColor objectAtIndex:row] isEqualToString:@"Other"])
        {
            [self.view endEditing:YES];
            
            if(![self.arrayEyeColor containsObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strEyeColor])
            {
                txtEyeColor.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strEyeColor;
                
                NSInteger indexOfObject = [self.arrayEyeColor indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strEyeColor];
                
                if(indexOfObject != NSNotFound)
                {
                    [eyeColorPickerView selectRow:indexOfObject inComponent:0 animated:YES];
                }
            }
            else
            {
                txtEyeColor.text = @"";
                [eyeColorPickerView selectRow:0 inComponent:0 animated:YES];
            }
            
            self.strOtherPopupOpenedFor = @"eye color";
            lblOtherPopupContainerViewTitle.text = @"Eye Color";
            otherPopupContainerView.hidden = false;
        }
        else
        {
            txtEyeColor.text = [self.arrayEyeColor objectAtIndex:row];
        }
    }
    else if(pickerView.tag == 4)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:row];
        txtSchoolState.text = self.objSelectedState.strStateName;
        [cityPickerView reloadAllComponents];
        
        if(self.objSelectedState.arrayCity.count > 0)
        {
            [cityPickerView selectRow:0 inComponent:0 animated:NO];
            txtSchoolCity.text = @"";
            txtSchoolCity.userInteractionEnabled = true;
        }
        else
        {
            txtSchoolCity.text = @"";
            txtSchoolCity.userInteractionEnabled = true;
            
            [self.view endEditing:true];
            [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
        }
    }
    else if(pickerView.tag == 5)
    {
        self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:row];
        txtSchoolCity.text = self.objSelectedCity.strCityName;
    }
    else if(pickerView.tag == 6)
    {
        if([[self.arrayHairStyle objectAtIndex:row] isEqualToString:@"Other"])
        {
            [self.view endEditing:YES];
            
            if(![self.arrayHairStyle containsObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strHairStyle])
            {
                txtHairStyle.text = [MySingleton sharedManager].dataManager.objLoggedInChild.strHairStyle;
                
                NSInteger indexOfObject = [self.arrayHairStyle indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInChild.strHairStyle];
                
                if(indexOfObject != NSNotFound)
                {
                    [hairStylePickerView selectRow:indexOfObject inComponent:0 animated:YES];
                }
            }
            else
            {
                txtHairStyle.text = @"";
                [hairStylePickerView selectRow:0 inComponent:0 animated:YES];
            }
            
            self.strOtherPopupOpenedFor = @"hair style";
            lblOtherPopupContainerViewTitle.text = @"Hair Style";
            otherPopupContainerView.hidden = false;
        }
        else
        {
            txtHairStyle.text = [self.arrayHairStyle objectAtIndex:row];
        }
        
    }
}

#pragma mark - UIImagePickerController Delegate Method

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try
    {
        UIImage *image = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        
        UIImage* smaller = [self imageWithImage:image scaledToWidth:320];
        
        if(self.boolIsImagePickerOpenedForProfilePicture)
        {
            self.imageSelectedProfilePicture = smaller;
            imageViewProfilePicture.image = self.imageSelectedProfilePicture;
            imageViewProfilePicture.layer.masksToBounds = YES;
            
            //SEND IMAGE DATA TO SERVER
            self.imageSelectedProfilePictureData = UIImagePNGRepresentation(smaller);
            
            UIGraphicsEndImageContext();
            
            [picker dismissViewControllerAnimated:NO completion:NULL];
            
            [[MySingleton sharedManager].dataManager uploadChildProfilePictureWith_File:self.imageSelectedProfilePictureData];
        }
        else
        {
            //SEND IMAGE DATA TO SERVER
            NSData *selectedImageData = UIImagePNGRepresentation(smaller);
            NSString *selectedImageBase64Data = [selectedImageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
            
            UIGraphicsEndImageContext();
            
            [picker dismissViewControllerAnimated:NO completion:NULL];
            
            [self.arraySelectedPhotosData addObject:selectedImageData];
            
            txtPhotos.text = [NSString stringWithFormat:@"%d Photo(s) selected", ([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count + self.arraySelectedPhotosData.count)];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception in imagePickerController's didFinishPickingMediaWithInfo Method, %@",exception);
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Selection Methods

-(void)takeAPhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing = YES;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [imagePickerController setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [imagePickerController
                                                             popoverPresentationController];
            popPresenter.sourceView = imageViewProfilePicture;
            popPresenter.sourceRect = imageViewProfilePicture.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            });
        }
        else
        {
            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                }];
            }
            else
            {
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Camera Unavailable" withDetails:@"Unable to find a camera on your device."];
        });
    }
}

-(void)chooseFromGallery
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [imagePickerController setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [imagePickerController
                                                             popoverPresentationController];
            popPresenter.sourceView = imageViewProfilePicture;
            popPresenter.sourceRect = imageViewProfilePicture.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            });
        }
        else
        {
            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
            {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                }];
            }
            else
            {
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Photo library Unavailable" withDetails:@"Unable to find photo library on your device."];
        });
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void)imageViewProfilePictureTapped: (UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    self.boolIsImagePickerOpenedForProfilePicture = true;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Take a Photo
        [self dismissViewControllerAnimated:YES completion:nil];
        [self takeAPhoto];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Choose from Gallery
        [self dismissViewControllerAnimated:YES completion:nil];
        [self chooseFromGallery];
    }]];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = imageViewProfilePicture;
        popPresenter.sourceRect = imageViewProfilePicture.bounds;
        popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:actionSheet animated:YES completion:nil];
        });
    }
}

- (IBAction)btnUploadPhotosClicked:(id)sender
{
    [self.view endEditing:true];
    
    self.boolIsImagePickerOpenedForProfilePicture = false;
    
    if(([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count + self.arraySelectedPhotosData.count) < 4)
    {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Take a Photo
            [self dismissViewControllerAnimated:YES completion:nil];
            [self takeAPhoto];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Choose from Gallery
            [self dismissViewControllerAnimated:YES completion:nil];
            [self chooseFromGallery];
        }]];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *popPresenter = [actionSheet
                                                             popoverPresentationController];
            popPresenter.sourceView = btnUploadPhotos;
            popPresenter.sourceRect = btnUploadPhotos.bounds;
            popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:actionSheet animated:YES completion:nil];
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:actionSheet animated:YES completion:nil];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Max Limit Reached" withDetails:@"You can select maximum 4 photos"];
        });
    }
}

- (IBAction)btnCloseOtherPopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    otherPopupContainerView.hidden = TRUE;
}

-(void)btnSaveInOtherPopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(txtOther.text.length > 0)
    {
        otherPopupContainerView.hidden = true;
        
        if([self.strOtherPopupOpenedFor isEqualToString:@"hair color"])
        {
            txtHairColor.text = txtOther.text;
            txtOther.text = @"";
        }
        else if([self.strOtherPopupOpenedFor isEqualToString:@"hair style"])
        {
            txtHairStyle.text = txtOther.text;
            txtOther.text = @"";
        }
        else if([self.strOtherPopupOpenedFor isEqualToString:@"eye color"])
        {
            txtEyeColor.text = txtOther.text;
            txtOther.text = @"";
        }
    }
    else
    {
        if(txtOther.text.length <= 0)
        {
            if([self.strOtherPopupOpenedFor isEqualToString:@"hair color"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter your hair color."];
                });
            }
            else if([self.strOtherPopupOpenedFor isEqualToString:@"hair style"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter your hair style."];
                });
            }
            else if([self.strOtherPopupOpenedFor isEqualToString:@"eye color"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter your eye color."];
                });
            }
        }
    }
}

- (IBAction)btnPhotosClicked:(id)sender
{
    [self openPhotosPopupContainerView];
}

- (void)openPhotosPopupContainerView
{
    [self.view endEditing:YES];
    
    photosPopupContainerView.hidden = false;
    
    [self fillPhotosScrollViewWithPhotos];
}

-(void)emptyScrollview
{
    NSArray *viewsToRemove = [photosScrollView subviews];
    for (UIView *v in viewsToRemove)
    {
        [v removeFromSuperview];
    }
}

-(void)fillPhotosScrollViewWithPhotos
{
    float btnDeleteSize;
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        btnDeleteSize = 50;
    }
    else
    {
        btnDeleteSize = 20;
    }
    
    float x = ((photosScrollView.frame.size.width - (self.childPhotoViewViewWidth * 2))/3);
    float y = 0;
    
    int temp = 0;
    
    for(int i = 0; i < ([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count + self.arraySelectedPhotosData.count); i++)
    {
        temp++;
        
        if(temp == 3)
        {
            temp = 1;
            x = ((photosScrollView.frame.size.width - (self.childPhotoViewViewWidth * 2))/3);
            y += (self.childPhotoViewViewHeight + 10);
        }
        
        ChildPhotoView *childPhotoViewView = [[ChildPhotoView alloc] initWithFrame:CGRectMake(x, y, self.childPhotoViewViewWidth, self.childPhotoViewViewHeight)];
        
        if(i < [MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count)
        {
            childPhotoViewView.imageViewMain.image = [UIImage imageWithData:[[MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData objectAtIndex:i]];
        }
        else
        {
            int intIndex = abs([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count - i);
            childPhotoViewView.imageViewMain.image = [UIImage imageWithData:[self.arraySelectedPhotosData objectAtIndex:intIndex]];
        }
        
        childPhotoViewView.btnDelete.tag = i;
        [childPhotoViewView.btnDelete addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect btnDeleteFrame = childPhotoViewView.btnDelete.frame;
        btnDeleteFrame.size.width = btnDeleteSize;
        btnDeleteFrame.size.height = btnDeleteSize;
        btnDeleteFrame.origin.x = (childPhotoViewView.frame.size.width-btnDeleteSize);
        childPhotoViewView.btnDelete.frame = btnDeleteFrame;
        
        x += (((photosScrollView.frame.size.width - (self.childPhotoViewViewWidth * 2))/3) + self.childPhotoViewViewWidth);
        
        [photosScrollView addSubview:childPhotoViewView];
        [photosScrollView setContentSize:CGSizeMake(photosScrollView.frame.size.width, (childPhotoViewView.frame.origin.y + childPhotoViewView.frame.size.height + 10))];
    }
}

-(IBAction)btnDeleteClicked:(id)sender
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Are you sure you want to delete this photo?";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        UIButton *btnSender = (UIButton *)sender;
        NSLog(@"btnSender.tag : %d",btnSender.tag);
        
        if(btnSender.tag < [MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count)
        {
            ChildPhoto *objChildPhoto = [[MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosObjects objectAtIndex:btnSender.tag];
            
            [[MySingleton sharedManager].dataManager deleteChildPhotoByPhotoId:objChildPhoto.strChildPhotoID];
            
            [[MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosObjects removeObjectAtIndex:btnSender.tag];
            [[MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData removeObjectAtIndex:btnSender.tag];
            
            [self emptyScrollview];
            [self fillPhotosScrollViewWithPhotos];
        }
        else
        {
            int intIndex = abs([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count - btnSender.tag);
            [self.arraySelectedPhotosData removeObjectAtIndex:intIndex];
            
            [self emptyScrollview];
            [self fillPhotosScrollViewWithPhotos];
        }
    }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
    
    
}

- (IBAction)btnClosePhotosPopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    photosPopupContainerView.hidden = true;
    
    txtPhotos.text = [NSString stringWithFormat:@"%d Photo(s) selected", ([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count + self.arraySelectedPhotosData.count)];
}

- (IBAction)btnAddSchoolInformationClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddSchoolInformationSelected = true;
    
    imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
    imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    schoolInformationContainerView.hidden = false;
    
    float floatMargin = lblName.frame.origin.y - txtEmailBottomSeparatorView.frame.origin.y;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y + schoolInformationContainerView.frame.size.height;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height + 10;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

- (IBAction)btnDoNotAddSchoolInformationClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsAddSchoolInformationSelected = false;
    
    imageViewDoNotAddSchoolInformation.image = [UIImage imageNamed:@"radio_selected.png"];
    imageViewAddSchoolInformation.image = [UIImage imageNamed:@"radio_deselected.png"];
    
    mainContainerView.autoresizesSubviews = false;
    
    CGRect compulsoryAddressContainerViewFrame = compulsoryAddressContainerView.frame;
    compulsoryAddressContainerViewFrame.origin.y = schoolInformationContainerView.frame.origin.y;
    compulsoryAddressContainerView.frame = compulsoryAddressContainerViewFrame;
    
    CGRect bottomInformationContainerViewFrame = bottomInformationContainerView.frame;
    bottomInformationContainerViewFrame.origin.y = compulsoryAddressContainerView.frame.origin.y + compulsoryAddressContainerView.frame.size.height + 10;
    bottomInformationContainerView.frame = bottomInformationContainerViewFrame;
    
    CGRect mainContainerViewFrame = mainContainerView.frame;
    mainContainerViewFrame.size.height = bottomInformationContainerView.frame.origin.y + bottomInformationContainerView.frame.size.height;
    mainContainerView.frame = mainContainerViewFrame;
    
    schoolInformationContainerView.hidden = true;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, (mainContainerView.frame.origin.y + mainContainerView.frame.size.height));
    
    mainContainerView.autoresizesSubviews = true;
}

-(void)bindDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInChild == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild = [[Child alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strName = txtName.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strPhoneNumber = txtPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strAge = txtAge.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strGender = txtGender.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strHeight = txtHeight.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strWeight = txtWeight.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosDataForUpdation = self.arraySelectedPhotosData;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strHairColor = txtHairColor.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strHairStyle = txtHairStyle.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEyeColor = txtEyeColor.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strNationality = txtNationality.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactName = txtEmergencyName.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactPhoneNumber = txtEmergencyPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strEmergencyContactEmail = txtEmergencyEmail.text;
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.boolIsAddSchoolInformationSelected = self.boolIsAddSchoolInformationSelected;
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolStateId = self.objSelectedState.strStateID;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolStateName = txtSchoolState.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCityId = self.objSelectedCity.strCityID;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCityName = txtSchoolCity.text;
    [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolZipcode = txtSchoolZipCode.text;
    
    if(self.boolIsAddSchoolInformationSelected)
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolName = txtSchoolName.text;
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolAddress = txtViewSchoolAddress.text;
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolPhoneNumber = txtSchoolPhoneNumber.text;
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCurrentGrade = txtSchoolCurrentGrade.text;
    }
    else
    {
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolName = @"";
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolAddress = @"";
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolPhoneNumber = @"";
        [MySingleton sharedManager].dataManager.objLoggedInChild.strSchoolCurrentGrade = @"";
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInChild.strBestFriendsNames = txtViewBestFriendsNames.text;
}

- (IBAction)btnUpdateClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(([MySingleton sharedManager].dataManager.objLoggedInChild.arrayPhotosData.count + self.arraySelectedPhotosData.count) > 0)
    {
        [self bindDataToObject];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInChild isValidateChildForUpdation])
        {
            [[MySingleton sharedManager].dataManager updateChildProfile:[MySingleton sharedManager].dataManager.objLoggedInChild];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInChild.strValidationMessage];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please upload atleast one photo"];
        });
    }
}

@end
