//
//  CommonMoreAlertsFromThisChildViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 17/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonMoreAlertsFromThisChildViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewSearch;
@property (nonatomic,retain) IBOutlet UIButton *btnSearch;

@property (nonatomic,retain) IBOutlet UIView *searchTextContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtSearch;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseSearchTextContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseSearchTextContainerView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;

@property (nonatomic,retain) IBOutlet UIView *bottomContainerView;

@property (nonatomic,retain) IBOutlet UIView *moreParentRespondAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblMoreParentRespondAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMoreParentRespondAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnMoreParentRespondAlerts;

@property (nonatomic,retain) IBOutlet UIView *moreCommunityRespondAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblMoreCommunityRespondAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMoreCommunityRespondAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnMoreCommunityRespondAlerts;

//========== OTHER VARIABLES ==========//

@property (nonatomic,assign) BOOL boolIsLoadedFromMyAlert;
@property (nonatomic,retain) NSString *strIsLoadedBy;

@property (nonatomic,retain) NSMutableArray *dataRows;

@property (nonatomic,retain) NSString *strSelectedTab;
@property (nonatomic,retain) NSString *strSelectedChildId;

@end
