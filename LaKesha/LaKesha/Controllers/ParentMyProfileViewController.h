//
//  ParentMyProfileViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 17/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "AsyncImageView.h"

#import "State.h"
#import "City.h"

@interface ParentMyProfileViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewOptions;
@property (nonatomic,retain) IBOutlet UIButton *btnOptions;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet AsyncImageView *imageViewProfilePicture;

@property (nonatomic,retain) IBOutlet UILabel *lblEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail;
@property (nonatomic,retain) IBOutlet UIView *txtEmailBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblName;
@property (nonatomic,retain) IBOutlet UITextField *txtName;
@property (nonatomic,retain) IBOutlet UIView *txtNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *lblDoYouWantToAddAddressContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblDoYouWantToAddAddress;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewDoNotAddAddress;
@property (nonatomic,retain) IBOutlet UILabel *lblDoNotAddAddress;
@property (nonatomic,retain) IBOutlet UIButton *btnDoNotAddAddress;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewAddAddress;
@property (nonatomic,retain) IBOutlet UILabel *lblAddAddress;
@property (nonatomic,retain) IBOutlet UIButton *btnAddAddress;

@property (nonatomic,retain) IBOutlet UIView *addressContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblStreetAddress;
@property (nonatomic,retain) IBOutlet UITextView *txtViewStreetAddress;
@property (nonatomic,retain) IBOutlet UIView *txtViewStreetAddressBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *compulsoryAddressContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblState;
@property (nonatomic,retain) IBOutlet UITextField *txtState;
@property (nonatomic,retain) IBOutlet UIView *txtStateBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewStateDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblCity;
@property (nonatomic,retain) IBOutlet UITextField *txtCity;
@property (nonatomic,retain) IBOutlet UIView *txtCityBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCityDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblZipCode;
@property (nonatomic,retain) IBOutlet UITextField *txtZipCode;
@property (nonatomic,retain) IBOutlet UIView *txtZipCodeBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIView *bottomInformationContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblPhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblSecondaryPhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtSecondaryPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtSecondaryPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblGender;
@property (nonatomic,retain) IBOutlet UITextField *txtGender;
@property (nonatomic,retain) IBOutlet UIView *txtGenderBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewGenderDropDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblWorkPlaceInformation;

@property (nonatomic,retain) IBOutlet UILabel *lblWorkPlaceName;
@property (nonatomic,retain) IBOutlet UITextField *txtWorkPlaceName;
@property (nonatomic,retain) IBOutlet UIView *txtWorkPlaceNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblWorkPlacePhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtWorkPlacePhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtWorkPlacePhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIButton *btnUpdate;

//========== OTHER VARIABLES ==========//

@property (strong,nonatomic) UIToolbar* keyboardDoneButtonView;

@property (nonatomic,retain) NSMutableArray *arrayGender;
@property (nonatomic,retain) UIPickerView *genderPickerView;

@property (nonatomic,retain) UIPickerView *statePickerView;

@property (nonatomic,retain) State *objSelectedState;

@property (nonatomic,retain) UIPickerView *cityPickerView;

@property (nonatomic,retain) City *objSelectedCity;

@property (nonatomic,retain) UIImage *imageSelectedProfilePicture;
@property (nonatomic,retain) NSData *imageSelectedProfilePictureData;

@property (nonatomic,assign) BOOL boolIsAddAddressSelected;

@end
