//
//  CommonNearByAlertsViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 18/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface CommonNearByAlertsViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewSearch;
@property (nonatomic,retain) IBOutlet UIButton *btnSearch;

@property (nonatomic,retain) IBOutlet UIView *searchTextContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtSearch;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseSearchTextContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseSearchTextContainerView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;

@property (nonatomic,retain) IBOutlet UIView *bottomContainerView;

@property (nonatomic,retain) IBOutlet UIView *moreParentRespondAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblMoreParentRespondAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMoreParentRespondAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnMoreParentRespondAlerts;

@property (nonatomic,retain) IBOutlet UIView *moreCommunityRespondAlertsContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblMoreCommunityRespondAlerts;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMoreCommunityRespondAlerts;
@property (nonatomic,retain) IBOutlet UIButton *btnMoreCommunityRespondAlerts;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;
@property (nonatomic,retain) NSString *strLoadedFor;

@property (nonatomic,retain) NSString *strSelectedTab;
@property (nonatomic,assign) BOOL boolIsLoadedFromMyAlert;

@end
