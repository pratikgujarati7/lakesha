//
//  CommonViewLocationViewController.h
//  LaKesha
//
//  Created by Pratik Gujarati on 15/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildAlert.h"
#import "CommunityAlert.h"

#import <GoogleMaps/GoogleMaps.h>

@interface CommonViewLocationViewController : UIViewController<UIScrollViewDelegate, GMSMapViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet GMSMapView *mainMapView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) ChildAlert *objSelectedChildAlert;
@property (nonatomic,retain) CommunityAlert *objSelectedCommunityAlert;

@end
