//
//  City.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 28/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property(nonatomic,retain) NSString *strCityID;
@property(nonatomic,retain) NSString *strCityName;

@end
