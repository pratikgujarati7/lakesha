//
//  ParentRequest.h
//  LaKesha
//
//  Created by Pratik Gujarati on 10/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParentRequest : NSObject

@property(nonatomic,retain) NSString *strParentRequestID;
@property(nonatomic,retain) NSString *strParentProfilePictureImageUrl;
@property(nonatomic,retain) NSString *strParentName;
@property(nonatomic,retain) NSString *strParentRole;
@property(nonatomic,retain) NSString *strParentWorkPlaceName;

@end
