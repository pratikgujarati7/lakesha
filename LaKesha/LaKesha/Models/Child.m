//
//  Child.m
//  LaKesha
//
//  Created by Pratik Gujarati on 04/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "Child.h"

@implementation Child

-(BOOL)isValidateChildForLogin
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    //================BLANK FIELD VALIDATION===========//
    if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 )
    {
        if(self.strEmail.length <= 0)
        {
            self.strValidationMessage = @"Please enter email";
            return false;
        }
        else if(self.strPassword.length <= 0)
        {
            self.strValidationMessage = @"Please enter password";
            return false;
        }
    }
    else if(![objUtility isValidEmailAddress:self.strEmail])
    {
        self.strValidationMessage = @"Invalid email address";
        return false;
    }
    else
    {
        return true;
    }
    
    return true;
}

-(BOOL)isValidateChildForRegistration
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if(self.boolIsAddSchoolInformationSelected)
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 || self.strName.length <= 0 || self.strPhoneNumber.length <= 0 || self.strAge.length <= 0 || self.strGender.length <= 0 || self.strHeight.length <= 0 || self.strWeight.length <= 0 || self.strHairColor.length <= 0  || self.strHairStyle.length <= 0|| self.strEyeColor.length <= 0 || self.strNationality.length <= 0 || self.strEmergencyContactName.length <= 0 || self.strEmergencyContactPhoneNumber.length <= 0 || self.strEmergencyContactEmail.length <= 0 || self.strSchoolName.length <= 0 || self.strSchoolStateName.length <= 0 || self.strSchoolCityName.length <= 0  || self.strSchoolZipcode.length <= 0 || (self.strSchoolAddress.length <= 0 || [self.strSchoolAddress isEqualToString:@"Address"]) || self.strSchoolPhoneNumber.length <= 0 || self.strSchoolCurrentGrade.length <= 0 || (self.strBestFriendsNames.length <= 0  || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"]))
        {
            if(self.strEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter email";
                return false;
            }
            else if(self.strPassword.length <= 0)
            {
                self.strValidationMessage = @"Please enter password";
                return false;
            }
            else if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strAge.length <= 0)
            {
                self.strValidationMessage = @"Please enter your age";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strHeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your height";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strHairColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair color";
                return false;
            }
            else if(self.strHairStyle.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair style";
                return false;
            }
            else if(self.strEyeColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your eye color";
                return false;
            }
            else if(self.strNationality.length <= 0)
            {
                self.strValidationMessage = @"Please enter your nationality";
                return false;
            }
            else if(self.strEmergencyContactName.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact name";
                return false;
            }
            else if(self.strEmergencyContactPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact phone number";
                return false;
            }
            else if(self.strEmergencyContactEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter your emergency contact email";
                return false;
            }
            else if(self.strSchoolName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school name";
                return false;
            }
            else if(self.strSchoolStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school state";
                return false;
            }
            else if(self.strSchoolCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school city";
                return false;
            }
            else if(self.strSchoolZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school zipcode";
                return false;
            }
            else if(self.strSchoolAddress.length <= 0 || [self.strSchoolAddress isEqualToString:@"Address"])
            {
                self.strValidationMessage = @"Please enter your school address";
                return false;
            }
            else if(self.strSchoolPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school phone number";
                return false;
            }
            else if(self.strSchoolCurrentGrade.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school current grade";
                return false;
            }
            else if(self.strBestFriendsNames.length <= 0 || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"])
            {
                self.strValidationMessage = @"Please enter your best friends names";
                return false;
            }
            
        }
        else if(![objUtility isValidEmailAddress:self.strEmail])
        {
            self.strValidationMessage = @"Please enter a valid email address";
            return false;
        }
        else if(![objUtility isValidEmailAddress:self.strEmergencyContactEmail])
        {
            self.strValidationMessage = @"Please enter a valid emergency contact email address";
            return false;
        }
        else if(self.arrayPhotosData.count <= 0)
        {
            self.strValidationMessage = @"Please upload atleast one photo";
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 || self.strName.length <= 0 || self.strPhoneNumber.length <= 0 || self.strAge.length <= 0 || self.strGender.length <= 0 || self.strHeight.length <= 0 || self.strWeight.length <= 0 || self.strHairColor.length <= 0  || self.strHairStyle.length <= 0|| self.strEyeColor.length <= 0 || self.strNationality.length <= 0 || self.strEmergencyContactName.length <= 0 || self.strEmergencyContactPhoneNumber.length <= 0 || self.strEmergencyContactEmail.length <= 0 || self.strSchoolStateName.length <= 0 || self.strSchoolCityName.length <= 0  || self.strSchoolZipcode.length <= 0  || (self.strBestFriendsNames.length <= 0  || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"]))
        {
            if(self.strEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter email";
                return false;
            }
            else if(self.strPassword.length <= 0)
            {
                self.strValidationMessage = @"Please enter password";
                return false;
            }
            else if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strAge.length <= 0)
            {
                self.strValidationMessage = @"Please enter your age";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strHeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your height";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strHairColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair color";
                return false;
            }
            else if(self.strHairStyle.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair style";
                return false;
            }
            else if(self.strEyeColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your eye color";
                return false;
            }
            else if(self.strNationality.length <= 0)
            {
                self.strValidationMessage = @"Please enter your nationality";
                return false;
            }
            else if(self.strEmergencyContactName.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact name";
                return false;
            }
            else if(self.strEmergencyContactPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact phone number";
                return false;
            }
            else if(self.strEmergencyContactEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter your emergency contact email";
                return false;
            }
            else if(self.strSchoolStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school state";
                return false;
            }
            else if(self.strSchoolCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school city";
                return false;
            }
            else if(self.strSchoolZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school zipcode";
                return false;
            }
            else if(self.strBestFriendsNames.length <= 0 || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"])
            {
                self.strValidationMessage = @"Please enter your best friends names";
                return false;
            }
            
        }
        else if(![objUtility isValidEmailAddress:self.strEmail])
        {
            self.strValidationMessage = @"Please enter a valid email address";
            return false;
        }
        else if(![objUtility isValidEmailAddress:self.strEmergencyContactEmail])
        {
            self.strValidationMessage = @"Please enter a valid emergency contact email address";
            return false;
        }
        else if(self.arrayPhotosData.count <= 0)
        {
            self.strValidationMessage = @"Please upload atleast one photo";
            return false;
        }
        else
        {
            return true;
        }
    }
    
    return true;
}

-(BOOL)isValidateChildForUpdation
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if(self.boolIsAddSchoolInformationSelected)
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strName.length <= 0 || self.strPhoneNumber.length <= 0 || self.strAge.length <= 0 || self.strGender.length <= 0 || self.strHeight.length <= 0 || self.strWeight.length <= 0 || self.strHairColor.length <= 0 || self.strHairStyle.length <= 0  || self.strEyeColor.length <= 0 || self.strNationality.length <= 0 || self.strEmergencyContactName.length <= 0 || self.strEmergencyContactPhoneNumber.length <= 0 || self.strEmergencyContactEmail.length <= 0 || self.strSchoolName.length <= 0 || self.strSchoolStateName.length <= 0 || self.strSchoolCityName.length <= 0  || self.strSchoolZipcode.length <= 0 || (self.strSchoolAddress.length <= 0 || [self.strSchoolAddress isEqualToString:@"Address"]) || self.strSchoolPhoneNumber.length <= 0 || self.strSchoolCurrentGrade.length <= 0 || (self.strBestFriendsNames.length <= 0  || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"]))
        {
            if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strAge.length <= 0)
            {
                self.strValidationMessage = @"Please enter your age";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strHeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your height";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strHairColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair color";
                return false;
            }
            else if(self.strHairStyle.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair style";
                return false;
            }
            else if(self.strEyeColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your eye color";
                return false;
            }
            else if(self.strNationality.length <= 0)
            {
                self.strValidationMessage = @"Please enter your nationality";
                return false;
            }
            else if(self.strEmergencyContactName.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact name";
                return false;
            }
            else if(self.strEmergencyContactPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact phone number";
                return false;
            }
            else if(self.strEmergencyContactEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter your emergency contact email";
                return false;
            }
            else if(self.strSchoolName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school name";
                return false;
            }
            else if(self.strSchoolStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school state";
                return false;
            }
            else if(self.strSchoolCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school city";
                return false;
            }
            else if(self.strSchoolZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school zipcode";
                return false;
            }
            else if(self.strSchoolAddress.length <= 0 || [self.strSchoolAddress isEqualToString:@"Address"])
            {
                self.strValidationMessage = @"Please enter your school address";
                return false;
            }
            else if(self.strSchoolPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school phone number";
                return false;
            }
            else if(self.strSchoolCurrentGrade.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school current grade";
                return false;
            }
            else if(self.strBestFriendsNames.length <= 0 || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"])
            {
                self.strValidationMessage = @"Please enter your best friends names";
                return false;
            }
            
        }
        else if(![objUtility isValidEmailAddress:self.strEmergencyContactEmail])
        {
            self.strValidationMessage = @"Please enter a valid emergency contact email address";
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strName.length <= 0 || self.strPhoneNumber.length <= 0 || self.strAge.length <= 0 || self.strGender.length <= 0 || self.strHeight.length <= 0 || self.strWeight.length <= 0 || self.strHairColor.length <= 0 || self.strHairStyle.length <= 0  || self.strEyeColor.length <= 0 || self.strNationality.length <= 0 || self.strEmergencyContactName.length <= 0 || self.strEmergencyContactPhoneNumber.length <= 0 || self.strEmergencyContactEmail.length <= 0 || self.strSchoolStateName.length <= 0 || self.strSchoolCityName.length <= 0  || self.strSchoolZipcode.length <= 0  || (self.strBestFriendsNames.length <= 0  || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"]))
        {
            if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strAge.length <= 0)
            {
                self.strValidationMessage = @"Please enter your age";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strHeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your height";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strWeight.length <= 0)
            {
                self.strValidationMessage = @"Please enter your weight";
                return false;
            }
            else if(self.strHairColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair color";
                return false;
            }
            else if(self.strHairStyle.length <= 0)
            {
                self.strValidationMessage = @"Please enter your hair style";
                return false;
            }
            else if(self.strEyeColor.length <= 0)
            {
                self.strValidationMessage = @"Please enter your eye color";
                return false;
            }
            else if(self.strNationality.length <= 0)
            {
                self.strValidationMessage = @"Please enter your nationality";
                return false;
            }
            else if(self.strEmergencyContactName.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact name";
                return false;
            }
            else if(self.strEmergencyContactPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter emergency contact phone number";
                return false;
            }
            else if(self.strEmergencyContactEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter your emergency contact email";
                return false;
            }
            else if(self.strSchoolStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school state";
                return false;
            }
            else if(self.strSchoolCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select your school city";
                return false;
            }
            else if(self.strSchoolZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your school zipcode";
                return false;
            }
            else if(self.strBestFriendsNames.length <= 0 || [self.strBestFriendsNames isEqualToString:@"Best Friends Names"])
            {
                self.strValidationMessage = @"Please enter your best friends names";
                return false;
            }
            
        }
        else if(![objUtility isValidEmailAddress:self.strEmergencyContactEmail])
        {
            self.strValidationMessage = @"Please enter a valid emergency contact email address";
            return false;
        }
        else
        {
            return true;
        }
    }
    
    return true;
}

@end
