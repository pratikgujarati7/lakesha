//
//  Notification.h
//  LaKesha
//
//  Created by INNOVATIVE ITERATION 4 on 09/10/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject

@property(nonatomic,retain) NSString *strNotificationID;
@property(nonatomic,retain) NSString *strNotificationType;
@property(nonatomic,retain) NSString *strNotificationTitle;
@property(nonatomic,retain) NSString *strNotificationText;
@property(nonatomic,retain) NSString *strNotificationTime;
@property(nonatomic,retain) NSString *strNotificationStateID;
@property(nonatomic,retain) NSString *strNotificationStateName;
@property(nonatomic,retain) NSString *strNotificationCityID;
@property(nonatomic,retain) NSString *strNotificationCityName;

//type 0 -> nation wide
//type 1 -> state wide
//type 2 -> city wide
//type 3 -> boycott alerts

@end
