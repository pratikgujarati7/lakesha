//
//  ChildPhoto.h
//  LaKesha
//
//  Created by Pratik Gujarati on 03/07/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChildPhoto : NSObject

@property(nonatomic,retain) NSString *strChildPhotoID;
@property(nonatomic,retain) NSURL *childPhotoUrl;

@end
