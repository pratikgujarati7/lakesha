//
//  Child.h
//  LaKesha
//
//  Created by Pratik Gujarati on 04/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonUtility.h"

@interface Child : NSObject

@property(nonatomic,retain) NSString *strValidationMessage;

@property(nonatomic,retain) NSString *strChildID;
@property(nonatomic,retain) NSString *strEmail;
@property(nonatomic,retain) NSString *strPassword;
@property(nonatomic,retain) NSString *strName;
@property(nonatomic,retain) NSString *strPhoneNumber;
@property(nonatomic,retain) NSString *strAge;
@property(nonatomic,retain) NSString *strGender;
@property(nonatomic,retain) NSString *strHeight;
@property(nonatomic,retain) NSString *strWeight;
@property(nonatomic,retain) NSMutableArray *arrayPhotosData;
@property(nonatomic,retain) NSMutableArray *arrayPhotosDataForUpdation;
@property(nonatomic,retain) NSMutableArray *arrayPhotosObjects;
@property(nonatomic,retain) NSString *strHairColor;
@property(nonatomic,retain) NSString *strHairStyle;
@property(nonatomic,retain) NSString *strEyeColor;
@property(nonatomic,retain) NSString *strNationality;

@property(nonatomic,retain) NSString *strEmergencyContactName;
@property(nonatomic,retain) NSString *strEmergencyContactPhoneNumber;
@property(nonatomic,retain) NSString *strEmergencyContactEmail;

@property(nonatomic,retain) NSString *strSchoolName;
@property(nonatomic,retain) NSString *strSchoolStateId;
@property(nonatomic,retain) NSString *strSchoolStateName;
@property(nonatomic,retain) NSString *strSchoolCityId;
@property(nonatomic,retain) NSString *strSchoolCityName;
@property(nonatomic,retain) NSString *strSchoolZipcode;
@property(nonatomic,retain) NSString *strSchoolAddress;
@property(nonatomic,retain) NSString *strSchoolPhoneNumber;
@property(nonatomic,retain) NSString *strSchoolCurrentGrade;

@property(nonatomic,retain) NSString *strBestFriendsNames;

@property(nonatomic,retain) NSString *strDeviceToken;

@property(nonatomic,retain) NSString *strProfilePictureImageUrl;
@property(nonatomic,retain) NSString *strChildLatitude;
@property(nonatomic,retain) NSString *strChildLongitude;
@property(nonatomic,assign) BOOL boolIsChildAlertActive;
@property(nonatomic,retain) NSMutableArray *arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers;
@property(nonatomic,retain) NSMutableArray *arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers;
@property(nonatomic,retain) NSMutableArray *arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers;
@property(nonatomic,retain) NSMutableArray *arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert;
@property(nonatomic,retain) NSMutableArray *arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert;

@property(nonatomic,assign) BOOL boolIsPhoneNumberVerified;

@property(nonatomic,retain) NSString *strAuthorityEmail;

@property(nonatomic,retain) NSString *strParentRequest;

@property(nonatomic,assign) BOOL boolIsAddSchoolInformationSelected;

-(BOOL)isValidateChildForLogin;
-(BOOL)isValidateChildForRegistration;
-(BOOL)isValidateChildForUpdation;

@end
