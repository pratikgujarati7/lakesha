//
//  Donator.h
//  LaKesha
//
//  Created by Pratik Gujarati on 14/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Donator : NSObject

@property(nonatomic,retain) NSString *strDonatorID;
@property(nonatomic,retain) NSString *strDonatorName;

@end
