//
//  Parent.h
//  LaKesha
//
//  Created by Pratik Gujarati on 05/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonUtility.h"

@interface Parent : NSObject

@property(nonatomic,retain) NSString *strValidationMessage;

@property(nonatomic,retain) NSString *strParentID;
@property(nonatomic,retain) NSString *strEmail;
@property(nonatomic,retain) NSString *strPassword;
@property(nonatomic,retain) NSString *strName;
@property(nonatomic,retain) NSString *strStateId;
@property(nonatomic,retain) NSString *strStateName;
@property(nonatomic,retain) NSString *strCityId;
@property(nonatomic,retain) NSString *strCityName;
@property(nonatomic,retain) NSString *strZipcode;
@property(nonatomic,retain) NSString *strStreetAddress;
@property(nonatomic,retain) NSString *strPhoneNumber;
@property(nonatomic,retain) NSString *strSecondaryPhoneNumber;
@property(nonatomic,retain) NSString *strGender;

@property(nonatomic,retain) NSString *strWorkPlaceName;
@property(nonatomic,retain) NSString *strWorkPlacePhoneNumber;

@property(nonatomic,retain) NSString *strDeviceToken;

@property(nonatomic,retain) NSString *strProfilePictureImageUrl;

@property(nonatomic,retain) NSString *strParentResponseTime;
@property(nonatomic,retain) NSString *strAuthorityEmail;

@property(nonatomic,assign) BOOL boolIsPhoneNumberVerified;

@property(nonatomic,assign) BOOL boolIsAddAddressSelected;

@property(nonatomic,assign) BOOL boolIsPaid;

-(BOOL)isValidateParentForLogin;
-(BOOL)isValidateParentForRegistration;
-(BOOL)isValidateParentForUpdation;

//FOR CHILD APP

@property(nonatomic,retain) NSString *strParentRole;

@end
