//
//  Parent.m
//  LaKesha
//
//  Created by Pratik Gujarati on 05/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "Parent.h"

@implementation Parent

-(BOOL)isValidateParentForLogin
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    //================BLANK FIELD VALIDATION===========//
    if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 )
    {
        if(self.strEmail.length <= 0)
        {
            self.strValidationMessage = @"Please enter email";
            return false;
        }
        else if(self.strPassword.length <= 0)
        {
            self.strValidationMessage = @"Please enter password";
            return false;
        }
    }
    else if(![objUtility isValidEmailAddress:self.strEmail])
    {
        self.strValidationMessage = @"Invalid email address";
        return false;
    }
    else
    {
        return true;
    }
    
    return true;
}

-(BOOL)isValidateParentForRegistration
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if(self.boolIsAddAddressSelected)
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 || self.strName.length <= 0 || (self.strStreetAddress.length <= 0 || [self.strStreetAddress isEqualToString:@"Street Address"]) || self.strStateName.length <= 0 || self.strCityName.length <= 0  || self.strZipcode.length <= 0 || self.strPhoneNumber.length <= 0 || self.strGender.length <= 0 || self.strWorkPlaceName.length <= 0 || self.strWorkPlacePhoneNumber.length <= 0)
        {
            if(self.strEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter email";
                return false;
            }
            else if(self.strPassword.length <= 0)
            {
                self.strValidationMessage = @"Please enter password";
                return false;
            }
            else if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strStreetAddress.length <= 0 || [self.strStreetAddress isEqualToString:@"Street Address"])
            {
                self.strValidationMessage = @"Please enter your address";
                return false;
            }
            else if(self.strStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select a state";
                return false;
            }
            else if(self.strCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select a city";
                return false;
            }
            else if(self.strZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your zipcode";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strWorkPlaceName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place name";
                return false;
            }
            else if(self.strWorkPlacePhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place phone number";
                return false;
            }
        }
        else if(![objUtility isValidEmailAddress:self.strEmail])
        {
            self.strValidationMessage = @"Please enter a valid email address";
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 || self.strName.length <= 0 || self.strStateName.length <= 0 || self.strCityName.length <= 0  || self.strZipcode.length <= 0 || self.strPhoneNumber.length <= 0 || self.strGender.length <= 0 || self.strWorkPlaceName.length <= 0 || self.strWorkPlacePhoneNumber.length <= 0)
        {
            if(self.strEmail.length <= 0)
            {
                self.strValidationMessage = @"Please enter email";
                return false;
            }
            else if(self.strPassword.length <= 0)
            {
                self.strValidationMessage = @"Please enter password";
                return false;
            }
            else if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select a state";
                return false;
            }
            else if(self.strCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select a city";
                return false;
            }
            else if(self.strZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your zipcode";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strWorkPlaceName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place name";
                return false;
            }
            else if(self.strWorkPlacePhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place phone number";
                return false;
            }
        }
        else if(![objUtility isValidEmailAddress:self.strEmail])
        {
            self.strValidationMessage = @"Please enter a valid email address";
            return false;
        }
        else
        {
            return true;
        }
    }
    
    return true;
}

-(BOOL)isValidateParentForUpdation
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if(self.boolIsAddAddressSelected)
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strName.length <= 0 || (self.strStreetAddress.length <= 0 || [self.strStreetAddress isEqualToString:@"Street Address"]) || self.strStateName.length <= 0 || self.strCityName.length <= 0  || self.strZipcode.length <= 0 || self.strPhoneNumber.length <= 0 || self.strGender.length <= 0 || self.strWorkPlaceName.length <= 0 || self.strWorkPlacePhoneNumber.length <= 0)
        {
            if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strStreetAddress.length <= 0 || [self.strStreetAddress isEqualToString:@"Street Address"])
            {
                self.strValidationMessage = @"Please enter your address";
                return false;
            }
            else if(self.strStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select a state";
                return false;
            }
            else if(self.strCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select a city";
                return false;
            }
            else if(self.strZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your zipcode";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strWorkPlaceName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place name";
                return false;
            }
            else if(self.strWorkPlacePhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place phone number";
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        //================BLANK FIELD VALIDATION===========//
        if(self.strName.length <= 0 || self.strStateName.length <= 0 || self.strCityName.length <= 0  || self.strZipcode.length <= 0 || self.strPhoneNumber.length <= 0 || self.strGender.length <= 0 || self.strWorkPlaceName.length <= 0 || self.strWorkPlacePhoneNumber.length <= 0)
        {
            if(self.strName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your name";
                return false;
            }
            else if(self.strStateName.length <= 0)
            {
                self.strValidationMessage = @"Please select a state";
                return false;
            }
            else if(self.strCityName.length <= 0)
            {
                self.strValidationMessage = @"Please select a city";
                return false;
            }
            else if(self.strZipcode.length <= 0)
            {
                self.strValidationMessage = @"Please enter your zipcode";
                return false;
            }
            else if(self.strPhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your phone number";
                return false;
            }
            else if(self.strGender.length <= 0)
            {
                self.strValidationMessage = @"Please select your gender";
                return false;
            }
            else if(self.strWorkPlaceName.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place name";
                return false;
            }
            else if(self.strWorkPlacePhoneNumber.length <= 0)
            {
                self.strValidationMessage = @"Please enter your work place phone number";
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    
    return true;
}

@end
