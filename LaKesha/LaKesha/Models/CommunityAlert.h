//
//  CommunityAlert.h
//  LaKesha
//
//  Created by Pratik Gujarati on 17/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommunityAlert : NSObject

@property(nonatomic,retain) NSString *strCommunityAlertID;
@property(nonatomic,retain) NSString *strCommunityAlertDateAndTime;
@property(nonatomic,retain) NSString *strCommunityAlertMessageFromChild;
@property(nonatomic,retain) NSString *strCommunityAlertMessageFromParent;

@property(nonatomic,retain) NSString *strChildID;
@property(nonatomic,retain) NSString *strEmail;
@property(nonatomic,retain) NSString *strPassword;
@property(nonatomic,retain) NSString *strName;
@property(nonatomic,retain) NSString *strPhoneNumber;
@property(nonatomic,retain) NSString *strAge;
@property(nonatomic,retain) NSString *strGender;
@property(nonatomic,retain) NSString *strHeight;
@property(nonatomic,retain) NSString *strWeight;
@property(nonatomic,retain) NSMutableArray *arrayChildPhotoUrls;
@property(nonatomic,retain) NSString *strHairColor;
@property(nonatomic,retain) NSString *strHairStyle;
@property(nonatomic,retain) NSString *strEyeColor;
@property(nonatomic,retain) NSString *strNationality;

@property(nonatomic,retain) NSString *strEmergencyContactName;
@property(nonatomic,retain) NSString *strEmergencyContactPhoneNumber;
@property(nonatomic,retain) NSString *strEmergencyContactEmail;

@property(nonatomic,retain) NSString *strSchoolName;
@property(nonatomic,retain) NSString *strSchoolAddress;
@property(nonatomic,retain) NSString *strSchoolPhoneNumber;
@property(nonatomic,retain) NSString *strSchoolCurrentGrade;

@property(nonatomic,retain) NSString *strBestFriendsNames;

@property(nonatomic,retain) NSString *strProfilePictureImageUrl;
@property(nonatomic,retain) NSString *strChildLatitude;
@property(nonatomic,retain) NSString *strChildLongitude;
@property(nonatomic,assign) BOOL boolIsCommunityAlertActive;
@property(nonatomic,retain) NSMutableArray *arrayInformationTypesToDisplayToOthers;
@property(nonatomic,retain) NSMutableArray *arrayInformationTypesToSendToAuthority;

@end
