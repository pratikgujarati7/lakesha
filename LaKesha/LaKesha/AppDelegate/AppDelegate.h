//
//  AppDelegate.h
//  PROJECT
//
//  Created by Pratik Gujarati on 00/00/00.
//  Copyright © 2017 Innovative Iteration Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashViewController.h"
#import "MBProgressHUD.h"
#import "NYAlertViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SplashViewController *splashVC;
@property (strong, nonatomic) UINavigationController *navC;

-(void)showErrorAlertViewWithTitle:(NSString *)title withDetails:(NSString *)detail;

-(MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title;
-(void)dismissGlobalHUD;

-(BOOL)isClock24Hour;

@end

