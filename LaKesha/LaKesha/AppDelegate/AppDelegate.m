//
//  AppDelegate.m
//  PROJECT
//
//  Created by Pratik Gujarati on 00/00/00.
//  Copyright © 2017 Innovative Iteration Software Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "MySingleton.h"

@import GoogleMaps;
@import GooglePlaces;

//@import Stripe;
#import "Stripe.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize splashVC,navC;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //TEST PUBLISHABLE KEY
//    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_foUSA62DrLi1HtC8GQmxuNef"];
    //LIVE PUBLISHABLE KEY
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_live_z4SBfs1cWOCTSyIj7JKJinRD"];
    
//    [[STPPaymentConfiguration sharedConfiguration] setAppleMerchantIdentifier:@"merchant.com.LaKesha"];
    [[STPTheme defaultTheme] setAccentColor:[UIColor orangeColor]];
    
    [GMSServices provideAPIKey:@"AIzaSyCf73bM6ocTCF34nTW0S93GF6csEPc55cw"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyCf73bM6ocTCF34nTW0S93GF6csEPc55cw"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSUUID *deviceId;
    #if !TARGET_IPHONE_SIMULATOR
        deviceId = [UIDevice currentDevice].identifierForVendor;
    #else
        deviceId = [[NSUUID alloc] initWithUUIDString:@"A5D59C2F-FE68-4BE7-B318-95029619C759"];
    #endif
    
    NSString *strDeviceId = [deviceId UUIDString];
    
    [prefs setObject:strDeviceId forKey:@"uniqueDeviceId"];
    [prefs synchronize];
    
    
    #if !(TARGET_IPHONE_SIMULATOR)
        //-- Set Notification
        if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
        {
            // iOS 8 Notifications
        
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            // iOS < 8 Notifications
            [application registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        }
    #endif
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.splashVC = [[SplashViewController alloc] init];
    self.navC = [[UINavigationController alloc]initWithRootViewController:self.splashVC];
    self.window.rootViewController = self.navC;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self.window endEditing:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationDidEnterBackground" object:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Notification Methods

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *strDeviceToken = [[[deviceToken description]
                                 stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                                stringByReplacingOccurrencesOfString:@" "
                                withString:@""];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:strDeviceToken forKey:@"deviceToken"];
    [prefs synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError, error  : %@",error);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"" forKey:@"deviceToken"];
    [prefs synchronize];
}

// will be called when in foreground
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"didReceiveRemoteNotification called.");
    
    UIApplicationState state = [application applicationState];
    
    if (state == UIApplicationStateActive)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        NSString *strGuestId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"guestid"]];
        NSString *strAutoLogin = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"autologin"]];
        
        if(((strChildId != nil && ![strChildId isEqualToString:@"(null)"]) && strChildId.length > 0) && ([strAutoLogin isEqualToString:@"1"]))
        {
            NSString *strNotificationType = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"notification_type"]];
            if ([strNotificationType isEqualToString:@"116"] || [strNotificationType isEqualToString:@"118"])
            {
                NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
                
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                alertViewController.title = @"Notification";
                alertViewController.message = message;
                
                alertViewController.view.tintColor = [UIColor whiteColor];
                alertViewController.backgroundTapDismissalGestureEnabled = YES;
                alertViewController.swipeDismissalGestureEnabled = YES;
                alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                
                alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                    
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                    
//                    NSString *value = @"911";
//                    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel://%@",value]];
//                    [[UIApplication sharedApplication] openURL:url];
                    
                    NSString *phoneNumber = [@"tel://" stringByAppendingString:@"911"];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                    
                }]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                });
            }
            else
            {
                NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
                
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                alertViewController.title = @"Notification";
                alertViewController.message = message;
                
                alertViewController.view.tintColor = [UIColor whiteColor];
                alertViewController.backgroundTapDismissalGestureEnabled = YES;
                alertViewController.swipeDismissalGestureEnabled = YES;
                alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                
                alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                    
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                    
                }]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                });
            }
        }
        else
        {
            
            NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
            
            NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
            alertViewController.title = @"Notification";
            alertViewController.message = message;
            
            alertViewController.view.tintColor = [UIColor whiteColor];
            alertViewController.backgroundTapDismissalGestureEnabled = YES;
            alertViewController.swipeDismissalGestureEnabled = YES;
            alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
            
            alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
            alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
            alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
            alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                
                [alertViewController dismissViewControllerAnimated:YES completion:nil];
                
            }]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
            });
        }
    }
    else
    {
        //Do stuff that you would do if the application was not active
    }
}

#pragma mark - Other Methods

-(MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title
{
    [self dismissGlobalHUD];
    UIWindow *window = [(AppDelegate *)[[UIApplication sharedApplication] delegate] window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.label.text = title;
    hud.dimBackground = YES;
    return hud;
}

-(void)dismissGlobalHUD
{
    UIWindow *window = [(AppDelegate *)[[UIApplication sharedApplication] delegate] window];
    [MBProgressHUD hideHUDForView:window animated:YES];
}

//=========================FUNCTION TO SHOW THE HHAlertView ========================//

-(void)showErrorAlertViewWithTitle:(NSString *)title withDetails:(NSString *)detail
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = title;
    alertViewController.message = detail;
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(BOOL)isClock24Hour
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
    return is24h;
}

@end
