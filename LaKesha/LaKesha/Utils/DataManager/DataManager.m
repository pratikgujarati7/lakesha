 //
//  DataManager.m
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import "DataManager.h"
#import "MySingleton.h"

@implementation DataManager

-(id)init
{
    _dictionaryWebservicesUrls = [NSDictionary dictionaryWithContentsOfFile: [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent: @"WebservicesUrls.plist"]];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return self;
}

- (BOOL) isNetworkAvailable
{
    Reachability *reach = [Reachability reachabilityWithHostName:[_dictionaryWebservicesUrls objectForKey:@"AvailabilityHostToCheck"]];
    NetworkStatus status = [reach currentReachabilityStatus];
    isNetworkAvailable = [NSNumber numberWithBool:!(status == NotReachable)];
    reach = nil;
    return [isNetworkAvailable boolValue];
}

-(void)showInternetNotConnectedError
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"No Internet Connection";
    alertViewController.message = @"Please make sure that you are connected to the internet.";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action)
    {
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        if (UIApplicationOpenSettingsURLString != NULL)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }]];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
}

-(void)showErrorMessage:(NSString *)errorTitle withErrorContent:(NSString *)errorDescription
{
    if([errorTitle isEqualToString:@"Server Error"])
    {
        errorDescription = @"Oops! Something went wrong. Please try again later.";
    }
    else
    {
        errorDescription = errorDescription;
    }
    
    [appDelegate dismissGlobalHUD];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:errorTitle withDetails:errorDescription];
    });
}

- (void)connectionError
{
    [appDelegate dismissGlobalHUD];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"connectionErrorEvent" object:nil];
    [self showInternetNotConnectedError];
}

//==================== COMMON/OTHER USER APP WEBSERVICES ====================//
#pragma mark - COMMON/OTHER USER APP WEBSERVICES -

#pragma mark FUNCTION TO GET ALL DONATORS

-(void)getAllDonators
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllDonators"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
//
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getalldonators" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
                //========== FILL DONATORS ARRAY ==========//
                NSArray *arrayDonatorsList = [jsonResult objectForKey:@"donators"];
                
                self.arrayAllDonators = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayDonatorsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayDonatorsList objectAtIndex:i];
                    
                    Donator *objDonator = [[Donator alloc] init];
                    objDonator.strDonatorName = [currentDictionary objectForKey:@"DonatorName"];
                    
                    [self.arrayAllDonators addObject:objDonator];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllDonatorsEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL STATES

-(void)getAllStates
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllStates"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL STATES ARRAY ==========//
                NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                
                self.arrayStates = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayStateList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                    
                    State *objState = [[State alloc] init];
                    objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                    objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                    
                    NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                    
                    objState.arrayCity = [[NSMutableArray alloc] init];
                    
                    for(int j = 0 ; j < arrayCityList.count; j++)
                    {
                        NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                        
                        City *objCity = [[City alloc] init];
                        objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                        objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                        
                        [objState.arrayCity addObject:objCity];
                    }
                    
                    [self.arrayStates addObject:objState];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllStatesEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR FORGET PASSWORD

-(void)forgetPassword:(NSString *)strEmail withUserType:(NSString *)strUserType;
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ForgetPassword"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strEmail forKey:@"email"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showErrorMessage:@"" withErrorContent:message];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"forgotPasswordEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO SEND LOGGED IN CHILD/PARENT'S CURRENT LOCATION TO SERVER ON SPLASH

-(void)sendLoggedInChildOrParentOrGuestCurrentLocationToServer
{
    if([self isNetworkAvailable])
    {
//        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"SendLoggedInChildOrParentOrGuestCurrentLocationToServer"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
//        NSString *strParentId = [prefs objectForKey:@"parentid"];
//        NSString *strChildId = [prefs objectForKey:@"childid"];
//        NSString *strGuestId = [prefs objectForKey:@"guestid"];
        
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        NSString *strGuestId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"guestid"]];
        
        NSString *strUserType = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"usertype"]];
        
        if(((strChildId != nil && ![strChildId isEqualToString:@"(null)"]) && strChildId.length > 0) && (strUserType != nil && [strUserType isEqualToString:@"0"]))
        {
            [parameters setObject:strChildId forKey:@"user_id"];
            [parameters setObject:@"0" forKey:@"user_type"];
        }
        else if(((strParentId != nil  && ![strParentId isEqualToString:@"(null)"]) && strParentId.length > 0) && (strUserType != nil && [strUserType isEqualToString:@"1"]))
        {
            [parameters setObject:strParentId forKey:@"user_id"];
            [parameters setObject:@"1" forKey:@"user_type"];
        }
        else if(((strGuestId != nil && ![strGuestId isEqualToString:@"(null)"]) && strGuestId.length > 0) && (strUserType != nil && [strUserType isEqualToString:@"2"]))
        {
            [parameters setObject:strGuestId forKey:@"user_id"];
            [parameters setObject:@"2" forKey:@"user_type"];
        }
        
        [parameters setObject:self.strLocationLatitude forKey:@"latitude"];
        [parameters setObject:self.strLocationLongitude forKey:@"longitude"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sendloggedinchildorparentorguestcurrentlocationtoserver" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"sentLoggedInChildOrParentOrGuestCurrentLocationToServerEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR GUEST SIGNUP

//-(void)guestSignUp:(NSDictionary *)dictParameters
-(void)internationalGuestSignUp:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"InternationalGuestSignUp"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strDeviceToken = [prefs objectForKey:@"deviceToken"];
        if(strDeviceToken.length > 0)
        {
            [parameters setObject:strDeviceToken forKey:@"device_id"];
        }
        else
        {
            [parameters setObject:@"" forKey:@"device_id"];
        }
        [parameters setObject:self.strLocationLatitude forKey:@"latitude"];
        [parameters setObject:self.strLocationLongitude forKey:@"longitude"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        [parameters setObject:[dictParameters objectForKey:@"continent"] forKey:@"continent"];
        [parameters setObject:[dictParameters objectForKey:@"email"] forKey:@"email"];
        [parameters setObject:[dictParameters objectForKey:@"zipcode"] forKey:@"zipcode"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"guestSignUp" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:[NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"guest_id"]] forKey:@"guestid"];
                        [prefs setObject:@"2" forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"internationalGuestSignedUpEvent" object:nil];
                    });
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR GUEST SIGNUP

-(void)guestSignUp:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GuestSignUp"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strDeviceToken = [prefs objectForKey:@"deviceToken"];
        if(strDeviceToken.length > 0)
        {
            [parameters setObject:strDeviceToken forKey:@"device_id"];
        }
        else
        {
            [parameters setObject:@"" forKey:@"device_id"];
        }
        [parameters setObject:self.strLocationLatitude forKey:@"latitude"];
        [parameters setObject:self.strLocationLongitude forKey:@"longitude"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        [parameters setObject:[dictParameters objectForKey:@"email"] forKey:@"email"];
        [parameters setObject:[dictParameters objectForKey:@"state_id"] forKey:@"state_id"];
        [parameters setObject:[dictParameters objectForKey:@"city_id"] forKey:@"city_id"];
        [parameters setObject:[dictParameters objectForKey:@"zipcode"] forKey:@"zipcode"];
        [parameters setObject:@"" forKey:@"continent"];
        [parameters setObject:@"0" forKey:@"is_international"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"guestSignUp" ofType:@"json"];
                //                NSData *data = [NSData dataWithContentsOfFile:filePath];
                //                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:[NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"guest_id"]] forKey:@"guestid"];
                        [prefs setObject:@"2" forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"guestSignedUpEvent" object:nil];
                    });
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR GUEST LOGOUT

-(void)guestLogout:(NSString *)strGuestId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GuestLogout"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strGuestId forKey:@"guest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"guestlogout" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"guestLoggedoutEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}


#pragma mark FUNCTION TO CHANGE PHONE NUMBER

-(void)changePhoneNumber:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ChangePhoneNumber"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"phone_number"] forKey:@"phone_number"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sendotp" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    self.strOTP = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"otp"]];
                    NSLog(@"OTP : %@", self.strOTP);
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedPhoneNumberEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO RESEND OTP

-(void)resendOTP:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ResendOTP"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sendotp" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    self.strOTP = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"otp"]];
                    NSLog(@"OTP : %@", self.strOTP);
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"resentOTPEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO VERIFY PHONE NUMBER

-(void)verifyPhoneNumber:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"VerifyPhoneNumber"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"verifyphonenumber" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[dictParameters objectForKey:@"user_type"] isEqualToString:@"1"])
                    {
                        if(self.objLoggedInParent == nil)
                        {
                            self.objLoggedInParent = [[Parent alloc] init];
                        }
                        
                        self.objLoggedInParent.boolIsPhoneNumberVerified = true;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
//                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                            [prefs setObject:@"1" forKey:@"autologin"];
//                            [prefs synchronize];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"verifiedParentPhoneNumberEvent" object:nil];
                        });
                    }
                    else if([[dictParameters objectForKey:@"user_type"] isEqualToString:@"0"])
                    {
                        if(self.objLoggedInChild == nil)
                        {
                            self.objLoggedInChild = [[Child alloc] init];
                        }
                        
                        self.objLoggedInChild.boolIsPhoneNumberVerified = true;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                            [prefs setObject:@"1" forKey:@"autologin"];
                            [prefs synchronize];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"verifiedChildPhoneNumberEvent" object:nil];
                        });
                    }
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL NEAR BY ALERTS BY USER ID, USER TYPE, LATITUDE AND LONGITUDE

-(void)getAllNearByAlertsByUserId:(NSString *)strUserId withUserType:(NSString *)strUserType
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllNearByAlertsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallnearbyalertsbyuserid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL NEAR BY ALERTS ARRAY ==========//
                NSArray *arrayAllNearByAlertsByUserIdList = [jsonResult objectForKey:@"near_by_alerts"];
                
                if([strUserType isEqualToString:@"1"])
                {
                    self.arrayAllNearByAlertsByParentId = [[NSMutableArray alloc] init];
                }
                else if([strUserType isEqualToString:@"0"])
                {
                    self.arrayAllNearByAlertsByChildId = [[NSMutableArray alloc] init];
                }
                else if([strUserType isEqualToString:@"2"])
                {
                    self.arrayAllNearByAlertsByGuestLocation = [[NSMutableArray alloc] init];
                }
                
                for(int i = 0 ; i < arrayAllNearByAlertsByUserIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllNearByAlertsByUserIdList objectAtIndex:i];
                    
                    ChildAlert *objChildAlert = [[ChildAlert alloc] init];
                    objChildAlert.strChildAlertID = [currentDictionary objectForKey:@"ChildAlertId"];
                    objChildAlert.strChildAlertType = [currentDictionary objectForKey:@"ChildAlertType"];
                    objChildAlert.strChildAlertParentResponseDateAndTime = [currentDictionary objectForKey:@"ChildAlertParentResponseDateAndTime"];
                    objChildAlert.strChildAlertDateAndTime = [currentDictionary objectForKey:@"ChildAlertDateAndTime"];
                    objChildAlert.strChildAlertMessageFromChild = [currentDictionary objectForKey:@"ChildAlertMessageFromChild"];
                    objChildAlert.strChildAlertMessageFromParent = [currentDictionary objectForKey:@"ChildAlertMessageFromParent"];
                    objChildAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objChildAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objChildAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objChildAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objChildAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objChildAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChildAlert.strProfilePictureImageUrl];
                    }
                    objChildAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objChildAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objChildAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objChildAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objChildAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objChildAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objChildAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objChildAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objChildAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objChildAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objChildAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objChildAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objChildAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objChildAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objChildAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objChildAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objChildAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objChildAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objChildAlert.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objChildAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objChildAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    if([strUserType isEqualToString:@"1"])
                    {
                        [self.arrayAllNearByAlertsByParentId addObject:objChildAlert];
                    }
                    else if([strUserType isEqualToString:@"0"])
                    {
                        [self.arrayAllNearByAlertsByChildId addObject:objChildAlert];
                    }
                    else if([strUserType isEqualToString:@"2"])
                    {
                        [self.arrayAllNearByAlertsByGuestLocation addObject:objChildAlert];
                    }
                }
                
                //========== FILL NEAR BY COMMUNITY RESPOND ALERTS ARRAY ==========//
                NSArray *arrayAllNearByCommunityRespondAlertsByUserIdList = [jsonResult objectForKey:@"near_by_community_respond_alerts"];
                
                if([strUserType isEqualToString:@"1"])
                {
                    self.arrayAllNearByCommunityAlertsByParentId = [[NSMutableArray alloc] init];
                }
                else if([strUserType isEqualToString:@"0"])
                {
                    self.arrayAllNearByCommunityAlertsByChildId = [[NSMutableArray alloc] init];
                }
                else if([strUserType isEqualToString:@"2"])
                {
                    self.arrayAllNearByCommunityAlertsByGuestLocation = [[NSMutableArray alloc] init];
                }
                
                for(int i = 0 ; i < arrayAllNearByCommunityRespondAlertsByUserIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllNearByCommunityRespondAlertsByUserIdList objectAtIndex:i];
                    
                    CommunityAlert *objCommunityAlert = [[CommunityAlert alloc] init];
                    objCommunityAlert.strCommunityAlertID = [currentDictionary objectForKey:@"CommunityAlertId"];
                    objCommunityAlert.strCommunityAlertDateAndTime = [currentDictionary objectForKey:@"CommunityAlertDateAndTime"];
                    objCommunityAlert.strCommunityAlertMessageFromChild = [currentDictionary objectForKey:@"CommunityAlertMessageFromChild"];
                    objCommunityAlert.strCommunityAlertMessageFromParent = [currentDictionary objectForKey:@"CommunityAlertMessageFromParent"];
                    objCommunityAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objCommunityAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objCommunityAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objCommunityAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objCommunityAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objCommunityAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objCommunityAlert.strProfilePictureImageUrl];
                    }
                    objCommunityAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objCommunityAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objCommunityAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objCommunityAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objCommunityAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objCommunityAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objCommunityAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objCommunityAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objCommunityAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objCommunityAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objCommunityAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objCommunityAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objCommunityAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objCommunityAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objCommunityAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objCommunityAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objCommunityAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objCommunityAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objCommunityAlert.boolIsCommunityAlertActive = [[currentDictionary objectForKey:@"IsCommunityAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objCommunityAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objCommunityAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    if([strUserType isEqualToString:@"1"])
                    {
                        [self.arrayAllNearByCommunityAlertsByParentId addObject:objCommunityAlert];
                    }
                    else if([strUserType isEqualToString:@"0"])
                    {
                        [self.arrayAllNearByCommunityAlertsByChildId addObject:objCommunityAlert];
                    }
                    else if([strUserType isEqualToString:@"2"])
                    {
                        [self.arrayAllNearByCommunityAlertsByGuestLocation addObject:objCommunityAlert];
                    }
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllNearByAlertsByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR CHANGE PASSWORD

-(void)commonChangePassword:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"CommonChangePassword"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        [parameters setObject:[dictParameters objectForKey:@"oldpassword"] forKey:@"oldpassword"];
        [parameters setObject:[dictParameters objectForKey:@"newpassword"] forKey:@"newpassword"];
        [parameters setObject:[dictParameters objectForKey:@"repeatpassword"] forKey:@"repeatpassword"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"commonchangepassword" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showErrorMessage:@"" withErrorContent:@"Your password has been changed successfully."];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"commonChangedPasswordEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET MORE ALERTS BY CHILD ID

-(void)getMoreAlertsByChildId:(NSString *)strChild
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetMoreAlertsByChildId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strChild forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getmorealertsbychildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL PARENT RESPOND ALERTS ARRAY ==========//
                NSArray *arrayAllParentRespondAlertsByChildIdList = [jsonResult objectForKey:@"parent_respond_alerts"];
                
                self.arrayAllParentRespondAlertsByChildId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllParentRespondAlertsByChildIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllParentRespondAlertsByChildIdList objectAtIndex:i];
                    
                    ChildAlert *objChildAlert = [[ChildAlert alloc] init];
                    objChildAlert.strChildAlertID = [currentDictionary objectForKey:@"ChildAlertId"];
                    objChildAlert.strChildAlertType = [currentDictionary objectForKey:@"ChildAlertType"];
                    objChildAlert.strChildAlertParentResponseDateAndTime = [currentDictionary objectForKey:@"ChildAlertParentResponseDateAndTime"];
                    objChildAlert.strChildAlertDateAndTime = [currentDictionary objectForKey:@"ChildAlertDateAndTime"];
                    objChildAlert.strChildAlertMessageFromChild = [currentDictionary objectForKey:@"ChildAlertMessageFromChild"];
                    objChildAlert.strChildAlertMessageFromParent = [currentDictionary objectForKey:@"ChildAlertMessageFromParent"];
                    objChildAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objChildAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objChildAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objChildAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objChildAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objChildAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChildAlert.strProfilePictureImageUrl];
                    }
                    objChildAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objChildAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objChildAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objChildAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objChildAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objChildAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objChildAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objChildAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objChildAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objChildAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objChildAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objChildAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objChildAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objChildAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objChildAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objChildAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objChildAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objChildAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objChildAlert.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objChildAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objChildAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    [self.arrayAllParentRespondAlertsByChildId addObject:objChildAlert]; 
                }
                
                
                //========== FILL COMMUNITY RESPOND ALERTS ARRAY ==========//
                NSArray *arrayAllCommunityRespondAlertsByChildIdList = [jsonResult objectForKey:@"community_respond_alerts"];
                
                self.arrayAllCommunityRespondAlertsByChildId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllCommunityRespondAlertsByChildIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllCommunityRespondAlertsByChildIdList objectAtIndex:i];
                    
                    CommunityAlert *objCommunityAlert = [[CommunityAlert alloc] init];
                    objCommunityAlert.strCommunityAlertID = [currentDictionary objectForKey:@"CommunityAlertId"];
                    objCommunityAlert.strCommunityAlertDateAndTime = [currentDictionary objectForKey:@"CommunityAlertDateAndTime"];
                    objCommunityAlert.strCommunityAlertMessageFromChild = [currentDictionary objectForKey:@"CommunityAlertMessageFromChild"];
                    objCommunityAlert.strCommunityAlertMessageFromParent = [currentDictionary objectForKey:@"CommunityAlertMessageFromParent"];
                    objCommunityAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objCommunityAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objCommunityAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objCommunityAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objCommunityAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objCommunityAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objCommunityAlert.strProfilePictureImageUrl];
                    }
                    objCommunityAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objCommunityAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objCommunityAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objCommunityAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objCommunityAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objCommunityAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objCommunityAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objCommunityAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objCommunityAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objCommunityAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objCommunityAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objCommunityAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objCommunityAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objCommunityAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objCommunityAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objCommunityAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objCommunityAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objCommunityAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objCommunityAlert.boolIsCommunityAlertActive = [[currentDictionary objectForKey:@"IsCommunityAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objCommunityAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objCommunityAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    [self.arrayAllCommunityRespondAlertsByChildId addObject:objCommunityAlert];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllAlertsByChildIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - COMMON FUNCTION TO GET ALL NOTIFICATIONS

-(void)commonGetAllNotifications:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"CommonGetAllNotifications"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *responseArray = responseObject;
                
            }
            else if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *jsonResult = responseObject;
                
                //                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"common_get_all_notifications" ofType:@"json"];
                //                NSData *data = [NSData dataWithContentsOfFile:filePath];
                //                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL ALL ORDERS ARRAY ==========//
                NSArray *arrayAllNotificationList = [jsonResult objectForKey:@"all_notifications"];
                
                self.arrayAllNotifications = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllNotificationList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllNotificationList objectAtIndex:i];
                    
                    Notification *objNotification = [[Notification alloc] init];
                    objNotification.strNotificationID = [currentDictionary objectForKey:@"notification_id"];
                    objNotification.strNotificationTime = [currentDictionary objectForKey:@"notification_date_time"];
                    objNotification.strNotificationType = [currentDictionary objectForKey:@"notification_alert_type"];
                    objNotification.strNotificationTitle = [currentDictionary objectForKey:@"notification_title"];
                    objNotification.strNotificationText = [currentDictionary objectForKey:@"notification_message"];
                    objNotification.strNotificationStateID = [currentDictionary objectForKey:@"notification_state_id"];
                    objNotification.strNotificationStateName = [currentDictionary objectForKey:@"notification_state_name"];
                    objNotification.strNotificationCityID = [currentDictionary objectForKey:@"notification_city_id"];
                    objNotification.strNotificationCityName = [currentDictionary objectForKey:@"notification_city_name"];
                    
                    [self.arrayAllNotifications addObject:objNotification];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"commonGotAllNotificationsEvent" object:nil];
            }
            
        }
              failure:^(NSURLSessionTask *operation, NSError *error) {
                  [appDelegate dismissGlobalHUD];
                  [self showErrorMessage:@"Server Error" withErrorContent:@""];
              }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL BOYCOTT NOTIFICATIONS

-(void)commonGetAllBoycottNotifications:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"CommonGetAllBoycottNotifications"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL ALL ORDERS ARRAY ==========//
                NSArray *arrayAllBoycottNotificationList = [jsonResult objectForKey:@"all_notifications"];
                
                self.arrayAllBoycottNotifications = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllBoycottNotificationList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllBoycottNotificationList objectAtIndex:i];
                    
                    Notification *objNotification = [[Notification alloc] init];
                    objNotification.strNotificationID = [currentDictionary objectForKey:@"notification_id"];
                    objNotification.strNotificationTime = [currentDictionary objectForKey:@"notification_date_time"];
                    objNotification.strNotificationType = [currentDictionary objectForKey:@"notification_alert_type"];
                    objNotification.strNotificationTitle = [currentDictionary objectForKey:@"notification_title"];
                    objNotification.strNotificationText = [currentDictionary objectForKey:@"notification_message"];
                    objNotification.strNotificationStateID = [currentDictionary objectForKey:@"notification_state_id"];
                    objNotification.strNotificationStateName = [currentDictionary objectForKey:@"notification_state_name"];
                    objNotification.strNotificationCityID = [currentDictionary objectForKey:@"notification_city_id"];
                    objNotification.strNotificationCityName = [currentDictionary objectForKey:@"notification_city_name"];
                    
                    [self.arrayAllBoycottNotifications addObject:objNotification];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"commonGotAllBoycottNotificationsEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

//==================== PARENT APP WEBSERVICES ====================//
#pragma mark - PARENT APP WEBSERVICE -

#pragma mark FUNCTION FOR PARENT LOGIN

-(void)parentLogin:(Parent *)objParent
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ParentLogin"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objParent.strEmail forKey:@"email"];
        [parameters setObject:objParent.strPassword forKey:@"password"];
        [parameters setObject:self.strLocationLatitude forKey:@"latitude"];
        [parameters setObject:self.strLocationLongitude forKey:@"longitude"];
        [parameters setObject:objParent.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"parentlogin" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInParent == nil)
                    {
                        self.objLoggedInParent = [[Parent alloc] init];
                    }
                    
                    self.objLoggedInParent.strParentID = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"parent_id"]];
                    self.objLoggedInParent.strEmail = objParent.strEmail;
                    self.objLoggedInParent.strName = [jsonResult objectForKey:@"name"];
                    self.objLoggedInParent.boolIsPhoneNumberVerified = [[jsonResult objectForKey:@"is_phone_number_verified"] boolValue];
                    self.objLoggedInParent.strPhoneNumber = [jsonResult objectForKey:@"phone_number"];
                    self.strOTP = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"otp"]];
                    self.objLoggedInParent.boolIsPaid = [[jsonResult objectForKey:@"is_paid"] boolValue];
                    NSLog(@"OTP : %@", self.strOTP);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        if(self.objLoggedInParent.boolIsPhoneNumberVerified && self.objLoggedInParent.boolIsPaid)
                        {
                            [prefs setObject:@"1" forKey:@"autologin"];
                            [prefs setObject:@"1" forKey:@"ispaid"];
                        }
                        [prefs setObject:self.objLoggedInParent.strParentID forKey:@"parentid"];
                        [prefs setObject:self.objLoggedInParent.strEmail forKey:@"parentemail"];
                        [prefs setObject:self.objLoggedInParent.strName forKey:@"parentname"];
                        [prefs setObject:@"1" forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"parentLoggedinEvent" object:nil];
                    });
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Login Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR PARENT LOGOUT

-(void)parentLogout:(NSString *)strParentId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ParentLogout"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"parentlogout" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"parentLoggedoutEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    [self showErrorMessage:@"" withErrorContent:@""];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR PARENT TO ADD/REMOVE FROM EMAIL PREFERENCES

-(void)parentSubscribe:(NSString *)strParentId withFlag:(NSString *)boolFlag
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ParentSubscribe"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"user_id"];
        [parameters setObject:boolFlag forKey:@"flag"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"parentSubscribedEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR PARENT SIGNUP

-(void)parentSignUp:(Parent *)objParent
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ParentSignup"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objParent.strEmail forKey:@"email"];
        [parameters setObject:objParent.strPassword forKey:@"password"];
        [parameters setObject:objParent.strName forKey:@"name"];
        [parameters setObject:objParent.strStateId forKey:@"state_id"];
        [parameters setObject:objParent.strStateName forKey:@"state_name"];
        [parameters setObject:objParent.strCityId forKey:@"city_id"];
        [parameters setObject:objParent.strCityName forKey:@"city_name"];
        [parameters setObject:objParent.strZipcode forKey:@"zip_code"];
        [parameters setObject:objParent.strStreetAddress forKey:@"street_address"];
        [parameters setObject:objParent.strPhoneNumber forKey:@"phone_number"];
        [parameters setObject:objParent.strSecondaryPhoneNumber forKey:@"secondary_phone_number"];
        [parameters setObject:[objParent.strGender lowercaseString] forKey:@"gender"];
        [parameters setObject:objParent.strWorkPlaceName forKey:@"work_place_name"];
        [parameters setObject:objParent.strWorkPlacePhoneNumber forKey:@"work_place_phone_number"];
        
        [parameters setObject:self.strLocationLatitude forKey:@"latitude"];
        [parameters setObject:self.strLocationLongitude forKey:@"longitude"];
        [parameters setObject:objParent.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"parentsignup" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInParent == nil)
                    {
                        self.objLoggedInParent = [[Parent alloc] init];
                    }
                    
                    self.objLoggedInParent.strParentID = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"parent_id"]];
                    self.objLoggedInParent.strEmail = objParent.strEmail;
                    self.objLoggedInParent.strName = objParent.strName;
                    self.strOTP = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"otp"]];
                    NSLog(@"OTP : %@", self.strOTP);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:self.objLoggedInParent.strParentID forKey:@"parentid"];
                        [prefs setObject:self.objLoggedInParent.strEmail forKey:@"parentemail"];
                        [prefs setObject:self.objLoggedInParent.strName forKey:@"parentname"];
                        [prefs setObject:@"1" forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"parentSignedUpEvent" object:nil];
                    });
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Registration Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

-(void)displayErrorForSignup:(NSString *)strError
{
    [appDelegate dismissGlobalHUD];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Registration Failed" withDetails:strError];
    });
}

#pragma mark FUNCTION TO GET PARENT PROFILE DETAILS BY PARENT ID

-(void)getParentProfileDetailsByParentId:(NSString *)strParentId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetParentProfileDetailsByParentId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInParent == nil)
                    {
                        self.objLoggedInParent = [[Parent alloc] init];
                    }
                    
                    self.objLoggedInParent.strParentID = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"parent_id"]];
                    self.objLoggedInParent.strEmail = [jsonResult objectForKey:@"email"];
                    self.objLoggedInParent.strName = [jsonResult objectForKey:@"name"];
                    self.objLoggedInParent.strStateId = [jsonResult objectForKey:@"state_id"];
                    self.objLoggedInParent.strStateName = [jsonResult objectForKey:@"state_name"];
                    self.objLoggedInParent.strCityId = [jsonResult objectForKey:@"city_id"];
                    self.objLoggedInParent.strCityName = [jsonResult objectForKey:@"city_name"];
                    self.objLoggedInParent.strZipcode = [jsonResult objectForKey:@"zip_code"];
                    self.objLoggedInParent.strStreetAddress = [jsonResult objectForKey:@"street_address"];
                    self.objLoggedInParent.strPhoneNumber = [jsonResult objectForKey:@"phone_number"];
                    self.objLoggedInParent.strSecondaryPhoneNumber = [jsonResult objectForKey:@"secondary_phone_number"];
                    self.objLoggedInParent.strGender = [jsonResult objectForKey:@"gender"];
                    self.objLoggedInParent.strWorkPlaceName = [jsonResult objectForKey:@"work_place_name"];
                    self.objLoggedInParent.strWorkPlacePhoneNumber = [jsonResult objectForKey:@"work_place_phone_number"];
                    
                    self.objLoggedInParent.strProfilePictureImageUrl = [jsonResult objectForKey:@"profile_picture_image_url"];
                    if(![self.objLoggedInParent.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        self.objLoggedInParent.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", self.objLoggedInParent.strProfilePictureImageUrl];
                    }
                    
                    //========== FILL STATES ARRAY ==========//
                    NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                    
                    self.arrayStates = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayStateList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                        
                        State *objState = [[State alloc] init];
                        objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                        objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                        
                        NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                        
                        objState.arrayCity = [[NSMutableArray alloc] init];
                        
                        for(int j = 0 ; j < arrayCityList.count; j++)
                        {
                            NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                            
                            City *objCity = [[City alloc] init];
                            objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                            objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                            
                            [objState.arrayCity addObject:objCity];
                        }
                        
                        [self.arrayStates addObject:objState];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotParentProfileDetailsByParentIdEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    [self showErrorMessage:@"" withErrorContent:@"Oops! Some error occured with our server. Please try again after some time."];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE PARENT PROFILE

-(void)updateParentProfile:(Parent *)objParent
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdateParentProfile"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        [parameters setObject:strParentId forKey:@"parent_id"];
        [parameters setObject:objParent.strName forKey:@"name"];
        [parameters setObject:objParent.strStateId forKey:@"state_id"];
        [parameters setObject:objParent.strStateName forKey:@"state_name"];
        [parameters setObject:objParent.strCityId forKey:@"city_id"];
        [parameters setObject:objParent.strCityName forKey:@"city_name"];
        [parameters setObject:objParent.strZipcode forKey:@"zip_code"];
        [parameters setObject:objParent.strStreetAddress forKey:@"street_address"];
        [parameters setObject:objParent.strPhoneNumber forKey:@"phone_number"];
        [parameters setObject:objParent.strSecondaryPhoneNumber forKey:@"secondary_phone_number"];
        [parameters setObject:[objParent.strGender lowercaseString] forKey:@"gender"];
        [parameters setObject:objParent.strWorkPlaceName forKey:@"work_place_name"];
        [parameters setObject:objParent.strWorkPlacePhoneNumber forKey:@"work_place_phone_number"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updateparentprofile" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:objParent.strName forKey:@"parentname"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedParentProfileEvent" object:nil];
                    });
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPLOAD USER PROFILE PICTURE WITH USER ID WITH $_FILE

-(void)uploadParentProfilePictureWith_File:(NSData *)profilePictureData
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strLoggedInParentId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"parentid"]];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ParentUploadProfilePictureImage"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strLoggedInParentId forKey:@"parent_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:profilePictureData name:@"parent_profile_picture_data" fileName:[NSString stringWithFormat:@"parent_%@.png", strLoggedInParentId] mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"uploadedParentProfilePictureWith_FileEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO SEND PARENT PAYMENT

-(void)sendParentPayment:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"SendParentPayment"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"parent_id"] forKey:@"parent_id"];
        [parameters setObject:[dictParameters objectForKey:@"amount"] forKey:@"amount"];
        [parameters setObject:[dictParameters objectForKey:@"stripe_token"] forKey:@"stripe_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"verifyphonenumber" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInParent == nil)
                    {
                        self.objLoggedInParent = [[Parent alloc] init];
                    }
                    
                    self.objLoggedInParent.boolIsPaid = true;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:@"1" forKey:@"ispaid"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"sentParentPaymentEvent" object:nil];
                    });
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL CHILDS BY PARENT ID

-(void)getAllChildsByParentId:(NSString *)strParentId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllChildsByParentId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallchildsbyparentid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL CHILDS ARRAY ==========//
                NSArray *arrayChildsByParentIdList = [jsonResult objectForKey:@"child"];
                
                self.arrayChildsByParentId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayChildsByParentIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayChildsByParentIdList objectAtIndex:i];
                    
                    Child *objChild = [[Child alloc] init];
                    objChild.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objChild.strName = [currentDictionary objectForKey:@"ChildName"];
                    objChild.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objChild.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objChild.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objChild.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChild.strProfilePictureImageUrl];
                    }
                    objChild.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objChild.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objChild.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objChild.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objChild.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objChild.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objChild.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objChild.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objChild.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objChild.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objChild.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objChild.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                    
                    NSString *strSelectedInformationTypesToDisplayForParentRespondAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForParentRespondAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [strSelectedInformationTypesToDisplayForParentRespondAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToDisplayForAuthorityAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForAuthorityAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [strSelectedInformationTypesToDisplayForAuthorityAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToDisplayForKidnappingAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForKidnappingAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [strSelectedInformationTypesToDisplayForKidnappingAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToSendToAuthorityForAuthorityAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForAuthorityAlert"];
                    NSArray *arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [strSelectedInformationTypesToSendToAuthorityForAuthorityAlert componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert mutableCopy];
                    
                    NSString *strSelectedInformationTypesToSendToAuthorityForKidnappingAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForKidnappingAlert"];
                    NSArray *arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [strSelectedInformationTypesToSendToAuthorityForKidnappingAlert componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert mutableCopy];
                    
                    [self.arrayChildsByParentId addObject:objChild];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllChildsByParentIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL CHILDS BY PARENT ID AND SEARCH KEYWORD

-(void)getAllChildsByParentId:(NSString *)strParentId withSearchKeyword:(NSString *)strSearchKeyword
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllChildsBySearchKeyword"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        [parameters setObject:strSearchKeyword forKey:@"search_keyword"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallchildsbysearchkeyword" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL CHILDS ARRAY ==========//
                NSArray *arrayAllChildsByParentIdAndSearchKeywordList = [jsonResult objectForKey:@"child"];
                
                self.arrayAllChildsByParentIdAndSearchKeyword = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllChildsByParentIdAndSearchKeywordList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllChildsByParentIdAndSearchKeywordList objectAtIndex:i];
                    
                    Child *objChild = [[Child alloc] init];
                    objChild.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objChild.strName = [currentDictionary objectForKey:@"ChildName"];
                    objChild.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objChild.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objChild.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objChild.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChild.strProfilePictureImageUrl];
                    }
                    objChild.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objChild.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objChild.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objChild.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objChild.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objChild.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objChild.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objChild.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objChild.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objChild.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objChild.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objChild.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                    
                    NSString *strSelectedInformationTypesToDisplayForParentRespondAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForParentRespondAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [strSelectedInformationTypesToDisplayForParentRespondAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToDisplayForAuthorityAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForAuthorityAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [strSelectedInformationTypesToDisplayForAuthorityAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToDisplayForKidnappingAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForKidnappingAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [strSelectedInformationTypesToDisplayForKidnappingAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToSendToAuthorityForAuthorityAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForAuthorityAlert"];
                    NSArray *arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [strSelectedInformationTypesToSendToAuthorityForAuthorityAlert componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert mutableCopy];
                    
                    NSString *strSelectedInformationTypesToSendToAuthorityForKidnappingAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForKidnappingAlert"];
                    NSArray *arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [strSelectedInformationTypesToSendToAuthorityForKidnappingAlert componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert mutableCopy];
                    
                    objChild.strParentRequest = [currentDictionary objectForKey:@"ParentRequest"];
                    
                    [self.arrayAllChildsByParentIdAndSearchKeyword addObject:objChild];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllChildsByParentIdAndSearchKeywordEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO REMOVE CHILD BY PARENT ID AND CHILD ID

-(void)removeChildByParentId:(NSString *)strParentId withChildId:(NSString *)strChildId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"RemoveChildByParentIdAndChildId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"removechildbyparentidandchildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL CHILDS ARRAY ==========//
                    NSArray *arrayChildsByParentIdList = [jsonResult objectForKey:@"child"];
                    
                    self.arrayChildsByParentId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayChildsByParentIdList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayChildsByParentIdList objectAtIndex:i];
                        
                        Child *objChild = [[Child alloc] init];
                        objChild.strChildID = [currentDictionary objectForKey:@"ChildId"];
                        objChild.strName = [currentDictionary objectForKey:@"ChildName"];
                        objChild.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                        objChild.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                        if(![objChild.strProfilePictureImageUrl hasPrefix:@"http://"])
                        {
                            objChild.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChild.strProfilePictureImageUrl];
                        }
                        objChild.strAge = [currentDictionary objectForKey:@"ChildAge"];
                        objChild.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                        objChild.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                        objChild.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                        objChild.strGender = [currentDictionary objectForKey:@"ChildGender"];
                        objChild.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                        objChild.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                        objChild.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                        objChild.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                        objChild.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                        objChild.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                        objChild.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                        
                        NSString *strSelectedInformationTypesToDisplayForParentRespondAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForParentRespondAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [strSelectedInformationTypesToDisplayForParentRespondAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToDisplayForAuthorityAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForAuthorityAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [strSelectedInformationTypesToDisplayForAuthorityAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToDisplayForKidnappingAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForKidnappingAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [strSelectedInformationTypesToDisplayForKidnappingAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToSendToAuthorityForAuthorityAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForAuthorityAlert"];
                        NSArray *arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [strSelectedInformationTypesToSendToAuthorityForAuthorityAlert componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert mutableCopy];
                        
                        NSString *strSelectedInformationTypesToSendToAuthorityForKidnappingAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForKidnappingAlert"];
                        NSArray *arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [strSelectedInformationTypesToSendToAuthorityForKidnappingAlert componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert mutableCopy];
                        
                        [self.arrayChildsByParentId addObject:objChild];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"removedChildByParentIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO SEND REQUEST BY CHILD BY PARENT ID AND CHILD ID AND PARENT ROLE, AUTHORITY EMAIL

-(void)sendRequestToChildByParentId:(NSString *)strParentId withChildId:(NSString *)strChildId withParentRole:(NSString *)strParentRole withAuthorityEmail:(NSString *)strAuthorityEmail
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"SendRequestToChildByParentIdAndChildId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        [parameters setObject:strChildId forKey:@"child_id"];
        [parameters setObject:strParentRole forKey:@"parent_role"];
        [parameters setObject:strAuthorityEmail forKey:@"authority_email"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sendrequesttochildbyparentidandchildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Thank you Please wait for your request to be accepted."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"sentRequestToChildByParentIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE PRIVACY SETTINGS FOR CHILD

-(void)updatePrivacySettingsForChild:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdatePrivacySettingsForChild"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"parent_id"] forKey:@"parent_id"];
        [parameters setObject:[dictParameters objectForKey:@"child_id"] forKey:@"child_id"];
        [parameters setObject:[dictParameters objectForKey:@"alert_type"] forKey:@"alert_type"];
        [parameters setObject:[dictParameters objectForKey:@"selected_information_types_for_display"] forKey:@"selected_information_types_for_display"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updateprivacysettingsforchild" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL CHILDS ARRAY ==========//
                    NSArray *arrayChildsByParentIdList = [jsonResult objectForKey:@"child"];
                    
                    self.arrayChildsByParentId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayChildsByParentIdList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayChildsByParentIdList objectAtIndex:i];
                        
                        Child *objChild = [[Child alloc] init];
                        objChild.strChildID = [currentDictionary objectForKey:@"ChildId"];
                        objChild.strName = [currentDictionary objectForKey:@"ChildName"];
                        objChild.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                        objChild.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                        if(![objChild.strProfilePictureImageUrl hasPrefix:@"http://"])
                        {
                            objChild.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChild.strProfilePictureImageUrl];
                        }
                        objChild.strAge = [currentDictionary objectForKey:@"ChildAge"];
                        objChild.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                        objChild.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                        objChild.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                        objChild.strGender = [currentDictionary objectForKey:@"ChildGender"];
                        objChild.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                        objChild.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                        objChild.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                        objChild.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                        objChild.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                        objChild.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                        objChild.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                        
                        NSString *strSelectedInformationTypesToDisplayForParentRespondAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForParentRespondAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [strSelectedInformationTypesToDisplayForParentRespondAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToDisplayForAuthorityAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForAuthorityAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [strSelectedInformationTypesToDisplayForAuthorityAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToDisplayForKidnappingAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForKidnappingAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [strSelectedInformationTypesToDisplayForKidnappingAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToSendToAuthorityForAuthorityAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForAuthorityAlert"];
                        NSArray *arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [strSelectedInformationTypesToSendToAuthorityForAuthorityAlert componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert mutableCopy];
                        
                        NSString *strSelectedInformationTypesToSendToAuthorityForKidnappingAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForKidnappingAlert"];
                        NSArray *arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [strSelectedInformationTypesToSendToAuthorityForKidnappingAlert componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert mutableCopy];
                        
                        [self.arrayChildsByParentId addObject:objChild];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Privacy settings has been updated."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedPrivacySettingsForChildEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL MY CHILDS ALERTS BY PARENT ID

-(void)getAllMyChildsAlertsByParentId:(NSString *)strParentId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllMyChildsAlertsByParentId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
//
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallmychildsalertsbyparentid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
                //========== FILL MY CHILDS ALERTS ARRAY ==========//
                NSArray *arrayAllMyChildsParentRespondAlertsByParentIdList = [jsonResult objectForKey:@"my_childs_parent_respond_alerts"];
                
                self.arrayAllMyChildsParentRespondAlertsByParentId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllMyChildsParentRespondAlertsByParentIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllMyChildsParentRespondAlertsByParentIdList objectAtIndex:i];
                    
                    ChildAlert *objChildAlert = [[ChildAlert alloc] init];
                    objChildAlert.strChildAlertID = [currentDictionary objectForKey:@"ChildAlertId"];
                    objChildAlert.strChildAlertType = [currentDictionary objectForKey:@"ChildAlertType"];
                    objChildAlert.strChildAlertParentResponseDateAndTime = [currentDictionary objectForKey:@"ChildAlertParentResponseDateAndTime"];
                    objChildAlert.strChildAlertDateAndTime = [currentDictionary objectForKey:@"ChildAlertDateAndTime"];
                    objChildAlert.strChildAlertMessageFromChild = [currentDictionary objectForKey:@"ChildAlertMessageFromChild"];
                    objChildAlert.strChildAlertMessageFromParent = [currentDictionary objectForKey:@"ChildAlertMessageFromParent"];
                    objChildAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objChildAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objChildAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objChildAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objChildAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objChildAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChildAlert.strProfilePictureImageUrl];
                    }
                    objChildAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objChildAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objChildAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objChildAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objChildAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objChildAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objChildAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objChildAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objChildAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objChildAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objChildAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objChildAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objChildAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objChildAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objChildAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objChildAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objChildAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objChildAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objChildAlert.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objChildAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objChildAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    [self.arrayAllMyChildsParentRespondAlertsByParentId addObject:objChildAlert];
                }
                
                //========== FILL COMMUNITY RESPOND ALERTS ARRAY ==========//
                NSArray *arrayAllCommunityRespondAlertsByChildIdList = [jsonResult objectForKey:@"my_childs_community_respond_alerts"];
                
                self.arrayAllMyChildsCommunityRespondAlertsByParentId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllCommunityRespondAlertsByChildIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllCommunityRespondAlertsByChildIdList objectAtIndex:i];
                    
                    CommunityAlert *objCommunityAlert = [[CommunityAlert alloc] init];
                    objCommunityAlert.strCommunityAlertID = [currentDictionary objectForKey:@"CommunityAlertId"];
                    objCommunityAlert.strCommunityAlertDateAndTime = [currentDictionary objectForKey:@"CommunityAlertDateAndTime"];
                    objCommunityAlert.strCommunityAlertMessageFromChild = [currentDictionary objectForKey:@"CommunityAlertMessageFromChild"];
                    objCommunityAlert.strCommunityAlertMessageFromParent = [currentDictionary objectForKey:@"CommunityAlertMessageFromParent"];
                    objCommunityAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objCommunityAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objCommunityAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objCommunityAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objCommunityAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objCommunityAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objCommunityAlert.strProfilePictureImageUrl];
                    }
                    objCommunityAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objCommunityAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objCommunityAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objCommunityAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objCommunityAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objCommunityAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objCommunityAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objCommunityAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objCommunityAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objCommunityAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objCommunityAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objCommunityAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objCommunityAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objCommunityAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objCommunityAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objCommunityAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objCommunityAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objCommunityAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objCommunityAlert.boolIsCommunityAlertActive = [[currentDictionary objectForKey:@"IsCommunityAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objCommunityAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objCommunityAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    [self.arrayAllMyChildsCommunityRespondAlertsByParentId addObject:objCommunityAlert];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllMyChildsAlertsByParentIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO DELAY MY CHILD ALERT

-(void)delayMyChildAlert:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"DelayMyChildAlert"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"parent_id"] forKey:@"parent_id"];
        [parameters setObject:[dictParameters objectForKey:@"child_alert_id"] forKey:@"child_alert_id"];
        [parameters setObject:[dictParameters objectForKey:@"delayed_time"] forKey:@"delayed_time"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"delaymychildalert" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Alert has successfully been delayed to specified time."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"delayedMyChildAlertEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO RAISE MY CHILD ALERT

-(void)raiseMyChildAlert:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"RaiseMyChildAlert"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"parent_id"] forKey:@"parent_id"];
        [parameters setObject:[dictParameters objectForKey:@"child_alert_id"] forKey:@"child_alert_id"];
        [parameters setObject:[dictParameters objectForKey:@"message_from_parent"] forKey:@"message_from_parent"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"raisemychildalert" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Alert has successfully been raised."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"raisedMyChildAlertEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO DEACTIVATE MY CHILD ALERT

-(void)deactivateMyChildAlert:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"DeactivateMyChildAlert"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"parent_id"] forKey:@"parent_id"];
        [parameters setObject:[dictParameters objectForKey:@"child_alert_id"] forKey:@"child_alert_id"];
        [parameters setObject:[dictParameters objectForKey:@"message_from_parent"] forKey:@"message_from_parent"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"deactivatemychildalert" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Alert has successfully been deactivated."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deactivatedMyChildAlertEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET RESPONSE TIME BY PARENT ID

-(void)getParentResponseTimeByParentId:(NSString *)strParentId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetParentResponseTimeByParentId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getparentresponsetimebyparentid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInParent == nil)
                    {
                        self.objLoggedInParent = [[Parent alloc] init];
                    }
                    
                    self.objLoggedInParent.strParentResponseTime = [jsonResult objectForKey:@"response_time"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotParentResponseTimeByParentIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE RESPONSE TIME BY PARENT ID AND TIME

-(void)updateParentResponseTimeByParentId:(NSString *)strParentId withResponseTime:(NSString *)strResponseTime
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdateParentResponseTimeByParentId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        [parameters setObject:strResponseTime forKey:@"response_time"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updateparentresponsetimebyparentidandtime" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedParentResponseTimeByParentIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE PARENT MESSAGE FOR MY PARENT RESPOND ALERT

-(void)updateParentMessageForParentRespondAlert:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdateParentMessageForParentRespondAlert"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"parent_id"] forKey:@"parent_id"];
        [parameters setObject:[dictParameters objectForKey:@"child_alert_id"] forKey:@"child_alert_id"];
        [parameters setObject:[dictParameters objectForKey:@"message_from_parent"] forKey:@"message_from_parent"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updateparentmessageforparentrespondalert" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Your message has been updated successfully."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedParentMessageForParentRespondAlertEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE PARENT MESSAGE FOR MY CHILD COMMUNITY ALERT

-(void)updateParentMessageForCommunityAlert:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdateParentMessageForCommunityAlert"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"parent_id"] forKey:@"parent_id"];
        [parameters setObject:[dictParameters objectForKey:@"community_alert_id"] forKey:@"community_alert_id"];
        [parameters setObject:[dictParameters objectForKey:@"message_from_parent"] forKey:@"message_from_parent"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updateparentmessageforcommunityalert" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Your message has been updated successfully."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedParentMessageForCommunityAlertEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL CHILDS WITH AUTHORITY EMAIL BY PARENT ID

-(void)getAllMyChildsWithAuthorityEmailByParentId:(NSString *)strParentId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllMyChildsWithAuthorityEmailByParentId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
                //========== FILL CHILDS ARRAY ==========//
                NSArray *arrayChildsWithAuthorityEmailByParentIdList = [jsonResult objectForKey:@"child"];
                
                self.arrayChildsWithAuthorityEmailByParentId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayChildsWithAuthorityEmailByParentIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayChildsWithAuthorityEmailByParentIdList objectAtIndex:i];
                    
                    Child *objChild = [[Child alloc] init];
                    objChild.strAuthorityEmail = [currentDictionary objectForKey:@"authority_email"];
                    objChild.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objChild.strName = [currentDictionary objectForKey:@"ChildName"];
                    objChild.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objChild.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objChild.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objChild.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChild.strProfilePictureImageUrl];
                    }
                    objChild.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objChild.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objChild.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objChild.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objChild.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objChild.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objChild.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objChild.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objChild.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objChild.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objChild.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objChild.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                    
                    NSString *strSelectedInformationTypesToDisplayForParentRespondAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForParentRespondAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [strSelectedInformationTypesToDisplayForParentRespondAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToDisplayForAuthorityAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForAuthorityAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [strSelectedInformationTypesToDisplayForAuthorityAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToDisplayForKidnappingAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForKidnappingAlertToOthers"];
                    NSArray *arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [strSelectedInformationTypesToDisplayForKidnappingAlertToOthers componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers mutableCopy];
                    
                    NSString *strSelectedInformationTypesToSendToAuthorityForAuthorityAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForAuthorityAlert"];
                    NSArray *arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [strSelectedInformationTypesToSendToAuthorityForAuthorityAlert componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert mutableCopy];
                    
                    NSString *strSelectedInformationTypesToSendToAuthorityForKidnappingAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForKidnappingAlert"];
                    NSArray *arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [strSelectedInformationTypesToSendToAuthorityForKidnappingAlert componentsSeparatedByString:@","];
                    objChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert mutableCopy];
                    
                    [self.arrayChildsWithAuthorityEmailByParentId addObject:objChild];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllMyChildsWithAuthorityEmailByParentIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE AUTHORITY EMAIL BY PARENT ID AND AUTHORITY EMAIL AND CHILD IDS

-(void)updateAuthorityEmailByParentId:(NSString *)strParentId withAuthorityEmail:(NSString *)strAuthorityEmail withChildIds:(NSString *)strChildIds
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdateAuthorityEmailByParentId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentId forKey:@"parent_id"];
        [parameters setObject:strAuthorityEmail forKey:@"authority_email"];
        [parameters setObject:strChildIds forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updateauthorityemailbyparentid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL CHILDS ARRAY ==========//
                    NSArray *arrayChildsWithAuthorityEmailByParentIdList = [jsonResult objectForKey:@"child"];
                    
                    self.arrayChildsWithAuthorityEmailByParentId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayChildsWithAuthorityEmailByParentIdList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayChildsWithAuthorityEmailByParentIdList objectAtIndex:i];
                        
                        Child *objChild = [[Child alloc] init];
                        objChild.strAuthorityEmail = [currentDictionary objectForKey:@"authority_email"];
                        objChild.strChildID = [currentDictionary objectForKey:@"ChildId"];
                        objChild.strName = [currentDictionary objectForKey:@"ChildName"];
                        objChild.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                        objChild.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                        if(![objChild.strProfilePictureImageUrl hasPrefix:@"http://"])
                        {
                            objChild.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChild.strProfilePictureImageUrl];
                        }
                        objChild.strAge = [currentDictionary objectForKey:@"ChildAge"];
                        objChild.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                        objChild.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                        objChild.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                        objChild.strGender = [currentDictionary objectForKey:@"ChildGender"];
                        objChild.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                        objChild.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                        objChild.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                        objChild.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                        objChild.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                        objChild.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                        objChild.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                        
                        NSString *strSelectedInformationTypesToDisplayForParentRespondAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForParentRespondAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [strSelectedInformationTypesToDisplayForParentRespondAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers = [arraySelectedInformationTypesToDisplayForParentRespondAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToDisplayForAuthorityAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForAuthorityAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [strSelectedInformationTypesToDisplayForAuthorityAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers = [arraySelectedInformationTypesToDisplayForAuthorityAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToDisplayForKidnappingAlertToOthers = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToDisplayForKidnappingAlertToOthers"];
                        NSArray *arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [strSelectedInformationTypesToDisplayForKidnappingAlertToOthers componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers = [arraySelectedInformationTypesToDisplayForKidnappingAlertToOthers mutableCopy];
                        
                        NSString *strSelectedInformationTypesToSendToAuthorityForAuthorityAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForAuthorityAlert"];
                        NSArray *arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [strSelectedInformationTypesToSendToAuthorityForAuthorityAlert componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert = [arraySelectedInformationTypesToSendToAuthorityForAuthorityAlert mutableCopy];
                        
                        NSString *strSelectedInformationTypesToSendToAuthorityForKidnappingAlert = [currentDictionary objectForKey:@"ChildSelectedInformationTypesToSendToAuthorityForKidnappingAlert"];
                        NSArray *arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [strSelectedInformationTypesToSendToAuthorityForKidnappingAlert componentsSeparatedByString:@","];
                        objChild.arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert = [arraySelectedInformationTypesToSendToAuthorityForKidnappingAlert mutableCopy];
                        
                        [self.arrayChildsWithAuthorityEmailByParentId addObject:objChild];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedAuthorityEmailByParentIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

//==================== CHILD APP WEBSERVICES ====================//
#pragma mark - CHILD APP WEBSERVICES -

#pragma mark FUNCTION FOR CHILD LOGIN

-(void)childLogin:(Child *)objChild
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ChildLogin"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objChild.strEmail forKey:@"email"];
        [parameters setObject:objChild.strPassword forKey:@"password"];
        [parameters setObject:self.strLocationLatitude forKey:@"latitude"];
        [parameters setObject:self.strLocationLongitude forKey:@"longitude"];
        [parameters setObject:objChild.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"childlogin" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInChild == nil)
                    {
                        self.objLoggedInChild = [[Child alloc] init];
                    }
                    
                    self.objLoggedInChild.strChildID = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"child_id"]];
                    self.objLoggedInChild.strEmail = objChild.strEmail;
                    self.objLoggedInChild.strName = [jsonResult objectForKey:@"name"];
                    self.objLoggedInChild.boolIsPhoneNumberVerified = [[jsonResult objectForKey:@"is_phone_number_verified"] boolValue];
                    self.objLoggedInChild.strPhoneNumber = [jsonResult objectForKey:@"phone_number"];
                    self.strOTP = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"otp"]];
                    NSLog(@"OTP : %@", self.strOTP);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        if(self.objLoggedInChild.boolIsPhoneNumberVerified)
                        {
                            [prefs setObject:@"1" forKey:@"autologin"];
                        }
                        [prefs setObject:self.objLoggedInChild.strChildID forKey:@"childid"];
                        [prefs setObject:self.objLoggedInChild.strEmail forKey:@"childemail"];
                        [prefs setObject:self.objLoggedInChild.strName forKey:@"childname"];
                        [prefs setObject:@"0" forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"childLoggedinEvent" object:nil];
                    });
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Login Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR CHILD LOGOUT

-(void)childLogout:(NSString *)strChildId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ChildLogout"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"childlogout" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"childLoggedoutEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION FOR CHILD SIGNUP

-(void)childSignUp:(Child *)objChild
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ChildSignup"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objChild.strEmail forKey:@"email"];
        [parameters setObject:objChild.strPassword forKey:@"password"];
        [parameters setObject:objChild.strName forKey:@"name"];
        [parameters setObject:objChild.strPhoneNumber forKey:@"phone_number"];
        [parameters setObject:objChild.strAge forKey:@"age"];
        [parameters setObject:[objChild.strGender lowercaseString] forKey:@"gender"];
        [parameters setObject:objChild.strHeight forKey:@"height"];
        [parameters setObject:objChild.strWeight forKey:@"weight"];
//        [parameters setObject:objChild.arrayPhotosData forKey:@"photos"];
        [parameters setObject:objChild.strHairColor forKey:@"hair_color"];
        [parameters setObject:objChild.strHairStyle forKey:@"hair_style"];
        [parameters setObject:objChild.strEyeColor forKey:@"eye_color"];
        [parameters setObject:objChild.strNationality forKey:@"nationality"];
        [parameters setObject:objChild.strEmergencyContactName forKey:@"emergency_contact_name"];
        [parameters setObject:objChild.strEmergencyContactPhoneNumber forKey:@"emergency_contact_phone_number"];
        [parameters setObject:objChild.strEmergencyContactEmail forKey:@"emergency_contact_email"];
        [parameters setObject:objChild.strSchoolName forKey:@"school_name"];
        [parameters setObject:objChild.strSchoolStateId forKey:@"school_address_state_id"];
        [parameters setObject:objChild.strSchoolStateName forKey:@"school_address_state_name"];
        [parameters setObject:objChild.strSchoolCityId forKey:@"school_address_city_id"];
        [parameters setObject:objChild.strSchoolCityName forKey:@"school_address_city_name"];
        [parameters setObject:objChild.strSchoolZipcode forKey:@"school_address_zipcode"];
        [parameters setObject:objChild.strSchoolAddress forKey:@"school_address"];
        [parameters setObject:objChild.strSchoolPhoneNumber forKey:@"school_phone_number"];
        [parameters setObject:objChild.strSchoolCurrentGrade forKey:@"school_current_grade"];
        [parameters setObject:objChild.strBestFriendsNames forKey:@"best_friends_names"];
        
        [parameters setObject:self.strLocationLatitude forKey:@"latitude"];
        [parameters setObject:self.strLocationLongitude forKey:@"longitude"];
        [parameters setObject:objChild.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        NSString *strNumberOfPhotos = [NSString stringWithFormat:@"%d", [objChild.arrayPhotosData count]];
        [parameters setObject:strNumberOfPhotos forKey:@"number_of_photos"];
        
        NSData *dataOfArrayPhotos = [NSKeyedArchiver archivedDataWithRootObject:objChild.arrayPhotosData];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            for(int i = 0; i < objChild.arrayPhotosData.count; i++)
            {
//                NSData *dataOfPhoto = [NSKeyedArchiver archivedDataWithRootObject:[objChild.arrayPhotosData objectAtIndex:i]];
                NSData *dataOfPhoto = [objChild.arrayPhotosData objectAtIndex:i];
                
                NSString *strMultipartFormDataName = [NSString stringWithFormat:@"photo%d",i+1];
                NSString *strFileName = [NSString stringWithFormat:@"photo%d.png",i+1];
                [formData appendPartWithFileData:dataOfPhoto name:strMultipartFormDataName fileName:strFileName mimeType:@"image/png"];
            }
            
//            [formData appendPartWithFileData:dataOfArrayPhotos name:@"photos" fileName:@"" mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"childsignup" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInChild == nil)
                    {
                        self.objLoggedInChild = [[Child alloc] init];
                    }
                    
                    self.objLoggedInChild.strChildID = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"child_id"]];
                    self.objLoggedInChild.strEmail = objChild.strEmail;
                    self.objLoggedInChild.strName = objChild.strName;
                    self.strOTP = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"otp"]];
                    NSLog(@"OTP : %@", self.strOTP);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:self.objLoggedInChild.strChildID forKey:@"childid"];
                        [prefs setObject:self.objLoggedInChild.strEmail forKey:@"childemail"];
                        [prefs setObject:self.objLoggedInChild.strName forKey:@"childname"];
                        [prefs setObject:@"0" forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"childSignedUpEvent" object:nil];
                    });
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Registration Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET CHILD PROFILE DETAILS BY CHILD ID

-(void)getChildProfileDetailsByChildId:(NSString *)strChildId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetChildProfileDetailsByChildId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getchildprofiledetailsbychildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInChild == nil)
                    {
                        self.objLoggedInChild = [[Child alloc] init];
                    }
                    
                    self.objLoggedInChild.strChildID = [NSString stringWithFormat:@"%@", [jsonResult objectForKey:@"child_id"]];
                    self.objLoggedInChild.strEmail = [jsonResult objectForKey:@"email"];
                    self.objLoggedInChild.strName = [jsonResult objectForKey:@"name"];
                    self.objLoggedInChild.strPhoneNumber = [jsonResult objectForKey:@"phone_number"];
                    self.objLoggedInChild.strAge = [jsonResult objectForKey:@"age"];
                    self.objLoggedInChild.strGender = [jsonResult objectForKey:@"gender"];
                    self.objLoggedInChild.strHeight = [jsonResult objectForKey:@"height"];
                    self.objLoggedInChild.strWeight = [jsonResult objectForKey:@"weight"];
                    
                    self.objLoggedInChild.arrayPhotosData = [[NSMutableArray alloc] init];
                    self.objLoggedInChild.arrayPhotosObjects = [[NSMutableArray alloc] init];
                    NSArray *arrayPhotos = [jsonResult objectForKey:@"photos"];
                    for(int i = 0; i < arrayPhotos.count; i ++)
                    {
                        NSDictionary *currentDictionary = [arrayPhotos objectAtIndex:i];
                        
                        ChildPhoto *objChildPhoto = [[ChildPhoto alloc] init];
                        objChildPhoto.strChildPhotoID = [currentDictionary objectForKey:@"photo_id"];
//                        objChildPhoto.childPhotoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"photo_url"]]];
                        objChildPhoto.childPhotoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [currentDictionary objectForKey:@"photo_url"]]];
                        
                        NSError *error = nil;
                        NSData *photoData = [NSData dataWithContentsOfURL:objChildPhoto.childPhotoUrl options:NSDataReadingUncached error:&error];
                        if (error)
                        {
                            NSLog(@"%@", [error localizedDescription]);
                        }
                        else
                        {
                            NSLog(@"Data has loaded successfully.");
                            
                            [self.objLoggedInChild.arrayPhotosData addObject:photoData];
                        }
                        
                        [self.objLoggedInChild.arrayPhotosObjects addObject:objChildPhoto];
                    }
                    
                    self.objLoggedInChild.strHairColor = [jsonResult objectForKey:@"hair_color"];
                    self.objLoggedInChild.strHairStyle = [jsonResult objectForKey:@"hair_style"];
                    self.objLoggedInChild.strEyeColor = [jsonResult objectForKey:@"eye_color"];
                    self.objLoggedInChild.strNationality = [jsonResult objectForKey:@"nationality"];
                    
                    self.objLoggedInChild.strEmergencyContactName = [jsonResult objectForKey:@"emergency_contact_name"];
                    self.objLoggedInChild.strEmergencyContactPhoneNumber = [jsonResult objectForKey:@"emergency_contact_phone_number"];
                    self.objLoggedInChild.strEmergencyContactEmail = [jsonResult objectForKey:@"emergency_contact_email"];
                    
                    self.objLoggedInChild.strSchoolName = [jsonResult objectForKey:@"school_name"];
                    self.objLoggedInChild.strSchoolStateId = [jsonResult objectForKey:@"school_address_state_id"];
                    self.objLoggedInChild.strSchoolStateName = [jsonResult objectForKey:@"school_address_state_name"];
                    self.objLoggedInChild.strSchoolCityId = [jsonResult objectForKey:@"school_address_city_id"];
                    self.objLoggedInChild.strSchoolCityName = [jsonResult objectForKey:@"school_address_city_name"];
                    self.objLoggedInChild.strSchoolZipcode = [jsonResult objectForKey:@"school_address_zipcode"];
                    self.objLoggedInChild.strSchoolAddress = [jsonResult objectForKey:@"school_address"];
                    self.objLoggedInChild.strSchoolPhoneNumber = [jsonResult objectForKey:@"school_phone_number"];
                    self.objLoggedInChild.strSchoolCurrentGrade = [jsonResult objectForKey:@"school_current_grade"];
                    
                    self.objLoggedInChild.strBestFriendsNames = [jsonResult objectForKey:@"best_friends_names"];
                    
                    self.objLoggedInChild.strProfilePictureImageUrl = [jsonResult objectForKey:@"profile_picture_image_url"];
                    if(![self.objLoggedInChild.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        self.objLoggedInChild.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", self.objLoggedInChild.strProfilePictureImageUrl];
                    }
                    
                    //========== FILL STATES ARRAY ==========//
                    NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                    
                    self.arrayStates = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayStateList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                        
                        State *objState = [[State alloc] init];
                        objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                        objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                        
                        NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                        
                        objState.arrayCity = [[NSMutableArray alloc] init];
                        
                        for(int j = 0 ; j < arrayCityList.count; j++)
                        {
                            NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                            
                            City *objCity = [[City alloc] init];
                            objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                            objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                            
                            [objState.arrayCity addObject:objCity];
                        }
                        
                        [self.arrayStates addObject:objState];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotChildProfileDetailsByChildIdEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    [self showErrorMessage:@"" withErrorContent:@"Oops! Some error occured with our server. Please try again after some time."];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE CHILD PROFILE

-(void)updateChildProfile:(Child *)objChild
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdatedChildProfile"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [parameters setObject:strChildId forKey:@"child_id"];
        [parameters setObject:objChild.strName forKey:@"name"];
        [parameters setObject:objChild.strPhoneNumber forKey:@"phone_number"];
        [parameters setObject:objChild.strAge forKey:@"age"];
        [parameters setObject:objChild.strGender forKey:@"gender"];
        [parameters setObject:objChild.strHeight forKey:@"height"];
        [parameters setObject:objChild.strWeight forKey:@"weight"];
//        [parameters setObject:objChild.arrayPhotosData forKey:@"photos"];
        [parameters setObject:objChild.strHairColor forKey:@"hair_color"];
        [parameters setObject:objChild.strHairStyle forKey:@"hair_style"];
        [parameters setObject:objChild.strEyeColor forKey:@"eye_color"];
        [parameters setObject:objChild.strNationality forKey:@"nationality"];
        [parameters setObject:objChild.strEmergencyContactName forKey:@"emergency_contact_name"];
        [parameters setObject:objChild.strEmergencyContactPhoneNumber forKey:@"emergency_contact_phone_number"];
        [parameters setObject:objChild.strEmergencyContactEmail forKey:@"emergency_contact_email"];
        [parameters setObject:objChild.strSchoolName forKey:@"school_name"];
        [parameters setObject:objChild.strSchoolStateId forKey:@"school_address_state_id"];
        [parameters setObject:objChild.strSchoolStateName forKey:@"school_address_state_name"];
        [parameters setObject:objChild.strSchoolCityId forKey:@"school_address_city_id"];
        [parameters setObject:objChild.strSchoolCityName forKey:@"school_address_city_name"];
        [parameters setObject:objChild.strSchoolZipcode forKey:@"school_address_zipcode"];
        [parameters setObject:objChild.strSchoolAddress forKey:@"school_address"];
        [parameters setObject:objChild.strSchoolPhoneNumber forKey:@"school_phone_number"];
        [parameters setObject:objChild.strSchoolCurrentGrade forKey:@"school_current_grade"];
        [parameters setObject:objChild.strBestFriendsNames forKey:@"best_friends_names"];
        
        NSString *strNumberOfPhotos = [NSString stringWithFormat:@"%d", [objChild.arrayPhotosDataForUpdation count]];
        [parameters setObject:strNumberOfPhotos forKey:@"number_of_photos"];
        
        NSData *dataOfArrayPhotos = [NSKeyedArchiver archivedDataWithRootObject:objChild.arrayPhotosDataForUpdation];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            for(int i = 0; i < objChild.arrayPhotosDataForUpdation.count; i++)
            {
//                NSData *dataOfPhoto = [NSKeyedArchiver archivedDataWithRootObject:[objChild.arrayPhotosData objectAtIndex:i]];
                NSData *dataOfPhoto = [objChild.arrayPhotosDataForUpdation objectAtIndex:i];
                
                NSString *strMultipartFormDataName = [NSString stringWithFormat:@"photo%d",i+1];
                NSString *strFileName = [NSString stringWithFormat:@"photo%d.png",i+1];
                [formData appendPartWithFileData:dataOfPhoto name:strMultipartFormDataName fileName:strFileName mimeType:@"image/png"];
            }
            
//            [formData appendPartWithFileData:dataOfArrayPhotos name:@"photos" fileName:@"" mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updatechildprofile" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:objChild.strName forKey:@"childname"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedChildProfileEvent" object:nil];
                    });
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    [self showErrorMessage:@"" withErrorContent:@"Oops! Some error occured with our server. Please try again after some time."];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO DELETE CHILD PHOTO BY PHOTO ID

-(void)deleteChildPhotoByPhotoId:(NSString *)strPhotoId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"DeleteChildPhotoByPhotoId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strPhotoId forKey:@"photo_id"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"deletechildphotobyphotoid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedChildPhotoByPhotoIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPLOAD CHILD PROFILE PICTURE WITH CHILD ID WITH $_FILE

-(void)uploadChildProfilePictureWith_File:(NSData *)profilePictureData
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strLoggedInChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"ChildUploadProfilePictureImage"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strLoggedInChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:profilePictureData name:@"child_profile_picture_data" fileName:[NSString stringWithFormat:@"child_%@.png", strLoggedInChildId] mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"uploadedChildProfilePictureWith_FileEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL PARENTS BY CHILD ID

-(void)getAllParentsByChildId:(NSString *)strChildId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllParentsByChildId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallparentsbychildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL PARENTS ARRAY ==========//
                NSArray *arrayParentsByChildIdList = [jsonResult objectForKey:@"parents"];
                
                self.arrayParentsByChildId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayParentsByChildIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayParentsByChildIdList objectAtIndex:i];
                    
                    Parent *objParent = [[Parent alloc] init];
                    objParent.strParentID = [currentDictionary objectForKey:@"ParentId"];
                    objParent.strName = [currentDictionary objectForKey:@"ParentName"];
                    objParent.strParentRole = [currentDictionary objectForKey:@"ParentRole"];
                    objParent.strWorkPlaceName = [currentDictionary objectForKey:@"ParentWorkPlaceName"];
                    objParent.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ParentProfilePictureImageUrl"];
                    if(![objParent.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objParent.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objParent.strProfilePictureImageUrl];
                    }
                    
                    [self.arrayParentsByChildId addObject:objParent];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllParentsByChildIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GENERATE PARENT RESPOND ALERT BY CHILD ID

-(void)generateParentRespondByChildId:(NSString *)strChildId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GenerateParentRespondByChildId"]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        dateFormat.dateStyle = NSDateFormatterMediumStyle;
        [dateFormat setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
        NSString *strCurrentDateAndTime = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:[NSDate date]]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strChildId forKey:@"child_id"];
        [parameters setObject:strCurrentDateAndTime forKey:@"alert_time"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"generateparentrespondbychildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"generatedParentRespondByChildIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GENERATE COMMUNITY RESPOND ALERT BY CHILD ID

-(void)generateCommunityAlertByChildId:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GenerateCommunityRespondByChildId"]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        dateFormat.dateStyle = NSDateFormatterMediumStyle;
        [dateFormat setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
        NSString *strCurrentDateAndTime = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:[NSDate date]]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"child_id"] forKey:@"child_id"];
        [parameters setObject:[dictParameters objectForKey:@"child_message"] forKey:@"child_message"];
        [parameters setObject:[dictParameters objectForKey:@"latitude"] forKey:@"latitude"];
        [parameters setObject:[dictParameters objectForKey:@"longitude"] forKey:@"longitude"];
        [parameters setObject:strCurrentDateAndTime forKey:@"alert_time"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"generatecommunityrespondbychildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"generatedCommunityAlertByChildIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL PARENTS REQUESTS BY CHILD ID

-(void)getAllParentsRequestsByChildId:(NSString *)strChildId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllParentsRequestsByChildId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallparentsrequestsbychildid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL PARENTS REQUESTS ARRAY ==========//
                NSArray *arrayParentsRequestsByChildIdList = [jsonResult objectForKey:@"parents_requests"];
                
                self.arrayParentsRequestsByChildId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayParentsRequestsByChildIdList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayParentsRequestsByChildIdList objectAtIndex:i];
                    
                    ParentRequest *objParentRequest = [[ParentRequest alloc] init];
                    objParentRequest.strParentRequestID = [currentDictionary objectForKey:@"ParentRequestId"];
                    objParentRequest.strParentProfilePictureImageUrl = [currentDictionary objectForKey:@"ParentProfilePictureImageUrl"];
                    if(![objParentRequest.strParentProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objParentRequest.strParentProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objParentRequest.strParentProfilePictureImageUrl];
                    }
                    objParentRequest.strParentName = [currentDictionary objectForKey:@"ParentName"];
                    objParentRequest.strParentRole = [currentDictionary objectForKey:@"ParentRole"];
                    objParentRequest.strParentWorkPlaceName = [currentDictionary objectForKey:@"ParentWorkPlaceName"];
                    
                    [self.arrayParentsRequestsByChildId addObject:objParentRequest];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllParentsRequestsByChildIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO ACCEPT PARENT REQUEST BY PARENT REQUEST ID

-(void)acceptParentRequest:(NSString *)strParentRequestId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"AcceptParentRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentRequestId forKey:@"parent_request_id"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"acceptparentrequest" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL PARENTS REQUESTS ARRAY ==========//
                    NSArray *arrayParentsRequestsByChildIdList = [jsonResult objectForKey:@"parents_requests"];
                    
                    self.arrayParentsRequestsByChildId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayParentsRequestsByChildIdList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayParentsRequestsByChildIdList objectAtIndex:i];
                        
                        ParentRequest *objParentRequest = [[ParentRequest alloc] init];
                        objParentRequest.strParentRequestID = [currentDictionary objectForKey:@"ParentRequestId"];
                        objParentRequest.strParentProfilePictureImageUrl = [currentDictionary objectForKey:@"ParentProfilePictureImageUrl"];
                        if(![objParentRequest.strParentProfilePictureImageUrl hasPrefix:@"http://"])
                        {
                            objParentRequest.strParentProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objParentRequest.strParentProfilePictureImageUrl];
                        }
                        objParentRequest.strParentName = [currentDictionary objectForKey:@"ParentName"];
                        objParentRequest.strParentRole = [currentDictionary objectForKey:@"ParentRole"];
                        objParentRequest.strParentWorkPlaceName = [currentDictionary objectForKey:@"ParentWorkPlaceName"];
                        
                        [self.arrayParentsRequestsByChildId addObject:objParentRequest];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedParentRequestEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO REJECT PARENT REQUEST BY PARENT REQUEST ID

-(void)rejectParentRequest:(NSString *)strParentRequestId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"RejectParentRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strParentRequestId forKey:@"parent_request_id"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strChildId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"childid"]];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"rejectparentrequest" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL PARENTS REQUESTS ARRAY ==========//
                    NSArray *arrayParentsRequestsByChildIdList = [jsonResult objectForKey:@"parents_requests"];
                    
                    self.arrayParentsRequestsByChildId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayParentsRequestsByChildIdList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayParentsRequestsByChildIdList objectAtIndex:i];
                        
                        ParentRequest *objParentRequest = [[ParentRequest alloc] init];
                        objParentRequest.strParentRequestID = [currentDictionary objectForKey:@"ParentRequestId"];
                        objParentRequest.strParentProfilePictureImageUrl = [currentDictionary objectForKey:@"ParentProfilePictureImageUrl"];
                        if(![objParentRequest.strParentProfilePictureImageUrl hasPrefix:@"http://"])
                        {
                            objParentRequest.strParentProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objParentRequest.strParentProfilePictureImageUrl];
                        }
                        objParentRequest.strParentName = [currentDictionary objectForKey:@"ParentName"];
                        objParentRequest.strParentRole = [currentDictionary objectForKey:@"ParentRole"];
                        objParentRequest.strParentWorkPlaceName = [currentDictionary objectForKey:@"ParentWorkPlaceName"];
                        
                        [self.arrayParentsRequestsByChildId addObject:objParentRequest];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"rejectedParentRequestEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO GET ALL ALERTS RAISED BY ME

-(void)getAllAlertsRaisedByMe:(NSString *)strChildId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"GetAllAlertsRaisedByMe"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strChildId forKey:@"child_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallalertsraisedbyme" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
                //========== FILL ALERTS RAISED BY ME ARRAY ==========//
                NSArray *arrayAllParentRespondAlertsRaisedByMeList = [jsonResult objectForKey:@"parent_respond_alerts_raised_by_me"];
                
                self.arrayAllParentRespondAlertsRaisedByMe = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllParentRespondAlertsRaisedByMeList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllParentRespondAlertsRaisedByMeList objectAtIndex:i];
                    
                    ChildAlert *objChildAlert = [[ChildAlert alloc] init];
                    objChildAlert.strChildAlertID = [currentDictionary objectForKey:@"ChildAlertId"];
                    objChildAlert.strChildAlertType = [currentDictionary objectForKey:@"ChildAlertType"];
                    objChildAlert.strChildAlertParentResponseDateAndTime = [currentDictionary objectForKey:@"ChildAlertParentResponseDateAndTime"];
                    objChildAlert.strChildAlertDateAndTime = [currentDictionary objectForKey:@"ChildAlertDateAndTime"];
                    objChildAlert.strChildAlertMessageFromChild = [currentDictionary objectForKey:@"ChildAlertMessageFromChild"];
                    objChildAlert.strChildAlertMessageFromParent = [currentDictionary objectForKey:@"ChildAlertMessageFromParent"];
                    objChildAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objChildAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objChildAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objChildAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objChildAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objChildAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objChildAlert.strProfilePictureImageUrl];
                    }
                    objChildAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objChildAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objChildAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objChildAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objChildAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objChildAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objChildAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objChildAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objChildAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objChildAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objChildAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objChildAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objChildAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objChildAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objChildAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objChildAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objChildAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objChildAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objChildAlert.boolIsChildAlertActive = [[currentDictionary objectForKey:@"IsChildAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objChildAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objChildAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objChildAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    [self.arrayAllParentRespondAlertsRaisedByMe addObject:objChildAlert];
                }
                
                //========== FILL COMMUNITY RESPOND ALERTS ARRAY ==========//
                NSArray *arrayAllCommunityRespondAlertsRaisedByMeList = [jsonResult objectForKey:@"community_respond_alerts_raised_by_me"];
                
                self.arrayAllCommunityRespondAlertsRaisedByMe = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllCommunityRespondAlertsRaisedByMeList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllCommunityRespondAlertsRaisedByMeList objectAtIndex:i];
                    
                    CommunityAlert *objCommunityAlert = [[CommunityAlert alloc] init];
                    objCommunityAlert.strCommunityAlertID = [currentDictionary objectForKey:@"CommunityAlertId"];
                    objCommunityAlert.strCommunityAlertDateAndTime = [currentDictionary objectForKey:@"CommunityAlertDateAndTime"];
                    objCommunityAlert.strCommunityAlertMessageFromChild = [currentDictionary objectForKey:@"CommunityAlertMessageFromChild"];
                    objCommunityAlert.strCommunityAlertMessageFromParent = [currentDictionary objectForKey:@"CommunityAlertMessageFromParent"];
                    objCommunityAlert.strChildID = [currentDictionary objectForKey:@"ChildId"];
                    objCommunityAlert.strName = [currentDictionary objectForKey:@"ChildName"];
                    objCommunityAlert.strPhoneNumber = [currentDictionary objectForKey:@"ChildPhoneNumber"];
                    objCommunityAlert.strProfilePictureImageUrl = [currentDictionary objectForKey:@"ChildProfilePictureImageUrl"];
                    if(![objCommunityAlert.strProfilePictureImageUrl hasPrefix:@"http://"])
                    {
                        objCommunityAlert.strProfilePictureImageUrl = [NSString stringWithFormat:@"http://%@", objCommunityAlert.strProfilePictureImageUrl];
                    }
                    objCommunityAlert.strAge = [currentDictionary objectForKey:@"ChildAge"];
                    objCommunityAlert.strHairColor = [currentDictionary objectForKey:@"ChildHairColor"];
                    objCommunityAlert.strHairStyle = [currentDictionary objectForKey:@"ChildHairStyle"];
                    objCommunityAlert.strEyeColor = [currentDictionary objectForKey:@"ChildEyeColor"];
                    objCommunityAlert.strGender = [currentDictionary objectForKey:@"ChildGender"];
                    objCommunityAlert.strHeight = [currentDictionary objectForKey:@"ChildHeight"];
                    objCommunityAlert.strWeight = [currentDictionary objectForKey:@"ChildWeight"];
                    objCommunityAlert.strNationality = [currentDictionary objectForKey:@"ChildNationality"];
                    objCommunityAlert.strEmergencyContactName = [currentDictionary objectForKey:@"ChildEmergencyContactName"];
                    objCommunityAlert.strEmergencyContactPhoneNumber = [currentDictionary objectForKey:@"ChildEmergencyContactPhoneNumber"];
                    objCommunityAlert.strEmergencyContactEmail = [currentDictionary objectForKey:@"ChildEmergencyContactEmail"];
                    objCommunityAlert.strSchoolName = [currentDictionary objectForKey:@"ChildSchoolName"];
                    objCommunityAlert.strSchoolAddress = [currentDictionary objectForKey:@"ChildSchoolAddress"];
                    objCommunityAlert.strSchoolPhoneNumber = [currentDictionary objectForKey:@"ChildSchoolPhoneNumber"];
                    objCommunityAlert.strSchoolCurrentGrade = [currentDictionary objectForKey:@"ChildSchoolCurrentGrade"];
                    objCommunityAlert.strBestFriendsNames = [currentDictionary objectForKey:@"ChildBestFriendsNames"];
                    objCommunityAlert.strChildLatitude = [currentDictionary objectForKey:@"ChildLatitude"];
                    objCommunityAlert.strChildLongitude = [currentDictionary objectForKey:@"ChildLongitude"];
                    objCommunityAlert.boolIsCommunityAlertActive = [[currentDictionary objectForKey:@"IsCommunityAlertActive"] boolValue];
                    
                    NSString *strInformationTypesToDisplayToOthers = [currentDictionary objectForKey:@"ChildInformationTypesToDisplayToOthers"];
                    NSArray *arrayInformationTypesToDisplayToOthers = [strInformationTypesToDisplayToOthers componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToDisplayToOthers = [arrayInformationTypesToDisplayToOthers mutableCopy];
                    
                    
                    NSString *strInformationTypesToSendToAuthority = [currentDictionary objectForKey:@"ChildInformationTypesToSendToAuthority"];
                    NSArray *arrayInformationTypesToSendToAuthority = [strInformationTypesToSendToAuthority componentsSeparatedByString:@","];
                    objCommunityAlert.arrayInformationTypesToSendToAuthority = [arrayInformationTypesToSendToAuthority mutableCopy];
                    
                    NSArray *arrayChildPhotoUrlsList = [currentDictionary objectForKey:@"ChildPhotoUrls"];
                    
                    objCommunityAlert.arrayChildPhotoUrls = [[NSMutableArray alloc] init];
                    
                    for(int j = 0; j < arrayChildPhotoUrlsList.count; j++)
                    {
                        NSDictionary *currentDictionaryPhotoUrl = [arrayChildPhotoUrlsList objectAtIndex:j];
//                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@%@", [_dictionaryWebservicesUrls objectForKey:@"ServerIPForImages"], [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        NSString *strChildPhotoUrl = [NSString stringWithFormat:@"%@", [currentDictionaryPhotoUrl objectForKey:@"ChildPhotoUrl"]];
                        [objCommunityAlert.arrayChildPhotoUrls addObject:strChildPhotoUrl];
                    }
                    
                    [self.arrayAllCommunityRespondAlertsRaisedByMe addObject:objCommunityAlert];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllAlertsRaisedByMeEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE CHILD MESSAGE FOR MY PARENT RESPOND ALERT

-(void)updateChildMessageForParentRespondAlert:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdateChildMessageForParentRespondAlert"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"child_id"] forKey:@"child_id"];
        [parameters setObject:[dictParameters objectForKey:@"child_alert_id"] forKey:@"child_alert_id"];
        [parameters setObject:[dictParameters objectForKey:@"message_from_child"] forKey:@"message_from_child"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updatechildmessageforparentrespondalert" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Your message has been updated successfully."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedChildMessageForParentRespondAlertEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark FUNCTION TO UPDATE CHILD MESSAGE FOR MY COMMUNITY ALERT

-(void)updateChildMessageForCommunityAlert:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictionaryWebservicesUrls objectForKey:@"ServerIP"],[_dictionaryWebservicesUrls objectForKey:@"UpdateChildMessageForCommunityAlert"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"child_id"] forKey:@"child_id"];
        [parameters setObject:[dictParameters objectForKey:@"community_alert_id"] forKey:@"community_alert_id"];
        [parameters setObject:[dictParameters objectForKey:@"message_from_child"] forKey:@"message_from_child"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"updatechildmessageforcommunityalert" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Your message has been updated successfully."];
                    });
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedChildMessageForCommunityAlertEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

@end
