//
//  DataManager.h
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "CommonUtility.h"

//IMPORTING MODELS
#import "State.h"
#import "City.h"
#import "Parent.h"
#import "Child.h"
#import "ParentRequest.h"
#import "ChildAlert.h"
#import "CommunityAlert.h"
#import "Donator.h"
#import "ChildPhoto.h"
#import "Notification.h"

@interface DataManager : NSObject
{
    // The delegate - Will be notified of various changes of state via the DataManagerDelegate
    
    NSNumber  *isNetworkAvailable;
    AppDelegate *appDelegate;
    
    Parent *objLoggedInParent;
    Child *objLoggedInChild;
}

@property(nonatomic,retain) AppDelegate *appDelegate;
@property(nonatomic,retain) NSDictionary *dictionaryWebservicesUrls;

@property(nonatomic,retain) NSMutableArray *arrayStates;

@property(nonatomic,retain) NSMutableArray *arrayAllDonators;

@property(nonatomic,retain) Parent *objLoggedInParent;
@property(nonatomic,retain) Child *objLoggedInChild;

@property(nonatomic,retain) Notification *objNotification;

@property(nonatomic,retain) NSString *strLocationLatitude;
@property(nonatomic,retain) NSString *strLocationLongitude;

@property(nonatomic,retain) NSMutableArray *arrayChildsByParentId;
@property(nonatomic,retain) NSMutableArray *arrayAllChildsByParentIdAndSearchKeyword;
@property(nonatomic,retain) NSMutableArray *arrayAllMyChildsParentRespondAlertsByParentId;
@property(nonatomic,retain) NSMutableArray *arrayAllMyChildsCommunityRespondAlertsByParentId;
@property(nonatomic,retain) NSMutableArray *arrayAllNearByAlertsByParentId;
@property(nonatomic,retain) NSMutableArray *arrayAllNearByCommunityAlertsByParentId;
@property(nonatomic,retain) NSMutableArray *arrayChildsWithAuthorityEmailByParentId;

@property(nonatomic,retain) NSMutableArray *arrayParentsByChildId;
@property(nonatomic,retain) NSMutableArray *arrayParentsRequestsByChildId;
@property(nonatomic,retain) NSMutableArray *arrayAllNearByAlertsByChildId;
@property(nonatomic,retain) NSMutableArray *arrayAllNearByCommunityAlertsByChildId;
@property(nonatomic,retain) NSMutableArray *arrayAllParentRespondAlertsRaisedByMe;
@property(nonatomic,retain) NSMutableArray *arrayAllCommunityRespondAlertsRaisedByMe;

@property(nonatomic,retain) NSMutableArray *arrayAllNearByAlertsByGuestLocation;
@property(nonatomic,retain) NSMutableArray *arrayAllNearByCommunityAlertsByGuestLocation;
@property(nonatomic,retain) NSMutableArray *arrayAllParentRespondAlertsByChildId;
@property(nonatomic,retain) NSMutableArray *arrayAllCommunityRespondAlertsByChildId;

@property(nonatomic,retain) NSMutableArray *arrayAllNotifications;
@property(nonatomic,retain) NSMutableArray *arrayAllBoycottNotifications;

@property(nonatomic,retain) NSString *strOTP;

#pragma mark - Server communication

//FUNCTION TO CHECK IF INTERNET CONNECTION IS AVAILABLE OR NOT
-(BOOL)isNetworkAvailable;

//FUNCTION TO SHOW ERROR ALERT
-(void)showErrorMessage:(NSString *)errorTitle withErrorContent:(NSString *)errorDescription;

//==================== COMMON/GUEST USER APP WEBSERVICES ====================//

//FUNCTION TO GET ALL DONATORS
-(void)getAllDonators;

//FUNCTION TO GET ALL STATES
-(void)getAllStates;

//FUNCTION FOR FORGET PASSWORD
-(void)forgetPassword:(NSString *)strEmail withUserType:(NSString *)strUserType;

//FUNCTION TO SEND LOGGED IN CHILD/PARENT/GUEST'S CURRENT LOCATION TO SERVER ON SPLASH
-(void)sendLoggedInChildOrParentOrGuestCurrentLocationToServer;

//FUNCTION FOR GUEST SIGNUP
//-(void)guestSignUp:(NSDictionary *)dictParameters;

//FUNCTION FOR INTERNATIONAL GUEST SIGNUP
-(void)internationalGuestSignUp:(NSDictionary *)dictParameters;

//FUNCTION FOR LOACL GUEST SIGNUP
-(void)guestSignUp:(NSDictionary *)dictParameters;

//FUNCTION FOR GUEST LOGOUT
-(void)guestLogout:(NSString *)strGuestId;

//FUNCTION TO CHANGE PHONE NUMBER
-(void)changePhoneNumber:(NSDictionary *)dictParameters;

//FUNCTION TO RESEND OTP
-(void)resendOTP:(NSDictionary *)dictParameters;

//FUNCTION TO VERIFY PHONE NUMBER
-(void)verifyPhoneNumber:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL NEAR BY ALERTS BY USER ID, USER TYPE, LATITUDE AND LONGITUDE
-(void)getAllNearByAlertsByUserId:(NSString *)strUserId withUserType:(NSString *)strUserType;

//FUNCTION FOR CHANGE PASSWORD
-(void)commonChangePassword:(NSDictionary *)dictParameters;

//FUNCTION TO GET MORE ALERTS BY CHILD ID
-(void)getMoreAlertsByChildId:(NSString *)strChild;

//COMMON FUNCTION TO GET ALL NOTIFICATIONS
-(void)commonGetAllNotifications:(NSDictionary *)dictParameters;

//COMMON FUNCTION TO GET ALL BOYCOTT NOTIFICATIONS
-(void)commonGetAllBoycottNotifications:(NSDictionary *)dictParameters;

//==================== PARENT APP WEBSERVICES ====================//

//FUNCTION FOR PARENT LOGIN
-(void)parentLogin:(Parent *)objParent;

//FUNCTION FOR PARENT LOGOUT
-(void)parentLogout:(NSString *)strParentId;

//FUNCTION FOR PARENT TO ADD/REMOVE FROM EMAIL PREFERENCES
-(void)parentSubscribe:(NSString *)strParentId withFlag:(NSString *)boolFlag;

//FUNCTION FOR PARENT SIGNUP
-(void)parentSignUp:(Parent *)objParent;

//FUNCTION TO GET PARENT PROFILE DETAILS BY PARENT ID
-(void)getParentProfileDetailsByParentId:(NSString *)strParentId;

//FUNCTION TO UPDATE PARENT PROFILE
-(void)updateParentProfile:(Parent *)objParent;

//FUNCTION TO UPLOAD PARENT PROFILE PICTURE WITH PARENT ID WITH $_FILE
-(void)uploadParentProfilePictureWith_File:(NSData *)profilePictureData;

//FUNCTION TO SEND PARENT PAYMENT
-(void)sendParentPayment:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL CHILDS BY PARENT ID
-(void)getAllChildsByParentId:(NSString *)strParentId;

//FUNCTION TO GET ALL CHILDS BY PARENT ID AND SEARCH KEYWORD
-(void)getAllChildsByParentId:(NSString *)strParentId withSearchKeyword:(NSString *)strSearchKeyword;

//FUNCTION TO REMOVE CHILD BY PARENT ID AND CHILD ID
-(void)removeChildByParentId:(NSString *)strParentId withChildId:(NSString *)strChildId;

//FUNCTION TO SEND REQUEST BY CHILD BY PARENT ID AND CHILD ID AND PARENT ROLE, AUTHORITY EMAIL
-(void)sendRequestToChildByParentId:(NSString *)strParentId withChildId:(NSString *)strChildId withParentRole:(NSString *)strParentRole withAuthorityEmail:(NSString *)strAuthorityEmail;

//FUNCTION TO UPDATE PRIVACY SETTINGS FOR CHILD
-(void)updatePrivacySettingsForChild:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL MY CHILDS ALERTS BY PARENT ID
-(void)getAllMyChildsAlertsByParentId:(NSString *)strParentId;

//FUNCTION TO DELAY MY CHILD ALERT
-(void)delayMyChildAlert:(NSDictionary *)dictParameters;

//FUNCTION TO RAISE MY CHILD ALERT
-(void)raiseMyChildAlert:(NSDictionary *)dictParameters;

//FUNCTION TO DEACTIVATE MY CHILD ALERT
-(void)deactivateMyChildAlert:(NSDictionary *)dictParameters;

//FUNCTION TO GET RESPONSE TIME BY PARENT ID
-(void)getParentResponseTimeByParentId:(NSString *)strParentId;

//FUNCTION TO UPDATE RESPONSE TIME BY PARENT ID AND TIME
-(void)updateParentResponseTimeByParentId:(NSString *)strParentId withResponseTime:(NSString *)strResponseTime;

//FUNCTION TO UPDATE PARENT MESSAGE FOR MY PARENT RESPOND ALERT
-(void)updateParentMessageForParentRespondAlert:(NSDictionary *)dictParameters;

//FUNCTION TO UPDATE PARENT MESSAGE FOR MY CHILD COMMUNITY ALERT
-(void)updateParentMessageForCommunityAlert:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL CHILDS WITH AUTHORITY EMAIL BY PARENT ID
-(void)getAllMyChildsWithAuthorityEmailByParentId:(NSString *)strParentId;

//FUNCTION TO UPDATE AUTHORITY EMAIL BY PARENT ID AND AUTHORITY EMAIL AND CHILD IDS
-(void)updateAuthorityEmailByParentId:(NSString *)strParentId withAuthorityEmail:(NSString *)strAuthorityEmail withChildIds:(NSString *)strChildIds;

//==================== CHILD APP WEBSERVICES ====================//

//FUNCTION FOR CHILD LOGIN
-(void)childLogin:(Child *)objChild;

//FUNCTION FOR CHILD LOGOUT
-(void)childLogout:(NSString *)strChildId;

//FUNCTION FOR CHILD SIGNUP
-(void)childSignUp:(Child *)objChild;

//FUNCTION TO GET CHILD PROFILE DETAILS BY CHILD ID
-(void)getChildProfileDetailsByChildId:(NSString *)strChildId;

//FUNCTION TO UPDATE CHILD PROFILE
-(void)updateChildProfile:(Child *)objChild;

//FUNCTION TO DELETE CHILD PHOTO BY PHOTO ID
-(void)deleteChildPhotoByPhotoId:(NSString *)strPhotoId;

//FUNCTION TO UPLOAD CHILD PROFILE PICTURE WITH CHILD ID WITH $_FILE
-(void)uploadChildProfilePictureWith_File:(NSData *)profilePictureData;

//FUNCTION TO GET ALL PARENTS BY CHILD ID
-(void)getAllParentsByChildId:(NSString *)strChildId;

//FUNCTION TO GENERATE PARENT RESPOND ALERT BY CHILD ID
-(void)generateParentRespondByChildId:(NSString *)strChildId;

//FUNCTION TO GENERATE COMMUNITY RESPOND ALERT BY CHILD ID
-(void)generateCommunityAlertByChildId:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL PARENTS REQUESTS BY CHILD ID
-(void)getAllParentsRequestsByChildId:(NSString *)strChildId;

//FUNCTION TO ACCEPT PARENT REQUEST BY PARENT REQUEST ID
-(void)acceptParentRequest:(NSString *)strParentRequestId;

//FUNCTION TO REJECT PARENT REQUEST BY PARENT REQUEST ID
-(void)rejectParentRequest:(NSString *)strParentRequestId;

//FUNCTION TO GET ALL ALERTS RAISED BY ME
-(void)getAllAlertsRaisedByMe:(NSString *)strChildId;

//FUNCTION TO UPDATE CHILD MESSAGE FOR MY PARENT RESPOND ALERT
-(void)updateChildMessageForParentRespondAlert:(NSDictionary *)dictParameters;

//FUNCTION TO UPDATE CHILD MESSAGE FOR MY COMMUNITY ALERT
-(void)updateChildMessageForCommunityAlert:(NSDictionary *)dictParameters;

@end
