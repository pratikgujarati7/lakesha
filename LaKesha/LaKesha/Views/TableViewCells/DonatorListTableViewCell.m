//
//  DonatorListTableViewCell.m
//  LaKesha
//
//  Created by Pratik Gujarati on 14/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "DonatorListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 60

@implementation DonatorListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD MAIN INNER CONTAINER VIEW =======//
        self.mainInnerContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 5, (self.mainContainer.frame.size.width - 10), (self.mainContainer.frame.size.height - 5))];
        self.mainInnerContainer.backgroundColor =  [UIColor whiteColor];
        
        //======= ADD PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, (self.mainInnerContainer.frame.size.height - 10), (self.mainInnerContainer.frame.size.height - 10))];
        self.imageViewProfilePicture.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewProfilePicture.layer.masksToBounds = YES;
        self.imageViewProfilePicture.image = [UIImage imageNamed:@"donator_avatar.png"];
        [self.mainInnerContainer addSubview:self.imageViewProfilePicture];
        
        //======= ADD LABEL DONATOR NAME INTO BOX =======//
        self.lblDonatorName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewProfilePicture.frame.origin.x + self.imageViewProfilePicture.frame.size.width + 10), 10, self.mainInnerContainer.frame.size.width - (self.imageViewProfilePicture.frame.origin.x + self.imageViewProfilePicture.frame.size.width + 10) - 30, 40)];
        self.lblDonatorName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblDonatorName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblDonatorName.textAlignment = NSTextAlignmentLeft;
        self.lblDonatorName.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblDonatorName];
        
        [self.mainContainer addSubview:self.mainInnerContainer];
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

@end
