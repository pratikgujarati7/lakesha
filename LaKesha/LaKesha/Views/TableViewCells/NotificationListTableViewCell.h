//
//  NotificationListTableViewCell.h
//  OSIAN STAR
//
//  Created by Pratik Gujarati on 17/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface NotificationListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) UILabel *lblNotificationTime;
@property (nonatomic, retain) UILabel *lblNotificationType;
@property (nonatomic, retain) UILabel *lblNotificationTitle;
@property (nonatomic, retain) UILabel *lblNotificationText;

@property (nonatomic, retain) UIView *separatorView;

@end
