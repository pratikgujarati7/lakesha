//
//  DonatorListTableViewCell.h
//  LaKesha
//
//  Created by Pratik Gujarati on 14/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface DonatorListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) UIView *mainInnerContainer;

@property (nonatomic, retain) AsyncImageView *imageViewProfilePicture;

@property (nonatomic, retain) UILabel *lblDonatorName;

@end
