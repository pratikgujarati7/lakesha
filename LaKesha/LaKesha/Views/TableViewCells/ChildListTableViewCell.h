//
//  ChildListTableViewCell.h
//  LaKesha
//
//  Created by Pratik Gujarati on 10/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ChildListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) UIView *mainInnerContainer;

@property (nonatomic, retain) AsyncImageView *imageViewChildProfilePicture;

@property (nonatomic, retain) UILabel *lblChildName;
@property (nonatomic, retain) UILabel *lblChildAuthorityEmail;

@property (nonatomic, retain) AsyncImageView *imageViewSettings;
@property (nonatomic, retain) UIButton *btnSettings;

@property (nonatomic, retain) AsyncImageView *imageViewCheckbox;

- (id)initWithAuthorityEmailListStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
- (id)initWithUpdateAuthorityEmailListStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
