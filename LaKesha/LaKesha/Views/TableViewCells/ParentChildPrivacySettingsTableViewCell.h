//
//  ParentChildPrivacySettingsTableViewCell.h
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ParentChildPrivacySettingsTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewCheckbox;
@property (nonatomic, retain) UILabel *lblInformationType;

@property (nonatomic, retain) UIView *separatorView;

- (id)initWithTwoLinesStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
