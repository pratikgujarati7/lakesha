//
//  ChildListTableViewCell.m
//  LaKesha
//
//  Created by Pratik Gujarati on 10/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ChildListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 60

@implementation ChildListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD MAIN INNER CONTAINER VIEW =======//
        self.mainInnerContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 5, (self.mainContainer.frame.size.width - 10), (self.mainContainer.frame.size.height - 5))];
        self.mainInnerContainer.backgroundColor =  [UIColor whiteColor];
        
        //======= ADD CHILD PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewChildProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, (self.mainInnerContainer.frame.size.height - 10), (self.mainInnerContainer.frame.size.height - 10))];
        self.imageViewChildProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewChildProfilePicture.layer.masksToBounds = YES;
        self.imageViewChildProfilePicture.layer.cornerRadius = (self.imageViewChildProfilePicture.frame.size.width/2);
        self.imageViewChildProfilePicture.layer.borderWidth = 1.0f;
        self.imageViewChildProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        [self.mainInnerContainer addSubview:self.imageViewChildProfilePicture];
        
        //======= ADD LABEL CHILD NAME INTO BOX =======//
        self.lblChildName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10), 10, self.mainInnerContainer.frame.size.width - (self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10) - 30, 40)];
        self.lblChildName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblChildName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblChildName.textAlignment = NSTextAlignmentLeft;
        self.lblChildName.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblChildName];
        
        //======= ADD SETTINGS IMAGE VIEW INTO BOX =======//
        self.imageViewSettings = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.mainInnerContainer.frame.size.width - 30), 20, 20, 20)];
        self.imageViewSettings.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewSettings.layer.masksToBounds = YES;
        self.imageViewSettings.image = [UIImage imageNamed:@"settings.png"];
        [self.mainInnerContainer addSubview:self.imageViewSettings];
        
        //======== ADD BUTTON SETTINGS INTO BOX ========//
        self.btnSettings = [[UIButton alloc]initWithFrame:CGRectMake((self.imageViewSettings.frame.origin.x), 0, (self.imageViewSettings.frame.size.width), self.mainInnerContainer.frame.size.height)];
        [self.mainInnerContainer addSubview:self.btnSettings];
        
        [self.mainContainer addSubview:self.mainInnerContainer];
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (id)initWithAuthorityEmailListStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD MAIN INNER CONTAINER VIEW =======//
        self.mainInnerContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 5, (self.mainContainer.frame.size.width - 10), (self.mainContainer.frame.size.height - 5))];
        self.mainInnerContainer.backgroundColor =  [UIColor whiteColor];
        
        //======= ADD CHILD PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewChildProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, (self.mainInnerContainer.frame.size.height - 10), (self.mainInnerContainer.frame.size.height - 10))];
        self.imageViewChildProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewChildProfilePicture.layer.masksToBounds = YES;
        self.imageViewChildProfilePicture.layer.cornerRadius = (self.imageViewChildProfilePicture.frame.size.width/2);
        self.imageViewChildProfilePicture.layer.borderWidth = 1.0f;
        self.imageViewChildProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        [self.mainInnerContainer addSubview:self.imageViewChildProfilePicture];
        
        //======= ADD LABEL CHILD NAME INTO BOX =======//
        self.lblChildName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10), 10, self.mainInnerContainer.frame.size.width - (self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10) - 10, 20)];
        self.lblChildName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblChildName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblChildName.textAlignment = NSTextAlignmentLeft;
        self.lblChildName.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblChildName];
        
        //======= ADD LABEL CHILD AUTHORITY EMAIL INTO BOX =======//
        self.lblChildAuthorityEmail = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10), 30, self.mainInnerContainer.frame.size.width - (self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10) - 10, 20)];
        self.lblChildAuthorityEmail.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblChildAuthorityEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblChildAuthorityEmail.textAlignment = NSTextAlignmentLeft;
        self.lblChildAuthorityEmail.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblChildAuthorityEmail];
        
        [self.mainContainer addSubview:self.mainInnerContainer];
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (id)initWithUpdateAuthorityEmailListStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD MAIN INNER CONTAINER VIEW =======//
        self.mainInnerContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 5, (self.mainContainer.frame.size.width - 10), (self.mainContainer.frame.size.height - 5))];
        self.mainInnerContainer.backgroundColor =  [UIColor whiteColor];
        
        //======= ADD CHILD PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewChildProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, (self.mainInnerContainer.frame.size.height - 10), (self.mainInnerContainer.frame.size.height - 10))];
        self.imageViewChildProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewChildProfilePicture.layer.masksToBounds = YES;
        self.imageViewChildProfilePicture.layer.cornerRadius = (self.imageViewChildProfilePicture.frame.size.width/2);
        self.imageViewChildProfilePicture.layer.borderWidth = 1.0f;
        self.imageViewChildProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        [self.mainInnerContainer addSubview:self.imageViewChildProfilePicture];
        
        //======= ADD LABEL CHILD NAME INTO BOX =======//
        self.lblChildName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10), 10, self.mainInnerContainer.frame.size.width - (self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10) - 30, 20)];
        self.lblChildName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblChildName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblChildName.textAlignment = NSTextAlignmentLeft;
        self.lblChildName.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblChildName];

        //======= ADD LABEL CHILD AUTHORITY EMAIL INTO BOX =======//
        self.lblChildAuthorityEmail = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10), 30, self.mainInnerContainer.frame.size.width - (self.imageViewChildProfilePicture.frame.origin.x + self.imageViewChildProfilePicture.frame.size.width + 10) - 10, 20)];
        self.lblChildAuthorityEmail.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblChildAuthorityEmail.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblChildAuthorityEmail.textAlignment = NSTextAlignmentLeft;
        self.lblChildAuthorityEmail.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblChildAuthorityEmail];
        
        //======= ADD CHECKBOX IMAGE VIEW INTO BOX =======//
        self.imageViewCheckbox = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.mainInnerContainer.frame.size.width - 30), 20, 20, 20)];
        self.imageViewCheckbox.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewCheckbox.layer.masksToBounds = YES;
        self.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
        [self.mainInnerContainer addSubview:self.imageViewCheckbox];
        
        [self.mainContainer addSubview:self.mainInnerContainer];
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

@end
