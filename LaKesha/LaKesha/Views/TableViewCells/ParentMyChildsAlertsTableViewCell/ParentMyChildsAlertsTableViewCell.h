//
//  ParentMyChildsAlertsTableViewCell.h
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ParentMyChildsAlertsTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIView *mainContainer;

@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewChildProfilePicture;

@property (nonatomic, retain) IBOutlet UILabel *lblChildName;
@property (nonatomic, retain) IBOutlet UILabel *lblChildAge;
@property (nonatomic, retain) IBOutlet UILabel *lblChildGender;
@property (nonatomic, retain) IBOutlet UILabel *lblChildNationality;

@property (nonatomic, retain) IBOutlet UILabel *lblChildHairColor;
@property (nonatomic, retain) IBOutlet UILabel *lblChildEyeColor;

@property (nonatomic, retain) IBOutlet UILabel *lblChildHeight;
@property (nonatomic, retain) IBOutlet UILabel *lblChildWeight;

@property (nonatomic, retain) IBOutlet UILabel *lblChildSchoolName;

@property (nonatomic, retain) IBOutlet UIView *lblAlertTypeContainer;
@property (nonatomic, retain) IBOutlet UILabel *lblAlertType;

@property (nonatomic, retain) IBOutlet UILabel *lblAlertDateAndTime;
@property (nonatomic, retain) IBOutlet UILabel *lblAlertDeactivated;

@property (nonatomic, retain) IBOutlet UIView *separatorView;

@end
