//
//  ParentChildPrivacySettingsTableViewCell.m
//  LaKesha
//
//  Created by Pratik Gujarati on 15/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentChildPrivacySettingsTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 50
#define CellTwoLinesHeight 70

@implementation ParentChildPrivacySettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].floatParentChildPrivacySettingInformationSelectorTableViewWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD LABEL INFORMATION TYPE INTO BOX =======//
        self.lblInformationType = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, (self.mainContainer.frame.size.width - 40), 30)];
        self.lblInformationType.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblInformationType.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblInformationType.textAlignment = NSTextAlignmentLeft;
        self.lblInformationType.numberOfLines = 1;
        self.lblInformationType.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblInformationType];
        
        //======= ADD IMAGE VIEW CHECKBOX INTO BOX =======//
        self.imageViewCheckbox = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblInformationType.frame.origin.x + self.lblInformationType.frame.size.width + 10), 15, 20, 20)];
        self.imageViewCheckbox.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewCheckbox.layer.masksToBounds = YES;
        self.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked_privacy_settings.png"];
        [self.mainContainer addSubview:self.imageViewCheckbox];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(0, self.mainContainer.frame.size.height-1, self.mainContainer.frame.size.width, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSideMenuSeperatorGreyColor;
        self.separatorView.alpha = 0.5f;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (id)initWithTwoLinesStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellTwoLinesHeight;
        float cellWidth = [MySingleton sharedManager].floatParentChildPrivacySettingInformationSelectorTableViewWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD LABEL INFORMATION TYPE INTO BOX =======//
        self.lblInformationType = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, (self.mainContainer.frame.size.width - 40), 50)];
        self.lblInformationType.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblInformationType.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblInformationType.textAlignment = NSTextAlignmentLeft;
        self.lblInformationType.numberOfLines = 2;
        self.lblInformationType.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblInformationType];
        
        //======= ADD IMAGE VIEW CHECKBOX INTO BOX =======//
        self.imageViewCheckbox = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblInformationType.frame.origin.x + self.lblInformationType.frame.size.width + 10), 25, 20, 20)];
        self.imageViewCheckbox.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewCheckbox.layer.masksToBounds = YES;
        self.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked_privacy_settings.png"];
        [self.mainContainer addSubview:self.imageViewCheckbox];

        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(0, self.mainContainer.frame.size.height-1, self.mainContainer.frame.size.width, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSideMenuSeperatorGreyColor;
        self.separatorView.alpha = 0.5f;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
