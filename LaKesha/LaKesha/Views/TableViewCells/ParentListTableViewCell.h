//
//  ParentListTableViewCell.h
//  LaKesha
//
//  Created by Pratik Gujarati on 05/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ParentListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) UIView *mainInnerContainer;

@property (nonatomic, retain) AsyncImageView *imageViewParentProfilePicture;

@property (nonatomic, retain) UILabel *lblParentName;
@property (nonatomic, retain) UILabel *lblParentRole;
@property (nonatomic, retain) UILabel *lblParentWorkPlaceName;

@property (nonatomic, retain) UIView *bottomContainer;

@property (nonatomic, retain) UIView *acceptContainer;
@property (nonatomic, retain) AsyncImageView *imageViewAccept;
@property (nonatomic, retain) UILabel *lblAccept;
@property (nonatomic, retain) UIButton *btnAccept;

@property (nonatomic, retain) UIView *rejectContainer;
@property (nonatomic, retain) AsyncImageView *imageViewReject;
@property (nonatomic, retain) UILabel *lblReject;
@property (nonatomic, retain) UIButton *btnReject;

@property (nonatomic, retain) AsyncImageView *imageViewInformation;

@property (nonatomic, retain) UIView *separatorView;

- (id)initWithParentRequestStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
- (id)initWithMyParentsStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
