//
//  ParentMyChildsTableViewCell.m
//  LaKesha
//
//  Created by Pratik Gujarati on 13/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentMyChildsTableViewCell.h"

@implementation ParentMyChildsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

@end
