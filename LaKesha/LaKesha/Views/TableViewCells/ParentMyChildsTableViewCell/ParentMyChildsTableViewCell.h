//
//  ParentMyChildsTableViewCell.h
//  LaKesha
//
//  Created by Pratik Gujarati on 13/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ParentMyChildsTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIView *mainContainer;

@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewChildProfilePicture;

@property (nonatomic, retain) IBOutlet UILabel *lblChildName;
@property (nonatomic, retain) IBOutlet UILabel *lblChildAge;
@property (nonatomic, retain) IBOutlet UILabel *lblChildGender;
@property (nonatomic, retain) IBOutlet UILabel *lblChildNationality;

@property (nonatomic, retain) IBOutlet UILabel *lblChildHairColor;
@property (nonatomic, retain) IBOutlet UILabel *lblChildEyeColor;

@property (nonatomic, retain) IBOutlet UILabel *lblChildHeight;
@property (nonatomic, retain) IBOutlet UILabel *lblChildWeight;

@property (nonatomic, retain) IBOutlet UILabel *lblChildSchoolName;

@property (nonatomic, retain) IBOutlet UIView *bottomContainer;

@property (nonatomic, retain) IBOutlet UIView *removeContainer;
@property (nonatomic, retain) IBOutlet UILabel *lblRemove;
@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewRemove;
@property (nonatomic, retain) IBOutlet UIButton *btnRemove;

@property (nonatomic, retain) IBOutlet UIView *privacySettingsContainer;
@property (nonatomic, retain) IBOutlet UILabel *lblPrivacySettings;
@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewPrivacySettings;
@property (nonatomic, retain) IBOutlet UIButton *btnPrivacySettings;

@property (nonatomic, retain) IBOutlet UIView *separatorView;

@end
