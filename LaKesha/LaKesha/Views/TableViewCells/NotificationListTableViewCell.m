//
//  NotificationListTableViewCell.m
//  OSIAN STAR
//
//  Created by Pratik Gujarati on 17/08/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "NotificationListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 95

@implementation NotificationListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD LABEL NOTIFCIATION TIME INTO MAIN CONTAINER VIEW =======//
        self.lblNotificationTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, (self.mainContainer.frame.size.width - 20), 15)];
        self.lblNotificationTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblNotificationTime.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
        self.lblNotificationTime.textAlignment = NSTextAlignmentLeft;
        self.lblNotificationTime.numberOfLines = 1;
        self.lblNotificationTime.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblNotificationTime];
        
        //======= ADD LABEL NOTIFCIATION TYPE INTO MAIN CONTAINER VIEW =======//
        self.lblNotificationType = [[UILabel alloc] initWithFrame:CGRectMake(10, self.lblNotificationTime.frame.origin.y + self.lblNotificationTime.frame.size.height + 5, (self.mainContainer.frame.size.width - 20), 20)];
        self.lblNotificationType.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        self.lblNotificationType.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
        self.lblNotificationType.textAlignment = NSTextAlignmentLeft;
        self.lblNotificationType.numberOfLines = 1;
        self.lblNotificationType.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblNotificationType];
        
        //======= ADD LABEL NOTIFCIATION TITLE INTO MAIN CONTAINER VIEW =======//
        self.lblNotificationTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, self.lblNotificationType.frame.origin.y + self.lblNotificationType.frame.size.height + 5, (self.mainContainer.frame.size.width - 20), 20)];
        self.lblNotificationTitle.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
        self.lblNotificationTitle.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
        self.lblNotificationTitle.textAlignment = NSTextAlignmentLeft;
        self.lblNotificationTitle.numberOfLines = 0;
        self.lblNotificationTitle.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblNotificationTitle];
        
        //======= ADD LABEL NOTIFCIATION TEXT INTO MAIN CONTAINER VIEW =======//
        self.lblNotificationText = [[UILabel alloc] initWithFrame:CGRectMake(10, self.lblNotificationTitle.frame.origin.y + self.lblNotificationTitle.frame.size.height + 5, (self.mainContainer.frame.size.width - 20), 15)];
        self.lblNotificationText.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblNotificationText.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblNotificationText.textAlignment = NSTextAlignmentLeft;
        self.lblNotificationText.numberOfLines = 0;
        self.lblNotificationText.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblNotificationText];
        
        //======== ADD SEPERATOR INTO MAIN CONTAINER VIEW ========//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(0, self.mainContainer.frame.size.height - 1, self.mainContainer.frame.size.width, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
        self.separatorView.alpha = 0.5f;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

@end
