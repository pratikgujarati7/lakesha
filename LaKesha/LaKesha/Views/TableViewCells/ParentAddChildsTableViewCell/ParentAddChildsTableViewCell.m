//
//  ParentAddChildsTableViewCell.m
//  LaKesha
//
//  Created by Pratik Gujarati on 11/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentAddChildsTableViewCell.h"

@implementation ParentAddChildsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

@end
