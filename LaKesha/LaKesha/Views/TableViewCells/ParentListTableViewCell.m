//
//  ParentListTableViewCell.m
//  LaKesha
//
//  Created by Pratik Gujarati on 05/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ParentListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 80
#define CellParentRequestStyleHeight 130
#define CellMyParentsStyleHeight 80

@implementation ParentListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD MAIN INNER CONTAINER VIEW =======//
        self.mainInnerContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 5, (self.mainContainer.frame.size.width - 10), (self.mainContainer.frame.size.height - 5))];
        self.mainInnerContainer.backgroundColor =  [UIColor whiteColor];
        
        //======= ADD PARENT PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewParentProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, (self.mainInnerContainer.frame.size.height - 10), (self.mainInnerContainer.frame.size.height - 10))];
        self.imageViewParentProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewParentProfilePicture.layer.masksToBounds = YES;
        self.imageViewParentProfilePicture.layer.cornerRadius = self.imageViewParentProfilePicture.frame.size.width / 2;
        [self.mainInnerContainer addSubview:self.imageViewParentProfilePicture];
        
        //======= ADD LABEL PARENT NAME INTO BOX =======//
        self.lblParentName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), 10, self.mainInnerContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 20)];
        self.lblParentName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblParentName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblParentName.textAlignment = NSTextAlignmentLeft;
        self.lblParentName.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblParentName];
        
        //======= ADD LABEL PARENT ROLE INTO BOX =======//
        self.lblParentRole = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), (self.lblParentName.frame.origin.y + self.lblParentName.frame.size.height + 5), self.mainInnerContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 12)];
        self.lblParentRole.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblParentRole.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblParentRole.textAlignment = NSTextAlignmentLeft;
        self.lblParentRole.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblParentRole];
        
        //======= ADD LABEL PARENT WORK PLACE NAME INTO BOX =======//
        self.lblParentWorkPlaceName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), (self.lblParentRole.frame.origin.y + self.lblParentRole.frame.size.height + 5), self.mainInnerContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 12)];
        self.lblParentWorkPlaceName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblParentWorkPlaceName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblParentWorkPlaceName.textAlignment = NSTextAlignmentLeft;
        self.lblParentWorkPlaceName.layer.masksToBounds = YES;
        [self.mainInnerContainer addSubview:self.lblParentWorkPlaceName];
        
        [self.mainContainer addSubview:self.mainInnerContainer];
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (id)initWithParentRequestStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellParentRequestStyleHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD PARENT PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewParentProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, (self.mainContainer.frame.size.height - 60), (self.mainContainer.frame.size.height - 60))];
        self.imageViewParentProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewParentProfilePicture.layer.masksToBounds = YES;
        self.imageViewParentProfilePicture.layer.cornerRadius = self.imageViewParentProfilePicture.frame.size.width / 2;
        [self.mainContainer addSubview:self.imageViewParentProfilePicture];
        
        //======= ADD LABEL PARENT NAME INTO BOX =======//
        self.lblParentName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), 10, self.mainContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 20)];
        self.lblParentName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblParentName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblParentName.textAlignment = NSTextAlignmentLeft;
        self.lblParentName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblParentName];
        
        //======= ADD LABEL PARENT ROLE INTO BOX =======//
        self.lblParentRole = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), (self.lblParentName.frame.origin.y + self.lblParentName.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 12)];
        self.lblParentRole.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblParentRole.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblParentRole.textAlignment = NSTextAlignmentLeft;
        self.lblParentRole.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblParentRole];
        
        //======= ADD LABEL PARENT WORK PLACE NAME INTO BOX =======//
        self.lblParentWorkPlaceName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), (self.lblParentRole.frame.origin.y + self.lblParentRole.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 12)];
        self.lblParentWorkPlaceName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblParentWorkPlaceName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblParentWorkPlaceName.textAlignment = NSTextAlignmentLeft;
        self.lblParentWorkPlaceName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblParentWorkPlaceName];
        
        //======= ADD BOTTOM CONTAINER VIEW =======//
        self.bottomContainer = [[UIView alloc]initWithFrame:CGRectMake(10, (self.imageViewParentProfilePicture.frame.origin.y + self.imageViewParentProfilePicture.frame.size.height + 10), (self.mainContainer.frame.size.width - 20), 30)];
        self.bottomContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD ACCEPT CONTAINER VIEW =======//
        self.acceptContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 0, (self.bottomContainer.frame.size.width - 15)/2, self.bottomContainer.frame.size.height)];
        self.acceptContainer.backgroundColor =  [UIColor clearColor];
        self.acceptContainer.layer.masksToBounds = true;
        self.acceptContainer.layer.cornerRadius = 3.0f;
        self.acceptContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalParentRequestsAcceptGreenColor.CGColor;
        self.acceptContainer.layer.borderWidth = 1.0f;
        
        //======= ADD ACCEPT IMAGEVIEW INTO CONTAINER BOX =======//
        self.imageViewAccept = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, 7.5, 15, 15)];
        self.imageViewAccept.image = [UIImage imageNamed:@"accept.png"];
        self.imageViewAccept.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewAccept.layer.masksToBounds = YES;
        [self.acceptContainer addSubview:self.imageViewAccept];
        
        //======= ADD LABEL ACCEPT CONTAINER BOX =======//
        self.lblAccept = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (self.acceptContainer.frame.size.width), self.acceptContainer.frame.size.height)];
        self.lblAccept.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        self.lblAccept.textColor = [MySingleton sharedManager].themeGlobalParentRequestsAcceptGreenColor;
        self.lblAccept.textAlignment = NSTextAlignmentCenter;
        self.lblAccept.layer.masksToBounds = YES;
        self.lblAccept.text = @"ACCEPT";
        [self.acceptContainer addSubview:self.lblAccept];
        
        //======== ADD BUTTON ACCEPT INTO CONTAINER BOX ========//
        self.btnAccept = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.acceptContainer.frame.size.width, self.acceptContainer.frame.size.height)];
        [self.acceptContainer addSubview:self.btnAccept];
        
        [self.bottomContainer addSubview:self.acceptContainer];
        
        //======= ADD REJECT CONTAINER VIEW =======//
        self.rejectContainer = [[UIView alloc]initWithFrame:CGRectMake((self.acceptContainer.frame.origin.x + self.acceptContainer.frame.size.width + 5), 0, (self.bottomContainer.frame.size.width - 15)/2, self.bottomContainer.frame.size.height)];
        self.rejectContainer.backgroundColor =  [UIColor clearColor];
        self.rejectContainer.layer.masksToBounds = true;
        self.rejectContainer.layer.cornerRadius = 3.0f;
        self.rejectContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalParentRequestsRejectRedColor.CGColor;
        self.rejectContainer.layer.borderWidth = 1.0f;
        
        //======= ADD REJECT IMAGEVIEW INTO CONTAINER BOX =======//
        self.imageViewReject = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, 7.5, 15, 15)];
        self.imageViewReject.image = [UIImage imageNamed:@"reject.png"];
        self.imageViewReject.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewReject.layer.masksToBounds = YES;
        [self.rejectContainer addSubview:self.imageViewReject];
        
        //======= ADD LABEL REJECT CONTAINER BOX =======//
        self.lblReject = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (self.rejectContainer.frame.size.width), self.rejectContainer.frame.size.height)];
        self.lblReject.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        self.lblReject.textColor = [MySingleton sharedManager].themeGlobalParentRequestsRejectRedColor;
        self.lblReject.textAlignment = NSTextAlignmentCenter;
        self.lblReject.layer.masksToBounds = YES;
        self.lblReject.text = @"REJECT";
        [self.rejectContainer addSubview:self.lblReject];
        
        //======== ADD BUTTON REJECT INTO CONTAINER BOX ========//
        self.btnReject = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.rejectContainer.frame.size.width, self.rejectContainer.frame.size.height)];
        [self.rejectContainer addSubview:self.btnReject];
        
        [self.bottomContainer addSubview:self.rejectContainer];
        
        [self.mainContainer addSubview:self.bottomContainer];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(15, self.mainContainer.frame.size.height-1, self.mainContainer.frame.size.width - 30, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        self.separatorView.alpha = 0.5f;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (id)initWithMyParentsStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellMyParentsStyleHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD PARENT PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewParentProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(5, 5, (self.mainContainer.frame.size.height - 10), (self.mainContainer.frame.size.height - 10))];
        self.imageViewParentProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewParentProfilePicture.layer.masksToBounds = YES;
        self.imageViewParentProfilePicture.layer.cornerRadius = self.imageViewParentProfilePicture.frame.size.width / 2;
        [self.mainContainer addSubview:self.imageViewParentProfilePicture];
        
        //======= ADD LABEL PARENT NAME INTO BOX =======//
        self.lblParentName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), 10, self.mainContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 20)];
        self.lblParentName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblParentName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblParentName.textAlignment = NSTextAlignmentLeft;
        self.lblParentName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblParentName];
        
        //======= ADD LABEL PARENT ROLE INTO BOX =======//
        self.lblParentRole = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), (self.lblParentName.frame.origin.y + self.lblParentName.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 12)];
        self.lblParentRole.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblParentRole.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblParentRole.textAlignment = NSTextAlignmentLeft;
        self.lblParentRole.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblParentRole];
        
        //======= ADD LABEL PARENT WORK PLACE NAME INTO BOX =======//
        self.lblParentWorkPlaceName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10), (self.lblParentRole.frame.origin.y + self.lblParentRole.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewParentProfilePicture.frame.origin.x + self.imageViewParentProfilePicture.frame.size.width + 10) - 10, 12)];
        self.lblParentWorkPlaceName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblParentWorkPlaceName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblParentWorkPlaceName.textAlignment = NSTextAlignmentLeft;
        self.lblParentWorkPlaceName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblParentWorkPlaceName];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(15, self.mainContainer.frame.size.height-1, self.mainContainer.frame.size.width - 30, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        self.separatorView.alpha = 0.5f;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

@end
