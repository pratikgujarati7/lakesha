//
//  ChildPhotoView.h
//  LaKesha
//
//  Created by Pratik Gujarati on 17/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildPhotoView : UIView

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) UIImageView *imageViewMain;

@property (nonatomic, retain) UIButton *btnDelete;

@end
