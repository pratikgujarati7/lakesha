//
//  ChildPhotoView.m
//  LaKesha
//
//  Created by Pratik Gujarati on 17/05/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ChildPhotoView.h"
#import "MySingleton.h"

@implementation ChildPhotoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.mainContainer.backgroundColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        
        //======== ADD MAIN IMAGEVIEW INTO BOX ========//
        self.imageViewMain = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,self.mainContainer.frame.size.width,self.mainContainer.frame.size.height)];
        self.imageViewMain.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewMain.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewMain];
        
        //======== ADD DELETE BUTTON INTO BOX ========//
        self.btnDelete = [[UIButton alloc]initWithFrame:CGRectMake((self.mainContainer.frame.size.width-20),0,20,20)];
        [self.btnDelete setImage:[UIImage imageNamed:@"button_delete.png"] forState:UIControlStateNormal];
        [self.mainContainer addSubview:self.btnDelete];
        
        [self addSubview:self.mainContainer];
    }
    
    return self;
}

@end
